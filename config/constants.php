<?php
return [
    'giorni' => [
        1 => 'Lunedì',
        2 => 'Martedì',
        3 => 'Mercoledì',
        4 => 'Giovedì',
        5 => 'Venerdì',
        6 => 'Sabato',
        7 => 'Domenica',
    ],
    // user roles
    'user_role_admin' => 'admin',
    // default avatars
    'user_def_avatar' => 'default-avatar.png',
    // generic status
    'status_active' => 'active',
    'status_inactive' => 'inactive',
    'status_deleted' => 'deleted',
    // IMG sizes
    'img_sizes_mimes' => [
        "game" => array(
            "menu_icon_small"=>array(
                "mimes"=>array(
                    "image/png",
                ),
                "size"=>0.5,
                "width"=>90,
                "height"=>60
            ),
            "menu_icon_large"=>array(
                "mimes"=>array(
                    "image/png",
                ),
                "size"=>0.5,
                "width"=>270,
                "height"=>180
            ),
            "slider"=>array(
                "mimes"=>array(
                    "image/png",
                    "image/jpg",
                    "image/jpeg",
                ),
                "size"=>2,
                "width"=>1420,
                "height"=>620
            ),
//            "slider1_over1"=>array(
//                "mimes"=>array(
//                    "image/png"
//                ),
//                "size"=>1,
//                "width"=>457,
//                "height"=>591
//            ),
            "slider1_over1"=>array(
                "mimes"=>array(
                    "image/png"
                ),
                "size"=>1,
                "width"=>1420,
                "height"=>620
            ),
            "slider2_over1"=>array(
                "mimes"=>array(
                    "image/png"
                ),
                "size"=>1,
                "width"=>593,
                "height"=>431
            ),
            "slider2_over2"=>array(
                "mimes"=>array(
                    "image/png"
                ),
                "size"=>1,
                "width"=>949,
                "height"=>503
            ),
        ),
        "level" => array(
            "icon"=>array(
                "mimes"=>array(
                    "image/png",
                ),
                "size"=>0.5,
                "width"=>300,
                "height"=>300
            ),
        ),
        "course" => array(
            "cover_image_xs"=>array(
                "mimes"=>array(
                    "image/png",
                    "image/jpg",
                    "image/jpeg",
                ),
                "size"=>0.5,
                "width"=>342,
                "height"=>86
            ),
            "cover_image_sm"=>array(
                "mimes"=>array(
                    "image/png",
                    "image/jpg",
                    "image/jpeg",
                ),
                "size"=>1,
                "width"=>378,
                "height"=>203
            ),
            "cover_image_md"=>array(
                "mimes"=>array(
                    "image/png",
                    "image/jpg",
                    "image/jpeg",
                ),
                "size"=>2,
                "width"=>378,
                "height"=>320
            ),
            "cover_image_lg"=>array(
                "mimes"=>array(
                    "image/png",
                    "image/jpg",
                    "image/jpeg",
                ),
                "size"=>2,
                "width"=>773,
                "height"=>438
            ),
            "video_preview"=>array(
                "mimes"=>array(
                    "video/mp4",
                ),
                "size"=>0,
                "width"=>0,
                "height"=>0
            ),
            "video"=>array(
                "mimes"=>array(
                    "video/mp4",
                ),
                "size"=>0,
                "width"=>0,
                "height"=>0
            ),
            "cover_image_video"=>array(
                "mimes"=>array(
                    "image/png",
                    "image/jpg",
                    "image/jpeg",
                ),
                "size"=>0.5,
                "width"=>727,
                "height"=>380
            ),
        ),
        "user" => array(
            "avatar"=>array(
                "mimes"=>array(
                    "image/png",
                    "image/jpg",
                    "image/jpeg",
                ),
                "size"=>0.5,
                "width"=>300,
                "height"=>300
            ),
        ),
        "partner" => array(
            "image"=>array(
                "mimes"=>array(
                    "image/png",
                ),
                "size"=>0.5,
                "width"=>106,
                "height"=>102
            ),
        ),
    ],
];


//        Video Type     Extension       MIME Type
//Flash           .flv            video/x-flv
//MPEG-4          .mp4            video/mp4
//iPhone Index    .m3u8           application/x-mpegURL
//iPhone Segment  .ts             video/MP2T
//3GP Mobile      .3gp            video/3gpp
//QuickTime       .mov            video/quicktime
//A/V Interleave  .avi            video/x-msvideo
//Windows Media   .wmv            video/x-ms-wmv
