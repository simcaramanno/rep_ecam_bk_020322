<!-- start header -->
<header>
@php
    $navClass = 'top-space navbar-light bg-white';
    if(Request::is('home') || Request::is('home/index') || Request::is('*/home') || Request::is('*/home/index')){
        $navClass = 'navbar-dark bg-transparent';
    }
@endphp

<!-- start navigation -->
    {{--    <nav class="navbar navbar-expand-lg navbar-boxed navbar-dark bg-transparent header-light fixed-top header-reverse-scroll">HOME--}}
    {{--        <nav class="navbar top-space navbar-expand-lg navbar-light bg-white header-light navbar-boxed fixed-top header-reverse-scroll">NO HOME--}}
    <nav class="navbar navbar-expand-lg header-light navbar-boxed fixed-top header-always-fixed-scroll always-fixed {{$navClass}}">
        <div class="container-fluid nav-header-container">
            <div class="col-auto col-sm-6 col-lg-2 mr-auto pl-lg-0">
                <a class="navbar-brand" href="{{url($layout["lang"].'/home/')}}">
                    @if(Request::is('home') || Request::is('home/index') || Request::is('*/home') || Request::is('*/home/index'))
                        <img src="{{asset('images/logo/'.$platform->logo_light)}}"
                             data-at2x="{{asset('images/logo/'.$platform->logo_light_2x)}}"
                             alt="{{$platform->site_name}}" class="default-logo" style="height: 100px;">
                    @else
                        <img src="{{asset('images/logo/'.$platform->logo_dark)}}"
                             data-at2x="{{asset('images/logo/'.$platform->logo_dark_2x)}}"
                             alt="{{$platform->site_name}}" class="default-logo" style="height: 100px;">

                    @endif
                    <img src="{{asset('images/logo/'.$platform->logo_dark)}}"
                         data-at2x="{{asset('images/logo/'.$platform->logo_dark_2x)}}"
                         class="alt-logo" alt="{{$platform->site_name}}" style="height: 36px;">
                    <img src="{{asset('images/logo/'.$platform->logo_dark)}}"
                         data-at2x="{{asset('images/logo/'.$platform->logo_dark_2x)}}"
                         class="mobile-logo" alt="{{$platform->site_name}}" style="height: 36px;">
                </a>
            </div>
            <div class="col-auto col-lg-8 menu-order px-lg-0">
                <button class="navbar-toggler float-right" type="button" data-toggle="collapse"
                        data-target="#navbarNav" aria-controls="navbarNav" aria-label="Toggle navigation">
                    <span class="navbar-toggler-line"></span>
                    <span class="navbar-toggler-line"></span>
                    <span class="navbar-toggler-line"></span>
                    <span class="navbar-toggler-line"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
                    <ul class="navbar-nav alt-font">
                        @if(!empty($layout["menu_items"]) && sizeof($layout["menu_items"])>0)
                            @foreach($layout["menu_items"] as $menu)
                                @php
                                    $_path=$menu["path"];
                                    $pos = strpos($menu["path"], '/');
                                    if($pos !== false){$_path=substr($menu["path"],0,$pos); }
                                @endphp
                                <li class="nav-item">
                                    <a href="{{url($layout["lang"].'/'.$menu["path"])}}"
                                       class="nav-link
                                    @if(Request::is($layout["lang"].'/'.$_path) || Request::is($_path)
                                        || Request::is($_path.'/'.$_path.'/*')
                                        || Request::is($layout["lang"].'/'.$_path.'/*')) active @endif">
                                        {{$menu["name"]}}
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            @if(!empty($platform->url_facebook) || !empty($platform->url_instagram)  || !empty($platform->url_twitter) )
                <div class="col-auto col-lg-2 text-right pr-0 font-size-0">
                    <div class="header-social-icon d-inline-block">
                        @if(!empty($platform->url_facebook))
                            <a href="{{$platform->url_facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                        @endif
                        @if(!empty($platform->url_instagram))
                            <a href="{{$platform->url_instagram}}" target="_blank"><i class="fab fa-instagram"></i></a>
                        @endif
                        @if(!empty($platform->url_twitter))
                            <a href="{{$platform->url_twitter}}" target="_blank"><i class="fab fa-twitter"></i></a>
                        @endif
                    </div>
                </div>

            @endif
        </div>
    </nav>
    <!-- end navigation -->
</header>
<!-- end header -->

