@if (Session::has('message'))
    <div class="col-md-12 text-center ">
        <div class="alert alert-success alert--rounded">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="text-color-primary">{{ e(Session::get('message')) }}</span>
        </div>
    </div>
@endif
