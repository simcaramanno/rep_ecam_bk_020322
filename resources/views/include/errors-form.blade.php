@if (Session::has('error'))
    <div class="col-md-12 text-center ">
        <div class="alert alert-danger alert--rounded alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <span class="text-danger">{{ e(Session::get('error')) }}</span>
        </div>
    </div>
@endif
@if ($errors->any())
    <div class="col-md-12 text-center ">
        <div class="alert alert-danger alert--rounded alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="text-danger">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
