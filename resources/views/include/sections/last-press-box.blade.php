@if(!empty($section["posts"] && sizeof($section["posts"])>0))
    <!-- start section -->
    <section style="padding-bottom:10px;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8 col-md-8 col-sm-9 text-center margin-4-rem-bottom md-margin-3-rem-bottom wow animate__fadeIn">
                    @if(!empty($section["sectionText"]["title"]))
                        <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-15px-bottom">
                            {!!html_entity_decode($section["sectionText"]["title"])!!}
                        </h4>
                    @endif
                    @if(!empty($section["sectionText"]["subtitle"]))
                        <p class="mx-auto lg-w-95 sm-w-100">{!!html_entity_decode($section["sectionText"]["subtitle"])!!}</p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-12 blog-content no-padding-lr">
                    <ul class="blog-classic blog-wrapper grid grid-loading grid-4col xl-grid-4col lg-grid-3col md-grid-2col sm-grid-2col xs-grid-1col gutter-double-extra-large">
                        <li class="grid-sizer"></li>
                    @foreach($section["posts"] as $post)
                        <!-- start blog item IMG 800x560-->
                            <li class="grid-item wow animate__fadeIn">
                                <div class="blog-post">
                                    <div class="blog-post-image margin-40px-bottom md-margin-35px-bottom xs-margin-25px-bottom">
                                        @if(!empty($post["gallery"]) && sizeof($post["gallery"])>0)
                                            <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}">
                                                <img src="{{asset('images/press/'.$post["gallery"][0]["filename_small"])}}"
                                                     class="border-radius-4px" alt="{{$post["post_text"]["title"]}}"/>
                                            </a>
                                        @endif
                                    </div>
                                    <div class="post-details">
                                        <div style="min-height: 60px;">
                                            <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}"
                                               class="@if($post["post"]["is_arabic"]==1) text-right @endif alt-font font-weight-500 text-extra-medium text-extra-dark-gray text-project-yellow-hover d-block margin-20px-bottom xs-margin-10px-bottom">
                                                @php
                                                    $limit1=50;
                                                    $limit2=100;
                                                    if($post["post"]["is_arabic"]==1){$limit1=120;$limit2=220;}
                                                    $title = strip_tags($post["post_text"]["title"]);
                                                    if(strlen($title)<=$limit1){
                                                        $html_title = $title;
                                                    }else{
                                                        $html_title = substr($title,0,$limit1).'...';
                                                    }
                                                @endphp
                                                {!!html_entity_decode($html_title)!!}
                                            </a>
                                        </div>
                                        <p class="w-95 @if($post["post"]["is_arabic"]==1) text-right @endif">
                                            @php
                                                $content = strip_tags($post["post_text"]["content"]);
                                                if(strlen($content)<=$limit2){
                                                    $html_content = $content;
                                                }else{
                                                    $html_content = substr($content,0,$limit2).'...';
                                                }
                                            @endphp
                                            {!!html_entity_decode($html_content)!!}
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <!-- end blog item -->
                        @endforeach
                        {{-- <!-- end blog item -->--}}
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-12 margin-30px-top">
                    <!--   <hr> -->
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
@endif

