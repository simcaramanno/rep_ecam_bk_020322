<!-- start section -->
<section class="p-0 parallax home-events-conference">
    <div class="container-fluid position-relative">
        <div class="row">
            <div class="swiper-container white-move full-screen "
                 data-slider-options='{ "slidesPerView": 1, "loop": false, "autoplay": { "delay": 30500, "disableOnInteraction": false },  "pagination": { "el": ".swiper-pagination", "clickable": true }, "navigation": { "nextEl": ".swiper-button-next-nav", "prevEl": ".swiper-button-previous-nav" }, "keyboard": { "enabled": true, "onlyInViewport": true }, "effect": "slide" }'>
                <div class="swiper-wrapper">
                @if(isset($section["hero_area_sliders"]) && !empty($section["hero_area_sliders"]))
                    @foreach($section["hero_area_sliders"] as $slider)
                        <!-- start slider item background-image:url('https://via.placeholder.com/1920x1080');-->
                            <div class="swiper-slide cover-background"
                                 style="background-image:url('{{$slider["image_path"]}}');">
                            @if($slider["area"]=='upcoming_event')
                                <!-- <div class="opacity-medium  bg-dark-opacity-60"></div> -->
                                    <div class="opacity-medium"></div>
                                @else
                                    <div class="opacity-medium  bg-dark-opacity-40"></div>
                                @endif
                                <div class="container h-100">
                                    @if($slider["area"]=='upcoming_event')
                                        <div class="row justify-content-center h-100">
                                            <div class="col-12 col-xl-9 col-lg-10 col-md-11 d-flex flex-column justify-content-center full-screen text-center padding-4-half-rem-lr md-landscape-h-600px xs-padding-15px-lr h-100">
                                                <div class="bg-transparent-extra-dark-gray-project border-radius-6px padding-6-half-rem-all lg-padding-5-rem-all md-padding-4-half-rem-all sm-padding-2-rem-lr">
                                                    @if(!empty($slider["small_title"]))
                                                        <p class="alt-font font-weight-500 text-uppercase letter-spacing-3px text-project-yellow sm-w-100 sm-margin-25px-bottom">
                                                            {!!html_entity_decode($slider["small_title"])!!}
                                                        </p>
                                                    @endif
                                                    <h1 class="alt-font font-weight-600 letter-spacing-minus-3px text-white margin-2-rem-bottom sm-margin-20px-bottom xs-letter-spacing-minus-1-half"
                                                        style="font-size: 3.7rem;line-height: 4rem;">
                                                        {!!html_entity_decode($slider["title"])!!}
                                                    </h1>
                                                    <div class="d-flex flex-column flex-sm-row justify-content-center border-tb border-color-white-transparent text-center padding-15px-tb margin-2-rem-bottom">
                                                        <div class="margin-25px-right xs-no-margin-right sm-margin-15px-right">
                                                            <i class="feather icon-feather-calendar text-white margin-10px-right"></i>
                                                            <span class="alt-font text-medium text-white font-weight-500  text-uppercase d-inline-block">
                                                                {{$slider["month"]}} {{$slider["day"]}}, {{$slider["year"]}}
                                                            </span>
                                                        </div>
                                                        <div>
                                                            <i class="feather icon-feather-map-pin text-white margin-10px-right"></i>
                                                            <span class="alt-font text-medium text-white font-weight-500 text-uppercase d-inline-block">
                                                                {!!html_entity_decode($slider["location"])!!}
                                                            </span>
                                                        </div>
                                                    </div>
                                                    @if(!empty($slider["btn_text"]) && (!empty($slider["btn_link"])))
                                                        <a href="{{$slider["btn_link"]}}"
                                                           class="btn btn-fancy btn-large btn-round-edge-small btn-project-yellow section-link text-uppercase letter-spacing-3px">
                                                            {{$slider["btn_text"]}}
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row justify-content-center h-100">
                                            <div class="col-12 col-xl-9 col-sm-10 d-flex flex-column justify-content-center text-center h-100">
                                                @if(!empty($slider["small_title"]))
                                                    <span class="alt-font font-weight-500 text-extra-medium text-uppercase letter-spacing-3px d-block margin-35px-bottom sm-margin-15px-bottom text-project-yellow">
                                                        {!!html_entity_decode($slider["small_title"])!!}
                                                    </span>
                                                @endif
                                                <h1 class="alt-font font-weight-600 letter-spacing-minus-3px text-white margin-2-half-rem-bottom sm-margin-10px-bottom xs-letter-spacing-minus-1-half">
                                                    {!!html_entity_decode($slider["title"])!!}
                                                </h1>
                                                @if(!empty($slider["subtitle"]))
                                                    <p class="alt-font text-white margin-3-half-rem-bottom sm-margin-20px-bottom xs-letter-spacing-minus-1-half">
                                                        {!!html_entity_decode($slider["subtitle"])!!}
                                                    </p>
                                                @endif
                                                @if(!empty($slider["btn_text"]) && (!empty($slider["btn_link"])))
                                                    <a href="{{$slider["btn_link"]}}"
                                                       class="btn btn-fancy btn-large btn-round-edge-small  btn-project-yellow text-uppercase letter-spacing-3px align-self-center">
                                                        {{$slider["btn_text"]}}
                                                    </a>
{{--                                                        <a href="{{url('en/events/show/1/ecam-council-summit')}}"--}}
{{--                                                           class="btn btn-fancy btn-large btn-round-edge-small  btn-project-yellow text-uppercase letter-spacing-3px align-self-center">--}}
{{--                                                            {{$slider["btn_text"]}}--}}
{{--                                                        </a>--}}
                                                @endif
                                            </div>
                                        </div>

                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                @if(isset($section["hero_area_sliders"]) && !empty($section["hero_area_sliders"]) && sizeof($section["hero_area_sliders"])>1)
                    <!-- start slider pagination -->
                    <div class="swiper-pagination swiper-light-pagination d-sm-none"></div>
                    <!-- end slider pagination -->
                    <!-- start slider navigation slider-navigation-style-07-->
                    <div class="swiper-button-next-nav swiper-button-next rounded-circle slider-navigation-style-custom-slider d-none d-sm-flex"
                         style="width: 59px;height:59px;border-color: rgba(255, 255, 255,0.2);">
                        <i class="feather icon-feather-arrow-right" style="font-size:20px;"></i>
                    </div>
                    <div class="swiper-button-previous-nav swiper-button-prev rounded-circle slider-navigation-style-custom-slider d-none d-sm-flex"
                         style="width: 59px;height:59px;border-color: rgba(255, 255, 255,0.2); ">
                        <i class="feather icon-feather-arrow-left" style="font-size:20px;"></i>
                    </div>
                    <!-- end slider navigation -->
                @endif
            </div>
        </div>
    </div>
</section>
<!-- end section -->



