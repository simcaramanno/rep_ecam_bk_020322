<!-- start section -->
<section class="bg-light-gray">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10 col-md-8 col-sm-10 text-center margin-4-rem-bottom md-margin-3-rem-bottom wow animate__fadeIn">
                <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-15px-bottom">
                    {!!html_entity_decode($section["sectionText"]["title"])!!}
                </h4>
                @if(!empty($event["eventText"]["program_desc"]))
                    <p class="w-75 mx-auto lg-w-95 sm-w-100">{!!html_entity_decode($event["eventText"]["program_desc"])!!}</p>
                @endif
            </div>
        </div>
        @if(!empty($event["days_program"]) && sizeof($event["days_program"])>0)
            <div class="row">
                <div class="col-12 tab-style-06 wow animate__fadeIn" data-wow-delay="0.2s">
                    <!-- start tab navigation -->
                    <ul class="nav nav-tabs text-center justify-content-center">
                        @php $i=1; @endphp
                        @foreach($event["days_program"] as $day_program)
                            <li class="nav-item">
                                <a class="alt-font nav-link text-uppercase @if($i==1) active @endif " data-toggle="tab"
                                   href="#program-{{$day_program["day"]["day_date"]}}">
                                    @php
                                        $itemDesc1 = date("F", strtotime($day_program["day"]["day_date"]))." ".date("d", strtotime($day_program["day"]["day_date"]));
                                        $_d = date("D", strtotime($day_program["day"]["day_date"]));
                                        $_dd = '';
                                        if(strtolower($_d)=='mon'){ $_dd = 'monday'; }
                                        if(strtolower($_d)=='tue'){ $_dd = 'tuesday'; }
                                        if(strtolower($_d)=='wed'){ $_dd = 'wednesday'; }
                                        if(strtolower($_d)=='thu'){ $_dd = 'thursday'; }
                                        if(strtolower($_d)=='fri'){ $_dd = 'friday'; }
                                        if(strtolower($_d)=='sat'){ $_dd = 'saturday'; }
                                        if(strtolower($_d)=='sun'){ $_dd = 'sunday'; }
                                    @endphp
                                    <span>{{$itemDesc1}}</span>{{$_dd}}
                                </a>
                            </li>
                            @php $i++; @endphp
                        @endforeach
                    </ul>
                    <!-- end tab navigation -->
                    <div class="tab-content">
                        @php $i=1; @endphp
                        @foreach($event["days_program"] as $day_program)
                            <!-- start tab content -->
                            <div class="tab-pane med-text fade in @if($i==1) active show @endif"
                                 id="program-{{$day_program["day"]["day_date"]}}">
                                <div class="panel-group time-table">
                                    @foreach($day_program["details"] as $details)
                                        @if(!empty($details["content2"]))
                                            <div class="panel border-color-black-transparent">
                                                <div class="panel-heading">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6">
                                                            <p class="w-65 lg-w-85 md-w-90 xs-w-100 font-weight-600 text-extra-dark-gray line-height-22px">
                                                                @php
                                                                $_content = "";
                                                                if(!empty($details["time_from"])){
                                                                    $_content = date('h:i', strtotime($details["time_from"])).' – '.date('h:i a', strtotime($details["time_to"])).' | '.$details["content1"];
                                                                } else {
                                                                    $_content = $details["content1"];
                                                                }
                                                                @endphp
                                                                {!!html_entity_decode($_content)!!}
                                                            </p>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6">
                                                            <p class="w-65 lg-w-85 md-w-90 xs-w-100 line-height-22px">
                                                                {!!html_entity_decode($details["content2"])!!}
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="panel border-color-black-transparent">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-lg-12" style="text-align:center;">
                                                        <p class="lg-w-85 md-w-90 xs-w-100 font-weight-600 text-extra-dark-gray line-height-22px"
                                                           style="margin-bottom:0px;">
                                                            @php
                                                                $_content = "";
                                                                if(!empty($details["time_from"])){
                                                                    $_content = date('h:i', strtotime($details["time_from"])).' – '.date('h:i a', strtotime($details["time_to"])).' | '.$details["content1"];
                                                                } else {
                                                                    $_content = $details["content1"];
                                                                }
                                                            @endphp
                                                            {!!html_entity_decode($_content)!!}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <!-- end tab content -->
                            @php $i++; @endphp
                        @endforeach
                    </div>
                </div>
            </div>

        @endif
    </div>
</section>
<!-- end section -->

