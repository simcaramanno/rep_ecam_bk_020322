<!-- start section -->
<section class="big-section wow animate__fadeIn" style="padding-top:40px;padding-bottom:40px;">
    <div class="container">
        <div class="row">
            <!-- start blockquote item -->
            <div class="col-12 col-md-6 cover-background sm-h-450px xs-h-300px wow animate__fadeIn"
                 data-wow-delay="0.4s"
                 style="background-image:url({{url(('images/sections/'.$section["sectionText"]["image1"]))}});">
                <!--background-image:url('https://via.placeholder.com/800x594')-->
            </div>
            <div class="col-12 col-md-6 text-center bg-extra-dark-gray background-no-repeat background-position-right-bottom padding-90px-lr padding-8-rem-tb lg-padding-2-half-rem-lr wow animate__fadeIn"
                 data-wow-delay="0.2s">
                <i class="fas fa-quote-left text-project-yellow icon-extra-medium margin-30px-bottom"></i>
                <h6 class="alt-font text-white font-weight-500" style="font-size: 1.2rem;line-height: 2rem;">
                    {!!html_entity_decode($section["sectionText"]["blockquote1"])!!}
                </h6>
                @if(!empty($section["sectionText"]["blockquote2"]))
                    <div class="alt-font text-project-yellow">{!!html_entity_decode($section["sectionText"]["blockquote2"])!!}</div>
                @endif
            </div>
            <!-- end blockquote item -->
        </div>
    </div>
</section>
<!-- end section -->

