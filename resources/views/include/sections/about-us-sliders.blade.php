<!-- start section -->
<section class="bg-light-gray wow animate__fadeIn">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-5 col-md-9 wow animate__fadeInLeft" data-wow-delay="0.2s">
                @if(!empty($section["sectionText"]["title"]))
                    <p class="alt-font text-project-yellow text-uppercase letter-spacing-3px">
                        {!! $section["sectionText"]["title"] !!}
                    </p>
                @endif
                @if(!empty($section["sectionText"]["subtitle"]))
                    <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-30px-bottom sm-margin-20px-bottom">
                        {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                    </h4>
                @endif
                @if(!empty($section["sectionText"]["content1"]))
                    <p class="w-90 md-w-100 margin-30px-bottom">
                        {!! $section["sectionText"]["content1"] !!}
                    </p>
                @endif
            </div>
            <div class="col-12 offset-lg-1 col-lg-6 col-md-9 p-0 md-margin-30px-bottom wow animate__fadeIn"
                 data-wow-delay="0.2s">
                <div class="position-relative">
                    <div class="swiper-container slider-one-slide black-move" style=""
                         data-slider-options='{ "slidesPerView": 1, "loop": false, "pagination": { "el": ".swiper-pagination", "clickable": true }, "navigation": { "nextEl": ".swiper-button-next-nav", "prevEl": ".swiper-button-previous-nav" }, "autoplay": { "delay": 450000, "disableOnInteraction": false }, "keyboard": { "enabled": true, "onlyInViewport": true }, "effect": "slide" }'>
                        <div class="swiper-wrapper">
                        @if(!empty($section["sectionText"]["image1"]))
                            <!-- start slider item -->
                                <div class="swiper-slide padding-15px-all">
                                    <div class="h-100 bg-white box-shadow position-relative">
                                        {{--<img src="https://via.placeholder.com/800x622" alt="">--}}
                                        <img src="{{asset('images/sections/'.$section["sectionText"]["image1"])}}"
                                             alt="{{$section["sectionText"]["image1_alt"]}}">
                                        <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                            <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                                <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                    {!!html_entity_decode($section["sectionText"]["item1_title"])!!}
                                                </span>
                                                <p>{!!html_entity_decode($section["sectionText"]["item1_subtitle"])!!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        @endif
                        @if(!empty($section["sectionText"]["image2"]))
                            <!-- start slider item -->
                            <div class="swiper-slide padding-15px-all">
                                <div class="h-100 bg-white box-shadow position-relative">
                                    <img src="{{asset('images/sections/'.$section["sectionText"]["image2"])}}"
                                         alt="{{$section["sectionText"]["image2_alt"]}}">
                                    <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                        <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                            <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                               {!!html_entity_decode($section["sectionText"]["item2_title"])!!}
                                            </span>
                                            <p>{!!html_entity_decode($section["sectionText"]["item2_subtitle"])!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($section["sectionText"]["image3"]))
                            <!-- start slider item -->
                            <div class="swiper-slide padding-15px-all">
                                <div class="h-100 bg-white box-shadow position-relative">
                                    <img src="{{asset('images/sections/'.$section["sectionText"]["image3"])}}"
                                         alt="{{$section["sectionText"]["image3_alt"]}}">
                                    <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                        <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                            <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                {!!html_entity_decode($section["sectionText"]["item3_title"])!!}
                                            </span>
                                            <p>{!!html_entity_decode($section["sectionText"]["item3_subtitle"])!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($section["sectionText"]["image4"]))
                            <!-- start slider item -->
                            <div class="swiper-slide padding-15px-all">
                                <div class="h-100 bg-white box-shadow position-relative">
                                    <img src="{{asset('images/sections/'.$section["sectionText"]["image4"])}}"
                                         alt="{{$section["sectionText"]["image4_alt"]}}">
                                    <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                        <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                            <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                {!!html_entity_decode($section["sectionText"]["item4_title"])!!}
                                            </span>
                                            <p>{!!html_entity_decode($section["sectionText"]["item4_subtitle"])!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($section["sectionText"]["image5"]))
                            <!-- start slider item -->
                            <div class="swiper-slide padding-15px-all">
                                <div class="h-100 bg-white box-shadow position-relative">
                                    <img src="{{asset('images/sections/'.$section["sectionText"]["image5"])}}"
                                         alt="{{$section["sectionText"]["image5_alt"]}}">
                                    <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                        <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                                <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                    {!!html_entity_decode($section["sectionText"]["item5_title"])!!}
                                                </span>
                                            <p>{!!html_entity_decode($section["sectionText"]["item5_subtitle"])!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($section["sectionText"]["image6"]))
                            <!-- start slider item -->
                            <div class="swiper-slide padding-15px-all">
                                <div class="h-100 bg-white box-shadow position-relative">
                                    <img src="{{asset('images/sections/'.$section["sectionText"]["image6"])}}"
                                         alt="{{$section["sectionText"]["image6_alt"]}}">
                                    <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                        <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                            <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                {!!html_entity_decode($section["sectionText"]["item6_title"])!!}
                                            </span>
                                            <p>{!!html_entity_decode($section["sectionText"]["item6_subtitle"])!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($section["sectionText"]["image7"]))
                            <!-- start slider item -->
                            <div class="swiper-slide padding-15px-all">
                                <div class="h-100 bg-white box-shadow position-relative">
                                    <img src="{{asset('images/sections/'.$section["sectionText"]["image7"])}}"
                                         alt="{{$section["sectionText"]["image7_alt"]}}">
                                    <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                        <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                            <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                {!!html_entity_decode($section["sectionText"]["item7_title"])!!}
                                            </span>
                                            <p>{!!html_entity_decode($section["sectionText"]["item7_subtitle"])!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($section["sectionText"]["image8"]))
                            <!-- start slider item -->
                            <div class="swiper-slide padding-15px-all">
                                <div class="h-100 bg-white box-shadow position-relative">
                                    <img src="{{asset('images/sections/'.$section["sectionText"]["image8"])}}"
                                         alt="{{$section["sectionText"]["image8_alt"]}}">
                                    <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                        <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                                <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                    {!!html_entity_decode($section["sectionText"]["item8_title"])!!}
                                                </span>
                                            <p>{!!html_entity_decode($section["sectionText"]["item8_subtitle"])!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($section["sectionText"]["image9"]))
                            <!-- start slider item -->
                            <div class="swiper-slide padding-15px-all">
                                <div class="h-100 bg-white box-shadow position-relative">
                                    <img src="{{asset('images/sections/'.$section["sectionText"]["image9"])}}"
                                         alt="{{$section["sectionText"]["image9_alt"]}}">
                                    <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                        <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                                <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                    {!!html_entity_decode($section["sectionText"]["item9_title"])!!}
                                                </span>
                                            <p>{!!html_entity_decode($section["sectionText"]["item9_subtitle"])!!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(!empty($section["sectionText"]["image10"]))
                            <!-- start slider item -->
                                <div class="swiper-slide padding-15px-all">
                                    <div class="h-100 bg-white box-shadow position-relative">
                                        <img src="{{asset('images/sections/'.$section["sectionText"]["image10"])}}"
                                             alt="{{$section["sectionText"]["image10_alt"]}}">
                                        <div class="padding-4-half-rem-lr padding-3-half-rem-tb feature-box feature-box-left-icon-middle last-paragraph-no-margin lg-padding-2-half-rem-lr sm-padding-4-rem-lr xs-padding-2-rem-all">
                                            <div class="feature-box-content lg-padding-35px-left xs-padding-15px-left">
                                                <span class="text-extra-dark-gray alt-font font-weight-500 d-block text-extra-medium ">
                                                    {!!html_entity_decode($section["sectionText"]["item10_title"])!!}
                                                </span>
                                                <p>{!!html_entity_decode($section["sectionText"]["item10_subtitle"])!!}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- start slider pagination -->
                    <div class="swiper-button-next-nav swiper-button-next slider-navigation-style-08 dark">
                        <i class="feather icon-feather-arrow-right text-extra-large"></i>
                    </div>
                    <div class="swiper-button-previous-nav swiper-button-prev slider-navigation-style-08 dark">
                        <i class="feather icon-feather-arrow-left text-extra-large"></i>
                    </div>
                    <!-- end slider pagination -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end section -->

