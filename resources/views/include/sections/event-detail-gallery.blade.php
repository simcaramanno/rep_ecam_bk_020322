@if(!empty($event["gallery"]) && sizeof($event["gallery"])>0)
    <!-- start section -->
    <section style="padding-top:60px;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8 col-md-8 col-sm-9 text-center margin-6-rem-bottom md-margin-4-rem-bottom wow animate__fadeIn">
                    <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-15px-bottom">
                        {!!html_entity_decode($section["sectionText"]["title"])!!}
                    </h4>
                    @if(!empty($section["sectionText"]["subtitle"]))
                        <p class="w-75 mx-auto lg-w-95 sm-w-100">{!!html_entity_decode($section["sectionText"]["subtitle"])!!}</p>
                    @endif
                </div>
            </div>
            <div class="col-12">
                <div class="swiper-container text-center"
                     data-slider-options='{ "slidesPerView": 1, "loop": false, "navigation": { "nextEl": ".swiper-button-next-nav", "prevEl": ".swiper-button-previous-nav" }, "autoplay": { "delay": 300000, "disableOnInteraction": false }, "keyboard": { "enabled": true, "onlyInViewport": true }, "breakpoints": { "1200": { "slidesPerView": 3 }, "992": { "slidesPerView": 3 }, "768": { "slidesPerView": 3 } } }'>
                    <div class="swiper-wrapper">
                        @foreach($event["gallery"] as $gallery)
                            <div class="swiper-slide wow animate__fadeIn"  style="padding-left:12px;padding-right:12px;">
<!--                                <img src="{{asset('images/events/'.$gallery["filename"])}}"
                                     alt="{{$gallery["alt_text"]}}" style="padding-left:12px;padding-right:12px;">
-->
                                <a href="{{asset('images/events/'.$gallery["filename"])}}" title="{{$gallery["alt_text"]}}" data-group="lightbox-gallery" class="lightbox-group-gallery-item">
                                    <div class="portfolio-box">
                                        <div class=" ">
                                            <img src="{{asset('images/events/'.$gallery["filename"])}}" alt="{{$gallery["alt_text"]}}" />
                                            <div class="portfolio-hover justify-content-end d-flex flex-column bg-transparent-mengo-yellow" style="margin-left:12px;width: 94%;">
                                                <i class="feather icon-feather-zoom-in portfolio-plus-icon font-weight-300 text-white absolute-middle-center icon-small move-top-bottom"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- start slider navigation -->
                <div class="swiper-button-next-nav swiper-button-next rounded-circle light slider-navigation-style-07 box-shadow-double-large">
                    <i class="feather icon-feather-arrow-right"></i></div>
                <div class="swiper-button-previous-nav swiper-button-prev rounded-circle light slider-navigation-style-07 box-shadow-double-large">
                    <i class="feather icon-feather-arrow-left"></i></div>
                <!-- end slider navigation -->
            </div>
        </div>
    </section>
    <!-- end section -->
@endif

