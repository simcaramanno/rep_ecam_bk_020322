<!-- start section -->
<section class="big-section cover-background"
         style="background-image:url('{{asset('images/sections/'.$section["sectionText"]["image1"])}}');">
    {{-- background-image:url('https://via.placeholder.com/1920x1100')--}}
    <div class="opacity-medium  bg-extra-dark-gray-about" style="opacity:1;"></div>
    <div class="container">
        <div class="row text-left">
            <div class="col-12 col-md-6 col-lg-6 col-xs-12 col-sm-12">
                @if(!empty($section["sectionText"]["title"]))
                    <p class="alt-font text-project-yellow text-uppercase letter-spacing-3px">{!!html_entity_decode($section["sectionText"]["title"])!!}</p>
                @endif
                @if(!empty($section["sectionText"]["subtitle"]))
                    <h3 class="alt-font text-white font-weight-600 margin-45px-bottom md-margin-35px-bottom xs-margin-25px-bottom wow animate__fadeIn">
                        {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                    </h3>
                @endif
                @if(!empty($section["sectionText"]["content1"]))
                    <p class="text-white"
                       style="color:#FFF;">{!!html_entity_decode($section["sectionText"]["content1"])!!}</p>
                @endif
                @if(!empty($section["sectionText"]["btn1_text"]))
                    <a href="{{url($layout["lang"].'/'.$section["sectionText"]["btn1_link"])}}"
                       class="btn btn-fancy btn-large btn-round-edge-small btn-project-yellow section-link text-uppercase letter-spacing-3px">
                        {!!html_entity_decode($section["sectionText"]["btn1_text"])!!}
                    </a>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- end section -->

