@if(!empty($project["gallery"]) && sizeof($project["gallery"])>0)
    <!-- start section -->
    <section class="p-0 position-relative overflow-visible wow animate__fadeIn margin-60px-bottom">
        <div class="slider-blog-banner swiper-container black-move"
             data-slider-options='{ "loop": true, "centeredSlides": true, "slidesPerView": "auto", "speed": 3000000, "keyboard": { "enabled": true, "onlyInViewport": true }, "autoplay": { "delay": 3000000, "disableOnInteraction": false }, "pagination": { "el": ".swiper-pagination", "clickable": true }, "navigation": { "nextEl": ".swiper-button-next-nav", "prevEl": ".swiper-button-previous-nav" } }'>
            <!--spped 500,delay 1500-->
            <div class="swiper-wrapper">
            @foreach($project["gallery"] as $gallery)
                <!-- start slider item -->
                    <div class="swiper-slide w-55 sm-w-65">
                        <div class="w-100 padding-15px-lr sm-padding-10px-lr xs-padding-5px-lr">
                            {{--                        <img src="https://via.placeholder.com/1400x933" alt="" />--}}
                            <img src="{{asset('images/projects/'.$gallery["filename"])}}" alt="{{$gallery["alt_text"]}}" />
                        </div>
                    </div>
                    <!-- end slider item -->
                @endforeach
            </div>
        </div>
    </section>
    <!-- end section -->
@endif
