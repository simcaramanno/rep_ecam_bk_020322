@if(isset($section["is_main_project"]) && $section["is_main_project"])
    <!-- start section -->
    <section class="big-sectionbg-light-gray wow animate__fadeIn">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <!-- start blockquote item -->
                <div class="col-12 col-sm-2 col-lg-1 text-sm-right xs-margin-10px-bottom wow animate__fadeIn" data-wow-delay="0.2s"
                     style="border-right:1px solid lightgrey;">
                    <i class="fas fa-quote-left text-project-yellow icon-extra-medium margin-30px-right"></i>
                </div>
                <div class="col-12 col-xl-6 col-lg-10 col-md-10 col-sm-10 border-width-1px border-color-medium-white-transparent xs-border-none wow animate__fadeIn"
                     data-wow-delay="0.4s">
                    <h6 class="text-extra-dark-gray margin-30px-left mb-0 line-height-26px xs-no-margin-left xs-line-height-30px font-weight-600 text-extra-large">
                        {!!html_entity_decode($section["sectionText"]["blockquote1"])!!}
                    </h6>
                </div>
                <!-- end blockquote item -->
            </div>
        </div>
    </section>
    <!-- end section -->

    <!-- start section -->
    <section id="reading" class="p-0 wow animate__fadeIn">
        <div class="container-fluid">
            <div class="row">
                <!--background-image: url('https://via.placeholder.com/1000x771');-->
                <div class="col-12 col-xl-6 col-lg-5 cover-background md-h-550px sm-h-400px xs-h-300px wow animate__fadeIn"
                     style="background-image: url('{{asset('images/projects/'.$section["main_project"]["project"]["home_image"])}}');"
                     id="latest-projects-image-box">
                </div>
                <div class="col-12 col-xl-6 col-lg-7 padding-9-half-rem-tb padding-10-half-rem-lr bg-project-brown xl-padding-5-rem-all md-padding-7-rem-all xs-padding-3-rem-lr wow animate__fadeIn"
                     data-wow-delay="0.3s"><!-- style="padding-left:10.5rem;padding-right: 10.5rem;"-->
                    <span class="alt-font font-weight-500 text-medium text-project-yellow text-uppercase letter-spacing-3px letter-spacing-1px d-block margin-30px-bottom xs-margin-20px-bottom">
                         {!!html_entity_decode($section["sectionText"]["small_title"])!!}
                    </span>
                    <style>
                        .btn-all-projects{color:#ffffff;}
                        .btn-all-projects:hover{color:#fabc36 !important;}
                    </style>
                    <h4 class="alt-font font-weight-500 text-white letter-spacing-minus-1px w-90 xl-w-100 xs-margin-3-half-rem-bottom">
                        <a href="{{url($layout["lang"].'/projects/show/'.$section["main_project"]["project"]["id"].'/'.$section["main_project"]["projectText"]["slug"])}}"
                           class="btn-all-projects">
                            {!!html_entity_decode($section["main_project"]["projectText"]["title_into_list"])!!}
                        </a>
                    </h4>
                    <p class="w-85 lg-w-100 line-height-36px text-white">
                        {!!html_entity_decode($section["main_project"]["projectText"]["short_description"])!!}
                    </p>
                    <a href="{{url($layout["lang"].'/projects/index')}}"
                       class="btn btn-fancy btn-large btn-round-edge-small btn-project-yellow section-link text-uppercase letter-spacing-3px margin-40px-top">
                        {!!html_entity_decode($section["sectionText"]["btn1_text"])!!}
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
@endif

