@if($section["upcoming_event_flag"])
    <!-- start section -->
    <section class="big-section overlap-height wow animate__fadeIn">
        <div class="container">
            <div class="row align-items-center overlap-gap-section">
                @php
                    $box1 = " col-lg-5 col-md-8 ";
                    $box2 = " col-lg-4";
                    if(empty($section["upcoming_event"]["eventText"]["counter1_num"]) && empty($section["upcoming_event"]["eventText"]["counter2_num"])
                        && empty($section["upcoming_event"]["eventText"]["counter3_num"])){
                        $box1 = " col-lg-8 col-md-8 ";
                        $box2 = " col-lg-4 col-md-4";
                    }
                @endphp
                <div class="{{$box1}} col-12 order-md-2 order-lg-1 sm-margin-5-rem-bottom wow animate__fadeIn"
                     data-wow-delay="0.1s">
                    <div class="margin-25px-right xs-no-margin-right sm-margin-15px-right margin-30px-bottom">
                        <i class="feather icon-feather-calendar  text-project-yellow margin-10px-right"></i>
                        <span class="alt-font text-medium text-project-yellow font-weight-500 text-uppercase letter-spacing-3px d-inline-block">
                            {{date("F", strtotime($section["upcoming_event"]["event"]["date_start"]))}} {{date("d", strtotime($section["upcoming_event"]["event"]["date_start"]))}}, {{date("Y", strtotime($section["upcoming_event"]["event"]["date_start"]))}}
                        </span>
                    </div>
                    @if(!empty($section["upcoming_event"]["eventText"]["title"]))
                        <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-5px-bottom">
                            {!!html_entity_decode($section["upcoming_event"]["eventText"]["title"])!!}
                        </h4>
                        <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-30px-bottom sm-margin-20px-bottom">
                            {!!html_entity_decode($section["upcoming_event"]["eventText"]["subtitle"])!!}
                        </h4>
                    @endif
                    @if(!empty($section["upcoming_event"]["eventText"]["short_description"]))
                        <p class="w-80 lg-w-100 line-height-36px">
                            {!!html_entity_decode($section["upcoming_event"]["eventText"]["short_description"])!!}
                        </p>
                    @endif
                    @if(!empty($section["upcoming_event"]["eventText"]["blockquote1"]))
                        <blockquote
                                class="text-extra-dark-gray border-width-5px border-color-yellow text-extra-medium padding-30px-left md-w-100 no-padding-right line-height-26px no-margin-bottom margin-40px-top lg-w-100 xs-padding-20px-left">
                            {!!html_entity_decode($section["upcoming_event"]["eventText"]["blockquote1"])!!}
                            @if(!empty($section["upcoming_event"]["eventText"]["blockquote2"]))
                                <span class="alt-font font-weight-500 text-medium text-project-yellow d-block margin-20px-top text-uppercase letter-spacing-3px">
                                    {!!html_entity_decode($section["upcoming_event"]["eventText"]["blockquote2"])!!}
                                </span>
                            @endif
                        </blockquote>
                    @endif
                </div>
                <div class="{{$box2}} col-12 order-md-1 order-lg-2 wow animate__fadeIn" data-wow-delay="0.3s">
                    <!-- md-margin-5-rem-bottom-->
                    {{--                    <img src="https://via.placeholder.com/720x886" alt="" class="border-radius-6px" />--}}
                    <img src="{{asset('images/events/'.$section["upcoming_event"]["eventText"]["image1_720x886"])}}"
                         class="w-100 sm-w-100 border-radius-6px"
                         alt="{{$section["upcoming_event"]["eventText"]["image1_alt"]}}"/>
                </div>
                @if(!empty($section["upcoming_event"]["eventText"]["counter1_num"]) || !empty($section["upcoming_event"]["eventText"]["counter2_num"])
                    || !empty($section["upcoming_event"]["eventText"]["counter3_num"]))
                    <div class="col-12 col-lg-2 col-md-3 offset-md-1 order-md-3 wow animate__fadeIn"
                         data-wow-delay="0.5s">
                        <div class="row align-items-center justify-content-center">
                        @if(!empty($section["upcoming_event"]["eventText"]["counter1_num"]))
                            <!-- start counter item -->
                                <div class="col-12 text-center text-sm-left">
                                    <div class="d-flex flex-row align-item-start margin-15px-bottom xs-margin-10px-bottom justify-content-center justify-content-sm-start">
                                        <h4 class="vertical-counter d-inline-flex text-extra-dark-gray alt-font appear font-weight-600 letter-spacing-minus-2px mb-0 sm-letter-spacing-minus-1-half"
                                            data-to="{{$section["upcoming_event"]["eventText"]["counter1_num"]}}"></h4>
                                    </div>
                                    <span class="alt-font text-medium text-uppercase letter-spacing-3px d-block">
                                {!!html_entity_decode($section["upcoming_event"]["eventText"]["counter1_txt"])!!}
                            </span>
                                    <div class="w-100 h-1px bg-medium-gray margin-2-rem-tb xs-margin-3-rem-tb"></div>
                                </div>
                        @endif
                        @if(!empty($section["upcoming_event"]["eventText"]["counter2_num"]))
                            <!-- start counter item -->
                            <div class="col-12 text-center text-sm-left">
                                <div class="d-flex flex-row align-item-start margin-15px-bottom xs-margin-10px-bottom justify-content-center justify-content-sm-start">
                                    <h4 class="vertical-counter d-inline-flex text-extra-dark-gray alt-font appear font-weight-600 letter-spacing-minus-2px mb-0 sm-letter-spacing-minus-1-half"
                                        data-to="{{$section["upcoming_event"]["eventText"]["counter2_num"]}}"></h4>
                                </div>
                                <span class="alt-font text-medium text-uppercase letter-spacing-3px d-block">
                                    {!!html_entity_decode($section["upcoming_event"]["eventText"]["counter2_txt"])!!}
                                </span>
                                <div class="w-100 h-1px bg-medium-gray margin-2-rem-tb xs-margin-3-rem-tb"></div>
                            </div>
                        @endif

                        @if(!empty($section["upcoming_event"]["eventText"]["counter3_num"]))
                            <!-- start counter item -->
                                <div class="col-12 text-center text-sm-left">
                                    <div class="d-flex flex-row align-item-start margin-15px-bottom xs-margin-10px-bottom justify-content-center justify-content-sm-start">
                                        <h4 class="vertical-counter d-inline-flex text-extra-dark-gray alt-font appear font-weight-600 letter-spacing-minus-2px mb-0 sm-letter-spacing-minus-1-half"
                                            data-to="{{$section["upcoming_event"]["eventText"]["counter3_num"]}}"></h4>
                                    </div>
                                    <span class="alt-font text-medium text-uppercase letter-spacing-3px d-block">
                                        {!!html_entity_decode($section["upcoming_event"]["eventText"]["counter3_txt"])!!}
                                    </span>
                                </div>
                            @endif
                        </div>
                    </div>

                @endif
            </div>
        </div>
    </section>

    <!-- end section -->

@endif

