@if(!empty($section["past_events"]) && sizeof($section["past_events"])>0)

    <section class="bg-project-grey" style="padding-bottom:10px;">

        <div class="container">

            <div class="row align-items-center">

                <div class="col-12 col-lg-3 wow animate__fadeIn">

                    <h5 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px mb-0">

                        {!!html_entity_decode($section["sectionText"]["title"])!!}

                    </h5>

                </div>

                <div class="col-12 col-lg-9 col-sm-auto wow animate__fadeInRight" data-wow-delay="0.3s"

                     style="">

                    <div class="w-100 events-subtitle-line"

                         style="margin-top:18px;background-color: rgba(0,0,0,.1);height: 1px;"></div>

                </div>

            </div>

        </div>

    </section>



    <!-- start section -->

    <section class="big-section wow animate__fadeIn bg-project-grey" style="padding-top:30px;">

        <div class="container">

            @php

                $_itemClass = "row-cols-1 row-cols-lg-3 row-cols-md-2";

                if(sizeof($section["past_events"])==1){

                    $_itemClass = "row-cols-12 row-cols-lg-12 row-cols-md-12";

                }

            @endphp

            <div class="row {{$_itemClass}} justify-content-center">

            @foreach($section["past_events"] as $event)

                <!-- start interactive banner item -->

                    <div class="col interactive-banners-style-14 md-margin-30px-bottom xs-margin-15px-bottom wow animate__fadeIn"

                         data-wow-delay="0.2s">

                        <figure class="mb-0 bg-black" style="min-height:350px;max-height: 396px;">

                            {{--                            <img src="https://via.placeholder.com/600x660" alt="" />--}}

                            <img src="{{asset('images/events/'.$event["eventText"]["bg_image"])}}"

                                 alt="{{$event["event"]["name"]}}"

                                 class=" " style="opacity:0.4;min-height:350px;"/>

                            <figcaption>

                                <div class="hover-content align-items-center d-flex flex-column h-100 text-center last-paragraph-no-margin">

                                    <div class="alt-font text-medium text-white opacity-6 text-uppercase letter-spacing-3px margin-10px-bottom mt-auto">

                                        <i class="feather icon-feather-calendar  text-white margin-10px-right"></i>

                                        <span class="text-medium text-white alt-font font-weight-500 text-uppercase letter-spacing-3px d-inline-block">

                                            {{date("F", strtotime($event["event"]["date_start"]))}}

                                            @if($event["event"]["date_start"]==$event["event"]["date_end"])

                                                {{date("d", strtotime($event["event"]["date_start"]))}},

                                            @else

                                                {{date("d", strtotime($event["event"]["date_start"]))}}

                                                - {{date("d", strtotime($event["event"]["date_end"]))}},

                                            @endif

                                            {{date("Y", strtotime($event["event"]["date_start"]))}}

                                        </span>

                                    </div>

                                    <h6 class="alt-font font-weight-600 text-white w-50 mb-auto">

                                        {!!html_entity_decode($event["eventText"]["title1"])!!}

                                        <br>

                                        {!!html_entity_decode($event["eventText"]["subtitle"])!!}

                                    </h6>

                                    <div class="hover-show-content">

                                        <p class="hover-content-detail mx-auto w-60 text-white line-height-26px lg-w-80 md-w-60 xs-w-70">

                                            {!!html_entity_decode($event["eventText"]["small_title"])!!}

                                        </p>

                                        <a href="{{url($layout["lang"].'/events/show/'.$event["event"]["id"].'/'.$event["eventText"]["slug"])}}"

                                           class="btn btn-small btn-fancy btn-project-yellow  btn-round-edge-small text-uppercase letter-spacing-3px margin-30px-top lg-margin-20px-top">

                                            see more

                                        </a>

                                    </div>

                                </div>

                                <div class="hover-action-btn text-center">

                                    <span class="w-40px h-40px line-height-44px d-inline-block bg-project-yellow rounded-circle">

                                        <i class="feather icon-feather-arrow-right text-white icon-extra-small"></i>

                                    </span>

                                </div>

                                <div class="overlayer-box bg-brown-opacity-80 z-index-minus-1"></div>

                            </figcaption>

                        </figure>

                    </div>

                    <!-- end interactive banner item -->

                @endforeach

            </div>

        </div>

    </section>

    <!-- end section -->



@endif











