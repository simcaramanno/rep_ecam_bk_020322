<!-- start section -->
<section class="wow animate__fadeIn bg-white" style="padding-top:50px;">
    <div class="container">
        <div class="row">
            <div class="col-12 tab-style-01 wow animate__fadeIn" data-wow-delay="0.4s">
                <div class="fade in active show">
                    <div class="row">
                        <div class="col-12 col-md-6 sm-margin-40px-bottom text-right">
                            {{--                                <img src="https://via.placeholder.com/800x717" class="w-90 sm-w-100" alt="" />--}}
                            <img src="{{asset('images/events/'.$event["eventText"]["image_box"])}}"
                                 class="w-90 sm-w-100" alt="{{$event["eventText"]["image_box_alt"]}}"/>
                            @if($event["event"]["date_start"]>date('Y-m-d'))

                                {{--<div class="text-left alt-font bg-project-yellow text-start text-extra-large w-300px padding-2-rem-tb padding-2-half-rem-lr position-absolute left-0px top-60px md-w-250px md-padding-2-rem-all">--}}

                                {{--        <i class="feather icon-feather-calendar align-middle text-white" style="font-size:42px;"></i>--}}

                                {{--        <span class="alt-font text-large text-white text-uppercase letter-spacing-3px d-inline-block margin-10px-left align-middle">--}}

                                {{--        <span class="font-weight-600">--}}

                                {{--           {{date("F", strtotime($event["event"]["date_start"]))}}, --}}

                                @if($event["event"]["date_start"]==$event["event"]["date_end"])
                                    {{--                {{date("d", strtotime($event["event"]["date_start"]))}} --}}
                                @else
                                    {{--                {{date("d", strtotime($event["event"]["date_start"]))}}-{{date("d", strtotime($event["event"]["date_end"]))}} --}}
                                @endif
                                {{--        </span>--}}
                                {{--        <br><span>{{date("Y", strtotime($event["event"]["date_start"]))}}</span>--}}
                                {{--    </span>--}}
                                {{--    </div>--}}
                            @endif
                        </div>
                        <div class="col-12 col-lg-5 offset-lg-1 col-md-6">
                            <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-30px-bottom sm-margin-20px-bottom">
                                {!!html_entity_decode($event["eventText"]["box_title"])!!}
                            </h4>
                            <p class="w-85 lg-w-100">
                                {!!html_entity_decode($event["eventText"]["description"])!!}
                            </p>
                            <div class="margin-25px-right xs-no-margin-right sm-margin-15px-right">
                                <hr>
                                <i class="feather icon-feather-map-pin align-middle text-project-yellow margin-10px-right"></i>
                                <span class="alt-font font-weight-600 text-medium  text-extra-dark-gray text-uppercase letter-spacing-3px d-inline-block">
                                   {!!html_entity_decode($event["eventText"]["city"])!!}, {!!html_entity_decode($event["eventText"]["location"])!!}
                                </span>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end section -->

