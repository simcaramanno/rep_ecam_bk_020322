@if(!empty($project["projectText"]["blockquotes"]))
    <!-- start section -->
    <section class="big-sectionbg-light-gray wow animate__fadeIn">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <!-- start blockquote item -->
                <div class="col-12 col-sm-2 col-lg-1 text-sm-right xs-margin-10px-bottom wow animate__fadeIn" data-wow-delay="0.2s"
                     style="border-right:1px solid lightgrey;">
                    <i class="fas fa-quote-left text-project-yellow icon-extra-medium margin-30px-right"></i>
                </div>
                <div class="col-12 col-xl-6 col-lg-8 col-md-9 col-sm-10 border-width-1px border-color-medium-white-transparent xs-border-none wow animate__fadeIn"
                     data-wow-delay="0.4s">
                    <h6 class="text-extra-dark-gray margin-30px-left mb-0 line-height-26px xs-no-margin-left xs-line-height-30px font-weight-600 text-extra-medium">
                        {!!html_entity_decode($project["projectText"]["blockquotes"])!!}
                    </h6>
                </div>
                <!-- end blockquote item -->
            </div>
        </div>
    </section>
    <!-- end section -->
@endif
