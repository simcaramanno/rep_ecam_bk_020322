<!-- start section -->
<style>
    .testxx {
        position: relative;
        color: #250c0c;
    }
    .testxx:after {
        content: "";
        display: inline-block;
        position: absolute;
        width: 100%;
        height: 90%;
        z-index: -1; /* Place the pseudo element right under the content */
        top: 0;
        left: 0;
        /*background: linear-gradient(to top, #fabc36 50%, transparent 80%);*/
        background-color: #fabc36;
        animation-name: highlightxx;
        animation-duration: 1.90s;
        animation-iteration-count: 1;
        /*animation-iteration-count: infinite;*/
        animation-direction: normal; /* Make the animation run back and forth */
        animation-fill-mode: forwards;
    }
    @keyframes highlightxx {
        0% {
            width: 0;
            opacity: 0;
        }
        /*50% {*/
        /*    width: 50%;*/
        /*    opacity: 1;*/
        /*}*/
        100% {
            width: 100%;
            color: #fff !important;
            background-color: #fabc36;
            opacity: 1;
        }
    }
</style>
<section class=" bg-project-grey">
    <div class="container">
        <div class="row">
            <div class="col-12 tab-style-01">
                <div class="">
                    <div class="row">
                        {{--                                <img src="https://via.placeholder.com/800x717" class="w-90 sm-w-100" alt="" />--}}
                        <div class="col-12 col-md-6 cover-background xs-h-300px wow animate__fadeIn sm-margin-40px-bottom xl-margin-twenty-bottom"
                             data-wow-delay="0.4s"
                             style="background-image:url({{asset('images/sections/'.$section["sectionText"]["image1"])}});background-position-y: top!important;">
                        </div>
                        <div class="col-12 col-lg-5 offset-lg-1 col-md-6">
                            @if(!empty($section["sectionText"]["title"]))
                                <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-30px-bottom sm-margin-20px-bottom">
                                    {!!html_entity_decode($section["sectionText"]["title"])!!}
                                </h4>
                            @endif
                            @if(!empty($section["sectionText"]["subtitle"]))
                                <p class="w-85 lg-w-100 line-height-36px">{!!html_entity_decode($section["sectionText"]["subtitle"])!!}</p>
                            @endif
                            @if(!empty($section["sectionText"]["blockquote1"]))
                                <blockquote
                                        class="text-extra-dark-gray border-width-5px border-color-yellow text-extra-medium padding-30px-left md-w-100 no-padding-right line-height-26px no-margin-bottom margin-40px-top lg-w-100 xs-padding-20px-left">
                                    {!!html_entity_decode($section["sectionText"]["blockquote1"])!!}
                                    @if(!empty($section["sectionText"]["blockquote2"]))
                                        <span class="alt-font font-weight-500 text-medium text-project-yellow d-block margin-20px-top">
                                            {!!html_entity_decode($section["sectionText"]["blockquote2"])!!}
                                        </span>
                                    @endif
                                </blockquote>
                            @endif
                            @if(!empty($section["sectionText"]["btn1_text"]) && !empty($section["sectionText"]["btn1_link_type"]) && !empty($section["sectionText"]["btn1_link"]))
                                @php
                                    $url = url($layout["lang"].'/'.$section["sectionText"]["btn1_link"]);
                                    if($section["sectionText"]["btn1_link_type"]=='ex'){
                                        $url = $section["sectionText"]["btn1_link"];
                                    }
                                @endphp
                                <a class="btn btn-medium btn-project-yellow btn-round-edge-small margin-30px-top text-uppercase letter-spacing-3px"
                                   href="{{$url}}"
                                   @if($section["sectionText"]["btn1_link_type"]=='ex') target="_blank" @endif>
                                    {{$section["sectionText"]["btn1_text"]}}
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end section -->

