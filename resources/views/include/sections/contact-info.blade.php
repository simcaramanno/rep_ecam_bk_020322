<!-- start section -->
<section class="big-section" style="padding-top:40px;padding-bottom:10px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10 margin-seven-bottom">
                <hr class="bg-white">
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-10 offset-sm-1">
                <div class="row row-cols-1 row-cols-lg-3 row-cols-sm-2 ">
                    <div class="col margin-60px-bottom sm-margin-30px-bottom xs-margin-40px-bottom wow animate__fadeIn">
                        <!-- start feature box item  align-items-center-->
                        <div class="feature-box feature-box-left-icon">
                            <div class="feature-box-icon margin-10px-top">
                                <i class="line-icon-Location-2 icon-medium text-project-yellow margin-35px-bottom md-margin-15px-bottom sm-margin-10px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="alt-font font-weight-500 d-block text-extra-dark-gray">{{$section["sectionText"]["item1_title"]}}</span>
                                <p class="w-60">{{$section["sectionText"]["item1_subtitle"]}}</p>
                            </div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-60px-bottom sm-margin-30px-bottom xs-margin-40px-bottom wow animate__fadeIn"
                         data-wow-delay="0.2s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-left-icon">
                            <div class="feature-box-icon margin-10px-top">
                                <i class="line-icon-Mail-2 icon-medium text-project-yellow margin-35px-bottom md-margin-15px-bottom sm-margin-10px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="alt-font font-weight-500 d-block text-extra-dark-gray">{{$section["sectionText"]["item2_title"]}}</span>
                                <p class="w-80">
                                    <a href="mailto: {{strtolower($section["sectionText"]["item2_subtitle"])}}"
                                       class="text-yellow-hover">{{$section["sectionText"]["item2_subtitle"]}}</a>
                                </p>
                            </div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                    <div class="col margin-60px-bottom sm-margin-30px-bottom xs-margin-40px-bottom wow animate__fadeIn"
                         data-wow-delay="0.3s">
                        <!-- start feature box item -->
                        <div class="feature-box feature-box-left-icon">
                            <div class="feature-box-icon margin-10px-top">
                                <i class="line-icon-Phone-2 icon-medium text-project-yellow margin-35px-bottom md-margin-15px-bottom sm-margin-10px-bottom"></i>
                            </div>
                            <div class="feature-box-content last-paragraph-no-margin">
                                <span class="alt-font font-weight-500 d-block text-extra-dark-gray">{{$section["sectionText"]["item3_title"]}}</span>
                                <p class="w-80"><a href="tel: {{trim($section["sectionText"]["item3_subtitle"])}}"
                                                   class="text-yellow-hover">{{$section["sectionText"]["item3_subtitle"]}}</a>
                                </p>
                            </div>
                        </div>
                        <!-- end feature box item -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end section -->

