<!-- start section -->
<section style="padding-top:40px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-xl-8 col-lg-8 col-md-10 text-center">
                @if(!empty($section["sectionText"]["title"]))
                    <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-15px-bottom">
                        {!!html_entity_decode($section["sectionText"]["title"])!!}
                    </h4>
                @endif
                @if(!empty($section["sectionText"]["subtitle"]))
                    <p class="w-75 mx-auto lg-w-95 sm-w-100">
                        {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                    </p>
                @endif
                <a href="mailto:{{$platform->email_to}}"
                   class="btn btn-fancy btn-large btn-project-yellow  btn-round-edge-small">
                    {!!html_entity_decode($section["sectionText"]["btn1_text"])!!}
                </a>
            </div>
        </div>
    </div>
</section>
<!-- end section -->

