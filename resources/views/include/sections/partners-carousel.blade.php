@if(!empty($section["partners"]) && sizeof($section["partners"])>0)

    <!-- start section -->
    <section class="big-section wow animate__fadeIn" style="padding-top:60px;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-12 col-md-12 col-sm-12 text-center sm-margin-30px-bottom">
                    @if(!empty($section["sectionText"]["title"]))
                        <h4 class="alt-font font-weight-600 text-extra-dark-gray mb-4 ">
                            {!!html_entity_decode($section["sectionText"]["title"])!!}
                        </h4>
                    @endif
                    @if(!empty($section["sectionText"]["subtitle"]))
                        <p class="text-dark-gray">{!!html_entity_decode($section["sectionText"]["subtitle"])!!}</p>
                    @endif
                </div>
                <div class="col-12 margin-30px-top">
                    <div class="swiper-container text-center"
                         data-slider-options='{ "slidesPerView": 1, "loop": false, "navigation": { "nextEl": ".swiper-button-next-nav", "prevEl": ".swiper-button-previous-nav" }, "autoplay": { "delay": 300000, "disableOnInteraction": false }, "keyboard": { "enabled": true, "onlyInViewport": true }, "breakpoints": { "1200": { "slidesPerView": 4 }, "992": { "slidesPerView": 3 }, "768": { "slidesPerView": 3 } } }'>
                        <div class="swiper-wrapper">
                            @foreach($section["partners"] as $partner)
                                <style>
                                    #partner-img-@php echo $partner["id"] @endphp  {
                                        background-color: #828282;
                                    }
                                    #partner-img-@php echo $partner["id"] @endphp :hover {
                                        background-color: #ff0000 !important;
                                    }
                                </style>
                                <div class="swiper-slide">
                                    <a href="{{$partner["url"]}}" target="_blank">
                                        {{--                                        <img alt="" src="https://via.placeholder.com/232x110">color:#828282 !important--}}
                                        @php
                                        $mt = "";
                                        if($partner["image"]=='gksd-healthcare-v5.svg'){$mt = "margin-top:50px;";}
                                        @endphp
                                        <img src="{{asset('images/partners/'.$partner["image"])}}"
                                             alt="{{$partner["image_alt"]}}"
                                             class="image-hover"
                                             style="{{$mt}}"
                                             data-imgbase="{{asset('images/partners/'.$partner["image"])}}"
                                             data-imghover="{{asset('images/partners/'.$partner["image_hover"])}}">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <!-- start slider navigation -->
                    <div class="swiper-button-next-nav swiper-button-next rounded-circle light slider-navigation-style-07 box-shadow-double-large">
                        <i class="feather icon-feather-arrow-right"></i>
                    </div>
                    <div class="swiper-button-previous-nav swiper-button-prev rounded-circle light slider-navigation-style-07 box-shadow-double-large">
                        <i class="feather icon-feather-arrow-left"></i>
                    </div>
                    <!-- end slider navigation -->
                </div>
            </div>
            @if(!empty($section["sectionText"]["image1"]) && !empty($section["sectionText"]["item1_title"]))
                <div class="row margin-60px-top">
                    <div class="col-12">
                        <div class="row md-top-40px">
                            <div class="col-md-12 col-lg-12 text-center">
                                <span class="d-inline-block margin-20px-right margin-20px-left">{!!html_entity_decode($section["sectionText"]["item1_title"])!!}</span>
                                <a href="{{$section["sectionText"]["image1_link"]}}" target="_blank"
                                   class="d-inline-block">
                                    <img alt="{{$section["sectionText"]["image1_alt"]}}"
                                         src="{{asset('images/partners/'.$section["sectionText"]["image1"])}}"
                                         class="image-hover margin-20px-right margin-20px-left"
                                         data-imgbase="{{asset('images/partners/'.$section["sectionText"]["image1"])}}"
                                         data-imghover="{{asset('images/partners/'.$section["sectionText"]["image1_hover"])}}">
                                    {{--                                    <img alt="" src="https://via.placeholder.com/232x110">--}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            @endif
        </div>
    </section>
    <!-- end section -->

@endif

