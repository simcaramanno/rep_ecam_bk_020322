<!-- start section -->
<section class="fancy-box-background fancy-box-col bg-light-gray p-0">
    <div class="container-fluid">
        <div class="row row-cols-1 row-cols-sm-2 justify-content-center">
            <!-- start prev pagination -->
            @if(!empty($page_data["prev_project"]))
                <div class="col fancy-box-item px-0">
                    <a href="{{url($layout["lang"].'/projects/show/'.$page_data["prev_project"]["project"]["id"].'/'.$page_data["prev_project"]["projectText"]["slug"])}}"
                       class="text-project-yello-hover2 d-flex h-100 align-items-center justify-content-center justify-content-lg-between justify-content-sm-start padding-7-rem-lr padding-4-rem-tb xl-padding-3-rem-all xs-padding-2-rem-tb xs-no-padding-lr">
{{--                        <div class="bg-banner-image cover-background" style="background-image: url(https://via.placeholder.com/955x185)"></div>--}}
                        <div class="light alt-font text-extra-dark-gray font-weight-500 btn-slide-icon-left text-uppercase mr-lg-auto">
                            <i id="project-prev-box0" class="line-icon-Arrow-OutLeft icon-medium align-middle margin-20px-right"></i>
                            <span id="project-prev-box1" class="d-none d-lg-inline-block text-small letter-spacing-3px">
                                {!!html_entity_decode($section["sectionText"]["item1_title"])!!}
                            </span>
                        </div>
                        <span id="project-prev-box2" class="light text-extra-large alt-font text-extra-dark-gray d-block font-weight-500">
                            {!!html_entity_decode($page_data["prev_project"]["projectText"]["title_into_list"])!!}
                        </span>
                    </a>
                </div>
            @else
                <div class="col fancy-box-item px-0">
                    <a class="d-flex h-100 align-items-center justify-content-center justify-content-lg-between justify-content-sm-start padding-7-rem-lr padding-4-rem-tb xl-padding-3-rem-all xs-padding-2-rem-tb xs-no-padding-lr">
                        <div class="light alt-font text-extra-dark-gray font-weight-500 btn-slide-icon-left text-uppercase mr-lg-auto"></div>
                        <span class="light text-extra-large alt-font text-extra-dark-gray d-block font-weight-500"></span>
                    </a>
                </div>
            @endif
            <!-- end prev pagination -->
            <!-- start next pagination -->
            @if(!empty($page_data["next_project"]))
                <div class="col fancy-box-item px-0">
                    <a href="{{url($layout["lang"].'/projects/show/'.$page_data["next_project"]["project"]["id"].'/'.$page_data["next_project"]["projectText"]["slug"])}}"
                       class="text-project-yello-hover3 d-flex h-100 align-items-center justify-content-center justify-content-sm-end justify-content-lg-between padding-7-rem-lr padding-4-rem-tb xl-padding-3-rem-all xs-padding-2-rem-tb xs-no-padding-lr">
{{--                        <div class="bg-banner-image cover-background" style="background-image: url(https://via.placeholder.com/955x185)"></div>--}}
                        <span id="project-next-box2" class="light text-extra-large alt-font text-extra-dark-gray d-block font-weight-500 mr-lg-auto">
                             {!!html_entity_decode($page_data["next_project"]["projectText"]["title_into_list"])!!}
                        </span>
                        <div class="light alt-font text-extra-dark-gray font-weight-500 btn-slide-icon text-uppercase">
                            <span id="project-next-box1" class="d-none d-lg-inline-block text-small letter-spacing-3px text-extra-dark-gray">
                                {!!html_entity_decode($section["sectionText"]["item2_title"])!!}
                            </span>
                            <i id="project-next-box0" class="line-icon-Arrow-OutRight icon-medium align-middle margin-20px-left text-extra-dark-gray"></i>
                        </div>
                    </a>
                </div>
            @else
                <div class="col fancy-box-item px-0">
                    <a class="d-flex h-100 align-items-center justify-content-center justify-content-sm-end justify-content-lg-between padding-7-rem-lr padding-4-rem-tb xl-padding-3-rem-all xs-padding-2-rem-tb xs-no-padding-lr">
                        <span class="light text-extra-large alt-font text-extra-dark-gray d-block font-weight-500 mr-lg-auto"></span>
                        <div class="light alt-font text-black font-weight-500 btn-slide-icon text-uppercase"></div>
                    </a>
                </div>
            @endif
            <!-- end next pagination -->
        </div>
    </div>
</section>
<!-- end section -->
