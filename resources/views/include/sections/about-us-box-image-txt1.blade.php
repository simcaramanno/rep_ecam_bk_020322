<!-- start section -->
<section class="wow animate__fadeIn bg-project-grey" style="padding-bottom:60px;">
    <div class="container">
        <div class="row">
            <div class="col-12 tab-style-01 wow animate__fadeIn" data-wow-delay="0.4s">
                <div class="fade in active show">
                    <div class="row">
                        <div class="col-12 col-md-6 text-right sm-margin-40px-bottom">
                            {{--                                <img src="https://via.placeholder.com/800x717" class="w-90 sm-w-100" alt="" />--}}
                            <img src="{{asset('images/sections/'.$section["sectionText"]["image1"])}}"
                                 class="w-100 sm-w-100" alt="{{$section["sectionText"]["image1_alt"]}}"/>
                        </div>
                        <div class="col-12 col-lg-5 offset-lg-1 col-md-6">
                            @if(!empty($section["sectionText"]["title"]))
                                <p class="alt-font text-project-yellow text-uppercase letter-spacing-3px"> {!!html_entity_decode($section["sectionText"]["title"])!!}</p>
                            @endif
                            @if(!empty($section["sectionText"]["subtitle"]))
                                <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-30px-bottom sm-margin-20px-bottom">
                                    {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                                </h4>
                            @endif
                            @if(!empty($section["sectionText"]["content1"]))
                                <p class="w-85 lg-w-100">{!!html_entity_decode($section["sectionText"]["content1"])!!}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end section -->



