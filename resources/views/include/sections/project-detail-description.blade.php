<!-- start section -->
<section>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-11">
                <div class="row">
                    <div class="col-lg-4 padding-five-right lg-padding-15px-right md-margin-50px-bottom wow animate__fadeIn" data-wow-delay="0.2s">
                        @if(!empty($project["projectText"]["title"]))
                            <h5 class="alt-font text-extra-dark-gray font-weight-500 margin-4-rem-bottom letter-spacing-minus-1px">
                                {!!html_entity_decode($project["projectText"]["title"])!!}
                            </h5>
                        @endif
                        <ul class="list-unstyled">
                            <li class="border-bottom border-color-medium-gray padding-20px-bottom margin-20px-bottom">
                                <span class="text-uppercase letter-spacing-3px text-extra-dark-gray w-35 d-inline-block font-weight-600 alt-font text-medium">
                                    {!!html_entity_decode($section["sectionText"]["item1_title"])!!}
                                </span>
                                {{date("d", strtotime($project["project"]["project_date"]))}} {{date("F", strtotime($project["project"]["project_date"]))}} {{date("Y", strtotime($project["project"]["project_date"]))}}
                            </li>
                            <li class="border-bottom border-color-medium-gray padding-20px-bottom margin-20px-bottom">
                                <span class="text-uppercase letter-spacing-3px text-extra-dark-gray w-35 d-inline-block font-weight-600 alt-font text-medium">
                                    {!!html_entity_decode($section["sectionText"]["item2_title"])!!}
                                </span>
                                {!!html_entity_decode($project["projectText"]["project_type"])!!}
                            </li>
                            <li>
                                <span class="text-uppercase letter-spacing-3px text-extra-dark-gray w-35 d-inline-block font-weight-600 alt-font text-medium">
                                    {!!html_entity_decode($section["sectionText"]["item3_title"])!!}
                                </span>
                                {!!html_entity_decode($project["projectText"]["location"])!!}
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-7 offset-lg-1 wow animate__fadeIn" data-wow-delay="0.4s">
                        @if(!empty($section["sectionText"]["title"]))
                            <span class="alt-font text-medium text-uppercase letter-spacing-3px font-weight-500 margin-20px-bottom d-inline-block text-extra-dark-gray">
                                {!!html_entity_decode($section["sectionText"]["title"])!!}
                            </span>
                        @endif
                        <p>{!!html_entity_decode($project["projectText"]["description"])!!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end section -->
