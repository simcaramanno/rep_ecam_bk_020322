@if(!empty($section["gallery_carousel"]) && sizeof($section["gallery_carousel"])>0)
    <section class="" style="padding-top:0px;padding-bottom:0px;">
        <div class="container">
            <div class="col-12">
                <div class="swiper-container text-center"
                     data-slider-options='{ "slidesPerView": 1, "loop": false, "navigation": { "nextEl": ".swiper-button-next-nav", "prevEl": ".swiper-button-previous-nav" }, "autoplay": { "delay": 300000, "disableOnInteraction": false }, "keyboard": { "enabled": true, "onlyInViewport": true }, "breakpoints": { "1200": { "slidesPerView": 3 }, "992": { "slidesPerView": 3 }, "768": { "slidesPerView": 3 } } }'>
                    <div class="swiper-wrapper">
                        @foreach($section["gallery_carousel"] as $gallery)
                            <div class="swiper-slide wow animate__fadeIn"  style="padding-left:12px;padding-right:12px;">
                                <a href="{{asset('images/sections/'.$gallery["image"])}}" title="." data-group="lightbox-gallery" class="lightbox-group-gallery-item">
                                    <div class="portfolio-box">
                                        <div class=" ">
                                            <img src="{{asset('images/sections/'.$gallery["image"])}}" alt="." />
                                            <div class="portfolio-hover justify-content-end d-flex flex-column bg-transparent-mengo-yellow" style="margin-left:12px;width: 94%;">
                                                <i class="feather icon-feather-zoom-in portfolio-plus-icon font-weight-300 text-white absolute-middle-center icon-small move-top-bottom"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <!-- start slider navigation -->
                <div class="swiper-button-next-nav swiper-button-next rounded-circle light slider-navigation-style-07 box-shadow-double-large">
                    <i class="feather icon-feather-arrow-right"></i></div>
                <div class="swiper-button-previous-nav swiper-button-prev rounded-circle light slider-navigation-style-07 box-shadow-double-large">
                    <i class="feather icon-feather-arrow-left"></i></div>
                <!-- end slider navigation -->
            </div>
        </div>

    </section>
    <!-- end section -->
@endif

