<section class="border-top border-color-medium-gray" style="padding-top:90px;padding-bottom:40px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-9 col-md-8 col-sm-9 text-center wow animate__fadeIn">
                @if(!empty($section["sectionText"]["title"]))
                    <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-15px-bottom">
                        {!!html_entity_decode($section["sectionText"]["title"])!!}
                    </h4>
                @endif
                @if(!empty($section["sectionText"]["subtitle"]))
                    <p class="w-75 mx-auto lg-w-95 sm-w-100">
                        {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                    </p>
                @endif
            </div>
        </div>
    </div>
</section>
<section class="" style="padding-top:40px;">
    <div class="container">
        @if(!empty($section["partners"]) || sizeof($section["partners"])>0)
            <div class="row row-cols-1 row-cols-lg-4 row-cols-md-4 row-cols-sm-2 client-logo-style-01 align-items-center">
                @foreach($section["partners"] as $partner)
                    <!-- start client logo item -->
                    <div class="col text-center margin-30px-bottom sm-margin-15px-bottom">
                        <div class="client-box padding-15px-all">
                            {{--                        <a href="#" target="_blank"><img class="client-box-image" src="https://via.placeholder.com/225x110" alt="" /></a>--}}
                            <a href="{{$partner["url"]}}" target="_blank">
                                <img class="client-box-image image-hover"
                                     src="{{asset('images/partners/'.$partner["image"])}}"
                                     alt="{{$partner["image_alt"]}}"
                                     data-imgbase="{{asset('images/partners/'.$partner["image"])}}"
                                     data-imghover="{{asset('images/partners/'.$partner["image_hover"])}}"/>
                            </a>
                        </div>
                    </div>
                    <!-- end client logo item -->
                @endforeach
            </div>
        @endif
        @if(!empty($section["sectionText"]["image1"]) && !empty($section["sectionText"]["item1_title"]))
            <div class="row" style="margin-top:60px;">
                <div class="col-12">
                    {{--                    <div class="row md-top-40px">--}}

                    {{--                        <div class="col-md-6 text-right" style="margin: auto;">--}}

                    {{--                            <p style="margin: auto;">--}}

                    {{--                                {!!html_entity_decode($section["sectionText"]["item1_title"])!!}--}}

                    {{--                            </p>--}}

                    {{--                        </div>--}}

                    {{--                        <div class="col-md-6 text-left">--}}

                    {{--                            <a href="{{$section["sectionText"]["image1_link"]}}" target="_blank">--}}

                    {{--                                <img alt="{{$section["sectionText"]["image1_alt"]}}"--}}

                    {{--                                     src="{{asset('images/partners/'.$section["sectionText"]["image1"])}}"--}}

                    {{--                                     class="image-hover"--}}

                    {{--                                     data-imgbase="{{asset('images/partners/'.$section["sectionText"]["image1"])}}"--}}

                    {{--                                     data-imghover="{{asset('images/partners/'.$section["sectionText"]["image1_hover"])}}">--}}

                    {{--                                --}}{{--                                    <img alt="" src="https://via.placeholder.com/232x110">--}}

                    {{--                            </a>--}}

                    {{--                        </div>--}}

                    {{--                    </div>--}}
                    <div class="row md-top-40px">
                        <div class="col-md-12 col-lg-12 text-center">
                            <span class="d-inline-block margin-20px-right margin-20px-left">{!!html_entity_decode($section["sectionText"]["item1_title"])!!}</span>
                            <a href="{{$section["sectionText"]["image1_link"]}}" target="_blank" class="d-inline-block">
                                <img alt="{{$section["sectionText"]["image1_alt"]}}"
                                     src="{{asset('images/partners/'.$section["sectionText"]["image1"])}}"
                                     class="image-hover margin-20px-right margin-20px-left"
                                     data-imgbase="{{asset('images/partners/'.$section["sectionText"]["image1"])}}"
                                     data-imghover="{{asset('images/partners/'.$section["sectionText"]["image1_hover"])}}">
                                {{--                                    <img alt="" src="https://via.placeholder.com/232x110">--}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>
</section>

