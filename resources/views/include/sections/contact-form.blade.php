<section class="border-top border-color-medium-gray" style="padding-top:90px;padding-bottom:40px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-9 col-md-8 col-sm-9 text-center wow animate__fadeIn">
                @if(!empty($section["sectionText"]["title"]))
                    <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-15px-bottom">
                        {!!html_entity_decode($section["sectionText"]["title"])!!}
                    </h4>
                @endif
                @if(!empty($section["sectionText"]["subtitle"]))
                    <p class="w-75 mx-auto lg-w-95 sm-w-100">
                        {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                    </p>
                @endif
            </div>
        </div>
    </div>
</section>
<section style="padding-top:40px;padding-bottom:10px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <!-- start contact form -->
                        @include('include.errors-form')

                        @include('include.message-form')
                        <form action="{{url('contacts/send-message')}}" method="post">
                            @csrf
                            <input type="hidden" name="lang" value="{{$layout["lang"]}}">
                            <div class="row row-cols-1 row-cols-md-2">
                                <div class="col margin-4-rem-bottom sm-margin-25px-bottom">
                                    <input class="medium-input bg-white margin-25px-bottom required" type="text"
                                           name="name" required placeholder="{{$section["sectionText"]["field1"]}}">
                                    <input class="medium-input bg-white margin-25px-bottom required" type="email"
                                           name="email" required placeholder="{{$section["sectionText"]["field2"]}}">
                                    <input class="medium-input bg-white mb-0 required" type="tel" name="phone"
                                           placeholder="{{$section["sectionText"]["field3"]}}">
                                </div>
                                <div class="col margin-4-rem-bottom sm-margin-10px-bottom">
                                    <textarea class="medium-textarea h-200px bg-white required" name="message" required
                                              placeholder="{{$section["sectionText"]["field4"]}}"></textarea>
                                </div>
                                <!--TODO REMOVE COMMENTS!!!-->
                                {{--                                <div class="col-md-12">--}}

                                {{--                                    <div class="text-left sm-margin-10px-bottom">--}}

                                {{--                                        <div class="col-md-12">--}}

                                {{--                                            <div class="form-group text-center">--}}

                                {{--                                                <div class="g-recaptcha text-center" data-sitekey="{{$platform->rechaptca_apikey}}"></div>--}}

                                {{--                                            </div>--}}

                                {{--                                        </div>--}}

                                {{--                                    </div>--}}

                                {{--                                </div>--}}
                                <div class="col text-left sm-margin-30px-bottom">
                                    <input type="checkbox" name="terms_condition" required
                                           class="terms-condition d-inline-block align-top w-auto mb-0 margin-5px-top margin-10px-right">
                                    <label for="terms_condition" class="text-small d-inline-block align-top w-85">
                                        {{$section["sectionText"]["content1"]}}
                                    </label>
                                </div>
                                <div class="col text-center text-md-right">
                                    <button class="btn btn-medium btn-project-yellow  btn-round-edge-small text-uppercase letter-spacing-3px mb-0 submit"
                                            type="submit">
                                        {{$section["sectionText"]["btn1_text"]}}
                                    </button>
                                </div>
                            </div>
                            <div class="form-results d-none"></div>
                        </form>
                        <!-- end contact form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

