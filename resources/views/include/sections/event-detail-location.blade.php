<!-- start section -->
<section class="wow animate__fadeIn bg-white" style="padding-bottom:0px;">
    <div class="container margin-10px-bottom">
        <div class="row">
            <div class="col-12 tab-style-01 wow animate__fadeIn" data-wow-delay="0.4s">
                <div class="fade in active show">
                    <div class="row">
                        <div class="col-12 col-lg-5 col-md-6 sm-margin-40px-bottom">
                            <div class="margin-25px-right xs-no-margin-right sm-margin-15px-right margin-20px-bottom">
                                <i class="feather icon-feather-map-pin text-project-yellow margin-10px-right"></i>
                                <span class="alt-font text-medium text-project-yellow font-weight-500 text-uppercase letter-spacing-3px d-inline-block">
                                    LOCATION
                                </span>
                            </div>
                            <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px no-margin-bottom">
                                {!!html_entity_decode($event["eventText"]["city"])!!},
                            </h4>
                            <h4 class="alt-font text-extra-dark-gray letter-spacing-minus-1px no-margin-bottom">
                                {!!html_entity_decode($event["eventText"]["location_box"])!!}
                            </h4>
                            <p class="w-85 lg-w-100 margin-30px-top">
                                {!!html_entity_decode($event["eventText"]["location_desc"])!!}
                            </p>
                            <blockquote
                                    class="border-width-6px alt-font border-color-yellow md-no-padding-right xs-no-margin-tb">
                                <small class="text-project-yellow font-weight-600">
                                    {!!html_entity_decode($event["eventText"]["location_box"])!!}
                                    <br>
                                    {!!html_entity_decode($event["event"]["location_address"])!!}
                                </small>
                            </blockquote>
                        </div>
                        <div class="col-12 col-md-6 offset-lg-1 text-right sm-margin-40px-bottom">
                            {{--                                <img src="https://via.placeholder.com/800x717" class="w-90 sm-w-100" alt="" />--}}
                            <img src="{{asset('images/events/'.$event["event"]["location_img"])}}"
                                 class="w-90 sm-w-100 border-radius-6px" alt="{{$event["event"]["location_img_alt"]}}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center margin-100px-bottom">
            <div class="col-12  margin-100px-top">
                <iframe class="w-100 h-500px"
                        frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAUfMYV3Z8pF4yEp5H4DvuwxepHz4LBYes&q={{$event["event"]["location_marker"]}}"
                        allowfullscreen>
                </iframe>
                {{--<iframe class="w-100 h-500px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.843821917424!2d144.956054!3d-37.817127!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1427947693651"></iframe>--}}
            </div>
        </div>
    </div>
</section>
<!-- end section -->

