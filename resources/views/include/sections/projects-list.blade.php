<!-- start section -->
<section class="bg-light-gray pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12 filter-content">
                @if(!empty($section["projects_list"]) && sizeof($section["projects_list"])>0)
                    <ul class="portfolio-classic portfolio-wrapper grid grid-loading grid-3col xl-grid-3col lg-grid-3col md-grid-2col sm-grid-2col xs-grid-1col gutter-extra-large text-center">
                        <li class="grid-sizer"></li>
                        @foreach($section["projects_list"] as $project)
                            <!-- start portfolio item -->
                            <li class="grid-item wow animate__fadeIn">
                                <div class="portfolio-box border-radius-6px box-shadow-large">
                                    <div class="portfolio-image bg-brown-opacity-90">
                                        <!--https://via.placeholder.com/800x650-->
                                        <img src="{{asset('images/projects/'.$project["project"]["small_image"])}}"
                                             alt="{{$project["project_texts"]["title_into_list"]}}" />
                                        <div class="portfolio-hover align-items-center justify-content-center d-flex h-100">
                                            <div class="portfolio-icon">
{{--                                                <a href="{{asset('images/projects/'.$project["project"]["small_image"])}}"--}}
{{--                                                   data-group="portfolio-items"--}}
{{--                                                   class="lightbox-group-gallery-item text-slate-blue text-slate-blue-hover rounded-circle bg-white">--}}
{{--                                                    <i class="fas fa-search icon-very-small" aria-hidden="true"></i>--}}
{{--                                                </a>--}}
                                                <a href="{{url($layout["lang"].'/projects/show/'.$project["project"]["id"].'/'.$project["project_texts"]["slug"])}}" class="text-slate-blue text-slate-blue-hover rounded-circle bg-white">
                                                    <i class="fas fa-link icon-very-small" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portfolio-caption bg-white padding-30px-tb lg-padding-20px-tb">
                                        <a href="{{url($layout["lang"].'/projects/show/'.$project["project"]["id"].'/'.$project["project_texts"]["slug"])}}">
                                            <span class="alt-font text-extra-dark-gray font-weight-600">
                                                {!!html_entity_decode($project["project_texts"]["title_into_list"])!!}
                                            </span>
                                        </a>
                                        <span class="text-medium d-block margin-one-bottom">
                                            {{date("d", strtotime($project["project"]["project_date"]))}} {{date("F", strtotime($project["project"]["project_date"]))}} {{date("Y", strtotime($project["project"]["project_date"]))}}
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <!-- end portfolio item -->
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- end section -->
