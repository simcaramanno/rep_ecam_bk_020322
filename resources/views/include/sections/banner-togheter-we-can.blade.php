<!-- start section background-image:url('https://via.placeholder.com/1920x1100');-->
<section class="parallax wow animate__fadeIn" data-parallax-background-ratio="0.5"
         style="background-image:url('{{asset('images/sections/'.$section["sectionText"]["image1"])}}');">
    <div class="opacity-medium  bg-dark-opacity-70"></div>
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-xl-12 col-md-12 col-sm-12 text-center">
                @if(!empty($section["sectionText"]["title"]))
                    <h3 class="alt-font font-weight-600 text-white" style="line-height: normal;margin-bottom:0px;">
                        {!!html_entity_decode($section["sectionText"]["title"])!!}
                    </h3>
                @endif
                @if(!empty($section["sectionText"]["subtitle"]))
                    <p class="alt-font text-white text-uppercase letter-spacing-3px">
                        {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                    </p>
                @endif
            </div>
        </div>
    </div>
</section>
<!-- end section -->
<!-- start section CON VIDEO-->
{{--    <section class="big-section cover-background" style="background-image:url('https://via.placeholder.com/1920x1100');">--}}
{{--        <div class="opacity-extra-medium-2 bg-dark-slate-blue"></div>--}}
{{--        <div class="container">--}}
{{--            <div class="row justify-content-center">--}}
{{--                <div class="col-12 col-xl-7 col-lg-8 col-md-10 text-center overlap-gap-section">--}}
{{--                    <a href="https://www.youtube.com/watch?v=g0f_BRYJLJE" class="popup-youtube video-icon-box video-icon-large position-relative d-inline-block margin-3-half-rem-bottom wow animate__bounceIn">--}}
{{--                            <span>--}}
{{--                                <span class="video-icon bg-neon-blue">--}}
{{--                                    <i class="icon-simple-line-control-play text-white"></i>--}}
{{--                                    <span class="video-icon-sonar"><span class="video-icon-sonar-afr bg-neon-blue"></span></span>--}}
{{--                                </span>--}}
{{--                            </span>--}}
{{--                    </a>--}}
{{--                    <h4 class="alt-font text-white font-weight-600 margin-45px-bottom md-margin-35px-bottom xs-margin-25px-bottom wow animate__fadeIn">Beautifully simple handcrafted templates for your website</h4>--}}
{{--                    <span class="text-white alt-font text-uppercase letter-spacing-1px wow animate__fadeIn">unlimited power and customization</span>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

<!-- end section -->

