@if(!empty($project["projectText"]["banner_text"]))
    <!-- start section background-image:url('https://via.placeholder.com/1920x1100');-->
    <section class="parallax wow animate__fadeIn" data-parallax-background-ratio="0.5"
             style="background-image:url('{{asset('images/projects/'.$project["project"]["banner_image"])}}');">
        <div class="opacity-medium  bg-dark-opacity-70"></div>
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 col-xl-12 col-md-12 col-sm-12 text-center">
                    <h3 class="alt-font font-weight-600 text-white" style="line-height: normal;margin-bottom:0px;">
                        {!!html_entity_decode($project["projectText"]["banner_text"])!!}
                    </h3>
                </div>
            </div>
        </div>
    </section>
@endif
