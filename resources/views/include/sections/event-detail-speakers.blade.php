@if(!empty($event["speakers"]) && sizeof($event["speakers"])>0)
    <!-- start section -->
    <section style="padding-bottom:60px;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-8 col-md-8 col-sm-9 text-center margin-6-rem-bottom md-margin-4-rem-bottom wow animate__fadeIn">
                    @if(!empty($section["sectionText"]["title"]))
                        <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-15px-bottom">
                            {!!html_entity_decode($section["sectionText"]["title"])!!}
                        </h4>
                    @endif
                    @if(!empty($section["sectionText"]["subtitle"]))
                        <p class="w-75 mx-auto lg-w-95 sm-w-100">
                            {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row row-cols-1 row-cols-lg-4 row-cols-sm-2">
                @foreach($event["speakers"] as $speaker)
                    <!-- start team member -->
                    <div class="col team-style-01 text-center md-margin-30px-bottom xs-margin-15px-bottom margin-2-rem-bottom">
                        <figure class="border-radius-5px">
                            <a>
                                <div class="team-member-image">
                                    {{--                                    <img alt="" src="https://via.placeholder.com/525x639">--}}
                                    <img alt="{{$speaker["first_name"].'-'.$speaker["last_name"]}}"
                                         src="{{asset('images/events/'.$speaker["image"])}}">
                                    <div class="team-overlay border-radius-5px bg-transparent-mengo-yellow"></div>
                                </div>
                                <figcaption
                                        class="align-items-center justify-content-center d-flex flex-column padding-20px-lr padding-30px-tb">
                                    <span class="team-title d-block alt-font text-white font-weight-500">
                                        {{$speaker["first_name"].' '.$speaker["last_name"]}}
                                    </span>
                                    <span class="team-sub-title text-small d-block text-white text-uppercase">
                                        {{$speaker["role"]}}
                                    </span>
                                </figcaption>
                            </a>
                        </figure>
                    </div>
                    <!-- end team member -->
                @endforeach
            </div>
        </div>
    </section>
    <!-- end section -->

@endif



