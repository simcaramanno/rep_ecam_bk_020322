<!-- start page title -->
<section class="parallax half-section" data-parallax-background-ratio="0.5"
         style="background-image:url('{{asset('images/events/'.$event["eventText"]["bg_image"])}}');">
    <!--background-image:url('https://via.placeholder.com/1920x1031');-->
    <div class="opacity-medium bg-brown-opacity-80"></div>
    <div class="container">
        <div class="row text-left">
            <div class="col-12 col-xl-4 col-lg-12 col-md-12">
                <div class="d-flex flex-column small-screen position-relative z-index-1"
                     style="justify-content: center;">
                    <div class="page-title-large text-left" style=" ">
                        <!-- align-items-center-->
                        <span class=" alt-font margin-5px-bottom d-block xs-line-height-20px ">
                            <span class="alt-font text-white text-uppercase letter-spacing-3px">
                                <mark class="alt-font text-white font-size-13"
                                      style="border-radius:4px;padding-left:8px;padding-right:2px;background-color:#250c0c;">
                                    {!!html_entity_decode($section["sectionText"]["title"])!!}
                                </mark>
                            </span>
                        </span>
                        <span class=" alt-font margin-5px-bottom d-block xs-line-height-20px ">
                            <i class="feather icon-feather-calendar text-project-yellow margin-10px-right"></i>
                            <span class="alt-font text-medium text-project-yellow font-weight-500 text-uppercase letter-spacing-3px d-inline-block">
                                {{date("F", strtotime($event["event"]["date_start"]))}}
                                @if($event["event"]["date_start"]==$event["event"]["date_end"])
                                    {{date("d", strtotime($event["event"]["date_start"]))}},
                                @else
                                    {{date("d", strtotime($event["event"]["date_start"]))}}
                                    - {{date("d", strtotime($event["event"]["date_end"]))}},
                                @endif
                                {{date("Y", strtotime($event["event"]["date_start"]))}}
                            </span>
                        </span>
                        <h1 class="text-white alt-font font-weight-600 letter-spacing-minus-1 margin-10px-bottom">
                            {!!html_entity_decode($event["eventText"]["title1"])!!}
                            @if(!empty($event["eventText"]["subtitle"]))
                                <br>
                                {!!html_entity_decode($event["eventText"]["subtitle"])!!}
                            @endif
                        </h1>
                    </div>
                </div>
            </div>
            @if(!empty($event["eventText"]["video_url"]) || !empty($event["eventText"]["video_url_youtube"]))
                <div class="col-12 col-xl-4 col-lg-12 col-md-12">
                    <div class="d-flex flex-column small-screen position-relative z-index-1"
                         style="justify-content: center;">
                        <div class="page-title-large text-left">
                            <span class=" alt-font margin-5px-bottom d-block xs-line-height-20px ">
                                @php
                                $videourl = asset('images/video/'.$event["eventText"]["video_url"]);
                                if(!empty($event["eventText"]["video_url_youtube"])){
                                    $videourl = $event["eventText"]["video_url_youtube"];
                                }
                                @endphp
                                <!-- start youtube video url(asset('images/video/'.$event["eventText"]["video_url"]))-->
                                <a href="{{$videourl}}"
                                   class="popup-youtube absolute-middle-center video-icon-box video-icon-large">
                                    <span>
                                        <span class="video-icon bg-white box-shadow-extra-large">
                                            <i class="icon-simple-line-control-play text-project-yellow"></i>
                                            <span class=" ">
                                                <span class="video-icon-sonar-bfr bg-white opacity-7"></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                                <!-- end vimeo video -->
                            </span>
                        </div>
                    </div>
                </div>

            @endif
        </div>
    </div>
</section>
<!-- end page title -->



