@if(!empty($section["sectionText"]["content1"]))
    <!-- start section -->
    <section class=" wow animate__fadeIn" style="padding-top:60px;padding-bottom:60px;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 col-lg-10 col-xl-10 text-left last-paragraph-no-margin">
                    {!!html_entity_decode($section["sectionText"]["content1"])!!}
                </div>
            </div>
        </div>
    </section>
    <!-- end page title -->
@endif

