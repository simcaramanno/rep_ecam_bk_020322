<!-- start page title -->
<section class="parallax half-section" data-parallax-background-ratio="0.5"
         style="background-image:url('{{asset('images/projects/'.$project["project"]["big_image"])}}');">
    <!--background-image:url('https://via.placeholder.com/1920x1031');-->
    {{--    <div class="opacity-medium bg-dark-opacity-60"></div>--}}
    <div class="opacity-medium  bg-brown-opacity-80"></div>
    <div class="container">
        <div class="row text-left">
            <div class="col-12 col-xl-4 col-lg-12 col-md-12">
                <div class="d-flex flex-column small-screen position-relative z-index-1"
                     style="justify-content: center;">
                    <div class="page-title-large text-left" style=" ">
                        <!-- align-items-center-->
                        <span class=" alt-font margin-5px-bottom d-block xs-line-height-20px ">
                            <span class=" alt-font margin-5px-bottom d-block xs-line-height-20px ">
                                <span class="alt-font text-medium text-project-yellow font-weight-500 text-uppercase letter-spacing-3px d-inline-block">
                                    {{$project["projectText"]["project_type"]}}
                                </span>
                            </span>
                        </span>
                        <h1 class="text-white alt-font font-weight-600 letter-spacing-minus-1 margin-10px-bottom">
                            @if(!empty($project["projectText"]["header_title"]))
                                {!!html_entity_decode($project["projectText"]["header_title"])!!}
                            @endif
                        </h1>
                    </div>
                </div>
            </div>
            @if(!empty($project["project"]["video_url"]) || !empty($project["project"]["video_url_youtube"]))
                <div class="col-12 col-xl-4 col-lg-12 col-md-12">
                    <div class="d-flex flex-column small-screen position-relative z-index-1"
                         style="justify-content: center;">
                        <div class="page-title-large text-left">
                            <span class=" alt-font margin-5px-bottom d-block xs-line-height-20px">
                                  @php
                                      $videourl = asset('images/projects/video/'.$project["project"]["video_url"]);
                                      if(!empty($project["project"]["video_url_youtube"])){
                                          $videourl = $project["project"]["video_url_youtube"];
                                      }
                                  @endphp
                                <!-- start vimeo video -->
                                <a href="{{$videourl}}"
                                   class="popup-youtube absolute-middle-center video-icon-box video-icon-large">
                                    <span>
                                        <span class="video-icon bg-white box-shadow-extra-large">
                                            <i class="icon-simple-line-control-play text-project-yellow"></i>
                                            <span class=" ">
                                                <span class="video-icon-sonar-bfr bg-white opacity-7"></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                                <!-- end vimeo video -->
                            </span>
                        </div>
                    </div>
                </div>

            @endif
        </div>
    </div>
</section>
<!-- end page title -->





