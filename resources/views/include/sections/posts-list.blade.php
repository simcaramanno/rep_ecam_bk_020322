<section class="border-top border-color-medium-gray" style="padding-top:90px;padding-bottom:40px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-9 col-md-8 col-sm-9 text-center wow animate__fadeIn">
                @if(!empty($section["sectionText"]["title"]))
                    <h4 class="alt-font font-weight-600 text-extra-dark-gray letter-spacing-minus-1px margin-15px-bottom">
                        {!!html_entity_decode($section["sectionText"]["title"])!!}
                    </h4>
                @endif
                @if(!empty($section["sectionText"]["subtitle"]))
                    <p class="w-75 mx-auto lg-w-95 sm-w-100">
                        {!!html_entity_decode($section["sectionText"]["subtitle"])!!}
                    </p>
                @endif
            </div>
        </div>
    </div>
</section>


@if(!empty($section["posts"]) && sizeof($section["posts"])>0)
    <!-- start section -->
    <section class="bg-white pt-0 xl-no-padding-lr">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 blog-content padding-seven-lr xs-no-padding-lr">
                    <ul class="blog-modern blog-wrapper grid grid-loading grid-4col xl-grid-4col lg-grid-3col md-grid-2col sm-grid-2col xs-grid-1col gutter-double-extra-large">
                        <li class="grid-sizer"></li>
                    @foreach($section["posts"] as $post)
                        <!-- start blog item -->
                            <li class="grid-item wow ">
                                <div class="blog-post">
                                    <div class="blog-post-image bg-extra-dark-gray">
                                        <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}">
                                            <img src="{{asset('images/press/'.$post["gallery"][0]["filename_800x1010"])}}"
                                                 alt="{{$post["post_text"]["title"]}}">
                                            {{--                                            <img src="https://via.placeholder.com/800x1010" alt="">--}}
                                        </a>
                                    </div>
                                    <div class="post-details bg-white text-center padding-1-rem-all xl-padding-1-rem-all padding-30px-bottom"
                                         style="min-height:200px;min-height: 200px;align-items: center;justify-content: center;display: flex;flex-wrap: wrap;align-content: space-between;">
                                        <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}"
                                           class="alt-font blog-category text-project-yellow text-project-yellow-hover text-medium font-weight-500 text-uppercase letter-spacing-2px">
                                            {{-- {!!html_entity_decode($post["category_name"])!!} --}}
                                            {!!html_entity_decode($post["post"]["newspaper"])!!}
                                        </a>
                                        <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}"
                                           class="alt-font text-extra-dark-gray text-extra-dark-gray-hover font-weight-600 d-block"
                                           style="font-size:13px;line-height: 13px;">
                                            {!!html_entity_decode($post["post_text"]["title"])!!}
                                        </a>
                                        <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}"
                                           class="alt-font text-uppercase letter-spacing-3px text-extra-small d-block">
                                            {{date("d M Y", strtotime($post["post"]["published_at"]))}}
                                        </a>
                                    </div>
                                </div>
                            </li>

                            <!-- end blog item -->

                        @endforeach
                    </ul>
{{--                    @if(sizeof($section["posts"])>20)--}}

{{--                        @foreach ($tasks as $task)--}}
{{--                            <!-- echo the task or whatever -->--}}
{{--                        @endforeach--}}

{{--                        {{ $tasks->links() }}--}}

{{--                        <!-- start pagination -->--}}

{{--                        <div class="col-12 d-flex justify-content-center margin-7-half-rem-top lg-margin-5-rem-top wow animate__fadeIn">--}}
{{--                            <ul class="pagination pagination-style-01 text-small font-weight-500 align-items-center">--}}
{{--                                <li class="page-item"><a class="page-link" href="#"><i--}}
{{--                                                class="feather icon-feather-arrow-left icon-extra-small d-xs-none"></i></a>--}}
{{--                                </li>--}}
{{--                                <li class="page-item  active"><a class="page-link" href="#">01</a></li>--}}
{{--                                <li class="page-item"><a class="page-link" href="#">02</a></li>--}}
{{--                                <li class="page-item"><a class="page-link" href="#">03</a></li>--}}
{{--                                <li class="page-item"><a class="page-link" href="#">04</a></li>--}}
{{--                                <li class="page-item"><a class="page-link" href="#"><i--}}
{{--                                                class="feather icon-feather-arrow-right icon-extra-small  d-xs-none"></i></a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}

{{--                        <!-- end pagination -->--}}

{{--                    @endif--}}
                </div>
            </div>
        </div>
    </section>



    <!-- end section -->

@else

    <!-- start section -->

    <section class="bg-white pt-0 xl-no-padding-lr">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 blog-content padding-nine-lr xs-no-padding-lr">
                    <p class="text-center">Opss...there are no posts.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- end section -->

@endif