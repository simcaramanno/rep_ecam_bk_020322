@if(!empty($event["event"]["report1_link"]) && !empty($event["event"]["report1_name"]))

    <!-- start section -->

    <section class="wow animate__fadeIn bg-project-grey">

        <div class="container">

            <div class="row">

                <div class="col-12 tab-style-01 wow animate__fadeIn" data-wow-delay="0.4s">

                    <div class="fade in active show">

                        <div class="row align-items-center">

                            <div class="col-12 col-lg-5 offset-lg-1 col-md-6">

                                <p class="alt-font text-uppercase letter-spacing-3px text-project-yellow">

                                    {!!html_entity_decode($event["event"]["title_report"])!!}

                                </p>

                                @if(!empty($event["event"]["subtitle_report"]))

                                    <h4 class="alt-font font-weight-600 text-extra-dark-gray mb-4 ">

                                        {!!html_entity_decode($event["event"]["subtitle_report"])!!}

                                    </h4>

                                @endif

                                <p class="w-85 lg-w-100">

                                    {!!html_entity_decode($event["event"]["content_report"])!!}

                                </p>

                            </div>

                            <div class="col-12 col-lg-5 offset-lg-1 col-md-6">

                                <ul class="list-style-03">

                                    @if(!empty($event["event"]["report1_link"]) && !empty($event["event"]["report1_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report1_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report1_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                    @if(!empty($event["event"]["report2_link"]) && !empty($event["event"]["report2_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report2_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report2_name"])!!}

                                            </a>

                                        </li>

                                    @endif



                                    @if(!empty($event["event"]["report3_link"]) && !empty($event["event"]["report3_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report3_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report3_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                    @if(!empty($event["event"]["report4_link"]) && !empty($event["event"]["report4_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report4_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report4_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                    @if(!empty($event["event"]["report5_link"]) && !empty($event["event"]["report5_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report5_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report5_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                    @if(!empty($event["event"]["report6_link"]) && !empty($event["event"]["report6_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report6_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report6_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                    @if(!empty($event["event"]["report7_link"]) && !empty($event["event"]["report7_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report7_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report7_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                    @if(!empty($event["event"]["report8_link"]) && !empty($event["event"]["report8_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report8_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report8_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                    @if(!empty($event["event"]["report9_link"]) && !empty($event["event"]["report9_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report9_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report9_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                    @if(!empty($event["event"]["report10_link"]) && !empty($event["event"]["report10_name"]))

                                        <li>

                                            <a href="{{asset('images/events/reports/'.$event["event"]["report10_link"])}}" target="_blank"

                                               class="text-project-yellow-hover text-extra-dark-gray font-weight-600"

                                               download="">

                                                {!!html_entity_decode($event["event"]["report10_name"])!!}

                                            </a>

                                        </li>

                                    @endif

                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </section>



    <!-- end section -->



@endif



