<!-- start blog content section -->
<section class="blog-right-side-bar">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-8 right-sidebar md-margin-60px-bottom sm-margin-40px-bottom">
                <div class="row">
                    <div class="col-12 blog-details-text last-paragraph-no-margin margin-6-rem-bottom">
                        <ul class="list-unstyled margin-2-rem-bottom">
                            <li class="d-inline-block align-middle margin-25px-right">
                                <i class="feather icon-feather-calendar text-project-yellow margin-10px-right"></i>
                                {{date("d M Y", strtotime($post["post"]["published_at"]))}}
                            </li>
                            <li class="d-inline-block align-middle margin-25px-right">
                                <i class="feather icon-feather-bookmark  text-project-yellow margin-10px-right"></i>
                                {{$post["category_name"]}}
                            </li>
                        </ul>
                        <h5 class="alt-font font-weight-600 text-extra-dark-gray margin-4-half-rem-bottom @if($post["post"]["is_arabic"]==1) text-right @endif">
                            {!!html_entity_decode($post["post_text"]["title"])!!}
                        </h5>
                        @if(!empty($post["gallery"]) && sizeof($post["gallery"])>0)
                            @if(sizeof($post["gallery"])==1)
                                {{--                                <img src="https://via.placeholder.com/780x500" alt="" class="w-100 border-radius-6px margin-4-half-rem-bottom">--}}
                                <img src="{{asset('images/press/'.$post["gallery"][0]["filename_big"])}}"
                                     alt="{{$post["post_text"]["title"]}}"
                                     class="w-100 border-radius-6px margin-4-half-rem-bottom">
                            @else
                                <!-- start slider -->
                                <div class="swiper-container white-move h-auto margin-4-half-rem-bottom  border-radius-6px"
                                     data-slider-options='{ "slidesPerView": 1, "loop": false, "pagination": { "el": ".swiper-pagination", "clickable": true }, "autoplay": { "delay": 3000000, "disableOnInteraction": false }, "navigation": { "nextEl": ".swiper-button-next-nav", "prevEl": ".swiper-button-previous-nav" }, "keyboard": { "enabled": true, "onlyInViewport": true }, "effect": "slide" }'>
                                    <div class="swiper-wrapper">
                                        @foreach($post["gallery"] as $image)
                                            <div class="swiper-slide">
                                                <img src="{{asset('images/press/'.$image["filename_big"])}}"
                                                     alt="{{$post["post_text"]["title"]}}-{{$image["id"]}}">
                                            </div>
                                        @endforeach
                                    </div>
                                    <!-- start slider pagination -->
                                    <!--<div class="swiper-pagination swiper-light-pagination"></div>-->
                                    <!-- end slider pagination -->
                                    <div class="swiper-button-next-nav swiper-button-next light slider-navigation-style-01">
                                        <i class="feather icon-feather-arrow-right" aria-hidden="true"></i></div>
                                    <div class="swiper-button-previous-nav swiper-button-prev light slider-navigation-style-01">
                                        <i class="feather icon-feather-arrow-left" aria-hidden="true"></i></div>
                                </div>
                                <!-- end slider -->
                            @endif
                        @endif
                        <!-- content -->
                        <p>{!!html_entity_decode($post["post_text"]["content"])!!}</p>
                    </div>
                    <div class="col-12 col-lg-12">
                        <hr>
                    </div>
                    <div class="col-12 d-flex flex-wrap align-items-center padding-20px-tb mx-auto wow animate__fadeIn">
                        <div class="col-12 col-md-6 text-center text-md-left px-0">
                            @if(!empty($post["post"]["external_link"]))
                                <a href="{{$post["post"]["external_link"]}}" target="_blank"
                                   class="btn btn-large btn-transparent-brown text-uppercase letter-spacing-3px btn-round-edge-small">
                                    <i class="feather icon-feather-link left-icon text-project-yellow"></i> {!!html_entity_decode($section["sectionText"]["btn1_text"])!!}
                                </a>
                            @endif
                        </div>
                        <div class="col-12 col-md-6 text-center text-md-right px-0">
                            <a href="{{url($layout["lang"].'/press/index')}}"
                               class="btn btn-large btn-transparent-brown text-uppercase letter-spacing-3px btn-round-edge-small">
                                {!!html_entity_decode($section["sectionText"]["btn2_text"])!!}
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-12">
                        <hr>
                    </div>
                    @if(!empty($post["tags"]) && sizeof($post["tags"])>0)
                        <div class="col-12 d-flex flex-wrap align-items-center padding-15px-tb mx-auto margin-20px-bottom wow animate__fadeIn">
                            <div class="col-12 col-md-12 text-center text-md-left sm-margin-10px-bottom px-0">
                                <div class="tag-cloud">
                                    @foreach($post["tags"] as $tag)
                                        <a href="{{url($layout["lang"].'/press/search?tag='.$tag["slug"])}}">
                                            {{$tag["name"]}}
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    @endif
                </div>
            </div>
            <!-- start sidebar -->
            <aside class="col-12 col-xl-3 offset-xl-1 col-lg-4 col-md-7 blog-sidebar lg-padding-4-rem-left md-padding-15px-left">
                <div class="d-inline-block w-100 margin-5-rem-bottom">
                    <span class="alt-font font-weight-500 text-large text-extra-dark-gray d-block margin-25px-bottom">
                        {!!html_entity_decode($section["sectionText"]["item1_title"])!!}
                    </span>
                    <form role="search" method="GET" action="{{url($layout["lang"].'/press/search')}}">
                        <div class="position-relative">
                            <input class="search-input medium-input border-color-medium-gray border-radius-4px mb-0"
                                   placeholder="{!!html_entity_decode($section["sectionText"]["field1"])!!}"
                                   name="s" value="" type="text" autocomplete="off" required/>
                            <button type="submit" name="btnSearch"
                                    class="bg-transparent btn text-project-yellow position-absolute right-5px top-8px text-medium md-top-8px sm-top-10px search-button">
                                <i class="feather icon-feather-search ml-0"></i>
                            </button>
                        </div>
                    </form>
                </div>
                @if(!empty($section["recent_posts"]) && sizeof($section["recent_posts"])>0)
                    <div class="margin-5-rem-bottom xs-margin-35px-bottom wow animate__fadeIn">
                        <span class="alt-font font-weight-500 text-large text-extra-dark-gray d-block margin-35px-bottom">
                            {!!html_entity_decode($section["sectionText"]["item2_title"])!!}
                        </span>
                        <ul class="latest-post-sidebar position-relative">
                            @foreach($section["recent_posts"] as $item)
                                <li class="media wow animate__fadeIn" data-wow-delay="0.2s">
                                    <figure>
                                        <a href="{{url($layout["lang"].'/press/show/'.$item["post"]["id"].'/'.$item["post_text"]["slug"])}}">
                                            <img class="border-radius-3px"
                                                 src="{{asset('images/press/'.$item["gallery"][0]["filename_big"])}}"
                                                 alt="{{$item["post_text"]["title"]}}">
                                            {{--                                            <img class="border-radius-3px" src="https://via.placeholder.com/940x940" alt="">--}}
                                        </a>
                                    </figure>
                                    <div class="media-body">
                                        <a href="{{url($layout["lang"].'/press/show/'.$item["post"]["id"].'/'.$item["post_text"]["slug"])}}"
                                           class="font-weight-500 text-extra-dark-gray text-project-yellow-hover d-inline-block margin-five-bottom md-margin-two-bottom">
                                            @php
                                                $content = strip_tags($item["post_text"]["title"]);
                                                if(strlen($content)<=60){
                                                    $html_content = $content;
                                                }else{
                                                    $html_content = substr($content,0,60).'...';
                                                }
                                            @endphp
                                            {!!html_entity_decode($html_content)!!}
                                        </a>
                                        <span class="text-medium d-block line-height-22px">
                                            @php
                                                $content = strip_tags($item["post_text"]["content"]);
                                                if(strlen($content)<=45){
                                                    $html_content = $content;
                                                }else{
                                                    $html_content = substr($content,0,45).'...';
                                                }
                                            @endphp
                                            {!!html_entity_decode($html_content)!!}
                                        </span>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(!empty($section["tags"]) && sizeof($section["tags"])>0)
                    <div class="margin-5-rem-bottom xs-margin-35px-bottom md-padding-3-rem-top wow animate__fadeIn">
                        <span class="alt-font font-weight-500 text-large text-extra-dark-gray d-block margin-35px-bottom">
                            {!!html_entity_decode($section["sectionText"]["item3_title"])!!}
                        </span>
                        <div class="tag-cloud">
                            @foreach($section["tags"] as $tag)
                                <a href="{{url($layout["lang"].'/press/search?tag='.$tag["slug"])}}">
                                    {{$tag["name"]}}
                                </a>
                            @endforeach
                        </div>
                    </div>
                @endif
            </aside>
            <!-- end sidebar -->
        </div>
    </div>
</section>
<!-- end blog content section -->

