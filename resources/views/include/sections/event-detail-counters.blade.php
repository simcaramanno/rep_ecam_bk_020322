@if(!empty($event["eventText"]["counter1_num"]) && !empty($event["eventText"]["counter1_txt"])
    && !empty($event["eventText"]["counter2_num"]) && !empty($event["eventText"]["counter2_txt"])
    && !empty($event["eventText"]["counter3_num"]) && !empty($event["eventText"]["counter3_txt"]) )

    <!-- start section -->
    <section class="bg-white" style="padding-top:20px;padding-bottom:20px;">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center margin-six-bottom">
                    <hr>
                </div>
            </div>
            <div class="row row-cols-1 row-cols-md-3 margin-six-bottom">
                <!-- start counter item -->
                <div class="col text-center sm-no-border-right sm-margin-30px-bottom">
                    <h3 class="text-project-yellow alt-font font-weight-500 mb-xl-0 d-inline-block align-top lg-w-100 counter w-130px"
                        style="font-size: 5rem;"
                        data-speed="2000" data-to="{{$event["eventText"]["counter1_num"]}}"></h3>
                    <div class="d-inline-block align-top text-center text-xl-left">
                    <span class="alt-font text-extra-dark-gray font-weight-500 line-height-14px d-block ">
                        {!!html_entity_decode($event["eventText"]["counter1_txt"])!!}
                    </span>
                    </div>
                </div>
                <!-- end counter item -->
                <!-- start counter item -->
                <div class="col text-center sm-no-border-right sm-margin-30px-bottom">
                    <h3 class="text-project-yellow alt-font font-weight-500 mb-xl-0 d-inline-block align-top lg-w-100 counter w-130px"
                        style="font-size: 5rem;"
                        data-speed="2000" data-to="{{$event["eventText"]["counter2_num"]}}"></h3>
                    <div class="d-inline-block align-top text-center text-xl-left">
                    <span class="alt-font text-extra-dark-gray font-weight-500 line-height-14px d-block ">
                        {!!html_entity_decode($event["eventText"]["counter2_txt"])!!}
                    </span>
                    </div>
                </div>
                <!-- end counter item -->
                <!-- start counter item -->
                <div class="col text-center">
                    <h3 class="text-project-yellow alt-font font-weight-500 mb-xl-0 d-inline-block align-top lg-w-100 counter w-130px"
                        style="font-size: 5rem;"
                        data-speed="2000" data-to="{{$event["eventText"]["counter3_num"]}}"></h3>
                    <div class="d-inline-block align-top text-center text-xl-left">
                    <span class="alt-font text-extra-dark-gray font-weight-500 line-height-14px d-block ">
                        {!!html_entity_decode($event["eventText"]["counter3_txt"])!!}
                    </span>
                    </div>
                </div>
                <!-- end counter item -->
            </div>
            <div class="row justify-content-center">
                <div class="col-md-12 text-center margin-six-bottom">
                    <hr>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
@endif


