<!-- start footer -->

<footer class="bg-extra-dark-gray">

    <div class="footer-top padding-six-top lg-padding-eight-tb md-padding-50px-tb" style="margin-bottom:50px;">

        <div class="container">

            <div class="row">

                <div class="col-12 col-xl-3 col-sm-6 order-sm-1 order-xl-0 lg-margin-50px-bottom xs-margin-25px-bottom">

                    <a href="{{url($layout["lang"].'/home/')}}" class="footer-logo margin-20px-bottom xs-margin-10px-bottom d-block">

                        <img src="{{asset('images/logo/'.$platform->logo_light)}}" data-at2x="{{asset('images/logo/'.$platform->logo_light_2x)}}"

                             alt="{{$platform->site_name}}" class="max-h-inherit" style="height: 60px;">

                    </a>

                    <span class="text-extra-medium line-height-24px letter-spacing-minus-1-half d-inline-block w-85 text-white line-height-30px" style="font-family: 'Poppins';line-height: 24px;">

                        {!!html_entity_decode($layout["footer"]["title"])!!}

                    </span>

                </div>

                <div class="col-12 col-xl-3 col-sm-6 order-sm-3 order-xl-0 xs-margin-25px-bottom">

                    <span class="alt-font font-weight-500 d-block text-white margin-20px-bottom xs-margin-20px-bottom margin-25px-top">

                         {!!html_entity_decode($layout["footer"]["item1_title"])!!}

                    </span>

                    <ul>

                        <li class="text-project-grey no-margin-bottom">{{$platform->company_address}}</li>

                        <li class="text-project-grey no-margin-bottom">{{$platform->company_zip}} {{$platform->company_city}}</li>

                    </ul>

                </div>

                <div class="col-12 col-xl-3 col-sm-6 order-sm-4 order-xl-0 xs-margin-25px-bottom">

                    <span class="alt-font font-weight-500 d-block text-white margin-20px-bottom xs-margin-20px-bottom margin-25px-top">

                        {!!html_entity_decode($layout["footer"]["item2_title"])!!}

                    </span>

                    <ul>

                        <li class=" no-margin-bottom">

                            <a class="text-project-grey text-white-hover" href="mailto:{{$platform->email_to}}" target="_blank">

                                {{$platform->email_to}}

                            </a>

                        </li>

                        @if(!empty($platform->phone))

                            <li class=" no-margin-bottom">

                                <a class="text-project-grey text-white-hover" href="tel: {{trim($platform->phone)}}">{{trim($platform->phone)}}</a>

                            </li>

                        @endif

                    </ul>

                </div>



                @if(!empty($platform->url_facebook) || !empty($platform->url_instagram)  || !empty($platform->url_twitter) )

                    <div class="col-12 col-xl-3 col-sm-6 order-sm-2 order-xl-0 lg-margin-50px-bottom xs-no-margin-bottom">

                        <span class="alt-font font-weight-500 d-block text-white margin-20px-bottom xs-margin-25px-bottom margin-25px-top">

                            {!!html_entity_decode($layout["footer"]["item3_title"])!!}

                        </span>

                        <div class="social-icon-style-12">

                            <ul class="extra-small-icon light">

                                @if(!empty($platform->url_facebook))

                                    <li><a class="facebook" href="{{$platform->url_facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>

                                @endif

                                @if(!empty($platform->url_instagram))

                                    <li><a class="instagram" href="{{$platform->url_instagram}}" target="_blank"><i class="fab fa-instagram"></i></a></li>

                                @endif

                                @if(!empty($platform->url_twitter))

                                    <li><a class="twitter" href="{{$platform->url_twitter}}" target="_blank"><i class="fab fa-twitter"></i></a></li>

                                @endif

                            </ul>

                        </div>

                    </div>

                @endif

            </div>

        </div>

    </div>

    <div class="footer-bottom padding-two-bottom padding-two-top xs-no-padding-top border-top border-width-1px border-color-medium-dark-gray">

        <div class="container">

            <div class="row">

                <div class="col-12 col-md-6 order-1 order-md-0 sm-margin-15px-bottom">

                    <ul class="footer-horizontal-link d-flex flex-column flex-sm-row">

                        <li>

                            <a href="{{url($layout["lang"].'/privacy/index')}}" class="text-white-hover">{!!html_entity_decode($layout["footer"]["field1"])!!}</a>

                        </li>

                        <li><a href="{{url($layout["lang"].'/cookies/index')}}" class="text-white-hover">{!!html_entity_decode($layout["footer"]["field2"])!!}</a></li>

                    </ul>

                </div>

                <div class="col-12 col-md-6 order-2 order-md-0 text-sm-right last-paragraph-no-margin">

                    <p>{{$platform->company_name}} {{date('Y')}}. {!!html_entity_decode($layout["footer"]["content1"])!!}</p>

                </div>

            </div>

        </div>

    </div>

</footer>



<!-- end footer -->

