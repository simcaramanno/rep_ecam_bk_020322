<div class="js-cookie-consent cookie-consent "
     style="display: flex;flex-direction: column;align-items: center;
     justify-content: space-around; padding-top: 30px; padding-bottom: 30px;align-content: space-between;
     left: 0;right: 0;z-index: 99999;padding: 20px 0;
     background-color: #250c0c;color: #ffffff;padding: 20px 0;padding-left: 8%;padding-right: 8%;
     overflow: hidden; position: fixed;  bottom: 0; width: 100%;">
{{--    <p class="cookie-consent__message" style="margin-bottom:0px;text-align:center; padding-bottom:10px;">--}}
{{--        This site uses cookies to improve the browsing experience of users and to gather information on the use of the--}}
{{--        site. We use both technical cookies and third-party cookies to send promotional messages based on user behavior.--}}
{{--        You can know check the details by consulting our <a class="cookie-container-a" href="{{url($layout['lang'].'/cookies/index')}}" target="_blank">cookie policy</a> and--}}
{{--        <a class="cookie-container-a" href="{{url($layout['lang'].'/privacy/index')}}" target="_blank">privacy policy</a>.--}}
{{--    </p>--}}
{{--    <p class="cookie-button-p">--}}
{{--        <button class="js-cookie-consent-agree cookie-consent__agree cookie-btn">--}}
{{--            Accept all--}}
{{--        </button>--}}
{{--        <button class="cookie-btn-2 js-cookie-consent-agree cookie-consent__agree">--}}
{{--            Disagree--}}
{{--        </button>--}}
{{--    </p>--}}


    <p class="cookie-consent__message" style="margin-bottom:0px;text-align:center; padding-bottom:10px;">
        This site uses cookies to improve the browsing experience of users and to gather information on the use of the
        site. We use both technical cookies and third-party cookies to send promotional messages based on user behavior.
        You can know check the details by consulting our <a class="cookie-container-a" href="#" target="_blank">cookie policy</a> and
        <a class="cookie-container-a" href="#" target="_blank">privacy policy</a>.
    </p>
    <p class="cookie-button-p">
        <button class="js-cookie-consent-agree cookie-consent__agree cookie-btn">
            Accept all
        </button>
        <button class="cookie-btn-2 js-cookie-consent-agree cookie-consent__agree">
            Disagree
        </button>
    </p>
</div>


