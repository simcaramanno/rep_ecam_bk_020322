@extends('app')

@section('content')
{{--    @if(!empty($page_data["sections"]) && sizeof($page_data["sections"])>0)--}}
{{--        @foreach($page_data["sections"] as $section)--}}
{{--            @include('include.sections.'.$section["section"]["blade_name"])--}}
{{--        @endforeach--}}
{{--    @endif--}}

<section class="border-top border-color-medium-gray" style="padding-top:90px;padding-bottom:10px;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6 col-md-8 col-sm-9 text-center margin-4-rem-bottom md-margin-3-rem-bottom wow animate__fadeIn">
                <h4 class="alt-font font-weight-600 text-slate-blue letter-spacing-minus-1px margin-15px-bottom">
                    Search result
                </h4>
                <p class="w-75 mx-auto lg-w-95 sm-w-100">
                    
                </p>
            </div>
        </div>
    </div>
</section>


@if(!empty($posts) && sizeof($posts)>0)
    <!-- start section -->
    <section class="bg-white pt-0 xl-no-padding-lr">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 blog-content xs-no-padding-lr">
                    <ul class="blog-modern blog-wrapper grid grid-loading grid-4col xl-grid-4col lg-grid-3col md-grid-2col sm-grid-2col xs-grid-1col gutter-double-extra-large">
                        <li class="grid-sizer"></li>
                    @foreach($posts as $post)
                        <!-- start blog item -->
                            <li class="grid-item wow animate__fadeIn">
                                <div class="blog-post">
                                    <div class="blog-post-image bg-project-yellow">
                                        <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}">
                                            <img src="{{asset('images/press/'.$post["gallery"][0]["filename_small"])}}" alt="{{$post["post_text"]["title"]}}">
                                            {{--                                            <img src="https://via.placeholder.com/800x1010" alt="">--}}
                                        </a>
                                    </div>
                                    <div class="post-details bg-white text-center padding-3-rem-all xl-padding-2-rem-all">
                                        <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}"
                                           class="blog-category text-project-yellow margin-15px-bottom text-medium font-weight-500 letter-spacing-1px text-uppercase">
                                            {!!html_entity_decode($post["post_text"]["title"])!!}
                                        </a>
                                        <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}"
                                           class="alt-font text-extra-dark-gray text-extra-dark-gray-hover text-large line-height-26px d-block margin-20px-bottom">
                                            @php
                                                $content = strip_tags($post["post_text"]["content"]);
                                                if(strlen($content)<=100){
                                                    $html_content = $content;
                                                }else{
                                                    $html_content = substr($content,0,100).'...';
                                                }
                                            @endphp
                                            {!!html_entity_decode($html_content)!!}
                                        </a>
                                        <a href="{{url($layout["lang"].'/press/show/'.$post["id"].'/'.$post["post_text"]["slug"])}}"
                                           class="alt-font text-uppercase text-extra-small letter-spacing-1px d-block">
                                            {{date("d M Y", strtotime($post["post"]["published_at"]))}}
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <!-- end blog item -->
                        @endforeach
                    </ul>
                @if(sizeof($posts)>12)
                    <!-- start pagination -->
                        <div class="col-12 d-flex justify-content-center margin-7-half-rem-top lg-margin-5-rem-top wow animate__fadeIn">
                            <ul class="pagination pagination-style-01 text-small font-weight-500 align-items-center">
                                <li class="page-item"><a class="page-link" href="#"><i class="feather icon-feather-arrow-left icon-extra-small d-xs-none"></i></a></li>
                                <li class="page-item"><a class="page-link" href="#">01</a></li>
                                <li class="page-item active"><a class="page-link" href="#">02</a></li>
                                <li class="page-item"><a class="page-link" href="#">03</a></li>
                                <li class="page-item"><a class="page-link" href="#">04</a></li>
                                <li class="page-item"><a class="page-link" href="#"><i class="feather icon-feather-arrow-right icon-extra-small  d-xs-none"></i></a></li>
                            </ul>
                        </div>
                        <!-- end pagination -->
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
@else
    <!-- start section -->
    <section class="bg-white pt-0 xl-no-padding-lr">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 blog-content xs-no-padding-lr">
                    <p class="text-center">Opss...there are no articles.</p>
                </div>
            </div>
        </div>
    </section>
    <!-- end section -->
@endif

@endsection
