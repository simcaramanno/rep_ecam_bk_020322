<!DOCTYPE html>
<html lang="en">
    <!-- START: Head-->
    <head>
        <meta charset="utf-8">
        <meta name="lang" content="it">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>{{ $platform->be_site_name}} | Admin - {{$layout["page_title"]}}</title>
        <meta name="description" content="{{ $platform->be_meta_desc}}">
        <meta name="keywords" content="{{ $platform->be_meta_keys}}">
        <meta name="author" content="{{$platform->site_name}}">

{{--        <meta http-equiv="refresh" content="{{ config('session.lifetime') * 60 }}">--}}

        <!-- FAVICON -->
        <link rel="apple-touch-icon" href="{{ asset('images/logo/'.$platform->favicon) }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/logo/'.$platform->favicon) }}">
        @if(!empty($platform->favicon114))
            <!-- For iPhone 4 Retina display: -->
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/logo/'.$platform->favicon114) }}">
        @endif
        @if(!empty($platform->favicon72))
            <!-- For iPad: -->
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/logo/'.$platform->favicon72) }}">
        @endif
        @if(!empty($platform->favicon57))
            <!-- For iPhone: -->
            <link rel="apple-touch-icon-precomposed" href="{{ asset('images/logo/'.$platform->favicon57) }}">
        @endif

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/jquery-ui/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/jquery-ui/jquery-ui.theme.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/simple-line-icons/css/simple-line-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/flags-icon/css/flag-icon.min.css') }}">
        <!-- END Template CSS-->

        <!-- START: Page CSS-->
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/social-button/bootstrap-social.css') }}"/>
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="{{ asset('theme-be/dist/css/main.css') }}">
        <!-- END: Custom CSS-->

        <!-- START: Page CSS-->
        @yield('css')
        <!-- END: Page CSS-->

        @yield('javascript_head')

    </head>
    <!-- END Head-->

    <!-- START: Body-->
    <body id="main-container" class="default">
        <div class="container">
            <div class="row vh-100 justify-content-between align-items-center">
                <div class="col-12">

                    <!-- page content -->
                    @yield('content')

                </div>
            </div>
        </div>
        <!-- END: Content-->

        <!-- START: Template JS-->
        <script src="{{ asset('theme-be/dist/vendors/jquery/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('theme-be/dist/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('theme-be/dist/vendors/moment/moment.js') }}"></script>
        <script src="{{ asset('theme-be/dist/vendors/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('theme-be/dist/vendors/slimscroll/jquery.slimscroll.min.js') }}"></script>

        <!-- END: Template JS-->
    </body>
    <!-- END: Body-->
</html>

