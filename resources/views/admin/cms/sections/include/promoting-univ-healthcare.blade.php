<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titolo</label>
                <textarea name="title" class="form-control"
                          placeholder="Titolo" rows="3"
                          required>{{!empty(old('title')) ? old('title') :$data_texts->title}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Sottotitolo* </label>
                <textarea name="subtitle" class="form-control"
                          placeholder="Sottotitolo" rows="3"
                          required>{{!empty(old('subtitle')) ? old('subtitle') :$data_texts->subtitle}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <hr>
            <div class="form-group">
                <label>Box 1: Titolo* </label>
                <textarea name="item1_title" class="form-control"
                          placeholder="Titolo" rows="2" required
                          required>{{!empty(old('item1_title')) ? old('item1_title') :$data_texts->item1_title}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Box 1: Testo* </label>
                <textarea name="item1_subtitle" class="form-control"
                          placeholder="Testo" rows="3" required
                          required>{{!empty(old('item1_subtitle')) ? old('item1_subtitle') :$data_texts->item1_subtitle}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <hr>
            <div class="form-group">
                <label>Box 2: Titolo* </label>
                <textarea name="item2_title" class="form-control"
                          placeholder="Titolo" rows="2" required
                          required>{{!empty(old('item2_title')) ? old('item2_title') :$data_texts->item2_title}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Box 2: Testo* </label>
                <textarea name="item2_subtitle" class="form-control"
                          placeholder="Testo" rows="3" required
                          required>{{!empty(old('item2_subtitle')) ? old('item2_subtitle') :$data_texts->item2_subtitle}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <hr>
            <div class="form-group">
                <label>Box 3: Titolo* </label>
                <textarea name="item3_title" class="form-control"
                          placeholder="Titolo" rows="2" required
                          required>{{!empty(old('item3_title')) ? old('item3_title') :$data_texts->item3_title}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Box 3: Testo* </label>
                <textarea name="item3_subtitle" class="form-control"
                          placeholder="Testo" rows="3" required
                          required>{{!empty(old('item3_subtitle')) ? old('item3_subtitle') :$data_texts->item3_subtitle}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>

