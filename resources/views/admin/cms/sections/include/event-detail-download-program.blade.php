<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo* </label>
                <input type="text" name="title" class="form-control"
                       placeholder="Testo" required maxlength="500"
                       value="{{!empty(old('title')) ? old('title') : $data_texts->title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo pulsante*</label>
                <input type="text" name="btn1_text" class="form-control"
                       placeholder="Testo pulsante" required maxlength="50"
                       value="{{!empty(old('btn1_text')) ? old('btn1_text') : $data_texts->btn1_text}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>
