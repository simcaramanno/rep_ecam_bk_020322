<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo data* </label>
                <input type="text" name="item1_title" class="form-control"
                       placeholder="Titolo" required maxlength="100"
                       value="{{!empty(old('item1_title')) ? old('item1_title') : $data_texts->item1_title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo tipo progetto* </label>
                <input type="text" name="item2_title" class="form-control"
                       placeholder="Titolo" required maxlength="100"
                       value="{{!empty(old('item2_title')) ? old('item2_title') : $data_texts->item2_title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo location* </label>
                <input type="text" name="item3_title" class="form-control"
                       placeholder="Titolo" required maxlength="100"
                       value="{{!empty(old('item3_title')) ? old('item3_title') : $data_texts->item3_title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titolo sopra alla descrizione* </label>
                <input type="text" name="title" class="form-control"
                       placeholder="Titolo" required maxlength="100"
                       value="{{!empty(old('title')) ? old('title') : $data_texts->title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>
