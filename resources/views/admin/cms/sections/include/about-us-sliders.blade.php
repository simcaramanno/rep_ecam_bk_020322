<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titolo* </label>
                <input type="text" name="title" class="form-control"
                       placeholder="Titolo" required maxlength="500"
                       value="{{!empty(old('title')) ? old('title') : $data_texts->title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Sottotitolo* </label>
                <textarea name="subtitle" class="form-control"
                          placeholder="Sottotitolo" rows="4" required
                          required>{{!empty(old('subtitle')) ? old('subtitle') :$data_texts->subtitle}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo* </label>
                <textarea name="content1" class="form-control"
                          placeholder="Testo" rows="4" required
                          required>{{!empty(old('content1')) ? old('content1') :$data_texts->content1}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-md-12">
        <hr>
        <h6>IMMAGINI SLIDERS</h6>
    </div>
</div>

<!--1-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 1</label>
            @if(empty($data_texts->image1))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image1)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image1))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="1">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="1">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="1">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo</label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item1_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--2-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 2</label>
            @if(empty($data_texts->image2))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image2)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image2))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="2">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="2">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="2">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item2_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--3-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 3</label>
            @if(empty($data_texts->image3))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image3)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image3))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="3">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="3">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="3">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item3_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--4-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 4</label>
            @if(empty($data_texts->image4))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image4)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image4))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="4">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="4">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="4">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item4_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--5-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 5</label>
            @if(empty($data_texts->image5))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image5)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image5))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="5">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="5">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="5">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item5_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--6-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 6</label>
            @if(empty($data_texts->image6))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image6)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image6))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="6">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="6">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="6">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item6_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--7-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 7</label>
            @if(empty($data_texts->image7))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image7)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image7))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="7">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="7">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="7">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item7_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--8-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 8</label>
            @if(empty($data_texts->image8))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image8)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image8))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="8">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="8">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="8">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item8_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--9-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 9</label>
            @if(empty($data_texts->image9))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image9)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image9))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="9">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="9">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="9">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item9_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

<!--10-->
<div class="row">
    <div class="col-md-12"><hr></div>
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>Slider 10</label>
            @if(empty($data_texts->image10))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image10)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        @if(!empty($data_texts->image10))
            <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data" class="mb-1">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_image">
                <input type="hidden" name="num" value="10">
                <button type="submit" class="btn btn-danger text-uppercase btn-sm">
                    elimina
                </button>
            </form>
        @endif

        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_image">
            <input type="hidden" name="num" value="10">
            <div class="row">
                <div class="col-md-9">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["18_image_slider"]["w"].'x'.$GV->image_size_sections["18_image_slider"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Upload
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="text">
            <input type="hidden" name="num" value="10">
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group">
                        <label>Testo* </label>
                        <input type="text" name="item_title" class="form-control"
                               placeholder="Testo" required maxlength="500"
                               value="{{!empty(old('item_title')) ? old('item_title') : $data_texts->item10_title}}"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
