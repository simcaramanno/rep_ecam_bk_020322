<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titolo* </label>
                <input type="text" name="title" class="form-control"
                       placeholder="Titolo" required maxlength="500"
                       value="{{!empty(old('title')) ? old('title') : $data_texts->title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo* </label>
                <textarea name="subtitle" class="form-control"
                          placeholder="Testo" rows="4" required
                          required>{{!empty(old('subtitle')) ? old('subtitle') :$data_texts->subtitle}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <hr>
            <h6>FORM CONTATTO</h6>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Nome* </label>
                <input type="text" name="field1" class="form-control"
                       placeholder="Testo" required maxlength="50"
                       value="{{!empty(old('field1')) ? old('field1') : $data_texts->field1}}"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Email* </label>
                <input type="text" name="field2" class="form-control"
                       placeholder="Testo" required maxlength="50"
                       value="{{!empty(old('field2')) ? old('field2') : $data_texts->field2}}"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Telefono* </label>
                <input type="text" name="field3" class="form-control"
                       placeholder="Testo" required maxlength="50"
                       value="{{!empty(old('field3')) ? old('field3') : $data_texts->field3}}"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Messaggio* </label>
                <input type="text" name="field4" class="form-control"
                       placeholder="Testo" required maxlength="50"
                       value="{{!empty(old('field4')) ? old('field4') : $data_texts->field4}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo accetazione privacy* </label>
                <textarea name="content1" class="form-control"
                          placeholder="Testo" rows="2" required
                          required>{{!empty(old('content1')) ? old('content1') :$data_texts->content1}}</textarea>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Testo pulsante* </label>
                <input type="text" name="btn1_text" class="form-control"
                       placeholder="Testo" required maxlength="50"
                       value="{{!empty(old('btn1_text')) ? old('btn1_text') : $data_texts->btn1_text}}"/>
            </div>
        </div>

        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>


