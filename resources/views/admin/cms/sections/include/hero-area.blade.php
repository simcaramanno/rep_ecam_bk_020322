<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titoletto* </label>
                <input type="text" name="small_title" class="form-control"
                       placeholder="Titoletto" required maxlength="100"
                       value="{{!empty(old('small_title')) ? old('small_title') : $data_texts->small_title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titolo* </label>
                <input type="text" name="title" class="form-control"
                       placeholder="Titolo" required maxlength="100"
                       value="{{!empty(old('title')) ? old('title') : $data_texts->title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Sottotitolo* </label>
                <input type="text" name="subtitle" class="form-control"
                       placeholder="Titolo" required maxlength="200"
                       value="{{!empty(old('subtitle')) ? old('subtitle') : $data_texts->subtitle}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo pulsante*</label>
                <input type="text" name="btn1_text" class="form-control"
                       placeholder="Testo pulsante" required maxlength="50"
                       value="{{!empty(old('btn1_text')) ? old('btn1_text') : $data_texts->btn1_text}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Link pulsante*</label>
                <input type="url" name="btn1_link" class="form-control"
                       placeholder="https://www.myurl.com" required
                       value="{{!empty(old('btn1_link')) ? old('btn1_link') : $data_texts->btn1_link}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>

<!--image1-->
<!--image1_alt-->
<div class="row">
    <div class="col-md-12">
        <h6>IMMAGINE SLIDER</h6>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>
                Immagine slider*
            </label>
            @if(empty($data_texts->image1))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image1)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="image1">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["1_image1"]["w"].'x'.$GV->image_size_sections["1_image1"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary text-uppercase float-right">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <hr>
        <h6>IMMAGINI AGGIUNTIVE SLIDER</h6>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="add_slider_img">
            <div class="row">
                <div class="col-md-12">
                    <label>
                        Aggiungi immagine
                    </label>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>Titoletto</label>
                        <input type="text" name="slider_small_title" class="form-control"
                               placeholder="Titoletto" maxlength="100"
                               value="{{!empty(old('slider_small_title')) ? old('slider_small_title') : ""}}"/>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>Titolo* </label>
                        <input type="text" name="slider_title" class="form-control"
                               placeholder="Titolo" required maxlength="100"
                               value="{{!empty(old('slider_title')) ? old('slider_title') : ""}}"/>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>Sottotitolo</label>
                        <input type="text" name="slider_subtitle" class="form-control"
                               placeholder="Titolo" maxlength="200"
                               value="{{!empty(old('slider_subtitle')) ? old('slider_subtitle') : ""}}"/>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>Testo pulsante</label>
                        <input type="text" name="slider_btn_text" class="form-control"
                               placeholder="Testo pulsante" maxlength="50"
                               value="{{!empty(old('slider_btn_text')) ? old('slider_btn_text') : ""}}"/>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>Link pulsante</label>
                        <input type="url" name="slider_btn_link" class="form-control"
                               placeholder="https://www.myurl.com"
                               value="{{!empty(old('slider_btn_link')) ? old('slider_btn_link') : ""}}"/>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>Alt text</label>
                        <input type="text" name="slider_image_alt" class="form-control"
                               placeholder="Alt text" maxlength="100"
                               value="{{!empty(old('slider_image_alt')) ? old('slider_image_alt') : ""}}"/>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <div class="form-group">
                        <label>
                            Seleziona file ({{$GV->image_size_sections["1_image1"]["w"].'x'.$GV->image_size_sections["1_image1"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12 text-left">
                    <div class="form-group" style="margin-top:25px;">
                        <button type="submit" class="btn btn-primary text-uppercase">
                            Aggiungi
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@if(isset($home_sliders))
    <div class="row">
        @if(!empty($home_sliders) && sizeof($home_sliders)>0)
            <div class="col-md-12 mb-1">
                <small class="text-info font-weight-bold">
                    <i class="fa fa-info-circle"></i> Clicca sull'immagine per modificarla
                </small>
            </div>
            <div class="col-md-12">
                <form method="POST" action="{{url('admin/cms/sections/update-details')}}">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                    <input type="hidden" name="lang" value="{{$lang}}">
                    <input type="hidden" name="section" value="update_slider_sort">

                    <div class="row">
                        <div class="col-md-12 text-right">
                            <hr>
                            <button type="submit"
                                    class="btn btn-sm btn-primary text-uppercase mb-2">
                                Salva ordinamento
                            </button>
                        </div>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="tab-home-sliders" class="display table dataTable table-striped table-bordered" >
                                    <thead>
                                    <tr>
                                        <th hidden></th>
                                        <th style="max-width: 10%">Ordine</th>
                                        <th>Immagine</th>
                                        <th>Titolo</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($home_sliders as $_item)
                                        <tr id="{{$_item["id"]}}">
                                            <td hidden>{{$_item["sorting"]}}</td>
                                            <td data-order="{{$_item["sorting"]}}" style="max-width: 10%">
                                                <input type="number" class="form-control bg-white"
                                                       name="sorting-{{$_item["id"]}}" required
                                                       value="{{$_item["sorting"]}}">
                                            </td>
                                            <td>
                                                <a data-toggle="modal" data-target="#modalEditImgSlider"
                                                   data-itemid="{{$_item["id"]}}"
                                                   style="cursor: pointer;"
                                                   class="editimage-item">
                                                    <img src="{{asset('images/sections/'.$_item["image"])}}" alt="."
                                                         style="max-width:100px;">
                                                </a>
                                            </td>
                                            <td>{{$_item["title"]}}</td>
                                            <td>
                                                <a data-toggle="modal" data-target="#modalDeleteSlider"
                                                   data-itemid="{{$_item["id"]}}"
                                                   class="delete-item btn btn-sm btn-danger text-white mb-1"
                                                   style="cursor:pointer;">
                                                    <small>Elimina</small>
                                                </a>
                                                <a data-toggle="modal" data-target="#modalEditSlider"
                                                   data-itemid="{{$_item["id"]}}"
                                                   data-image_alt="{{$_item["image_alt"]}}"
                                                   data-smalltitle="{{$_item["small_title"]}}"
                                                   data-title="{{$_item["title"]}}"
                                                   data-subtitle="{{$_item["subtitle"]}}"
                                                   data-btntext="{{$_item["btn_text"]}}"
                                                   data-btnlink="{{$_item["btn_link"]}}"
                                                   class="edit-item btn btn-sm btn-warning text-white mb-1"
                                                   style="cursor:pointer;">
                                                    <small>Modifica testi</small>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th hidden></th>
                                        <th style="max-width: 10%">Ordine</th>
                                        <th>Immagine</th>
                                        <th>Titolo</th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <!--MODALS-->
            <div id="modalDeleteSlider" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <form method="POST" action="{{ url('admin/cms/sections/update-details') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                    <input type="hidden" name="lang" value="{{$lang}}">
                    <input type="hidden" name="section" value="delete_slider_img">
                    <input type="hidden" name="item_id" id="delete_item_id">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header bg-danger">
                                <h5 class="modal-title text-white">Elimina</h5>
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Sei sicuro di voler rimuovere questo elemento ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-danger">Conferma</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="modalEditSlider" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <form method="POST" action="{{ url('admin/cms/sections/update-details') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                    <input type="hidden" name="lang" value="{{$lang}}">
                    <input type="hidden" name="section" value="edit_slider">
                    <input type="hidden" name="item_id" id="edit_item_id">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header bg-warning">
                                <h5 class="modal-title text-white">Modifica</h5>
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Titoletto</label>
                                            <input type="text" name="slider_small_title" id="mod_smalltitle" class="form-control"
                                                   placeholder="Titoletto" maxlength="100"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Titolo* </label>
                                            <input type="text" name="slider_title" id="mod_title" class="form-control"
                                                   placeholder="Titolo" required maxlength="100"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Sottotitolo</label>
                                            <input type="text" name="slider_subtitle" id="mod_subtitle" class="form-control"
                                                   placeholder="Titolo" maxlength="200"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Testo pulsante</label>
                                            <input type="text" name="slider_btn_text" id="mod_btntext" class="form-control"
                                                   placeholder="Testo pulsante" maxlength="50"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Link pulsante</label>
                                            <input type="url" name="slider_btn_link" id="mod_btnlink" class="form-control"
                                                   placeholder="https://www.myurl.com"/>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Alt text</label>
                                            <input type="text" name="slider_image_alt" id="mod_image_alt" class="form-control"
                                                   placeholder="Alt text" maxlength="100"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-warning text-white">Conferma</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="modalEditImgSlider" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <form method="POST" action="{{ url('admin/cms/sections/update-details') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="text_id" value="{{$data_texts->id}}">
                    <input type="hidden" name="lang" value="{{$lang}}">
                    <input type="hidden" name="section" value="edit_image_slider">
                    <input type="hidden" name="item_id" id="editimage_item_id">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <h5 class="modal-title text-white">Modifica</h5>
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Nuova immagine*</label>
                                            <input type="file" name="image" class="form-control" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-primary">Conferma</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        @else
            <div class="col-md-12 text-center">
                <p class="alert alert-warning">Non ci sono immagini aggiuntive</p>
            </div>
        @endif
    </div>


@endif

