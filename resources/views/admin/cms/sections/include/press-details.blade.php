<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Testo pulsante link articolo*</label>
                <input type="text" name="btn1_text" class="form-control"
                       placeholder="Testo" required maxlength="50"
                       value="{{!empty(old('btn1_text')) ? old('btn1_text') : $data_texts->btn1_text}}"/>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label>Testo pulsante indietro*</label>
                <input type="text" name="btn2_text" class="form-control"
                       placeholder="Testo" required maxlength="50"
                       value="{{!empty(old('btn2_text')) ? old('btn2_text') : $data_texts->btn2_text}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo ricerca posts* </label>
                <input type="text" name="item1_title" class="form-control"
                       placeholder="Testo" required maxlength="100"
                       value="{{!empty(old('item1_title')) ? old('item1_title') : $data_texts->item1_title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo placeholder ricerca* </label>
                <input type="text" name="field1" class="form-control"
                       placeholder="Testo" required maxlength="100"
                       value="{{!empty(old('field1')) ? old('field1') : $data_texts->field1}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo ultimi posts* </label>
                <input type="text" name="item2_title" class="form-control"
                       placeholder="Testo" required maxlength="255"
                       value="{{!empty(old('item2_title')) ? old('item2_title') : $data_texts->item2_title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo tags* </label>
                <input type="text" name="item3_title" class="form-control"
                       placeholder="Testo" required maxlength="255"
                       value="{{!empty(old('item3_title')) ? old('item3_title') : $data_texts->item3_title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>
