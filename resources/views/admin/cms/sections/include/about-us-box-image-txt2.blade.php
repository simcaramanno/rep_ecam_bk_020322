<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titolo* </label>
                <input type="text" name="title" class="form-control"
                       placeholder="Titolo" required maxlength="500"
                       value="{{!empty(old('title')) ? old('title') : $data_texts->title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Testo* </label>
                <textarea name="subtitle" class="form-control"
                          placeholder="Testo" rows="4" required
                          required>{{!empty(old('subtitle')) ? old('subtitle') :$data_texts->subtitle}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Descrizione* </label>
                <textarea name="content1" class="form-control"
                          placeholder="Descrizione" rows="4" required
                          required>{{!empty(old('content1')) ? old('content1') :$data_texts->content1}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-md-12">
        <h6>IMMAGINE</h6>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-lg-4 col-sm-12">
        <div class="form-group">
            <label>
                Immagine background*
            </label>
            @if(empty($data_texts->image1))
                <p class="alert alert-danger">Non presente!</p>
            @else
                <div class="col-md-12">
                    <img src="{{asset('images/sections/'.$data_texts->image1)}}"
                         alt="." style="max-height:150px;">
                </div>
            @endif
        </div>
    </div>
    <div class="col-md-8 col-lg-8 col-sm-12">
        <form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="image1">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>
                            Upload immagine ({{$GV->image_size_sections["20_image_box"]["w"].'x'.$GV->image_size_sections["20_image_box"]["h"].'px'}})*
                        </label>
                        <input type="file" class="form-control" name="image" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary text-uppercase float-right">
                        Salva
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

