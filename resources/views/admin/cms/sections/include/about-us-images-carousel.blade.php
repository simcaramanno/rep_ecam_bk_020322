<div class="row">
    <div class="col-md-12">
        <h6>IMMAGINI CAROUSEL</h6>
    </div>
</div>

<form method="POST" action="{{url('admin/cms/sections/update-details')}}" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" value="{{$data->id}}">
    <input type="hidden" name="text_id" value="{{$data_texts->id}}">
    <input type="hidden" name="lang" value="{{$lang}}">
    <input type="hidden" name="section" value="add_image">
    <div class="row">
        <div class="col-md-9">
            <div class="form-group">
                <label>
                    Upload immagine ({{$GV->image_size_sections["17_image_carousel"]["w"].'x'.$GV->image_size_sections["17_image_carousel"]["h"].'px'}})*
                </label>
                <input type="file" class="form-control" name="image" required>
            </div>
        </div>
        <div class="col-md-3 text-right">
            <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                Salva
            </button>
        </div>
    </div>
</form>

<div class="row">
    <div class="col-md-12">
        <hr>
        @if(!empty($data_gallery) && sizeof($data_gallery)>0)<div class="col-md-12">
            <form method="POST" action="{{url('admin/cms/sections/sort-about-carousel')}}">
                <div class="row">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <div class="col-md-12 mb-1">
                        <small class="text-info font-weight-bold">
                            <i class="fa fa-info-circle"></i> Clicca sull'immagine per modificarla
                        </small>
                    </div>

                    <div class="col-md-12 text-right">
                        <button type="submit"
                                class="btn btn-sm btn-primary text-uppercase mb-2">
                            Salva ordinamento
                        </button>
                    </div>
                    <div class="table-responsive">
                        <table id="tab-section-gallery" class="display table dataTable table-striped table-bordered" >
                            <thead>
                            <tr>
                                <th hidden></th>
                                <th style="max-width: 10%;">Ordine</th>
                                <th>Immagine</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data_gallery as $_item)
                                <tr id="{{$_item->id}}">
                                    <td hidden>{{$_item->sorting}}</td>
                                    <td data-order="{{$_item->sorting}}" style="max-width: 10%">
                                        <input type="number" class="form-control bg-white"
                                               name="sorting-{{$_item->id}}" required
                                               value="{{$_item->sorting}}">
                                    </td>
                                    <td>
                                        <a data-toggle="modal" data-target="#modalEditImge"
                                           data-itemid="{{$_item->id}}"
                                           style="cursor: pointer;"
                                           class="editimage-item">
                                            <img src="{{asset('images/sections/'.$_item->image)}}" alt="."
                                                 style="max-width:100px;">
                                        </a>
                                    </td>
                                    <td>
                                        <a data-toggle="modal" data-target="#modalDeleteItem"
                                           data-itemid="{{$_item->id}}"
                                           class="delete-item btn btn-sm btn-danger text-white mb-1"
                                           style="cursor:pointer;">
                                            <small>Elimina</small>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th hidden></th>
                                <th style="max-width: 10%;">Ordine</th>
                                <th>Immagine</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </form>
        @else
            <p>Non ci sono immagini</p>
        @endif
    </div>
</div>
@if(!empty($data_gallery) && sizeof($data_gallery)>0)
    <div id="modalDeleteItem" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form method="POST" action="{{ url('admin/cms/sections/update-details') }}">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="delete_image">
            <input type="hidden" name="item_id" id="delete_item_id">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title text-white">Elimina</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Sei sicuro di voler rimuovere questo elemento ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-danger">Conferma</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div id="modalEditImge" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form method="POST" action="{{ url('admin/cms/sections/update-details') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts->id}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="edit_image">
            <input type="hidden" name="item_id" id="editimage_item_id">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h5 class="modal-title text-white">Modifica</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nuova immagine*</label>
                                    <input type="file" name="image" class="form-control" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-primary">Conferma</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endif




