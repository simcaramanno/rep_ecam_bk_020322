<div class="row">
    <div class="col-12">
        <div class="profile-menu theme-background border  z-index-1 p-2">
            <div class="d-sm-flex">
                <div class="align-self-center">
                    <ul class="nav nav-pills flex-column flex-sm-row" id="myTab" role="tablist">
                        <li class="nav-item ml-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/cms/sections/edit-details/*')) active @endif"
                               href="{{url('admin/cms/sections/edit-details/'.$data->id)}}">
                                Dettagli
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="align-self-center ml-auto text-center text-sm-right">
                    @if(!Request::is('admin/cms/press/sections/edit-data/*'))
                        <a href="#" class="badge badge-info" title="Italiano">
                            <span class="text-uppercase">{{$lang}}</span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
