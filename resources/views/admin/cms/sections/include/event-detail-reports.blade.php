<form method="POST" action="{{url('admin/cms/sections/update-details')}}">
    <div class="row">
        @csrf
        <input type="hidden" name="id" value="{{$data->id}}">
        <input type="hidden" name="text_id" value="{{$data_texts->id}}">
        <input type="hidden" name="lang" value="{{$lang}}">
        <input type="hidden" name="section" value="texts">
        <div class="col-md-12">
            <h6>TESTI</h6>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titoletto* </label>
                <input type="text" name="title" class="form-control"
                       placeholder="Titolo" required maxlength="500"
                       value="{{!empty(old('title')) ? old('title') : $data_texts->title}}"/>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Titolo* </label>
                <textarea name="subtitle" class="form-control"
                          placeholder="Testo" rows="4" required
                          required>{{!empty(old('subtitle')) ? old('subtitle') :$data_texts->subtitle}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <label>Descrizione* </label>
                <textarea name="content1" class="form-control"
                          placeholder="Testo" rows="4" required
                          required>{{!empty(old('content1')) ? old('content1') :$data_texts->content1}}</textarea>
            </div>
        </div>
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary text-uppercase float-right">
                Salva
            </button>
        </div>
    </div>
</form>
