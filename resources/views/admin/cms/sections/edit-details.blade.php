@extends('admin.app-admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/css/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/buttons/css/buttons.bootstrap4.min.css') }}"/>
@endsection

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.cms.sections.include.title-box')
                <div class="card-body">
                    @include('admin.cms.sections.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    @include('admin.cms.sections.include.'.$data->blade_name)


                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <a class="btn btn-outline-primary float-right mr-2" href="{{ url('admin/cms/sections/index') }}">
                                                Annulla
                                            </a>
                                        </div>
                                    </div>
                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection

@section('javascript_vendor_footer')
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.print.min.js') }}"></script>
@endsection


@section('javascript_footer')
    <script src="{{ asset('theme-be/dist/js/app-datatable.script.js') }}"></script>

    <script>
        // delete-item
        $( ".delete-item" ).click(function() {
            var item_id = $(this).attr('data-itemid');
            $('#delete_item_id').val(item_id);
        });


        // delete-item
        $("#tab-section-gallery").on("click",".delete-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#delete_item_id').val(item_id);
        });
        // editimage-item
        $("#tab-section-gallery").on("click",".editimage-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#editimage_item_id').val(item_id);
        });

        /* ----------------
        * Home sliders
        * */
        if($("#tab-home-sliders").length){
            // delete-item
            $("#tab-home-sliders").on("click",".delete-item",function(e){
                e.preventDefault();
                var item_id = $(this).attr('data-itemid');
                $('#delete_item_id').val(item_id);
            });
            // editimage-item
            $("#tab-home-sliders").on("click",".editimage-item",function(e){
                e.preventDefault();
                var item_id = $(this).attr('data-itemid');
                $('#editimage_item_id').val(item_id);
            });
            // edit-item
            $("#tab-home-sliders").on("click",".edit-item",function(e){
                e.preventDefault();
                var item_id = $(this).attr('data-itemid');
                $('#edit_item_id').val(item_id);
                var smalltitle = $(this).attr('data-smalltitle');
                $('#mod_smalltitle').val(smalltitle);
                var title = $(this).attr('data-title');
                $('#mod_title').val(title);
                var subtitle = $(this).attr('data-subtitle');
                $('#mod_subtitle').val(subtitle);
                var btntext = $(this).attr('data-btntext');
                $('#mod_btntext').val(btntext);
                var btnlink = $(this).attr('data-btnlink');
                $('#mod_btnlink').val(btnlink);
                var image_alt = $(this).attr('data-image_alt');
                $('#mod_image_alt').val(image_alt);
            });
        }
    </script>
@endsection

