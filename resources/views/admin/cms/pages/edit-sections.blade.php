@extends('admin.app-admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/css/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/buttons/css/buttons.bootstrap4.min.css') }}"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.cms.pages.include.title-box')
                <div class="card-body">
                    @include('admin.cms.pages.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5></h5>
                                        </div>
                                        @if(!empty($data_sections) && sizeof($data_sections)>0)
{{--                                            <div class="col-md-12 text-right">--}}
{{--                                                <a href="" class="btn btn-sm btn-primary text-uppercase mb-2">--}}
{{--                                                    Ordinamento--}}
{{--                                                </a>--}}
{{--                                            </div>--}}
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="tab-pages-sections" class="display table dataTable table-striped table-bordered" >
                                                        <thead>
                                                        <tr>
                                                            <th>Sezione</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($data_sections as $_item)
                                                            <tr id="{{$_item["section"]["id"]}}">
                                                                <td>{{$_item["section"]["name"]}}</td>
                                                                <td>
                                                                    <a class="btn btn-sm btn-primary"
                                                                        href="{{url('admin/cms/sections/edit-details/'.$_item["section"]["id"])}}">
                                                                        <small>Dettagli</small>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <th>Sezione</th>
                                                            <th></th>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-12">
                                                <div class="text-center">
                                                    <p>Non ci sono immagini</p>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>



@endsection


@section('javascript_vendor_footer')
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.print.min.js') }}"></script>
@endsection

@section('javascript_footer')
    <script src="{{ asset('theme-be/dist/js/app-datatable.script.js') }}"></script>

@endsection

