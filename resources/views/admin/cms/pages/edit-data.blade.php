@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.cms.pages.include.title-box')
                <div class="card-body">
                    @include('admin.cms.pages.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/cms/pages/update_data')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nome*</label>
                                                    <input type="text" name="name" class="form-control"
                                                           placeholder="Nome"
                                                           maxlength="255" required
                                                           value="{{!empty(old('name')) ? old('name') : $data->name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <hr>
                                                <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                    Salva
                                                </button>
                                                <a class="btn btn-outline-primary float-right mr-2" href="{{ url('admin/cms/pages/index') }}">
                                                    Annulla
                                                </a>
                                            </div>
                                        </div>
                                    </form>

                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection


@section('javascript_footer')
    <script>

    </script>
@endsection

