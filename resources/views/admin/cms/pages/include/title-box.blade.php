<div class="card-header justify-content-between align-items-center bg-primary text-white">
    <h4 class="card-title font-weight-800">
        {{$data->name}}
    </h4>
    <small>
        ID# <span class="font-weight-bold">{{$data->id}}</span>
        | Creazione <span class="font-weight-bold">{{date("d/m/y", strtotime($data->created_at))}}</span>
        <br>
        @if($data->status=='active')
            <span class="badge badge-success text-uppercase">attivo</span>
        @else
            <span class="badge badge-danger text-uppercase">inattivo</span>
        @endif
    </small>
</div>
