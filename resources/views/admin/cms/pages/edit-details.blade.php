@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.cms.pages.include.title-box')
                <div class="card-body">
                    @include('admin.cms.pages.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/cms/pages/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["page_text_id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="descriptions">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Titolo*</label>
                                                    <input type="text" name="title" class="form-control"
                                                           placeholder="Titolo"
                                                           maxlength="100" required
                                                           value="{{!empty(old('title')) ? old('title') : $data_texts["title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Title*</label>
                                                    <input type="text" name="meta_title" class="form-control"
                                                           placeholder="Meta Title"
                                                           maxlength="100" required
                                                           value="{{!empty(old('meta_title')) ? old('meta_title') : $data_texts["meta_title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Description*</label>
                                                    <textarea name="meta_desc" class="form-control"
                                                              placeholder="Meta Description" maxlength="250" rows="3"
                                                              required>{{!empty(old('meta_desc')) ? old('meta_desc') : $data_texts["meta_desc"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Keys*</label>
                                                    <textarea name="meta_keys" class="form-control"
                                                              placeholder="Meta Keys" maxlength="250" rows="3"
                                                              required>{{!empty(old('meta_keys')) ? old('meta_keys') : $data_texts["meta_keys"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Header scripts</label>
                                                    <textarea name="head_scripts" class="form-control"
                                                              placeholder="scripts" maxlength="250" rows="5"
                                                              >{{!empty(old('head_scripts')) ? old('head_scripts') : $data_texts["head_scripts"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Footer scripts</label>
                                                    <textarea name="footer_scripts" class="form-control"
                                                              placeholder="scripts" maxlength="250"
                                                              rows="5">{{!empty(old('footer_scripts')) ? old('footer_scripts') : $data_texts["footer_scripts"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <hr>
                                                <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                    Salva
                                                </button>
                                                <a class="btn btn-outline-primary float-right mr-2" href="{{ url('admin/cms/pages/index') }}">
                                                    Annulla
                                                </a>
                                            </div>
                                        </div>
                                    </form>

                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection


@section('javascript_footer')
    <script>

    </script>
@endsection

