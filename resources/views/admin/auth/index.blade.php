@extends('admin.app-admin-auth')

@section('content')
    <form method="POST" action="{{ url('admin/auth/signin') }}" class="row row-eq-height lockscreen  mt-5 mb-5">
        @csrf
        <div class="lock-image col-12 col-sm-5"
             style="background-image:url('{{asset("images/logo/".$platform->logo_dark)}}');background-repeat: no-repeat;
                     background-size: auto;background-position: center"></div>
        <div class="login-form col-12 col-sm-7">
            <div class="form-group mb-3">
                <h5>Area riservata Admin</h5>
                <h2>LOGIN</h2>
            </div>
            @include('admin.include._errors-form')
            @include('admin.include._message-form')
            <div class="form-group mb-3">
                <label for="emailaddress">Email</label>
                <input class="form-control" type="email" name="email" required="" placeholder="Email">
            </div>
            <div class="form-group mb-3">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" required="" placeholder="Password">
            </div>

            <div class="form-group mb-0">
                <button class="btn btn-primary btn-block text-uppercase" type="submit"> Accedi </button>
            </div>
            <div class="form-group mb-0">
                <hr>
            </div>

{{--            <div class="mt-2">--}}
{{--                Password dimenticata? <a class="text-primary" href="{{ url('admin-area/forgot') }}">Recupera</a>--}}
{{--            </div>--}}
        </div>
    </form>

@endsection
