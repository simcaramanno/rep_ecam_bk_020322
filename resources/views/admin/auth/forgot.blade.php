@extends('admin.app-admin-auth')

@section('content')
    <form method="POST" action="{{ url('admin/auth/recover') }}" class="row row-eq-height lockscreen  mt-5 mb-5">
        @csrf
        <div class="lock-image col-12 col-sm-5"></div>
        <div class="login-form col-12 col-sm-7">
            <div class="form-group mb-3">
                <h5>Area riservata Admin</h5>
                <h2>RECUPERO PASSWORD</h2>
                <p>Inserisci il tuo indirizzo email, ti invieremo la procedura per recuperare la tua password.</p>
            </div>
            @include('admin.include._errors-form')
            @include('admin.include._message-form')
            <div class="form-group mb-3">
                <label for="emailaddress">Email</label>
                <input class="form-control" type="email" name="email" required="" placeholder="Email">
            </div>
            <div class="form-group mb-0">
                <button class="btn btn-primary btn-block text-uppercase" type="submit"> invia </button>
            </div>
            <div class="form-group mb-0">
                <hr>
            </div>

            <div class="mt-2"><a class="text-primary" href="{{ url('admin-area/login') }}">Accedi</a></div>
        </div>
    </form>

@endsection
