@extends('admin.app-admin')

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/staff/users/update-data')}}">
                        <div class="row">
                            @csrf
                            <input type="hidden" name="id" value="{{$data->id}}">
                            @include('admin.include._errors-form')
                            @include('admin.include._message-form')
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Cognome*</label>
                                    <input type="text" name="surname" class="form-control"
                                           placeholder="Cognome"
                                           maxlength="255" required
                                           value="{{!empty(old('surname')) ? old('surname') : $data->surname}}"/>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Nome*</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Nome"
                                           maxlength="255" required
                                           value="{{!empty(old('name')) ? old('name') : $data->name}}"/>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Email*</label>
                                    <input type="email" name="email" class="form-control"
                                           placeholder="Email"
                                           maxlength="150" required
                                           value="{{!empty(old('email')) ? old('email') : $data->email}}"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                @if($data->id!=1)
                                    <a class="btn btn-danger text-white text-uppercase" data-toggle="modal" data-target="#modalDelete">
                                        Elimina
                                    </a>
                                    <a class="btn btn-info text-white text-uppercase" data-toggle="modal" data-target="#modalSend">
                                        Invia Credenziali
                                    </a>
                                @endif
                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                    Salva
                                </button>
                                <a class="btn btn-outline-primary float-right mr-2" href="{{ url('admin/staff/users/index') }}">
                                    Annulla
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    @if($data->id!=1)
        <div id="modalDelete" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/staff/users/delete') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Elimina</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di voler rimuovere questo Utente ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


        <div id="modalSend" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/staff/users/send-credentials') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header bg-info">
                            <h5 class="modal-title text-white">Elimina</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Sei sicuro di voler resettare la password di questo utente ? Le nuove crendenziali saranno inviate via email (se non inserisci la password il sistema ne creerà una casuale).</p>
                                </div>
                                <div class="col-md-12">
                                   <div class="form-group">
                                       <label>Imposta password</label>
                                       <input type="password" class="form-control" placeholder="Imposta password" maxlength="20" name="password">
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-info">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif

@endsection



