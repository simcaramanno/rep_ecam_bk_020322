@extends('admin.app-admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/css/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/buttons/css/buttons.bootstrap4.min.css') }}"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.events.include.title-box')
                <div class="card-body">
                    @include('admin.events.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/events/update-speakers')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="store_new_item">
                                            <div class="col-md-12">
                                                <label class="font-weight-800">Inserisci nuovo speaker</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Nome*</label>
                                                    <input type="text" name="first_name" class="form-control" required
                                                           maxlength="500"
                                                           placeholder="Nome"
                                                           value="{{!empty(old('first_name')) ? old('first_name') : ""}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Cognome*</label>
                                                    <input type="text" name="last_name" class="form-control" required
                                                           maxlength="500"
                                                           placeholder="Cognome"
                                                           value="{{!empty(old('last_name')) ? old('last_name') : ""}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Nazione</label>
                                                    <input type="text" name="country" class="form-control"
                                                           maxlength="500" placeholder="Nazione"
                                                           value="{{!empty(old('country')) ? old('country') : ""}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Ruolo*</label>
                                                    <input type="text" name="role" class="form-control"
                                                           maxlength="500" required placeholder="Ruolo"
                                                           value="{{!empty(old('role')) ? old('role') : ""}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Immagine ({{$GV->image_size_events["speaker_image"]["w"].'x'.$GV->image_size_events["speaker_image"]["h"].'px'}})*</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Inserisci
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        @if(!empty($data_texts["speakers"]) && sizeof($data_texts["speakers"])>0)
                                            <div class="col-md-12 mb-1">
                                                <small class="text-info font-weight-bold">
                                                    <i class="fa fa-info-circle"></i> Clicca sull'immagine per modificarla
                                                </small>
                                            </div>
                                            <div class="col-md-12">
                                                <form method="POST" action="{{url('admin/archive/events/sort-speakers')}}">
                                                    <div class="row">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$data->id}}">

                                                        <div class="col-md-12 text-right">
                                                            <button type="submit"
                                                                    class="btn btn-sm btn-primary text-uppercase mb-2">
                                                                Salva ordinamento
                                                            </button>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="table-responsive">
                                                                <table id="tab-speakers" class="display table dataTable table-striped table-bordered" >
                                                                    <thead>
                                                                    <tr>
                                                                        <th hidden></th>
                                                                        <th>Ordine</th>
                                                                        <th>Avatar</th>
                                                                        <th>Nome</th>
                                                                        <th>Cognome</th>
                                                                        <th>Ruolo</th>
                                                                        <th>Nazione</th>
                                                                        <th></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($data_texts["speakers"] as $_item)
                                                                        <tr id="{{$_item->id}}">
                                                                            <td hidden>{{$_item->sorting}}</td>
                                                                            <td data-order="{{$_item->sorting}}">
                                                                                <input type="number" class="form-control bg-white"
                                                                                       name="sorting-{{$_item->id}}" required
                                                                                       value="{{$_item->sorting}}">
                                                                            </td>
                                                                            <td>
                                                                                <a data-toggle="modal" data-target="#modalEditImge"
                                                                                   data-itemid="{{$_item->id}}"
                                                                                   style="cursor: pointer;"
                                                                                   class="editimage-item">
                                                                                    <img src="{{asset('images/events/'.$_item->image)}}" alt="."
                                                                                         style="max-width: 60px;">
                                                                                </a>
                                                                            </td>
                                                                            <td>{{$_item->first_name}}</td>
                                                                            <td>{{$_item->last_name}}</td>
                                                                            <td>{{$_item->role}}</td>
                                                                            <td>{{$_item->country}}</td>
                                                                            <td>
                                                                                <a data-toggle="modal" data-target="#modalEditItem"
                                                                                   data-itemid="{{$_item->id}}"
                                                                                   data-firstname="{{$_item->first_name}}"
                                                                                   data-lastname="{{$_item->last_name}}"
                                                                                   data-country="{{$_item->country}}"
                                                                                   data-role="{{$_item->role}}"
                                                                                   class="edit-item btn btn-sm btn-primary text-white mb-1"
                                                                                   style="cursor:pointer;">
                                                                                    <small>Modifica testi</small>
                                                                                </a>
                                                                                <a data-toggle="modal" data-target="#modalDeleteItem"
                                                                                   data-itemid="{{$_item->id}}"
                                                                                   class="delete-item btn btn-sm btn-danger text-white mb-1"
                                                                                   style="cursor:pointer;">
                                                                                    <small>Elimina</small>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <th hidden></th>
                                                                        <th>Ordine</th>
                                                                        <th>Avatar</th>
                                                                        <th>Nome</th>
                                                                        <th>Cognome</th>
                                                                        <th>Ruolo</th>
                                                                        <th>Nazione</th>
                                                                        <th></th>
                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>

                                            </div>
                                        @else
                                            <div class="col-md-12">
                                                <div class="text-center">
                                                    <p>Non ci sono speakers</p>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    @if(!empty($data_texts["speakers"]) && sizeof($data_texts["speakers"])>0)
        <div id="modalDeleteItem" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/events/update-speakers') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_item">
                <input type="hidden" name="item_id" id="delete_item_id">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Elimina</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di voler rimuovere questo elemento ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="modalEditItem" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/events/update-speakers') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="edit_item">
                <input type="hidden" name="item_id" id="edit_item_id">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title text-white">Modifica</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nome*</label>
                                        <input type="text" name="first_name" class="form-control" required
                                               maxlength="500" id="edit_firstname"
                                               placeholder="Nome"
                                               value="{{!empty(old('first_name')) ? old('first_name') : ""}}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Cognome*</label>
                                        <input type="text" name="last_name" class="form-control" required
                                               maxlength="500" id="edit_lastname"
                                               placeholder="Cognome"
                                               value="{{!empty(old('last_name')) ? old('last_name') : ""}}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Nazione</label>
                                        <input type="text" name="country" class="form-control"
                                               maxlength="500" placeholder="Nazione" id="edit_country"
                                               value="{{!empty(old('country')) ? old('country') : ""}}"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Ruolo*</label>
                                        <input type="text" name="role" class="form-control"
                                               maxlength="500" required placeholder="Ruolo" id="edit_role"
                                               value="{{!empty(old('role')) ? old('role') : ""}}"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="modalEditImge" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/events/update-speakers') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="edit_image">
                <input type="hidden" name="item_id" id="editimage_item_id">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title text-white">Modifica</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nuova immagine ({{$GV->image_size_events["speaker_image"]["w"].'x'.$GV->image_size_events["speaker_image"]["h"].'px'}})*</label>
                                        <input type="file" name="image" class="form-control" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif


@endsection


@section('javascript_vendor_footer')
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.print.min.js') }}"></script>
@endsection

@section('javascript_footer')
    <script src="{{ asset('theme-be/dist/js/app-datatable.script.js') }}"></script>
    <script>

        // edit-item
        $("#tab-speakers").on("click",".edit-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#edit_item_id').val(item_id);
            var firstname = $(this).attr('data-firstname');
            $('#edit_firstname').val(firstname);
            var lastname = $(this).attr('data-lastname');
            $('#edit_lastname').val(lastname);
            var role = $(this).attr('data-role');
            $('#edit_role').val(role);
            var country = $(this).attr('data-country');
            $('#edit_country').val(country);
        });
        // delete-item
        $("#tab-speakers").on("click",".delete-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#delete_item_id').val(item_id);
        });
        // editimage-item
        $("#tab-speakers").on("click",".editimage-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#editimage_item_id').val(item_id);
        });
    </script>
@endsection

