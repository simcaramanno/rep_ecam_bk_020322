@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.events.include.title-box')
                <div class="card-body">
                    @include('admin.events.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <form method="POST" action="{{url('admin/archive/events/update-data')}}">
                                        <div class="row">
                                            @csrf
                                            @include('admin.include._errors-form')
                                            @include('admin.include._message-form')
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nome*</label>
                                                    <input type="text" name="name" class="form-control"
                                                           placeholder="Nome"
                                                           maxlength="255" required
                                                           value="{{!empty(old('name')) ? old('name') : $data->name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Dal*</label>
                                                    <div class="d-flex">
                                                        <input name="date_start" id="date_start"
                                                               placeholder="Dal" class="form-control" type="text" required
                                                               value="<?php echo !empty(old('date_start')) ? date("d-m-Y", strtotime(old('date_start'))) : date("d-m-Y", strtotime($data->date_start)) ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Al*</label>
                                                    <div class="d-flex">
                                                        <input name="date_end" id="date_end"
                                                               placeholder="Dal" class="form-control" type="text" required
                                                               value="<?php echo !empty(old('date_end')) ? date("d-m-Y", strtotime(old('date_end'))) : date("d-m-Y", strtotime($data->date_end)) ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Indirizzo location*</label>
                                                    <input type="text" name="location_address" class="form-control" required maxlength="100"
                                                           placeholder="Es. Via Verdi, 1, Roma"
                                                           value="{{!empty(old('location_address')) ? old('location_address') : $data->location_address}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <hr>
                                                <a class="btn btn-danger text-white text-uppercase" data-toggle="modal" data-target="#modalDelete">
                                                    elimina
                                                </a>
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                                <a class="btn btn-outline-primary float-right mr-2" href="{{ url('admin/archive/events/index') }}">
                                                    Annulla
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <h6>Cambia Status</h6>
                                        </div>
                                        <div class="col-md-12">
                                            @if($data->status=='active')
                                                <a class="btn btn-danger text-white text-uppercase" data-toggle="modal" data-target="#modalInactive">
                                                    Rendi inattivo
                                                </a>
                                            @else
                                                @if($activable["is_activable"])
                                                    <a class="btn btn-success text-uppercase" data-toggle="modal" data-target="#modalActive">
                                                        Rendi attivo
                                                    </a>
                                                @else
                                                    <div class="alert alert-warning text-center">
                                                        <i class="icon-exclamation font-15"></i>
                                                        <p class="font-15">
                                                            Non puoi attivare questo Evento.
                                                            <br>Inserisci tutte le informazioni mancanti:
                                                        </p>
                                                        <ul class="text-left">
                                                            @foreach($activable["info"] as $item)
                                                                <li>{{$item}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <div id="modalDelete" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form method="POST" action="{{ url('admin/archive/events/delete') }}">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-warning">
                        <h5 class="modal-title text-white">Elimina</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Sei sicuro di voler rimuovere questo Evento ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-danger">Conferma</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


    @if($data->status=='active')
        <div id="modalInactive" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/events/change-status') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="status" value="inactive">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Cambia status</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di rendere inattivo questo Evento ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @else
        @if($activable["is_activable"])
            <div id="modalActive" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <form method="POST" action="{{ url('admin/archive/events/change-status') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="status" value="active">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header bg-success">
                                <h5 class="modal-title text-white">Cambia status</h5>
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Sei sicuro di rendere attivo questo Evento ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-success">Conferma</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        @endif
    @endif


@endsection


@section('javascript_footer')
    <script>
        // Date
        var startDate = flatpickr(document.getElementById('date_start'), {
            locale: "it",
            enableTime: false,
            dateFormat: "d-m-Y"
        });
        flatpickr(document.getElementById('date_end'), {
            enableTime: true,
            dateFormat: "d-m-Y"
        });
        startDate.config.onChange.push(function (selectedDates, dateStr, instance) {
            flatpickr(document.getElementById('date_end'), {
                locale: "it",
                enableTime: false,
                dateFormat: "d-m-Y",
                minDate: dateStr
            });
        });
    </script>
@endsection

