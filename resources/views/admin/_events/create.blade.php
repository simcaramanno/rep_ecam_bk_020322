@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/archive/events/store')}}">
                        <div class="row">
                            @csrf
                            @include('admin.include._errors-form')
                            @include('admin.include._message-form')
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nome*</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Nome"
                                           maxlength="255" required
                                           value="{{old('name')}}"/>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Dal*</label>
                                    <div class="d-flex">
                                        <input name="date_start" id="date_start"
                                               placeholder="Dal" class="form-control" type="text" required
                                               value="<?php echo !empty(old('date_start')) ? date("d-m-Y", strtotime(old('date_start'))) : "" ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Al*</label>
                                    <div class="d-flex">
                                        <input name="date_end" id="date_end"
                                               placeholder="Dal" class="form-control" type="text" required
                                               value="<?php echo !empty(old('date_end')) ? date("d-m-Y", strtotime(old('date_end'))) : "" ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Indirizzo location*</label>
                                    <input type="text" name="location_address" class="form-control" required maxlength="100"
                                           placeholder="Es. Via Verdi, 1, Roma"
                                           value="{{old('location_address')}}"/>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <hr>
                                <a class="btn btn-outline-primary" href="{{ url('admin/archive/events/index') }}">
                                    Annulla
                                </a>
                                <button type="submit" class="btn btn-primary text-uppercase" name="btn_save">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('javascript_footer')
    <script>
        // Date
        var startDate = flatpickr(document.getElementById('date_start'), {
            locale: "it",
            enableTime: false,
            dateFormat: "d-m-Y"
        });
        flatpickr(document.getElementById('date_end'), {
            enableTime: true,
            dateFormat: "d-m-Y"
        });
        startDate.config.onChange.push(function (selectedDates, dateStr, instance) {
            flatpickr(document.getElementById('date_end'), {
                locale: "it",
                enableTime: false,
                dateFormat: "d-m-Y",
                minDate: dateStr
            });
        });
    </script>
@endsection

