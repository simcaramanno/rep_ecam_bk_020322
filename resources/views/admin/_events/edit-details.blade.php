@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.events.include.title-box')
                <div class="card-body">
                    @include('admin.events.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/events/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="descriptions">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Titolo*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Titolo header pagina dettaglio evento">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <input type="text" name="title1" class="form-control"
                                                           placeholder="Titolo"
                                                           maxlength="100" required
                                                           value="{{!empty(old('title1')) ? old('title1') : $data_texts["eventText"]["title1"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Sottotitolo
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Sottotitolo header pagina dettaglio evento">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <input type="text" name="subtitle" class="form-control"
                                                           placeholder="Sottotitolo"
                                                           maxlength="100"
                                                           value="{{!empty(old('subtitle')) ? old('subtitle') : $data_texts["eventText"]["subtitle"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Descrizione completa*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Descrizione pagina dettaglio evento">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <textarea name="full_desc" class="form-control"
                                                              placeholder="Descrizione completa" rows="4"
                                                              required>{{!empty(old('full_desc')) ? old('full_desc') : $data_texts["eventText"]["full_desc"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine background header*
                                                </label>
                                                @if(empty($data_texts["eventText"]["bg_image"]))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/events/'.$data_texts["eventText"]["bg_image"])}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/events/update-details')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="upload_bg_image">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_events["bg_image"]["w"].'x'.$GV->image_size_events["bg_image"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                    <!--video-->
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <h6>VIDEO header</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>File</label>
                                                @if(empty($data_texts["eventText"]["video_url"]))
                                                    <p class="alert alert-warning">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <video style="max-width: 100%;" controls>
                                                            <source src="{{asset('images/video/'.$data_texts["eventText"]["video_url"])}}" type="video/mp4">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/events/update-details')}}"  enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="upload_video_url">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload video (.mp4)*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="POST" action="{{url('admin/archive/events/update-details')}}"  enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="update_video_url_youtube">
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-12">
                                                        <div class="form-group">
                                                            <label>Link youtube
                                                            <br>
                                                                <span class="text-info"><i class="fa fa-info-circle"></i>Se inserito, l'url Youtube ha priorità rispetto al file caricato sopra. </span>
                                                            </label>
                                                            <input type="url" class="form-control" name="video_url_youtube"
                                                                   placeholder="https://www.youtube.com/watch?v=g0f_BRYJLJE"
                                                            value="{{!empty(old('video_url_youtube')) ? old('video_url_youtube') : $data_texts["eventText"]["video_url_youtube"]}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                    <!--Box con immagine [pagina dettaglio evento]-->
                                    <form method="POST" action="{{url('admin/archive/events/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="image_box">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>Box con immagine [pagina dettaglio evento]</h6>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Titolo*
                                                    </label>
                                                    <input type="text" name="box_title" class="form-control"
                                                           placeholder="Titolo" required
                                                           value="{{!empty(old('box_title')) ? old('box_title') : $data_texts["eventText"]["box_title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Descrizione*
                                                    </label>
                                                    <textarea name="description" class="form-control"
                                                              placeholder="Descrizione" rows="4"
                                                              required>{{!empty(old('description')) ? old('description') : $data_texts["eventText"]["description"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Città*
                                                    </label>
                                                    <input type="text" name="city" class="form-control"
                                                           placeholder="Città" required maxlength="100"
                                                           value="{{!empty(old('city')) ? old('city') : $data_texts["eventText"]["city"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Location*
                                                    </label>
                                                    <input type="text" name="location" class="form-control"
                                                           placeholder="Location" required maxlength="100"
                                                           value="{{!empty(old('location')) ? old('location') : $data_texts["eventText"]["location"]}}"/>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine sinistra*
                                                </label>
                                                @if(empty($data_texts["eventText"]["image_box"]))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/events/'.$data_texts["eventText"]["image_box"])}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/events/update-details')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="upload_image_box">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_events["image_box"]["w"].'x'.$GV->image_size_events["image_box"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Alt text*
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="image_box_alt" required maxlength="155"
                                                                   placeholder="Alt text"
                                                                   value="{{!empty(old('image_box_alt')) ? old('image_box_alt') : $data_texts["eventText"]["image_box_alt"]}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                    <!--Box location [pagina dettaglio evento]-->
                                    <form method="POST" action="{{url('admin/archive/events/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="location_box">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>Box location [pagina dettaglio evento]</h6>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Sottotitolo (location)*
                                                    </label>
                                                    <input type="text" name="location_box" class="form-control"
                                                           placeholder="Sottotitolo (location)" required maxlength="100"
                                                           value="{{!empty(old('location_box')) ? old('location_box') : $data_texts["eventText"]["location_box"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Descrizione*
                                                    </label>
                                                    <textarea name="location_desc" class="form-control"
                                                              placeholder="Descrizione" rows="4"
                                                              required>{{!empty(old('location_desc')) ? old('location_desc') : $data_texts["eventText"]["location_desc"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine sinistra*
                                                </label>
                                                @if(empty($data->location_img))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/events/'.$data->location_img)}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/events/update-details')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="upload_image_location_box">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_events["location_img"]["w"].'x'.$GV->image_size_events["location_img"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Alt text*
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   name="location_img_alt" required maxlength="155"
                                                                   placeholder="Alt text"
                                                                   value="{{!empty(old('location_img_alt')) ? old('location_img_alt') : $data->location_img_alt}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!--Hero area home page-->
                                    <form method="POST" action="{{url('admin/archive/events/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="home_data">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>Hero area home page</h6>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Titoletto*
                                                    </label>
                                                    <input type="text" name="home_small_title" class="form-control"
                                                           placeholder="Titoletto" required maxlength="100"
                                                           value="{{!empty(old('home_small_title')) ? old('home_small_title') : $data_texts["eventText"]["home_small_title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Titolo*</label>
                                                    <input type="text" name="home_title" class="form-control"
                                                           placeholder="Titolo" required maxlength="100"
                                                           value="{{!empty(old('home_title')) ? old('home_title') : $data_texts["eventText"]["home_title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo bottone*</label>
                                                    <input type="text" name="home_btn_text" class="form-control"
                                                           placeholder="Testo bottone" required maxlength="50"
                                                           value="{{!empty(old('home_btn_text')) ? old('home_btn_text') : $data_texts["eventText"]["home_btn_text"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine slider home*
                                                </label>
                                                @if(empty($data_texts["eventText"]["home_image"]))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/events/'.$data_texts["eventText"]["home_image"])}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/events/update-details')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="upload_home_image">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_events["home_image"]["w"].'x'.$GV->image_size_events["home_image"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!--UPCOMING BOX-->
                                    <form method="POST" action="{{url('admin/archive/events/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="upcoming_event_box">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>Box upcoming event [presente in Home page e in Events]</h6>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Titolo*
                                                    </label>
                                                    <input type="text" name="title" class="form-control"
                                                           placeholder="Titolo" required maxlength="100"
                                                           value="{{!empty(old('title')) ? old('title') : $data_texts["eventText"]["title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Descrizione breve*</label>
                                                    <textarea name="short_description" class="form-control"
                                                              placeholder="Descrizione breve" rows="4"
                                                              required>{{!empty(old('short_description')) ? old('short_description') : $data_texts["eventText"]["short_description"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Testo Blockquote 1
                                                    </label>
                                                    <input type="text" name="blockquote1" class="form-control"
                                                           placeholder="Testo Blockquote 1"
                                                           value="{{!empty(old('blockquote1')) ? old('blockquote1') : $data_texts["eventText"]["blockquote1"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Testo Blockquote 2
                                                    </label>
                                                    <input type="text" name="blockquote2" class="form-control"
                                                           placeholder="Testo Blockquote 2"
                                                           value="{{!empty(old('blockquote2')) ? old('blockquote2') : $data_texts["eventText"]["blockquote2"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine nel box*
                                                </label>
                                                @if(empty($data_texts["eventText"]["image1_720x886"]))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/events/'.$data_texts["eventText"]["image1_720x886"])}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/events/update-details')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="upcoming_event_image">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_events["image1_720x886"]["w"].'x'.$GV->image_size_events["image1_720x886"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!--META-->
                                    <form method="POST" action="{{url('admin/archive/events/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="meta">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>META</h6>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta title* </label>
                                                    <input type="text" name="meta_title" class="form-control"
                                                           placeholder="Meta title" required maxlength="155"
                                                           value="{{!empty(old('meta_title')) ? old('meta_title') : $data_texts["eventText"]["meta_title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Description*</label>
                                                    <textarea name="meta_desc" class="form-control"
                                                              placeholder="Descrizione breve" maxlength="250" rows="3"
                                                              required>{{!empty(old('meta_desc')) ? old('meta_desc') : $data_texts["eventText"]["meta_desc"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Keys*</label>
                                                    <textarea name="meta_keys" class="form-control"
                                                              placeholder="Descrizione Keys" maxlength="250" rows="3"
                                                              required>{{!empty(old('meta_keys')) ? old('meta_keys') : $data_texts["eventText"]["meta_keys"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>


                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>



@endsection


@section('javascript_footer')
    <script>

    </script>
@endsection

