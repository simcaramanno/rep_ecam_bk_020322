<!DOCTYPE html>
<html lang="it">
    <!-- START: Head-->
    <head>
        <meta charset="utf-8">
        <meta name="lang" content="it">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>{{ $platform->be_site_name}} | Admin - {{$layout["page_title"]}}</title>
        <meta name="description" content="{{ $platform->be_meta_desc}}">
        <meta name="keywords" content="{{ $platform->be_meta_keys}}">
        <meta name="author" content="{{$platform->site_name}}">

        <link rel="apple-touch-icon" href="{{ asset('images/logo/'.$platform->favicon) }}">
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/logo/'.$platform->favicon) }}">
        @if(!empty($platform->favicon114))
            <!-- For iPhone 4 Retina display: -->
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/logo/'.$platform->favicon114) }}">
        @endif
        @if(!empty($platform->favicon72))
            <!-- For iPad: -->
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/logo/'.$platform->favicon72) }}">
        @endif
        @if(!empty($platform->favicon57))
            <!-- For iPhone: -->
            <link rel="apple-touch-icon-precomposed" href="{{ asset('images/logo/'.$platform->favicon57) }}">
        @endif

        <!-- START: Template CSS-->
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/jquery-ui/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/jquery-ui/jquery-ui.theme.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/simple-line-icons/css/simple-line-icons.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/flags-icon/css/flag-icon.min.css') }}">
        <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/fontawesome/css/all.min.css') }}">
        <!-- END Template CSS-->

        <!-- START: Page CSS-->
        @yield('css')
        <!-- END: Page CSS-->

        <!-- START: Custom CSS-->
        <link rel="stylesheet" href="{{ asset('theme-be/dist/css/main.css') }}">
        <!-- END: Custom CSS-->

        @yield('javascript_head')

    </head>
    <!-- END Head-->

    <!-- START: Body-->
    <body id="main-container" class="default semi-dark">
        <!-- START: Pre Loader-->
        <div class="se-pre-con">
            <div class="loader"></div>
        </div>
        <!-- END: Pre Loader-->

        <!-- START: Header-->
        @include('admin.include._header')
        <!-- END: Header-->


        <!-- START: Main Menu-->
        @include('admin.include._header')
        <div class="sidebar">
            <div class="site-width">
                <!-- START: Menu-->
                @include('admin.include._aside-menu')
                <!-- END: Menu-->
                @include('admin.include._breadcrumb1')
            </div>
        </div>
        <!-- END: Main Menu-->

        <!-- START: Main Content-->
        <main style="min-height: 800px;">
            <div class="container-fluid site-width">
                <!-- START: Breadcrumbs-->
                @include('admin.include._breadcrumb2')
                <!-- END: Breadcrumbs-->

                <!-- START: Card Data-->
                @yield('content')

                <!-- END: Card DATA-->
            </div>
        </main>
        <!-- END: Content-->
        <!-- START: Footer-->
        <footer class="site-footer">
            {{date('Y')}} © <span class="text-primary font-weight-800">{{$platform->site_name}}</span></small>
        </footer>
        <!-- END: Footer-->

        <!-- START: Back to top-->
        <a href="#" class="scrollup text-center">
            <i class="icon-arrow-up"></i>
        </a>
        <!-- END: Back to top-->

        <!-- START: Template JS-->
        <script src="{{ asset('theme-be/dist/vendors/jquery/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('theme-be/dist/vendors/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('theme-be/dist/vendors/moment/moment.js') }}"></script>
        <script src="{{ asset('theme-be/dist/vendors/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('theme-be/dist/vendors/slimscroll/jquery.slimscroll.min.js') }}"></script>
        <!-- END: Template JS-->

        <!-- START: APP JS-->
        <script src="{{ asset('theme-be/dist/js/app.js?vers=5.1.3') }}"></script>
        <!-- END: APP JS-->

        <!-- START: Page Vendor JS-->
        @yield('javascript_vendor_footer')
        <!-- END: Page Vendor JS-->

        <!-- flatpickr-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
        <script src="https://npmcdn.com/flatpickr/dist/l10n/it.js"></script>

        <!-- START: Page JS-->
        @yield('javascript_footer')
        <!-- END: Page JS-->

    </body>
    <!-- END: Body-->
</html>
