@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.projects.include.title-box')
                <div class="card-body">
                    @include('admin.projects.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <!--small image -->
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine small (presente pagina lista progetti)*
                                                </label>
                                                @if(empty($data->small_image))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/projects/'.$data->small_image)}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/projects/update-images')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="small_image">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_projects["small_image"]["w"].'x'.$GV->image_size_projects["small_image"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!--big image -->
                                    <div class="row">
                                        <div class="col-md-12"><hr></div>
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine big (presente pagina dettaglio progetto - header)*
                                                </label>
                                                @if(empty($data->big_image))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/projects/'.$data->big_image)}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/projects/update-images')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="big_image">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_projects["big_image"]["w"].'x'.$GV->image_size_projects["big_image"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!--video-->
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <h6>VIDEO header</h6>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>File</label>
                                                @if(empty($data->video_url))
                                                    <p class="alert alert-warning">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <video style="max-width: 100%;" controls>
                                                            <source src="{{asset('images/projects/video/'.$data->video_url)}}" type="video/mp4">
                                                            Your browser does not support the video tag.
                                                        </video>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/projects/update-images')}}"  enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="video_url">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload video (.mp4)*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form method="POST" action="{{url('admin/archive/projects/update-images')}}"  enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="update_video_url_youtube">
                                                <div class="row">
                                                    <div class="col-md-9 col-sm-12">
                                                        <div class="form-group">
                                                            <label>Link youtube
                                                                <br>
                                                                <span class="text-info"><i class="fa fa-info-circle"></i>Se inserito, l'url Youtube ha priorità rispetto al file caricato sopra. </span>
                                                            </label>
                                                            <input type="url" class="form-control" name="video_url_youtube"
                                                                   placeholder="https://www.youtube.com/watch?v=g0f_BRYJLJE"
                                                                   value="{{!empty(old('video_url_youtube')) ? old('video_url_youtube') : $data->video_url_youtube}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                    <!--home image -->
                                    <div class="row">
                                        <div class="col-md-12"><hr></div>
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine home page*
                                                </label>
                                                @if(empty($data->home_image))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/projects/'.$data->home_image)}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/projects/update-images')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="home_image">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_projects["home_image"]["w"].'x'.$GV->image_size_projects["home_image"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>


                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>



@endsection


@section('javascript_footer')
    <script>

    </script>
@endsection

