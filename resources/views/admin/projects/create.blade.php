@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/archive/projects/store')}}">
                        <div class="row">
                            @csrf
                            @include('admin.include._errors-form')
                            @include('admin.include._message-form')
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nome*</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Nome"
                                           maxlength="255" required
                                           value="{{old('name')}}"/>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Data*</label>
                                    <div class="d-flex">
                                        <input name="project_date" id="project_date"
                                               placeholder="Data" class="form-control" type="text" required
                                               value="<?php echo !empty(old('project_date')) ? date("d-m-Y", strtotime(old('project_date'))) : "" ?>"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <hr>
                                <a class="btn btn-outline-primary" href="{{ url('admin/archive/projects/index') }}">
                                    Annulla
                                </a>
                                <button type="submit" class="btn btn-primary text-uppercase">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('javascript_footer')
    <script>
        // Date
        var startDate = flatpickr(document.getElementById('project_date'), {
            locale: "it",
            enableTime: false,
            dateFormat: "d-m-Y"
        });
    </script>
@endsection

