@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.projects.include.title-box')
                <div class="card-body">
                    @include('admin.projects.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <form method="POST" action="{{url('admin/archive/projects/update-data')}}">
                                        <div class="row">
                                            @csrf
                                            @include('admin.include._errors-form')
                                            @include('admin.include._message-form')
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nome*</label>
                                                    <input type="text" name="name" class="form-control"
                                                           placeholder="Nome"
                                                           maxlength="255" required
                                                           value="{{!empty(old('name')) ? old('name') : $data->name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Data*</label>
                                                    <div class="d-flex">
                                                        <input name="project_date" id="project_date"
                                                               placeholder="Data" class="form-control" type="text" required
                                                               value="<?php echo !empty(old('project_date')) ? date("d-m-Y", strtotime(old('project_date'))) : date("d-m-Y", strtotime($data->project_date)) ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Progetto in home page*</label>
                                                    <select class="form-control" required name="is_main">
                                                        <option value="0" @if($data->is_main==0) selected @endif>
                                                            No
                                                        </option>
                                                        <option value="1" @if($data->is_main==1) selected @endif>
                                                            Si
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <hr>
                                                <a class="btn btn-danger text-white text-uppercase" data-toggle="modal" data-target="#modalDelete">
                                                    elimina
                                                </a>
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                                <a class="btn btn-outline-primary float-right mr-2" href="{{ url('admin/archive/projects/index') }}">
                                                    Annulla
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <h6>Cambia Status</h6>
                                        </div>
                                        <div class="col-md-12">
                                            @if($data->status=='active')
                                                <a class="btn btn-danger text-white text-uppercase" data-toggle="modal" data-target="#modalInactive">
                                                    Rendi inattivo
                                                </a>
                                            @else
                                                @if($activable["is_activable"])
                                                    <a class="btn btn-success text-uppercase" data-toggle="modal" data-target="#modalActive">
                                                        Rendi attivo
                                                    </a>
                                                @else
                                                    <div class="alert alert-warning text-center">
                                                        <i class="icon-exclamation font-15"></i>
                                                        <p class="font-15">
                                                            Non puoi attivare questo Progetto.
                                                            <br>Inserisci tutte le informazioni mancanti:
                                                        </p>
                                                        <ul class="text-left">
                                                            @foreach($activable["info"] as $item)
                                                                <li>{{$item}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <div id="modalDelete" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form method="POST" action="{{ url('admin/archive/projects/delete') }}">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-warning">
                        <h5 class="modal-title text-white">Elimina</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Sei sicuro di voler rimuovere questo Progetto ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-danger">Conferma</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


    @if($data->status=='active')
        <div id="modalInactive" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/projects/change-status') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="status" value="inactive">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Cambia status</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di rendere inattivo questo Progetto ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @else
        @if($activable["is_activable"])
            <div id="modalActive" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <form method="POST" action="{{ url('admin/archive/projects/change-status') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="status" value="active">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header bg-success">
                                <h5 class="modal-title text-white">Cambia status</h5>
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Sei sicuro di rendere attivo questo Progetto ?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-success">Conferma</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        @endif
    @endif


@endsection


@section('javascript_footer')
    <script>
        // Date
        var startDate = flatpickr(document.getElementById('project_date'), {
            locale: "it",
            enableTime: false,
            dateFormat: "d-m-Y"
        });
    </script>
@endsection

