@extends('admin.app-admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/css/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/buttons/css/buttons.bootstrap4.min.css') }}"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.projects.include.title-box')
                <div class="card-body">
                    @include('admin.projects.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/projects/update-gallery')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="store_new_item">
                                            <div class="col-md-12">
                                                <label class="font-weight-800">Inserisci nuova immagine</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Alt text</label>
                                                    <input type="text" name="alt_text" class="form-control"
                                                           maxlength="150" placeholder="Alt text"
                                                           value="{{!empty(old('alt_text')) ? old('alt_text') : ""}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Immagine ({{$GV->image_size_projects["slider_gallery"]["w"].'x'.$GV->image_size_projects["slider_gallery"]["h"].'px'}})*</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Inserisci
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        @if(!empty($data_texts["gallery"]) && sizeof($data_texts["gallery"])>0)
                                            <div class="col-md-12 mb-1">
                                                <small class="text-info font-weight-bold">
                                                    <i class="fa fa-info-circle"></i> Clicca sull'immagine per modificarla
                                                </small>
                                            </div>
                                            <div class="col-md-12">
                                                <form method="POST" action="{{url('admin/archive/projects/sort-gallery')}}">
                                                    <div class="row">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$data->id}}">

                                                        <div class="col-md-12 text-right">
                                                            <button type="submit"
                                                                    class="btn btn-sm btn-primary text-uppercase mb-2">
                                                                Salva ordinamento
                                                            </button>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="table-responsive">
                                                                <table id="tab-project-gallery" class="display table dataTable table-striped table-bordered" >
                                                                    <thead>
                                                                    <tr>
                                                                        <th hidden></th>
                                                                        <th style="max-width: 10%">Ordine</th>
                                                                        <th>Immagine</th>
                                                                        <th>Alt text</th>
                                                                        <th></th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($data_texts["gallery"] as $_item)
                                                                        <tr id="{{$_item->id}}">
                                                                            <td hidden>{{$_item->sorting}}</td>
                                                                            <td data-order="{{$_item->sorting}}" style="max-width: 10%">
                                                                                <input type="number" class="form-control bg-white"
                                                                                       name="sorting-{{$_item->id}}" required
                                                                                       value="{{$_item->sorting}}">
                                                                            </td>
                                                                            <td>
                                                                                <a data-toggle="modal" data-target="#modalEditImge"
                                                                                   data-itemid="{{$_item->id}}"
                                                                                   style="cursor: pointer;"
                                                                                   class="editimage-item">
                                                                                    <img src="{{asset('images/projects/'.$_item->filename)}}" alt="."
                                                                                         style="max-width:100px;">
                                                                                </a>
                                                                            </td>
                                                                            <td>{{$_item->alt_text}}</td>
                                                                            <td>
                                                                                <a data-toggle="modal" data-target="#modalDeleteItem"
                                                                                   data-itemid="{{$_item->id}}"
                                                                                   class="delete-item btn btn-sm btn-danger text-white mb-1"
                                                                                   style="cursor:pointer;">
                                                                                    <small>Elimina</small>
                                                                                </a>
                                                                                <a data-toggle="modal" data-target="#modalEditItem"
                                                                                   data-itemid="{{$_item->id}}"
                                                                                   data-alttext="{{$_item->alt_text}}"
                                                                                   class="edit-item btn btn-sm btn-warning text-white mb-1"
                                                                                   style="cursor:pointer;">
                                                                                    <small>Modifica alt text</small>
                                                                                </a>
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                    <tfoot>
                                                                    <tr>
                                                                        <th hidden></th>
                                                                        <th style="max-width: 10%">Ordine</th>
                                                                        <th>Immagine</th>
                                                                        <th>Alt text</th>
                                                                        <th></th>
                                                                    </tr>
                                                                    </tfoot>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        @else
                                            <div class="col-md-12">
                                                <div class="text-center">
                                                    <p>Non ci sono immagini</p>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    @if(!empty($data_texts["gallery"]) && sizeof($data_texts["gallery"])>0)
        <div id="modalDeleteItem" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/projects/update-gallery') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_item">
                <input type="hidden" name="item_id" id="delete_item_id">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Elimina</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di voler rimuovere questo elemento ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!--modalEditItem-->
        <div id="modalEditItem" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/projects/update-gallery') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="edit_item">
                <input type="hidden" name="item_id" id="edit_item_id">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header bg-warning">
                            <h5 class="modal-title text-white">Modifica</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Alt text</label>
                                        <input type="text" name="alt_text" class="form-control"
                                               maxlength="150" placeholder="Alt text" id="mod_alt_text"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-warning text-white">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="modalEditImge" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/projects/update-gallery') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="edit_image">
                <input type="hidden" name="item_id" id="editimage_item_id">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title text-white">Modifica</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nuova immagine*</label>
                                        <input type="file" name="image" class="form-control" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


    @endif

@endsection


@section('javascript_vendor_footer')
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.print.min.js') }}"></script>
@endsection

@section('javascript_footer')
    <script src="{{ asset('theme-be/dist/js/app-datatable.script.js') }}"></script>

    <script>

        // delete-item
        $("#tab-project-gallery").on("click",".delete-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#delete_item_id').val(item_id);
        });

        // edit-item
        $("#tab-project-gallery").on("click",".edit-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#edit_item_id').val(item_id);
            var alttext = $(this).attr('data-alttext');
            $('#mod_alt_text').val(alttext);
        });

        // editimage-item
        $("#tab-project-gallery").on("click",".editimage-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#editimage_item_id').val(item_id);
        });

    </script>
@endsection

