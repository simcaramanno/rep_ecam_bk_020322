@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.projects.include.title-box')
                <div class="card-body">
                    @include('admin.projects.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/projects/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="descriptions">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Titolo*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Titolo header pagina dettaglio progetto">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <input type="text" name="header_title" class="form-control"
                                                           placeholder="Titolo"
                                                           maxlength="100" required
                                                           value="{{!empty(old('header_title')) ? old('header_title') : $data_texts["projectText"]["header_title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Titoletto*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Titolo nella pagina lista progetti">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <input type="text" name="title_into_list" class="form-control"
                                                           placeholder="Titoletto"
                                                           maxlength="100" required
                                                           value="{{!empty(old('title_into_list')) ? old('title_into_list') : $data_texts["projectText"]["title_into_list"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Tipo progetto*</label>
                                                    <input type="text" name="project_type" class="form-control"
                                                           placeholder="Tipo progetto"
                                                           maxlength="100" required
                                                           value="{{!empty(old('project_type')) ? old('project_type') : $data_texts["projectText"]["project_type"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Location*</label>
                                                    <input type="text" name="location" class="form-control"
                                                           placeholder="Location"
                                                           maxlength="100" required
                                                           value="{{!empty(old('location')) ? old('location') : $data_texts["projectText"]["location"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Descrizione*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Descrizione pagina dettaglio progetto">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <textarea name="description" class="form-control"
                                                              placeholder="Descrizione" rows="4"
                                                              required>{{!empty(old('description')) ? old('description') : $data_texts["projectText"]["description"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Descrizione breve*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Descrizione breve presente in home page">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <textarea name="short_description" class="form-control"
                                                              placeholder="Descrizione breve" rows="4"
                                                              required>{{!empty(old('short_description')) ? old('short_description') : $data_texts["projectText"]["short_description"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <!--META-->
                                    <form method="POST" action="{{url('admin/archive/projects/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["projectText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="meta">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>META</h6>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta title* </label>
                                                    <input type="text" name="meta_title" class="form-control"
                                                           placeholder="Meta title" required maxlength="155"
                                                           value="{{!empty(old('meta_title')) ? old('meta_title') : $data_texts["projectText"]["meta_title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Description*</label>
                                                    <textarea name="meta_desc" class="form-control"
                                                              placeholder="Descrizione breve" maxlength="250" rows="3"
                                                              required>{{!empty(old('meta_desc')) ? old('meta_desc') : $data_texts["projectText"]["meta_desc"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Keys*</label>
                                                    <textarea name="meta_keys" class="form-control"
                                                              placeholder="Descrizione Keys" maxlength="250" rows="3"
                                                              required>{{!empty(old('meta_keys')) ? old('meta_keys') : $data_texts["projectText"]["meta_keys"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>


                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>



@endsection


@section('javascript_footer')
    <script>

    </script>
@endsection

