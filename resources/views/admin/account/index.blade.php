@extends('admin.app-admin')
@section('content')
    <div class="row">
        <div class="col-12">
            @include('admin.include._errors-form')
            @include('admin.include._message-form')
        </div>
        <div class="col-6 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        Modifica email
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/account/update-email')}}">
                        <div class="row">
                            @csrf
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email*</label>
                                    <input type="email" name="email" class="form-control"
                                           placeholder="https://www.myurl.com"
                                           maxlength="150" required
                                           value="{{!empty(old('email')) ? old('email') : $data->email}}"/>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-primary text-uppercase">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-6 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        Modifica Password
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/account/update-pw')}}">
                        <div class="row">
                            @csrf
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password attuale*</label>
                                    <input type="password" name="password1" class="form-control"
                                           placeholder="Password attuale"
                                           maxlength="20" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Password nuova*</label>
                                    <input type="password" name="password2" class="form-control"
                                           placeholder="Password nuova"
                                           maxlength="20" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Conferma nuova password*</label>
                                    <input type="password" name="password3" class="form-control"
                                           placeholder="Conferma nuova password"
                                           maxlength="20" required/>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-primary text-uppercase">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection



