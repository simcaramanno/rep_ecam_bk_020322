@extends('admin.app-admin')
@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/settings/config/scripts/update')}}">
                        <div class="row">
                            @csrf
                            @include('admin.include._errors-form')
                            @include('admin.include._message-form')
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Header scripts</label>
                                    <textarea rows="25" name="header_scripts" class="form-control"
                                           placeholder="Header scripts">{{!empty(old('header_scripts')) ? old('header_scripts') : $data->header_scripts}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-primary text-uppercase">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection



