@extends('admin.app-admin')
@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/settings/contacts/update')}}">
                        <div class="row">
                            @csrf
                            @include('admin.include._errors-form')
                            @include('admin.include._message-form')
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Url Facebook</label>
                                    <input type="url" name="url_facebook" class="form-control"
                                           placeholder="https://www.myurl.com"
                                           maxlength="255"
                                           value="{{!empty(old('url_facebook')) ? old('url_facebook') : $data->url_facebook}}"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Url Instagram</label>
                                    <input type="url" name="url_instagram" class="form-control"
                                           placeholder="https://www.myurl.com"
                                           maxlength="255"
                                           value="{{!empty(old('url_instagram')) ? old('url_instagram') : $data->url_instagram}}"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Url Twitter</label>
                                    <input type="url" name="url_twitter" class="form-control"
                                           placeholder="https://www.myurl.com"
                                           maxlength="255"
                                           value="{{!empty(old('url_twitter')) ? old('url_twitter') : $data->url_twitter}}"/>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-primary text-uppercase">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection



