@extends('admin.app-admin')
@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/settings/company/update')}}">
                        <div class="row">
                            @csrf
                            @include('admin.include._errors-form')
                            @include('admin.include._message-form')
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Ragione sociale*</label>
                                    <input type="text" name="company_name" class="form-control"
                                           placeholder="Ragione sociale"
                                           maxlength="100" required
                                           value="{{!empty(old('company_name')) ? old('company_name') : $data->company_name}}"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nazione*</label>
                                    <input type="text" name="company_country" class="form-control"
                                           placeholder="Nazione"
                                           maxlength="100" required
                                           value="{{!empty(old('company_country')) ? old('company_country') : $data->company_country}}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Città*</label>
                                    <input type="text" name="company_city" class="form-control"
                                           placeholder="Città"
                                           maxlength="100" required
                                           value="{{!empty(old('company_city')) ? old('company_city') : $data->company_city}}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Indirizzo*</label>
                                    <input type="text" name="company_address" class="form-control"
                                           placeholder="Indirizzo"
                                           maxlength="100" required
                                           value="{{!empty(old('company_address')) ? old('company_address') : $data->company_address}}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Cap*</label>
                                    <input type="text" name="company_zip" class="form-control"
                                           placeholder="Cap"
                                           maxlength="5" required
                                           value="{{!empty(old('company_zip')) ? old('company_zip') : $data->company_zip}}"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>P.IVA</label>
                                    <input type="text" name="company_vatcode" class="form-control"
                                           placeholder="P.IVA"
                                           maxlength="20"
                                           value="{{!empty(old('company_vatcode')) ? old('company_vatcode') : $data->company_vatcode}}"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>C.F.</label>
                                    <input type="text" name="company_fc" class="form-control"
                                           placeholder="C.F."
                                           maxlength="20"
                                           value="{{!empty(old('company_fc')) ? old('company_fc') : $data->company_fc}}"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Telefono*</label>
                                    <input type="text" name="phone" class="form-control"
                                           placeholder="Telefono"
                                           maxlength="20" required
                                           value="{{!empty(old('phone')) ? old('phone') : $data->phone}}"/>
                                </div>
                            </div>
                            <div class="col-md-12"><hr></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email ricezione*</label>
                                    <input type="email" name="email_to" class="form-control"
                                           placeholder="Email ricezione"
                                           maxlength="155" required
                                           value="{{!empty(old('email_to')) ? old('email_to') : $data->email_to}}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Email di invio*</label>
                                    <input type="email" name="email_from" class="form-control"
                                           placeholder="Email di invio"
                                           maxlength="155" required
                                           value="{{!empty(old('email_from')) ? old('email_from') : $data->email_from}}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Nome mittente*</label>
                                    <input type="text" name="email_from_name" class="form-control"
                                           placeholder="Nome mittente"
                                           maxlength="155" required
                                           value="{{!empty(old('email_from_name')) ? old('email_from_name') : $data->email_from_name}}"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Firma email*</label>
                                    <input type="text" name="email_sign" class="form-control"
                                           placeholder="Firma email"
                                           maxlength="155" required
                                           value="{{!empty(old('email_sign')) ? old('email_sign') : $data->email_sign}}"/>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-primary text-uppercase">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection



