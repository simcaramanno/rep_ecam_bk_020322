@extends('admin.app-admin')
@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/settings/config/api-keys/update')}}">
                        <div class="row">
                            @csrf
                            @include('admin.include._errors-form')
                            @include('admin.include._message-form')
                            <input type="hidden" name="id" value="{{$data->id}}">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Google API Key</label>
                                    <input type="text" name="google_apikey" class="form-control"
                                           placeholder="Google API Key"
                                           maxlength="255"
                                           value="{{!empty(old('google_apikey')) ? old('google_apikey') : $data->google_apikey}}"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Rechaptca API Key</label>
                                    <input type="text" name="rechaptca_apikey" class="form-control"
                                           placeholder="Rechaptca API Key"
                                           maxlength="255"
                                           value="{{!empty(old('rechaptca_apikey')) ? old('rechaptca_apikey') : $data->rechaptca_apikey}}"/>
                                </div>
                            </div>
                            <div class="col-md-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-primary text-uppercase">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection



