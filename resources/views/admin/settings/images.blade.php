@extends('admin.app-admin')
@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        @csrf
                        @include('admin.include._errors-form')
                        @include('admin.include._message-form')
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>
                                    Logo footer
                                    <a data-container="body" data-toggle="popover" data-placement="bottom"
                                       data-content="Logo in basso a sinistra nel footer">
                                        <i class="icon-info text-primary font-15"></i>
                                    </a>
                                </label>
                                @if(empty($data->main_logo))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->main_logo)}}"
                                             alt="." style="max-height:100px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="main_logo">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>(consigliato .svg, {{$GV->image_size_logo["footer"]["w"].'x'.$GV->image_size_logo["footer"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>
                                    Logo bianco
                                    <a data-container="body" data-toggle="popover" data-placement="bottom"
                                       data-content="Logo presente nell'header con sfondo scuro">
                                        <i class="icon-info text-primary font-15"></i>
                                    </a>
                                </label>
                                @if(empty($data->logo_light))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->logo_light)}}"
                                             alt="." style="max-height:100px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="logo_light">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>(consigliato .svg, {{$GV->image_size_logo["white_logo"]["w"].'x'.$GV->image_size_logo["white_logo"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>
                                    Logo bianco 2x
                                    <a data-container="body" data-toggle="popover" data-placement="bottom"
                                       data-content="Logo presente nell'header con sfondo scuro">
                                        <i class="icon-info text-primary font-15"></i>
                                    </a>
                                </label>
                                @if(empty($data->logo_light_2x))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->logo_light_2x)}}"
                                             alt="." style="max-height:100px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="logo_light_2x">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>(consigliato .svg, {{$GV->image_size_logo["white_logo"]["w"].'x'.$GV->image_size_logo["white_logo"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>
                                    Logo bianco 3x
                                    <a data-container="body" data-toggle="popover" data-placement="bottom"
                                       data-content="Logo presente nell'header con sfondo scuro">
                                        <i class="icon-info text-primary font-15"></i>
                                    </a>
                                </label>
                                @if(empty($data->logo_light_3x))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->logo_light_3x)}}"
                                             alt="." style="max-height:100px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="logo_light_3x">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>(consigliato .svg, {{$GV->image_size_logo["white_logo"]["w"].'x'.$GV->image_size_logo["white_logo"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>
                                    Logo dark
                                    <a data-container="body" data-toggle="popover" data-placement="bottom"
                                       data-content="Logo presente nell'header con sfondo chiaro">
                                        <i class="icon-info text-primary font-15"></i>
                                    </a>
                                </label>
                                @if(empty($data->logo_dark))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->logo_dark)}}"
                                             alt="." style="max-height:100px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="logo_dark">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>(consigliato .svg, {{$GV->image_size_logo["white_logo"]["w"].'x'.$GV->image_size_logo["white_logo"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>
                                    Logo dark 2x
                                    <a data-container="body" data-toggle="popover" data-placement="bottom"
                                       data-content="Logo presente nell'header con sfondo chiaro">
                                        <i class="icon-info text-primary font-15"></i>
                                    </a>
                                </label>
                                @if(empty($data->logo_dark_2x))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->logo_dark_2x)}}"
                                             alt="." style="max-height:100px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="logo_dark_2x">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>(consigliato .svg, {{$GV->image_size_logo["white_logo"]["w"].'x'.$GV->image_size_logo["white_logo"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>
                                    Logo dark 3x
                                    <a data-container="body" data-toggle="popover" data-placement="bottom"
                                       data-content="Logo presente nell'header con sfondo chiaro">
                                        <i class="icon-info text-primary font-15"></i>
                                    </a>
                                </label>
                                @if(empty($data->logo_dark_3x))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->logo_dark_3x)}}"
                                             alt="." style="max-height:100px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="logo_dark_3x">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>(consigliato .svg, {{$GV->image_size_logo["white_logo"]["w"].'x'.$GV->image_size_logo["white_logo"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>FAVICON 57x57px</label>
                                @if(empty($data->favicon57))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->favicon57)}}"
                                             alt="." style="height:57px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="favicon57">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>({{$GV->image_size_logo["favicon57"]["w"].'x'.$GV->image_size_logo["favicon57"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>FAVICON 72x72px</label>
                                @if(empty($data->favicon72))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->favicon72)}}"
                                             alt="." style="height:57px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="favicon72">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>({{$GV->image_size_logo["favicon72"]["w"].'x'.$GV->image_size_logo["favicon72"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12"><hr></div>
                        <div class="col-md-4 col-lg-4 col-sm-12">
                            <div class="form-group">
                                <label>FAVICON 114x114px</label>
                                @if(empty($data->favicon114))
                                    <p class="alert alert-danger">Non presente!</p>
                                @else
                                    <div class="col-md-12">
                                        <img src="{{asset('images/logo/'.$data->favicon114)}}"
                                             alt="." style="height:57px;">
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8 col-sm-12">
                            <form method="POST" action="{{url('admin/settings/images/update')}}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$data->id}}">
                                <input type="hidden" name="image_type" value="favicon114">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>
                                                Cambia immagine*<br>({{$GV->image_size_logo["favicon114"]["w"].'x'.$GV->image_size_logo["favicon114"]["h"].'px'}})
                                            </label>
                                            <input type="file" class="form-control" name="image" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
@endsection



