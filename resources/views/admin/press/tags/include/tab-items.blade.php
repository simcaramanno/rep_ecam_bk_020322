<div class="row">
    <div class="col-12">
        <div class="profile-menu theme-background border  z-index-1 p-2">
            <div class="d-sm-flex">
                <div class="align-self-center">
                    <ul class="nav nav-pills flex-column flex-sm-row" id="myTab" role="tablist">
                        <li class="nav-item ml-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/press/tags/edit-data/*')) active @endif"
                               href="{{url('admin/archive/press/tags/edit-data/'.$data->id)}}">
                                Generale
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="align-self-center ml-auto text-center text-sm-right">

                </div>
            </div>
        </div>
    </div>
</div>
