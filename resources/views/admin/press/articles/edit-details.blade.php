@extends('admin.app-admin')

@section('javascript_head')
<!-- Summernote -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.css" rel="stylesheet">
<style>
    .ck-editor__editable_inline {
        min-height: 400px;
    }

    .modal-backdrop, .modal-backdrop.in {
        display: none;
    }

    .note-modal {
        margin-top: 100px !important;
    }
</style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.press.articles.include.title-box')
                <div class="card-body">
                    @include('admin.press.articles.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/press/articles/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["post_text"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="descriptions">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Titolo*</label>
                                                    <input type="text" name="title" class="form-control"
                                                           placeholder="Titolo"
                                                           maxlength="255" required
                                                           value="{{!empty(old('title')) ? old('title') : $data_texts["post_text"]["title"]}}"/>
                                                </div>
                                            </div>
{{--                                            <div class="col-md-12">--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <label>Testo*</label>--}}
{{--                                                    <textarea name="post_content" class="form-control"--}}
{{--                                                              placeholder="Testo" rows="15"--}}
{{--                                                              required>{{!empty(old('post_content')) ? old('post_content') : $data_texts["post_text"]["content"]}}</textarea>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo*</label>
                                                    @php
                                                        $content = "";
                                                        if(!empty(old('post_content'))){
                                                         $content = html_entity_decode(old('post_content'));
                                                        } else {
                                                            $content = html_entity_decode($data_texts["post_text"]["content"]);
                                                        }
                                                    @endphp
                                                    <textarea id="summernote-content" name="post_content"
                                                              name="post_content">{{$content}}</textarea>
                                                </div>
                                            </div>



                                            <div class="col-md-12">
                                                <button type="submit" id="btnSubmit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                                <button type="submit" name="btnRemoveHtml" value="1" class="btn btn-info mr-2 float-right">
                                                    Rimuovi html
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <!--META-->
                                    <form method="POST" action="{{url('admin/archive/press/articles/update-details')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["post_text"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="meta">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>META</h6>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta title* </label>
                                                    <input type="text" name="meta_title" class="form-control"
                                                           placeholder="Meta title" required maxlength="155"
                                                           value="{{!empty(old('meta_title')) ? old('meta_title') : $data_texts["post_text"]["meta_title"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Description*</label>
                                                    <textarea name="meta_desc" class="form-control"
                                                              placeholder="Descrizione breve" maxlength="250" rows="3"
                                                              required>{{!empty(old('meta_desc')) ? old('meta_desc') : $data_texts["post_text"]["meta_desc"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Meta Keys*</label>
                                                    <textarea name="meta_keys" class="form-control"
                                                              placeholder="Descrizione Keys" maxlength="250" rows="3"
                                                              required>{{!empty(old('meta_keys')) ? old('meta_keys') : $data_texts["post_text"]["meta_keys"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>


                                    <!--TAGS-->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <h6>TAGS</h6>
                                        </div>
                                    </div>
                                    @if(!empty($all_tags) && sizeof($all_tags)>0)
                                        <form method="POST" action="{{url('admin/archive/press/articles/update-details')}}">
                                            <div class="row">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["post_text"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="add_tag">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Aggiungi tag* </label>
                                                        <select name="tag_id" class="form-control" required>
                                                            @foreach($all_tags as $tag)
                                                                <option value="{{$tag->id}}" @if(old('tag_id')==$tag->id) selected @endif>
                                                                    {{$tag->name}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary text-uppercase" style="margin-top:25px;">
                                                        Aggiungi
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    @endif
                                    @if(!empty($data_tags) && sizeof($data_tags)>0)
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul>
                                                    @foreach($data_tags as $tag)
                                                        <li>
                                                            {{$tag["tag_name"]}}
                                                            <a data-toggle="modal" data-target="#modalDeleteTag"
                                                                class="text-danger delete-item" style="cursor:pointer;"
                                                                data-itemid="{{$tag["id"]}}"
                                                                data-tagid="{{$tag["tag_id"]}}">
                                                                <i class="icon-trash"></i>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="alert alert-warning">
                                                    Non ci sono tags
                                                </div>
                                            </div>
                                        </div>
                                    @endif


                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    @if(!empty($data_tags) && sizeof($data_tags)>0)
        <div id="modalDeleteTag" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{url('admin/archive/press/articles/update-details')}}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["post_text"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="item_id" id="delete_item_id">
                <input type="hidden" name="tag_id" id="mod_tag_id">
                <input type="hidden" name="section" value="delete_tag">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Rimuovi</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di voler rimuovere questo tag?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif

@endsection


@section('javascript_footer')
    <!-- Summernote -->
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.16/dist/summernote.min.js"></script>
    <script>
        // delete-item
        $( ".delete-item" ).click(function() {
            var item_id = $(this).attr('data-itemid');
            $('#delete_item_id').val(item_id);
            var tagid = $(this).attr('data-tagid');
            $('#mod_tag_id').val(tagid);
        });


        // CONTENT Article
        if ($('#summernote-content').length) {
            $('#summernote-content').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    //['para', ['ul', 'ol', 'paragraph', 'style']],
                    ['para', ['paragraph', 'style']],
                    ['insert', ['link']],
                    ['view', ['codeview']]
                ],
                styleTags: ['p','h3'],
                placeholder: 'Inserisci testo',
                popover: {
                    image: [
                        ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                        ['remove', ['removeMedia']]
                    ],
                    link: [
                        ['link', ['linkDialogShow', 'unlink']]
                    ],
                    table: [
                        ['add', ['addRowDown', 'addRowUp', 'addColLeft', 'addColRight']],
                        ['delete', ['deleteRow', 'deleteCol', 'deleteTable']],
                    ],
                    air: [
                        ['color', ['color']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['para', ['ul', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']]
                    ]
                },
                callbacks: {
                    onChange: function (contents, $editable) {

                    }
                }
            });
        }
    </script>
@endsection

