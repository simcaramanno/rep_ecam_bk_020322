@extends('admin.app-admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/css/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/buttons/css/buttons.bootstrap4.min.css') }}"/>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.press.articles.include.title-box')
                <div class="card-body">
                    @include('admin.press.articles.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/press/articles/update-gallery')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["post_text"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="store_new_item">
                                            <div class="col-md-12">
                                                <label class="font-weight-800">Inserisci immagini</label>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>
                                                        Immagine miniatura ({{$GV->image_size_press["filename_small"]["w"].'x'.$GV->image_size_press["filename_small"]["h"].'px'}})*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Dettaglio articolo - Right sidebar: ultimi posts">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>
                                                        Immagine dettaglio ({{$GV->image_size_press["filename_big"]["w"].'x'.$GV->image_size_press["filename_big"]["h"].'px'}})*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Dettaglio articolo - Immagine principale o all'interno della gallery">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <input type="file" name="image2" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>
                                                        Immagine lista ({{$GV->image_size_press["filename_800x1010"]["w"].'x'.$GV->image_size_press["filename_800x1010"]["h"].'px'}})*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Pagina Press - Lista articoli">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <input type="file" name="image3" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Inserisci
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        @if(!empty($data_texts["gallery"]) && sizeof($data_texts["gallery"])>0)

                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table id="tab-press-gallery" class="display table dataTable table-striped table-bordered" >
                                                        <thead>
                                                        <tr>
                                                            <th>Miniatura</th>
                                                            <th>Dettaglio</th>
                                                            <th>Lista</th>
                                                            <th>Principale</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($data_texts["gallery"] as $_item)
                                                            <tr id="{{$_item->id}}">
                                                                <td>
                                                                    <a data-toggle="modal" data-target="#modalEditImage"
                                                                       data-itemid="{{$_item->id}}"
                                                                       data-image="filename_small"
                                                                       class="edit-item"
                                                                       style="cursor:pointer;">
                                                                        <img src="{{asset('images/press/'.$_item->filename_small)}}" alt="."
                                                                             style="max-width:100px;">
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a data-toggle="modal" data-target="#modalEditImage"
                                                                       data-itemid="{{$_item->id}}"
                                                                       data-image="filename_big"
                                                                       class="edit-item"
                                                                       style="cursor:pointer;">
                                                                        <img src="{{asset('images/press/'.$_item->filename_big)}}" alt="."
                                                                             style="max-width:100px;">
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a data-toggle="modal" data-target="#modalEditImage"
                                                                       data-itemid="{{$_item->id}}"
                                                                       data-image="filename_800x1010"
                                                                       class="edit-item"
                                                                       style="cursor:pointer;">
                                                                        <img src="{{asset('images/press/'.$_item->filename_800x1010)}}" alt="."
                                                                             style="max-width:100px;">
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    @if($_item->is_main==0)
                                                                        <a data-toggle="modal" data-target="#modalSetMainItem"
                                                                           data-itemid="{{$_item->id}}"
                                                                           class="main-item btn btn-sm btn-info text-white mb-1"
                                                                           style="cursor:pointer;">
                                                                            <small>Imposta come principale</small>
                                                                        </a>
                                                                    @else
                                                                        <i class="icon-check text-dark-gray font-15"></i>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <a data-toggle="modal" data-target="#modalDeleteItem"
                                                                       data-itemid="{{$_item->id}}"
                                                                       class="delete-item btn btn-sm btn-danger text-white mb-1"
                                                                       style="cursor:pointer;">
                                                                        <small>Elimina</small>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <th>Miniatura</th>
                                                            <th>Dettaglio</th>
                                                            <th>Lista</th>
                                                            <th>Principale</th>
                                                            <th></th>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-12">
                                                <div class="text-center">
                                                    <p>Non ci sono immagini</p>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    @if(!empty($data_texts["gallery"]) && sizeof($data_texts["gallery"])>0)
        <div id="modalDeleteItem" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/press/articles/update-gallery') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["post_text"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_item">
                <input type="hidden" name="item_id" id="delete_item_id">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Elimina</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di voler rimuovere questo elemento ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="modalEditImage" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/press/articles/update-gallery') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["post_text"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="edit_item">
                <input type="hidden" name="item_id" id="edit_item_id">
                <input type="hidden" name="image_type" id="edit_image">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title text-white">Modifica</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Cambia immagine*</label>
                                        <input type="file" class="form-control" required name="image">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="modalSetMainItem" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/press/articles/update-gallery') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["post_text"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="main_item">
                <input type="hidden" name="item_id" id="main_item_id">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-info">
                            <h5 class="modal-title text-white">Imposta come principale</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di voler impostare questo elemento come principale ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-info">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    @endif

@endsection


@section('javascript_vendor_footer')
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.print.min.js') }}"></script>
@endsection

@section('javascript_footer')
    <script src="{{ asset('theme-be/dist/js/app-datatable.script.js') }}"></script>

    <script>
        // delete-item
        $("#tab-press-gallery").on("click",".delete-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#delete_item_id').val(item_id);
        });
        // delete-item
        $("#tab-press-gallery").on("click",".edit-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            var image = $(this).attr('data-image');
            $('#edit_item_id').val(item_id);
            $('#edit_image').val(image);
        });
        // delete-item
        $("#tab-press-gallery").on("click",".main-item",function(e){
            e.preventDefault();
            var item_id = $(this).attr('data-itemid');
            $('#main_item_id').val(item_id);
        });


    </script>
@endsection

