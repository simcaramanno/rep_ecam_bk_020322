@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">
                        {{$layout["page_subtitle"]}}
                    </h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{url('admin/archive/press/articles/store')}}">
                        <div class="row">
                            @csrf
                            @include('admin.include._errors-form')
                            @include('admin.include._message-form')
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nome*</label>
                                    <input type="text" name="name" class="form-control"
                                           placeholder="Nome"
                                           maxlength="255" required
                                           value="{{old('name')}}"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Url link esterno</label>
                                    <input type="url" name="external_link" class="form-control"
                                           placeholder="https://www.google.com"
                                           maxlength="255"
                                           value="{{old('external_link')}}"/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Testata*</label>
                                    <input type="text" name="newspaper" class="form-control"
                                           placeholder="Testata"
                                           maxlength="255" required
                                           value="{{old('newspaper')}}"/>
                                </div>
                            </div>
                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                <div class="form-group">
                                    <label>Articolo in lingua araba*</label>
                                    <select class="form-control" required name="is_arabic">
                                        <option value="0" @if(old('is_arabic')=='0') selected @endif>
                                            no
                                        </option>
                                        <option value="1" @if(old('is_arabic')=='1') selected @endif>
                                            si
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 text-right">
                                <hr>
                                <a class="btn btn-outline-primary" href="{{ url('admin/archive/press/articles/index') }}">
                                    Annulla
                                </a>
                                <button type="submit" class="btn btn-primary text-uppercase" name="btn_save">
                                    Salva
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection



