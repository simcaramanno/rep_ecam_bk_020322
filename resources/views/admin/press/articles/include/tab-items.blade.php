<div class="row">
    <div class="col-12">
        <div class="profile-menu theme-background border  z-index-1 p-2">
            <div class="d-sm-flex">
                <div class="align-self-center">
                    <ul class="nav nav-pills flex-column flex-sm-row" id="myTab" role="tablist">
                        <li class="nav-item ml-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/press/articles/edit-data/*')) active @endif"
                               href="{{url('admin/archive/press/articles/edit-data/'.$data->id)}}">
                                Generale
                            </a>
                        </li>
                        <li class="nav-item ml-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/press/articles/edit-details/*')) active @endif"
                               href="{{url('admin/archive/press/articles/edit-details/'.$data->id)}}">
                                Contenuto
                            </a>
                        </li>
                        <li class="nav-item ml-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/press/articles/edit-gallery/*')) active @endif"
                               href="{{url('admin/archive/press/articles/edit-gallery/'.$data->id)}}">
                                Gallery
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="align-self-center ml-auto text-center text-sm-right">
                    @if(!Request::is('admin/archive/press/articles/edit-data/*'))
                        <a href="#" class="badge badge-info" title="Italiano">
                            <span class="text-uppercase">{{$lang}}</span>
                        </a>
                    @endif
                    <a href="{{url($lang.'/press/show-preview/'.$data->id.'/'.$data->slug)}}"
                       class="btn btn-sm btn-primary" target="_blank">
                        Preview
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
