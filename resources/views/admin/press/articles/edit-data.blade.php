@extends('admin.app-admin')

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.press.articles.include.title-box')
                <div class="card-body">
                    @include('admin.press.articles.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <form method="POST" action="{{url('admin/archive/press/articles/update-data')}}">
                                        <div class="row">
                                            @csrf
                                            @include('admin.include._errors-form')
                                            @include('admin.include._message-form')
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nome*</label>
                                                    <input type="text" name="name" class="form-control"
                                                           placeholder="Nome"
                                                           maxlength="255" required
                                                           value="{{!empty(old('name')) ? old('name') : $data->name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Url link esterno</label>
                                                    <input type="url" name="external_link" class="form-control"
                                                           placeholder="https://www.google.com"
                                                           maxlength="255"
                                                           value="{{!empty(old('external_link')) ? old('external_link') : $data->external_link}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testata*</label>
                                                    <input type="text" name="newspaper" class="form-control"
                                                           placeholder="Testata"
                                                           maxlength="255" required
                                                           value="{{!empty(old('newspaper')) ? old('newspaper') : $data->newspaper}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Articolo in lingua araba*</label>
                                                    <select class="form-control" required name="is_arabic">
                                                        @if(!empty(old('is_arabic')))
                                                            <option value="0" @if(old('is_arabic')=='0') selected @endif>
                                                                no
                                                            </option>
                                                            <option value="1" @if(old('is_arabic')=='1') selected @endif>
                                                                si
                                                            </option>
                                                        @else
                                                            <option value="0" @if($data->is_arabic=='0') selected @endif>
                                                                no
                                                            </option>
                                                            <option value="1" @if($data->is_arabic=='1') selected @endif>
                                                                si
                                                            </option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            @if($data->status=='active')
                                                <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Data Pubblicazione*</label>
                                                        <div class="d-flex">
                                                            <input name="published_at" id="published_at"
                                                                   placeholder="Dal" class="form-control" type="text" required
                                                                   value="<?php echo !empty(old('published_at')) ? date("d-m-Y", strtotime(old('published_at'))) : date("d-m-Y", strtotime($data->published_at)) ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="col-md-12">
                                                <hr>
                                                <a class="btn btn-danger text-white text-uppercase" data-toggle="modal" data-target="#modalDelete">
                                                    elimina
                                                </a>
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                                <a class="btn btn-outline-primary float-right mr-2" href="{{ url('admin/archive/press/articles/index') }}">
                                                    Annulla
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                            <h6>Cambia Status</h6>
                                        </div>
                                        <div class="col-md-12">
                                            @if($data->status=='active')
                                                <a class="btn btn-danger text-white text-uppercase" data-toggle="modal" data-target="#modalInactive">
                                                    Rendi bozza
                                                </a>
                                            @else
                                                @if($activable["is_activable"])
                                                    <a class="btn btn-success text-uppercase" data-toggle="modal" data-target="#modalActive">
                                                        Pubblica
                                                    </a>
                                                @else
                                                    <div class="alert alert-warning text-center">
                                                        <i class="icon-exclamation font-15"></i>
                                                        <p class="font-15">
                                                            Non puoi pubblicare questo Articolo.
                                                            <br>Inserisci tutte le informazioni mancanti:
                                                        </p>
                                                        <ul class="text-left">
                                                            @foreach($activable["info"] as $item)
                                                                <li>{{$item}}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <div id="modalDelete" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form method="POST" action="{{ url('admin/archive/press/articles/delete') }}">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-warning">
                        <h5 class="modal-title text-white">Elimina</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Sei sicuro di voler rimuovere questo Articolo ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-danger">Conferma</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


    @if($data->status=='active')
        <div id="modalInactive" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/press/articles/change-status') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="status" value="inactive">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Cambia status</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di rendere bozza questo Articolo ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @else
        @if($activable["is_activable"])
            <div id="modalActive" class="modal fade" tabindex="-1" role="dialog"
                 aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <form method="POST" action="{{ url('admin/archive/press/articles/change-status') }}">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <input type="hidden" name="status" value="active">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header bg-success">
                                <h5 class="modal-title text-white">Cambia status</h5>
                                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Sei sicuro di voler pubblicare questo Articolo ?</p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Data Pubblicazione*</label>
                                            <div class="d-flex">
                                                <input name="published_at" id="mod_published_at"
                                                       placeholder="Dal" class="form-control" type="text" required
                                                       value="<?php echo !empty($data->published_at) ? date("d-m-Y", strtotime($data->published_at)) : "" ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-success">Conferma</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        @endif
    @endif


@endsection


@section('javascript_footer')
    <script>
        // Date
        if($('#published_at').length){
            var startDate = flatpickr(document.getElementById('published_at'), {
                locale: "it",
                enableTime: false,
                dateFormat: "d-m-Y"
            });
        }
        if($('#mod_published_at').length){
            var startDate2 = flatpickr(document.getElementById('mod_published_at'), {
                locale: "it",
                enableTime: false,
                dateFormat: "d-m-Y"
            });
        }
    </script>
@endsection

