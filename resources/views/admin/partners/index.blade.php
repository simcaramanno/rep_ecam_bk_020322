@extends('admin.app-admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/css/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/buttons/css/buttons.bootstrap4.min.css') }}"/>
@endsection

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">{{$layout["page_subtitle"]}}</h4>
                </div>
                <div class="card-body">
                    <div class="row mb-1">
                        <div class="col-md-12 text-right">
                            <a href="{{url('admin/archive/partners/create')}}" class="btn btn-primary text-uppercase">
                                Nuovo
                            </a>
                        </div>
                    </div>
                    @if(!empty($data) && sizeof($data)>0)

                        <form method="POST" action="{{url('admin/archive/partners/sort-partners')}}">
                            <div class="row">
                                @csrf
                                <div class="col-md-12 text-right">
                                    <button type="submit"
                                            class="btn btn-sm btn-primary text-uppercase mb-2">
                                        Salva ordinamento
                                    </button>
                                    <div class="row mt-1">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>
                                </div>

                                <div class="col-md-12 mt-1">
                                    <div class="table-responsive">
                                        <table id="tab-partners" class="display table dataTable table-striped table-bordered" >
                                            <thead>
                                            <tr>
                                                <th hidden></th>
                                                <th style="max-width: 10%">Ordine</th>
                                                <th></th>
                                                <th>Nome</th>
                                                <th>Status</th>
                                                <th>Gestisci</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($data as $_item)
                                                <tr>
                                                    <td hidden>{{$_item->sorting}}</td>
                                                    <td data-order="{{$_item->sorting}}" style="max-width: 10%">
                                                        <input type="number" class="form-control bg-white"
                                                               name="sorting-{{$_item->id}}" required
                                                               value="{{$_item->sorting}}">
                                                    </td>
                                                    <td>
                                                        <img src="{{asset('images/partners/'.$_item->image)}}" alt="." style="max-height:50px;">
                                                    </td>
                                                    <td>{{$_item->name}}</td>
                                                    <td>
                                                        @if($_item->status=='active')
                                                            <span class="badge badge-success text-uppercase">attivo</span>
                                                        @else
                                                            <span class="badge badge-primary text-uppercase">inattivo</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{url('admin/archive/partners/edit-data/'.$_item->id)}}" class="btn btn-sm btn-warning text-white">
                                                            Gestisci
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th hidden></th>
                                                <th style="max-width: 10%">Ordine</th>
                                                <th></th>
                                                <th>Nome</th>
                                                <th>Status</th>
                                                <th>Gestisci</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </form>
                    @else
                        <div class="col-md-12">
                            <div class="alert alert-warning text-center">
                                Non ci sono dati
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection

@section('javascript_vendor_footer')
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.print.min.js') }}"></script>
@endsection

@section('javascript_footer')
    <script src="{{ asset('theme-be/dist/js/app-datatable.script.js') }}"></script>
@endsection



