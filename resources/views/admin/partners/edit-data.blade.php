@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.partners.include.title-box')
                <div class="card-body">
                    @include('admin.partners.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>
                                    <form method="POST" action="{{url('admin/archive/partners/update-data')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Nome*</label>
                                                    <input type="text" name="name" class="form-control"
                                                           placeholder="Nome"
                                                           maxlength="255" required
                                                           value="{{!empty(old('name')) ? old('name') : $data->name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Link*</label>
                                                    <input type="url" name="url" class="form-control"
                                                           placeholder="https://www.googl.com"
                                                           maxlength="255" required
                                                           value="{{!empty(old('url')) ? old('url') : $data->url}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                                <div class="form-group">
                                                    <label>Status*</label>
                                                    <select class="form-control" required name="status">
                                                        <option value="active" @if($data->status=='active') selected @endif>
                                                            attivo
                                                        </option>
                                                        <option value="inactive" @if($data->status=='inactive') selected @endif>
                                                            inattivo
                                                        </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <hr>
                                                <a class="btn btn-danger text-white text-uppercase" data-toggle="modal" data-target="#modalDelete">
                                                    elimina
                                                </a>
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                                <a class="btn btn-outline-primary float-right mr-2" href="{{ url('admin/archive/partners/index') }}">
                                                    Annulla
                                                </a>
                                            </div>
                                        </div>
                                    </form>

                                    <!--image -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine*
                                                </label>
                                                @if(empty($data->image))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/partners/'.$data->image)}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/partners/update-image')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_partners["image"]["w"].'x'.$GV->image_size_partners["image"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>



                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


    <div id="modalDelete" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form method="POST" action="{{ url('admin/archive/partners/delete') }}">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-warning">
                        <h5 class="modal-title text-white">Elimina</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Sei sicuro di voler rimuovere questo Partner ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-danger">Conferma</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection


@section('javascript_footer')
    <script>

    </script>
@endsection

