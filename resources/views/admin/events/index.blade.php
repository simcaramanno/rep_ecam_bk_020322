@extends('admin.app-admin')

@section('css')
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/css/dataTables.bootstrap4.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('theme-be/dist/vendors/datatable/buttons/css/buttons.bootstrap4.min.css') }}"/>
@endsection
@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">{{$layout["page_subtitle"]}}</h4>
                </div>
                <div class="card-body">
                    <div class="row mb-1">
                        <div class="col-md-12 text-right">
                            <a href="{{url('admin/archive/events/create')}}" class="btn btn-primary text-uppercase">
                                Nuovo
                            </a>
                        </div>
                    </div>
                    @if(!empty($data) && sizeof($data)>0)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="tab-events" class="display table dataTable table-striped table-bordered" >
                                        <thead>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Dal</th>
                                                <th>Al</th>
                                                <th>Stato</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $_item)
                                            <tr id="{{$_item->id}}" style="cursor:pointer;">
                                                <td>{{$_item->name}}</td>
                                                <td>{{date("d/m/y", strtotime($_item->date_start))}}</td>
                                                <td>{{date("d/m/y", strtotime($_item->date_end))}}</td>
                                                <td>
                                                    <!--if($_item->date_end>date('Y-m-d'))-->
                                                    @if(\App\Models\AppFunctions::get_event_is_upcoming_withtime($_item->date_start,$_item->time_start,$_item->date_end,$_item->time_end))
                                                        <small class="badge badge-info">UPCOMING</small>
                                                    @else
                                                        <small class="badge badge-secondary">PAST</small>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($_item->status=='active')
                                                        <span class="badge badge-success text-uppercase">attivo</span>
                                                    @else
                                                        <span class="badge badge-danger text-uppercase">inattivo</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Nome</th>
                                                <th>Dal</th>
                                                <th>Al</th>
                                                <th>Stato</th>
                                                <th>Status</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-md-12">
                            <div class="alert alert-warning text-center">
                                Non ci sono dati
                            </div>
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endsection

@section('javascript_vendor_footer')
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/jszip/jszip.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/pdfmake/vfs_fonts.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.colVis.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('theme-be/dist/vendors/datatable/buttons/js/buttons.print.min.js') }}"></script>
@endsection

@section('javascript_footer')
    <script src="{{ asset('theme-be/dist/js/app-datatable.script.js') }}"></script>
@endsection



