<div class="row">
    <div class="col-12">
        <div class="profile-menu theme-background border  z-index-1 p-2">
            <div class="d-sm-flex">
                <div class="align-self-center">
                    <ul class="nav nav-pills flex-column flex-sm-row" id="myTab" role="tablist">
                        <li class="nav-item ml-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/events/edit-data/*')) active @endif"
                               href="{{url('admin/archive/events/edit-data/'.$data->id)}}">
                                Generale
                            </a>
                        </li>
                        <li class="nav-item ml-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/events/edit-details/*')) active @endif"
                               href="{{url('admin/archive/events/edit-details/'.$data->id)}}">
                                Dettagli
                            </a>
                        </li>
                        <li class="nav-item ml-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/events/edit-program/*')) active @endif"
                               href="{{url('admin/archive/events/edit-program/'.$data->id)}}">
                                Programma
                            </a>
                        </li>
                        <li class="nav-item ml-0 mb-2 mb-sm-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/events/edit-speakers/*')) active @endif"
                               href="{{url('admin/archive/events/edit-speakers/'.$data->id)}}">
                                Speakers
                            </a>
                        </li>
                        <li class="nav-item ml-0 mb-2 mb-sm-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/events/edit-gallery/*')) active @endif"
                               href="{{url('admin/archive/events/edit-gallery/'.$data->id)}}">
                                Gallery
                            </a>
                        </li>
                        <li class="nav-item ml-0 mb-2 mb-sm-0">
                            <a class="nav-link  py-2 px-3 px-lg-4 @if(Request::is('admin/archive/events/edit-reports/*')) active @endif"
                               href="{{url('admin/archive/events/edit-reports/'.$data->id)}}">
                                Reports
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="align-self-center ml-auto text-center text-sm-right">
                    @if(!Request::is('admin/archive/events/edit-data/*'))
                        <a href="#" class="badge badge-info" title="Italiano">
                            <span class="text-uppercase">{{$lang}}</span>
                        </a>
                    @endif
                    <a href="{{url($lang.'/events/show-preview/'.$data->id.'/'.$data_texts["eventText"]["slug"])}}"
                       class="btn btn-sm btn-primary" target="_blank">
                        Preview
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
