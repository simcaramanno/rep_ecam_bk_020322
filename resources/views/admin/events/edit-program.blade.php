@extends('admin.app-admin')

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.events.include.title-box')
                <div class="card-body">
                    @include('admin.events.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/events/update-program')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="description">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>
                                                        Descrizione*
                                                        <a data-container="body" data-toggle="popover" data-placement="bottom"
                                                           data-content="Descrizione sopra alla tabella del programma evento [pagina dettaglio evento]">
                                                            <i class="icon-info text-primary font-15"></i>
                                                        </a>
                                                    </label>
                                                    <textarea name="program_desc" class="form-control"
                                                              placeholder="Descrizione" rows="4"
                                                              required>{{!empty(old('program_desc')) ? old('program_desc') : $data_texts["eventText"]["program_desc"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <form method="POST" action="{{url('admin/archive/events/update-program')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="upload_program">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>Banner download programma</h6>
                                            </div>
                                            @if(!empty($data->program_pdf))
                                                <div class="col-md-12">
                                                    <a class="btn btn-sm btn-success"
                                                       href="{{asset('images/events/programs/'.$data->program_pdf)}}" download=""
                                                       target="_blank">
                                                        Download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-white"
                                                       data-toggle="modal" data-target="#deletePdfProgram">
                                                        Rimuovi
                                                    </a>
                                                </div>
                                            @else
                                                <div class="col-md-12">
                                                    <p class="alert alert-danger">Non è stato caricato il file del programma</p>
                                                </div>
                                            @endif
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label>
                                                        Upload file PDF*
                                                    </label>
                                                    <input type="file" class="form-control" name="image" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="row">
                                        <div class="col-md-4 col-lg-4 col-sm-12">
                                            <div class="form-group">
                                                <label>
                                                    Immagine background banner*
                                                </label>
                                                @if(empty($data->bg_image_download))
                                                    <p class="alert alert-danger">Non presente!</p>
                                                @else
                                                    <div class="col-md-12">
                                                        <img src="{{asset('images/events/'.$data->bg_image_download)}}"
                                                             alt="." style="max-height:150px;">
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-lg-8 col-sm-12">
                                            <form method="POST" action="{{url('admin/archive/events/update-program')}}" enctype="multipart/form-data">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                                <input type="hidden" name="lang" value="{{$lang}}">
                                                <input type="hidden" name="section" value="upload_bg_image_banner">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Upload immagine ({{$GV->image_size_events["bg_image_download_banner"]["w"].'x'.$GV->image_size_events["bg_image_download_banner"]["h"].'px'}})*
                                                            </label>
                                                            <input type="file" class="form-control" name="image" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                            Salva
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/events/update-program')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="store_new_item">
                                            <div class="col-md-12">
                                                <hr>
                                                <h6>Dettaglio programma</h6>
                                            </div>

                                            <div class="col-md-12">
                                                <label class="font-weight-800">Inserisci nuova voce</label>
                                            </div>
                                            <div class="col-md-2 col-lg-2 col-sm-6">
                                                <div class="form-group">
                                                    <label>Giorno* </label>
                                                    <select class="form-control" required name="event_day_id">
                                                        @foreach($data_texts["days_program"] as $day_program)
                                                            <option value="{{$day_program["day"]->id}}">
                                                                {{date("d/m/y", strtotime($day_program["day"]->day_date))}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-sm-6">
                                                <div class="form-group">
                                                    <label>Ora da*</label>
                                                    <div class="d-flex">
                                                        <input name="time_from" id="time_from"
                                                               placeholder="Ora da" class="form-control" type="text" required
                                                               value="<?php echo !empty(old('time_from')) ? date("H:m", strtotime(old('time_from'))) : "" ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-sm-6">
                                                <div class="form-group">
                                                    <label>Ora a*</label>
                                                    <div class="d-flex">
                                                        <input name="time_to" id="time_to"
                                                               placeholder="Ora da" class="form-control" type="text" required
                                                               value="<?php echo !empty(old('time_to')) ? date("H:m", strtotime(old('time_to'))) : "" ?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-11">
                                                <div class="form-group">
                                                    <label>Testo*</label>
                                                    <input type="text" name="content1" class="form-control" required
                                                           placeholder="Testo" maxlength="500"
                                                           value="{{!empty(old('content1')) ? old('content1') : ""}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Inserisci
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="row">
                                        <div class="col-md-12">
                                            @foreach($data_texts["days_program"] as $day_program)
                                                <strong>{{date("d/m/y", strtotime($day_program["day"]->day_date))}}</strong>
                                                @if(!empty($day_program["details"]) && sizeof($day_program["details"])>0)
                                                    <ul>
                                                        @foreach($day_program["details"] as $detail)
                                                            <li>
                                                                @php
                                                                $_content = "";
                                                                if(!empty($detail->time_from)){
                                                                    $_content = date('h:i', strtotime($detail->time_from)).' – '.date('h:i a', strtotime($detail->time_to)).' | '.$detail->content1;
                                                                } else {
                                                                    $_content = $detail->content1;
                                                                }
                                                                @endphp
                                                                {{$_content}}
                                                                <a data-toggle="modal" data-target="#modalDeleteItem"
                                                                    data-itemid="{{$detail->id}}"
                                                                    class="delete-item"
                                                                    style="cursor:pointer;">
                                                                    <i class="icon-trash text-danger"></i>
                                                                </a>
                                                                <a data-toggle="modal" data-target="#modalEditItem"
                                                                   data-itemid="{{$detail->id}}"
                                                                   data-text="{{$detail->content1}}"
                                                                   data-timefrom="{{substr($detail->time_from,0,5)}}"
                                                                   data-timeto="{{substr($detail->time_to,0,5)}}"
                                                                   class="edit-item"
                                                                   style="cursor:pointer;">
                                                                    <i class="icon-pencil text-primary"></i>
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @else
                                                    <span class="text-danger">Non ci sono informazioni</span><br>
                                                @endif
                                            @endforeach
                                        </div>

                                    </div>

                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    @if(!empty($data_texts["days_program"]) && sizeof($data_texts["days_program"])>0)
        <div id="modalDeleteItem" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/events/update-program') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_item">
                <input type="hidden" name="item_id" id="delete_item_id">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Elimina</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di voler rimuovere questo elemento ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="modalEditItem" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/events/update-program') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="edit_item">
                <input type="hidden" name="item_id" id="edit_item_id">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h5 class="modal-title text-white">Modifica</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Ora da*</label>
                                        <div class="d-flex">
                                            <input name="time_from" id="edit_item_timefrom"
                                                   placeholder="Ora da" class="form-control" type="text" required
                                                   value="<?php echo !empty(old('time_from')) ? date("H:m", strtotime(old('time_from'))) : "" ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-lg-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Ora a*</label>
                                        <div class="d-flex">
                                            <input name="time_to" id="edit_item_timeto"
                                                   placeholder="Ora da" class="form-control" type="text" required
                                                   value="<?php echo !empty(old('time_to')) ? date("H:m", strtotime(old('time_to'))) : "" ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Testo*</label>
                                        <textarea name="content1" id="edit_item_content1" class="form-control" required
                                                  placeholder="Testo" rows="3"
                                                  maxlength="500"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-primary">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif

    @if(!empty($data->program_pdf))
        <div id="deletePdfProgram" class="modal fade" tabindex="-1" role="dialog"
             aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <form method="POST" action="{{ url('admin/archive/events/update-program') }}">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                <input type="hidden" name="lang" value="{{$lang}}">
                <input type="hidden" name="section" value="delete_pdf">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <h5 class="modal-title text-white">Elimina</h5>
                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Sei sicuro di voler rimuovere il programma ?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                            <button type="submit" class="btn btn-danger">Conferma</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    @endif

@endsection


@section('javascript_footer')
    <script>
        // Date
        var startDate = flatpickr(document.getElementById('time_from'), {
            locale: "it",
            enableTime: true,
            noCalendar:true,
            dateFormat: "H:i"
        });
        flatpickr(document.getElementById('time_to'), {
            enableTime: true,
            noCalendar:true,
            dateFormat: "H:i"
        });
        startDate.config.onChange.push(function (selectedDates, dateStr, instance) {
            flatpickr(document.getElementById('time_to'), {
                locale: "it",
                enableTime: true,
                noCalendar:true,
                dateFormat: "H:i",
                minDate: dateStr
            });
        });


        // Date
        var startDate2 = flatpickr(document.getElementById('edit_item_timefrom'), {
            locale: "it",
            enableTime: true,
            noCalendar:true,
            dateFormat: "H:i"
        });
        flatpickr(document.getElementById('edit_item_timeto'), {
            enableTime: true,
            noCalendar:true,
            dateFormat: "H:i"
        });
        startDate2.config.onChange.push(function (selectedDates, dateStr, instance) {
            flatpickr(document.getElementById('edit_item_timeto'), {
                locale: "it",
                enableTime: true,
                noCalendar:true,
                dateFormat: "H:i",
                minDate: dateStr
            });
        });

        // delete-item
        $( ".delete-item" ).click(function() {
            var item_id = $(this).attr('data-itemid');
            $('#delete_item_id').val(item_id);
        });
        // edit-item
        $( ".edit-item" ).click(function() {
            var item_id = $(this).attr('data-itemid');
            $('#edit_item_id').val(item_id);
            var datatext = $(this).attr('data-text');
            $('#edit_item_content1').val(datatext);
            var timefrom = $(this).attr('data-timefrom');
            $('#edit_item_timefrom').val(timefrom);
            var timeto = $(this).attr('data-timeto');
            $('#edit_item_timeto').val(timeto);
        });
    </script>
@endsection
