@extends('admin.app-admin')

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                @include('admin.events.include.title-box')
                <div class="card-body">
                    @include('admin.events.include.tab-items')

                    <div class="row mt-3">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @include('admin.include._errors-form')
                                        @include('admin.include._message-form')
                                    </div>

                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="counters">
                                            <div class="col-md-12">
                                                <h6>Counters [pagina dettaglio evento]</h6>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label>Testo 1</label>
                                                    <input type="text" name="counter1_txt" class="form-control bg-light-gray"
                                                           placeholder="Testo 1"
                                                           maxlength="100"
                                                           value="{{!empty(old('counter1_txt')) ? old('counter1_txt') : $data_texts["eventText"]["counter1_txt"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Numero 1</label>
                                                    <input type="number" name="counter1_num" class="form-control"
                                                           placeholder="Numero 1"
                                                           value="{{!empty(old('counter1_num')) ? old('counter1_num') : $data_texts["eventText"]["counter1_num"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label>Testo 2</label>
                                                    <input type="text" name="counter2_txt" class="form-control bg-light-gray"
                                                           placeholder="Testo 2"
                                                           maxlength="100"
                                                           value="{{!empty(old('counter2_txt')) ? old('counter2_txt') : $data_texts["eventText"]["counter2_txt"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Numero 2</label>
                                                    <input type="number" name="counter2_num" class="form-control"
                                                           placeholder="Numero 2"
                                                           value="{{!empty(old('counter2_num')) ? old('counter2_num') : $data_texts["eventText"]["counter2_num"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label>Testo 3</label>
                                                    <input type="text" name="counter3_txt" class="form-control bg-light-gray"
                                                           placeholder="Testo 3"
                                                           maxlength="100"
                                                           value="{{!empty(old('counter3_txt')) ? old('counter3_txt') : $data_texts["eventText"]["counter3_txt"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Numero 3</label>
                                                    <input type="number" name="counter3_num" class="form-control"
                                                           placeholder="Numero 3"
                                                           value="{{!empty(old('counter3_num')) ? old('counter3_num') : $data_texts["eventText"]["counter3_num"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <h6>Reports [pagina dettaglio evento]</h6>
                                        </div>
                                    </div>
                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="descriptions">
                                            <div class="col-md-12">
                                                <label><strong>Box descrittivo a sinistra della lista dei reports</strong></label>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Titoletto*</label>
                                                    <input type="text" name="title_report" class="form-control bg-light-gray"
                                                           placeholder="Testo" required
                                                           value="{{!empty(old('title_report')) ? old('title_report') : $data_texts["event"]["title_report"]}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Titolo*</label>
                                                    <textarea name="subtitle_report" class="form-control"
                                                              placeholder="Testo" rows="4"
                                                              required>{{!empty(old('subtitle_report')) ? old('subtitle_report') : $data_texts["event"]["subtitle_report"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Descrizione*</label>
                                                    <textarea name="content_report" class="form-control"
                                                              placeholder="Testo" rows="4"
                                                              required>{{!empty(old('content_report')) ? old('content_report') : $data_texts["event"]["content_report"]}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" name="btn_save">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report1">
                                            <div class="col-md-12">
                                                <hr>
                                                <label><strong>REPORT 1</strong></label>
                                                @if(empty($data->report1_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report1_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="1"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report1_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report1_name')) ? old('report1_name') : $data->report1_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report2">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 2</strong></label>
                                                @if(empty($data->report2_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report2_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="2"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report2_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report2_name')) ? old('report2_name') : $data->report2_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report3">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 3</strong></label>
                                                @if(empty($data->report3_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report3_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="3"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report3_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report3_name')) ? old('report3_name') : $data->report3_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report4">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 4</strong></label>
                                                @if(empty($data->report4_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report4_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="4"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report4_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report4_name')) ? old('report4_name') : $data->report4_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report5">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 5</strong></label>
                                                @if(empty($data->report5_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report5_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="5"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report5_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report5_name')) ? old('report5_name') : $data->report5_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report6">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 6</strong></label>
                                                @if(empty($data->report6_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report6_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="6"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report6_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report6_name')) ? old('report6_name') : $data->report6_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report7">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 7</strong></label>
                                                @if(empty($data->report7_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report7_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="7"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report7_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report7_name')) ? old('report7_name') : $data->report7_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report8">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 8</strong></label>
                                                @if(empty($data->report8_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report8_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="8"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report8_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report8_name')) ? old('report8_name') : $data->report8_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report9">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 9</strong></label>
                                                @if(empty($data->report9_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report9_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="9"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report9_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report9_name')) ? old('report9_name') : $data->report9_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>

                                    <form method="POST" action="{{url('admin/archive/events/update-reports')}}" enctype="multipart/form-data">
                                        <div class="row">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$data->id}}">
                                            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
                                            <input type="hidden" name="lang" value="{{$lang}}">
                                            <input type="hidden" name="section" value="report10">
                                            <div class="col-md-12">
                                                <label><strong>REPORT 10</strong></label>
                                                @if(empty($data->report10_link))
                                                    <br><small class="badge badge-info">non presente</small>
                                                @else
                                                    <br>
                                                    <a href="{{url('images/events/reports/'.$data->report10_link)}}" download=""
                                                       class="btn btn-sm btn-success text-uppercase">
                                                        download
                                                    </a>
                                                    <a class="btn btn-sm btn-danger text-uppercase text-white delete-item"
                                                       data-toggle="modal" data-target="#modalDeleteItem"
                                                       data-itemid="10"
                                                       style="cursor:pointer;">
                                                        rimuovi
                                                    </a>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Testo</label>
                                                    <input type="text" name="report10_name" class="form-control"
                                                           placeholder="Testo" maxlength="255"
                                                           value="{{!empty(old('report10_name')) ? old('report10_name') : $data->report10_name}}"/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>File PDF:</label>
                                                    <input type="file" name="image" class="form-control" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" class="btn btn-primary text-uppercase float-right" style="margin-top:25px;">
                                                    Salva
                                                </button>
                                            </div>
                                        </div>
                                    </form>


                                </div><!--end card-body-->

                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div id="modalDeleteItem" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <form method="POST" action="{{ url('admin/archive/events/update-reports') }}">
            @csrf
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="hidden" name="text_id" value="{{$data_texts["eventText"]["id"]}}">
            <input type="hidden" name="lang" value="{{$lang}}">
            <input type="hidden" name="section" value="delete_item">
            <input type="hidden" name="item_id" id="delete_item_id">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h5 class="modal-title text-white">Elimina</h5>
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Chiudi">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Sei sicuro di voler rimuovere questo report ?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-danger">Conferma</button>
                    </div>
                </div>
            </div>
        </form>
    </div>


@endsection


@section('javascript_footer')
    <script>
        // delete-item
        $( ".delete-item" ).click(function() {
            var item_id = $(this).attr('data-itemid');
            $('#delete_item_id').val(item_id);
        });
    </script>
@endsection

