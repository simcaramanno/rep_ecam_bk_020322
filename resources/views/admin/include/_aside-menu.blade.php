
<ul id="side-menu" class="sidebar-menu">
    <li class="dropdown @if(Request::is('admin/contact-requests/*')) active @endif">
        <a href="#"><i class="icon-user"></i> Utenti</a>
        <ul>
            <li class="@if(Request::is('admin/contact-requests/*')) active @endif">
                <a href="{{ url('admin/contact-requests/index') }}">
                    <i class="icon-envelope"></i> Richieste contatto
                </a>
            </li>
            <li class="@if(Request::is('admin/staff/users/*')) active @endif">
                <a href="{{ url('admin/staff/users/index') }}">
                    <i class="icon-user"></i> Staff
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Request::is('admin/archive/events/*') || Request::is('admin/archive/projects/*')
        || Request::is('admin/archive/partners/*') || Request::is('admin/archive/press/*')) active @endif">
        <a href="#"><i class="icon-folder-alt"></i> Archivio</a>
        <ul>
            <li class="@if(Request::is('admin/archive/events/*')) active @endif">
                <a href="{{ url('admin/archive/events/index') }}">
                    <i class="icon-calendar"></i> Eventi
                </a>
            </li>
            <li class="@if(Request::is('admin/archive/projects/*')) active @endif">
                <a href="{{ url('admin/archive/projects/index') }}">
                    <i class="icon-puzzle"></i> Progetti
                </a>
            </li>
            <li class="@if(Request::is('admin/archive/partners/*')) active @endif">
                <a href="{{ url('admin/archive/partners/index') }}">
                    <i class="icon-people"></i> Partners
                </a>
            </li>
            <li class="dropdown @if(Request::is('admin/archive/press/*')) active @endif">
                <a href="#"><i class="icon-book-open"></i> Press</a>
                <ul class="sub-menu">
                    <li class="@if(Request::is('admin/archive/press/articles/*')) active @endif">
                        <a href="{{ url('admin/archive/press/articles/index') }}">
                            Articoli
                        </a>
                    </li>
{{--                    <li class="@if(Request::is('admin/archive/press/categories/*')) active @endif">--}}
{{--                        <a href="{{ url('admin/archive/press/categories/index') }}">--}}
{{--                            Testate--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li class="@if(Request::is('admin/archive/press/tags/*')) active @endif">
                        <a href="{{ url('admin/archive/press/tags/index') }}">
                            Tags
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(Request::is('admin/cms/*')) active @endif">
        <a href="#"><i class="icon-pencil"></i> CMS</a>
        <ul>
            <li class="@if(Request::is('admin/cms/menu/*')) active @endif">
                <a href="{{ url('admin/cms/menu/index') }}">
                    Menu
                </a>
            </li>
            <li class="@if(Request::is('admin/cms/pages/*')) active @endif">
                <a href="{{ url('admin/cms/pages/index') }}">
                    Pagine
                </a>
            </li>
            <li class="@if(Request::is('admin/cms/sections/*')) active @endif">
                <a href="{{ url('admin/cms/sections/index') }}">
                    Sezioni
                </a>
            </li>
        </ul>
    </li>
    <li class="dropdown">
        <a href="#"><i class="icon-settings mr-1"></i> Impostazioni</a>
        <ul>
            <li class="dropdown @if(Request::is('admin/settings/*')) active @endif">
                <a href="#"><i class="icon-puzzle"></i>Generali</a>
                <ul class="sub-menu">
                    <li class="@if(Request::is('admin/settings/company/*')) active @endif">
                        <a href="{{ url('admin/settings/company/index') }}">
                            <i class="icon-energy"></i> Dati Azienda
                        </a>
                    </li>
                    <li class="@if(Request::is('admin/settings/contacts/*')) active @endif">
                        <a href="{{ url('admin/settings/contacts/index') }}">
                            <i class="icon-disc"></i> Dati contatto
                        </a>
                    </li>
                    <li class="@if(Request::is('admin/settings/images/*')) active @endif">
                        <a href="{{ url('admin/settings/images/index') }}">
                            <i class="icon-disc"></i> Loghi
                        </a>
                    </li>
                </ul>
            </li>
            <li class="dropdown @if(Request::is('admin/settings/config/*')) active @endif">
                <a href="#"><i class="icon-calculator"></i>Configurazioni</a>
                <ul class="sub-menu">
                    <li class="@if(Request::is('admin/settings/config/api-keys/*')) active @endif">
                        <a href="{{ url('admin/settings/config/api-keys/index') }}">
                            API Keys
                        </a>
                    </li>
                    <li class="@if(Request::is('admin/settings/config/scripts/*')) active @endif">
                        <a href="{{ url('admin/settings/config/scripts/index') }}">
                            Scripts
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li>
</ul>
