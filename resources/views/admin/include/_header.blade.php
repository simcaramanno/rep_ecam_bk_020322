
<div id="header-fix" class="header fixed-top">
    <div class="site-width">
        <nav class="navbar navbar-expand-lg  p-0">
            <div class="navbar-header  h-100 h4 mb-0 align-self-center logo-bar text-left">
                <a href="#" class="horizontal-logo text-left">
                    <img id="top-menu-logo" src="{{ asset('images/logo/'.$platform->main_logo) }}" height="30" alt="{{$platform->site_name}}">
                    <img id="top-menu-logo-icon" style="display: none;" src="{{ asset('images/logo/'.$platform->logo32) }}" height="32" alt="{{$platform->site_name}}">
                </a>
            </div>
            <div class="navbar-header h4 mb-0 text-center h-100 collapse-menu-bar">
                <a href="#" class="sidebarCollapse" id="collapse"><i class="icon-menu"></i></a>
            </div>

            <div class="navbar-right ml-auto h-100">
                <ul class="ml-auto p-0 m-0 list-unstyled d-flex top-icon h-100">
                    <li class="dropdown user-profile align-self-center d-inline-block">
                        <a href="#" class="nav-link py-0" data-toggle="dropdown" aria-expanded="false">
                            <div class="media">
                                <img src="{{ asset('images/user-avatar.png') }}" alt="" class="d-flex img-fluid rounded-circle" width="29">
                            </div>
                        </a>
                        <div class="dropdown-menu border dropdown-menu-right p-0">
                            <a href="{{ url('admin/account/index') }}" class="dropdown-item px-2 align-self-center d-flex">
                                <span class="icon-user mr-2 h6 mb-0"></span> Account
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ url('home/index') }}" target="_blank" class="dropdown-item px-2 align-self-center d-flex">
                                <span class="icon-globe mr-2 h6 mb-0"></span> Vai al sito
                            </a>
                            <div class="dropdown-divider"></div>
                            <a onclick="$('#btn-logout').click()" style="cursor: pointer;" class="dropdown-item px-2 text-danger align-self-center d-flex">
                                <span class="icon-logout mr-2 h6  mb-0"></span> Esci
                            </a>
                            <form method="POST" action="{{ url('admin/auth/logout') }}" hidden>@csrf <button type="submit" id="btn-logout"></button></form>
                        </div>

                    </li>

                </ul>
            </div>
        </nav>
    </div>
</div>
