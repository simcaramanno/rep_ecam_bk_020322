@if (Session::has('message'))
    <div class="col-md-12 text-center ">
        <div class="alert alert-success">
            <span class="text-dark">{{ e(Session::get('message')) }}</span>
        </div>
    </div>
@endif
