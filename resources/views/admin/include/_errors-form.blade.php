@if (Session::has('error'))
<div class="col-md-12 text-center ">
    <div class="alert alert-danger">
        <span class="text-danger">{{ e(Session::get('error')) }}</span>
    </div>
</div>
@endif
@if ($errors->any())
<div class="col-md-12 text-center ">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li class="text-danger">{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

