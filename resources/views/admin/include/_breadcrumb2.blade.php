
<div class="row">
    <div class="col-12  align-self-center">
        <div class="sub-header mt-3 py-3 align-self-center d-sm-flex w-100 rounded">
            <div class="w-sm-100 mr-auto">
                <h4 class="mb-0 text-primary">{{$layout["page_title"]}}</h4>
                @if(!empty($layout["page_subtitle"]))
                    <p>{{$layout["page_subtitle"]}}</p>
                @endif
            </div>

            <ol class="breadcrumb bg-transparent align-self-center m-0 p-0" hidden>
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
</div>
