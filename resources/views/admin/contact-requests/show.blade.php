@extends('admin.app-admin')

@section('css')

@endsection

@section('javascript_head')

@endsection

@section('content')
    <div class="row">
        <div class="col-12 mt-3">
            <div class="card">
                <div class="card-header justify-content-between align-items-center bg-primary text-white">
                    <h4 class="card-title">{{$layout["page_subtitle"]}}</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <tbody>
                                        <tr>
                                            <td class="text-uppercase">Data</td>
                                            <th>{{date("d/m/y", strtotime($data->created_at))}}</th>
                                        </tr>
                                        <tr>
                                            <td class="text-uppercase">Nome</td>
                                            <th>{{$data->name}}</th>
                                        </tr>
                                        <tr>
                                            <td class="text-uppercase">Telefono</td>
                                            <th>@if(!empty($data->phone)) {{$data->phone}} @else - @endif</th>
                                        </tr>
                                        <tr>
                                            <td class="text-uppercase">Email</td>
                                            <th>
                                                <a href="mailto:{{$data->email}}" target="_blank" class="text-primary">
                                                    {{$data->email}}
                                                </a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td class="text-uppercase">Messaggio</td>
                                            <td>
                                                <p>{{$data->message}}</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('javascript_vendor_footer')

@endsection

@section('javascript_footer')

@endsection



