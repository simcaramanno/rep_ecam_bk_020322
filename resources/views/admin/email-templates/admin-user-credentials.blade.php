<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//IT" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
@include('admin.email-templates._head')

<body>
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="background-color:#ffffff;">
                    <tr>
                        <td class="content-wrap aligncenter">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                @include('admin.email-templates._header')
                                <tr>
                                    <td class="content-block">
                                        <h2 style="color:#000;">{{$all_data_arr["site_name"]}} - Pannello Admin</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <p style="text-align: left !important;color:#000;">
                                            Gentile {{$all_data_arr["user"]["name"]}} {{$all_data_arr["user"]["surname"]}},<br>
                                            di seguito le credenziali per accedere all'area riservata di {{$all_data_arr["site_name"]}}:
                                        </p>
                                        <ul style="text-align: left !important;color:#000;">
                                            <li>Email: {{$all_data_arr["user"]["email"]}}</li>
                                            <li>Password: {{$all_data_arr["user"]["password"]}}</li>
                                        </ul>
                                        <a href="{{$all_data_arr["login_url"]}}" style="text-transform: none;color:#000;">
                                            Accedi
                                        </a>
                                    </td>
                                </tr>
                                @include('admin.email-templates._sign')
                            </table>
                        </td>
                    </tr>
                </table>
                @include('admin.email-templates._footer')
            </div>
        </td>
        <td></td>
    </tr>
</table>

</body>
</html>


