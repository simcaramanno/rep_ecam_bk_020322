<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//IT" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
@include('admin.email-templates._head')

<body>
<table class="body-wrap">
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content">
                <table class="main" width="100%" cellpadding="0" cellspacing="0" style="background-color:#ffffff;">
                    <tr>
                        <td class="content-wrap aligncenter">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                @include('admin.email-templates._header')
                                <tr>
                                    <td class="content-block">
                                        <h2 style="color:#000;">New contact request</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <p style="text-align: left !important;color:#000;">
                                            Contact request details:
                                        </p>
                                        <ul style="text-align: left !important;color:#000;">
                                            <li>NAME: {{$all_data_arr["message"]["name"]}}</li>
                                            <li>EMAIL: {{$all_data_arr["message"]["email"]}}</li>
                                            @if(!empty($all_data_arr["message"]["phone"]))
                                                <li>PHONE: {{$all_data_arr["message"]["phone"]}}</li>
                                            @endif
                                            <li>MESSAGE:<br>{{$all_data_arr["message"]["message"]}}</li>
                                        </ul>
                                    </td>
                                </tr>
                                @include('admin.email-templates._sign')
                            </table>
                        </td>
                    </tr>
                </table>
                @include('admin.email-templates._footer')
            </div>
        </td>
        <td></td>
    </tr>
</table>

</body>
</html>


