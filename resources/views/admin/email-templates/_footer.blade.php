<?php $platform = App\Models\Platform::first(); ?>
<div class="footer" style="border-top:1px solid #fabc36;">
    <table width="100%">
        <tr>
            <td class="aligncenter content-block">
                Copyright &copy; <?php echo date("Y"); ?> | All right reserved.
                <a href="{{$platform->fe_url_root}}" class="text-primary" style="text-decoration: none;">
                    <strong>{{$platform->company_name}}</strong>
                </a>
                <br>
                <small>
                    <a target="_blank"  class="text-primary"
                       href="mailto: {{$platform->email_to}}"
                       style="text-decoration: none;">
                        {{$platform->email_to}}
                    </a>
                </small>
                <br>
                @if(!empty($platform->phone))
                    <small class="text-primary" style="text-decoration: none;font-size:11px;">
                        {{$platform->phone}}
                    </small>
                @endif
            </td>
        </tr>
    </table>
</div>

