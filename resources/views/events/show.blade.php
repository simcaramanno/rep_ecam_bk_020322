@extends('app')

@section('content')

    @if(!empty($page_data["sections"]) && sizeof($page_data["sections"])>0)
        @foreach($page_data["sections"] as $section)
            @include('include.sections.'.$section["section"]["blade_name"])
        @endforeach
    @endif

@endsection