<!doctype html>
<html class="no-js" lang="{{$layout["lang"]}}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1"/>
        <title>{{$layout["page"]["title"]}}</title>
        <meta name="description" content="{{html_entity_decode($layout["page"]["meta_desc"], ENT_QUOTES)}}">
        <meta name="keywords" content="{{html_entity_decode($layout["page"]["meta_keys"], ENT_QUOTES)}}"/>
        <meta name="author" content="{{$platform->site_name}}">
        <!-- favicon icon -->
        <link rel="shortcut icon" href="{{asset('images/logo/'.$platform->favicon)}}">
        <link rel="apple-touch-icon" href="{{asset('images/logo/'.$platform->favicon57)}}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{asset('images/logo/'.$platform->favicon72)}}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{asset('images/logo/'.$platform->favicon114)}}">
        <!-- style sheets and font icons  -->
        <link rel="stylesheet" type="text/css" href="{{asset('theme-fe/css/font-icons.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('theme-fe/css/theme-vendors.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('theme-fe/css/style.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('theme-fe/css/responsive.css')}}"/>
        <meta name="lang" content="{{$layout["lang"]}}">
        <meta name="_token" content="{!! csrf_token() !!}"/>
    <!--<script src="https://www.google.com/recaptcha/api.js?render=$platform->rechaptca_apikey"></script>

            <script>
            function onClick(e) {
                e.preventDefault();
                    grecaptcha.ready(function() {
    {{--                grecaptcha.execute('{{$platform->rechaptca_apikey}}', {action: 'submit'}).then(function(token) {--}}

                    });
                });
            }
           </script>

             Facebook Pixel Code -->
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '334361684530147');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1" width="1" src="https://www.facebook.com/tr?id=334361684530147&ev=PageView &noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-P6PDQ3Z');</script>
        <!-- End Google Tag Manager -->
        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P6PDQ3Z"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
        @yield('css')

        @yield('javascript_head')
    </head>
    <body data-mobile-nav-style="classic">
        <!-- header -->
        @include('include.header')

        <!-- page content -->
        @yield('content')

        <!-- footer -->
        @include('include.footer')

        <!-- start scroll to top -->
        <a class="scroll-top-arrow" href="javascript:void(0);"><i class="feather icon-feather-arrow-up"></i></a>
        <!-- end scroll to top -->

        {{--@include('cookieConsent::index')--}}
        @include('cookies-banner.index')

        <!-- javascript -->
        <script type="text/javascript" src="{{asset('theme-fe/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('theme-fe/js/theme-vendors.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('theme-fe/js/main.js')}}"></script>
        <script>
            var URL_BASE = "<?php echo e(url('/')); ?>";
            $(document).ready(function () {
                $('.image-hover').hover(function () {
                    var src = $(this).attr('data-imghover');
                    $(this).attr("src", src);
                });
                $(".image-hover").mouseleave(function () {
                    var src = $(this).attr('data-imgbase');
                    $(this).attr("src", src);
                });

                // Projects
                $('.project-item').click(function () {
                    var src = $(this).attr('data-image');
                    $('#latest-projects-image-box').css("background-image", "url(" + src +")");
                });

                // text-yellow-hover color:#fabc36 !important;
                $('.text-project-yello-hover2').hover(function () {
                    $("#project-prev-box0").css("color", "#fabc36 !important");
                    $("#project-prev-box1").css("color", "#fabc36 !important");
                    $("#project-prev-box2").css("color", "#fabc36 !important");
                });
                $(".text-project-yello-hover2").mouseleave(function(){
                    $("#project-prev-box0").css("color", "#250c0c !important");
                    $("#project-prev-box1").css("color", "#250c0c !important");
                    $("#project-prev-box2").css("color", "#250c0c !important");
                });

                // text-yellow-hover color:#fabc36 !important;
                $('.text-project-yello-hover3').hover(function () {
                    $("#project-next-box0").css("color", "#fabc36 !important");
                    $("#project-next-box1").css("color", "#fabc36 !important");
                    $("#project-next-box2").css("color", "#fabc36 !important");
                });
                $(".text-project-yello-hover3").mouseleave(function(){
                    $("#project-next-box0").css("color", "#250c0c !important");
                    $("#project-next-box1").css("color", "#250c0c !important");
                    $("#project-next-box2").css("color", "#250c0c !important");
                });


            })
        </script>

    </body>
</html>

