@php
    $cookieExists = false;
    if (\Illuminate\Support\Facades\Cookie::get(config('cookie-consent.cookie_name')) !== null){
        $cookieExists = true;
    }
@endphp
@if(!$cookieExists)

    <div class="js-cookie-consent cookie-consent "
         style="display: flex;flex-direction: column;align-items: center;
     justify-content: space-around; padding-top: 30px; padding-bottom: 30px;align-content: space-between;
     left: 0;right: 0;z-index: 99999;padding: 20px 0;
     background-color: #250c0c;color: #ffffff;padding: 20px 0;padding-left: 8%;padding-right: 8%;
     overflow: hidden; position: fixed;  bottom: 0; width: 100%;">
        <p class="cookie-consent__message" style="margin-bottom:0px;text-align:center; padding-bottom:10px;">
            This site uses cookies to improve the browsing experience of users and to gather information on the use of the
            site. We use both technical cookies and third-party cookies to send promotional messages based on user behavior.
            You can know check the details by consulting our
            <a class="cookie-container-a" href="{{url($layout["lang"].'/cookies/index')}}" target="_blank">cookie policy</a> and
            <a class="cookie-container-a" href="{{url($layout["lang"].'/privacy/index')}}" target="_blank">privacy policy</a>.
        </p>
        <p class="cookie-button-p">
            <button class="js-cookie-consent-agree cookie-consent__agree cookie-btn">
                Accept all
            </button>
            <button class="cookie-btn-2 js-cookie-consent-agree cookie-consent__agree">
                Disagree
            </button>
        </p>
    </div>

    <script>

        window.laravelCookieConsent = (function () {


            const COOKIE_VALUE = 1;
            const COOKIE_DOMAIN = '{{ config('session.domain') ?? request()->getHost() }}';

            function consentWithCookies() {
                setCookie('{{config('cookie-consent.cookie_name')}}', COOKIE_VALUE, {{config('cookie-consent.cookie_lifetime')}});
                hideCookieDialog();
            }

            function cookieExists(name) {
                return (document.cookie.split('; ').indexOf(name + '=' + COOKIE_VALUE) !== -1);
            }

            function hideCookieDialog() {
                const dialogs = document.getElementsByClassName('js-cookie-consent');

                for (let i = 0; i < dialogs.length; ++i) {
                    dialogs[i].style.display = 'none';
                }
            }

            function setCookie(name, value, expirationInDays) {
                const date = new Date();
                date.setTime(date.getTime() + (expirationInDays * 24 * 60 * 60 * 1000));
                document.cookie = name + '=' + value
                    + ';expires=' + date.toUTCString()
                    + ';domain=' + COOKIE_DOMAIN
                    + ';path=/{{ config('session.secure') ? ';secure' : null }}'
                    + '{{ config('session.same_site') ? ';samesite='.config('session.same_site') : null }}';
            }

            if (cookieExists('{{config('cookie-consent.cookie_name')}}')) {
                hideCookieDialog();
            }

            const buttons = document.getElementsByClassName('js-cookie-consent-agree');

            for (let i = 0; i < buttons.length; ++i) {
                buttons[i].addEventListener('click', consentWithCookies);
            }

            return {
                consentWithCookies: consentWithCookies,
                hideCookieDialog: hideCookieDialog
            };
        })();
    </script>
@endif
