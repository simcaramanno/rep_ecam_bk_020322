<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages be
    |--------------------------------------------------------------------------
    |
    |
    */
    // Messages
    'mess_update' => 'Data updated',
    'mess_email_sent' => 'Your message was sent!',


    // Errors
    'err_ops' => 'Ops.. something was wrong.',
    'err_ops_exception' => 'Ops..something was wrong (:exception)',



];

