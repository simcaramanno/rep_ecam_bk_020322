<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});

/*
|--------------------------------------------------------------------------
| FRONT-END
*/
// --------->> DEFAULT
Route::get('/', 'App\Http\Controllers\HomeController@index');
Route::get('{lang?}/', 'App\Http\Controllers\HomeController@index');

// --------->> AUTH
Route::get('admin-area/login','App\Http\Controllers\admin\AuthController@index');
Route::post('admin/auth/signin','App\Http\Controllers\admin\AuthController@signin');
Route::get('admin-area/forgot','App\Http\Controllers\admin\AuthController@forgot');
Route::post('admin/auth/recover','App\Http\Controllers\admin\AuthController@recover');
Route::post('admin/auth/logout','App\Http\Controllers\admin\AuthController@logout')->named('admin-logout');

// --------->> HOME
Route::get('home','App\Http\Controllers\HomeController@index');
Route::get('{lang?}/home','App\Http\Controllers\HomeController@index');
Route::get('home/index','App\Http\Controllers\HomeController@index');
Route::get('{lang?}/home/index','App\Http\Controllers\HomeController@index');
// --------->> ABOUT US
Route::get('about','App\Http\Controllers\AboutController@index');
Route::get('{lang?}/about','App\Http\Controllers\AboutController@index');
Route::get('{lang?}/about/index','App\Http\Controllers\AboutController@index');
Route::get('about/index','App\Http\Controllers\AboutController@index');
// --------->> EVENTS
Route::get('{lang?}/events','App\Http\Controllers\EventsController@index');
Route::get('events','App\Http\Controllers\EventsController@index');
Route::get('{lang?}/events/index','App\Http\Controllers\EventsController@index');
Route::get('events/index','App\Http\Controllers\EventsController@index');
Route::get('events/show/{id}/{slug}','App\Http\Controllers\EventsController@show')->where(array( 'id' => '[0-9]+'));
Route::get('{lang?}/events/show/{id}/{slug}','App\Http\Controllers\EventsController@show')->where(array( 'id' => '[0-9]+'));

// SOLO ADMIN
Route::get('events/show-preview/{id}/{slug}','App\Http\Controllers\EventsController@show_preview')->where(array( 'id' => '[0-9]+'));
Route::get('{lang?}/events/show-preview/{id}/{slug}','App\Http\Controllers\EventsController@show_preview')->where(array( 'id' => '[0-9]+'));

// --------->> PROJECTS
Route::get('{lang?}/projects','App\Http\Controllers\ProjectsController@index');
Route::get('projects','App\Http\Controllers\ProjectsController@index');
Route::get('{lang?}/projects/index','App\Http\Controllers\ProjectsController@index');
Route::get('projects/index','App\Http\Controllers\ProjectsController@index');
Route::get('projects/show/{id}/{slug}','App\Http\Controllers\ProjectsController@show')->where(array( 'id' => '[0-9]+'));
Route::get('{lang?}/projects/show/{id}/{slug}','App\Http\Controllers\ProjectsController@show')->where(array( 'id' => '[0-9]+'));

// SOLO ADMIN
Route::get('projects/show-preview/{id}/{slug}','App\Http\Controllers\ProjectsController@show_preview')->where(array( 'id' => '[0-9]+'));
Route::get('{lang?}/projects/show-preview/{id}/{slug}','App\Http\Controllers\ProjectsController@show_preview')->where(array( 'id' => '[0-9]+'));

// --------->> PRESS
Route::get('{lang?}/press','App\Http\Controllers\PressController@index');
Route::get('press','App\Http\Controllers\PressController@index');
Route::get('{lang?}/press/index','App\Http\Controllers\PressController@index');
Route::get('press/index','App\Http\Controllers\PressController@index');
Route::get('press/show/{id}/{slug}','App\Http\Controllers\PressController@show')->where(array( 'id' => '[0-9]+'));
Route::get('{lang?}/press/show/{id}/{slug}','App\Http\Controllers\PressController@show')->where(array( 'id' => '[0-9]+'));
Route::get('{lang?}/press/search','App\Http\Controllers\PressController@search');
Route::get('press/search','App\Http\Controllers\PressController@search');

// SOLO ADMIN
Route::get('press/show-preview/{id}/{slug}','App\Http\Controllers\PressController@show_preview')->where(array( 'id' => '[0-9]+'));
Route::get('{lang?}/press/show-preview/{id}/{slug}','App\Http\Controllers\PressController@show_preview')->where(array( 'id' => '[0-9]+'));

// --------->> PARTNERS
Route::get('{lang?}/partners','App\Http\Controllers\PartnersController@index');
Route::get('partners','App\Http\Controllers\PartnersController@index');
Route::get('{lang?}/partners/index','App\Http\Controllers\PartnersController@index');
Route::get('partners/index','App\Http\Controllers\PartnersController@index');
// --------->> CONTACTS
Route::get('{lang?}/contacts','App\Http\Controllers\ContactsController@index');
Route::get('contacts','App\Http\Controllers\ContactsController@index');
Route::get('{lang?}/contacts/index','App\Http\Controllers\ContactsController@index');
Route::get('contacts/index','App\Http\Controllers\ContactsController@index');
Route::post('contacts/send-message','App\Http\Controllers\ContactsController@send_message');
// --------->> PRIVACY
Route::get('{lang?}/privacy','App\Http\Controllers\PrivacyController@index');
Route::get('privacy','App\Http\Controllers\PrivacyController@index');
Route::get('{lang?}/privacy/index','App\Http\Controllers\PrivacyController@index');
Route::get('privacy/index','App\Http\Controllers\PrivacyController@index');
// --------->> COOKIES
Route::get('{lang?}/cookies','App\Http\Controllers\CookiesController@index');
Route::get('cookies','App\Http\Controllers\CookiesController@index');
Route::get('{lang?}/cookies/index','App\Http\Controllers\CookiesController@index');
Route::get('cookies/index','App\Http\Controllers\CookiesController@index');



/*
|--------------------------------------------------------------------------
| BACK-END [ADMIN AREA]
*/
Route::group(['middleware' => 'admin_role'], function() {
        //
    // ---------------------------------------------- CONTACT REQUESTS
    // CONTACT REQUESTS
    Route::get('admin/contact-requests/index','App\Http\Controllers\admin\ContactRequestsController@index');
    Route::get('admin/contact-requests/show/{id}','App\Http\Controllers\admin\ContactRequestsController@show')->where(array( 'id' => '[0-9]+'));

    // ---------------------------------------------- USERS
    // STAFF
    Route::get('admin/staff/users/index','App\Http\Controllers\admin\UsersController@index');
    Route::get('admin/staff/users/create','App\Http\Controllers\admin\UsersController@create');
    Route::post('admin/staff/users/store','App\Http\Controllers\admin\UsersController@store');
    Route::get('admin/staff/users/edit-data/{id}','App\Http\Controllers\admin\UsersController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/staff/users/update-data','App\Http\Controllers\admin\UsersController@update_data');
    Route::post('admin/staff/users/send-credentials','App\Http\Controllers\admin\UsersController@send_credentials');
    Route::post('admin/staff/users/delete','App\Http\Controllers\admin\UsersController@delete');

    // ---------------------------------------------- ARCHIVE
    // ARCHIVE -> EVENTS
    Route::get('admin/archive/events/index','App\Http\Controllers\admin\EventsController@index');
    Route::get('admin/archive/events/create','App\Http\Controllers\admin\EventsController@create');
    Route::post('admin/archive/events/store','App\Http\Controllers\admin\EventsController@store');
    // Data
    Route::get('admin/archive/events/edit-data/{id}','App\Http\Controllers\admin\EventsController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/events/update-data','App\Http\Controllers\admin\EventsController@update_data');
    Route::post('admin/archive/events/change-status','App\Http\Controllers\admin\EventsController@change_status');
    Route::post('admin/archive/events/delete','App\Http\Controllers\admin\EventsController@delete');
    // Details
    Route::get('admin/archive/events/edit-details/{id}','App\Http\Controllers\admin\EventsController@edit_details')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/events/update-details','App\Http\Controllers\admin\EventsController@update_details');
    // Program
    Route::get('admin/archive/events/edit-program/{id}','App\Http\Controllers\admin\EventsController@edit_program')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/events/update-program','App\Http\Controllers\admin\EventsController@update_program');
    // Speakers
    Route::get('admin/archive/events/edit-speakers/{id}','App\Http\Controllers\admin\EventsController@edit_speakers')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/events/update-speakers','App\Http\Controllers\admin\EventsController@update_speakers');
    Route::post('admin/archive/events/sort-speakers','App\Http\Controllers\admin\EventsController@sort_speakers');
    // Gallery
    Route::get('admin/archive/events/edit-gallery/{id}','App\Http\Controllers\admin\EventsController@edit_gallery')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/events/update-gallery','App\Http\Controllers\admin\EventsController@update_gallery');
    Route::post('admin/archive/events/sort-gallery','App\Http\Controllers\admin\EventsController@sort_gallery');
    // Reports
    Route::get('admin/archive/events/edit-reports/{id}','App\Http\Controllers\admin\EventsController@edit_reports')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/events/update-reports','App\Http\Controllers\admin\EventsController@update_reports');

    // ARCHIVE -> PROJECTS
    Route::get('admin/archive/projects/index','App\Http\Controllers\admin\ProjectsController@index');
    Route::get('admin/archive/projects/create','App\Http\Controllers\admin\ProjectsController@create');
    Route::post('admin/archive/projects/store','App\Http\Controllers\admin\ProjectsController@store');
    // Data
    Route::get('admin/archive/projects/edit-data/{id}','App\Http\Controllers\admin\ProjectsController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/projects/update-data','App\Http\Controllers\admin\ProjectsController@update_data');
    Route::post('admin/archive/projects/change-status','App\Http\Controllers\admin\ProjectsController@change_status');
    Route::post('admin/archive/projects/delete','App\Http\Controllers\admin\ProjectsController@delete');
    // Details
    Route::get('admin/archive/projects/edit-details/{id}','App\Http\Controllers\admin\ProjectsController@edit_details')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/projects/update-details','App\Http\Controllers\admin\ProjectsController@update_details');
    // Images
    Route::get('admin/archive/projects/edit-images/{id}','App\Http\Controllers\admin\ProjectsController@edit_images')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/projects/update-images','App\Http\Controllers\admin\ProjectsController@update_images');
    // Gallery
    Route::get('admin/archive/projects/edit-gallery/{id}','App\Http\Controllers\admin\ProjectsController@edit_gallery')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/projects/update-gallery','App\Http\Controllers\admin\ProjectsController@update_gallery');
    Route::post('admin/archive/projects/sort-gallery','App\Http\Controllers\admin\ProjectsController@sort_gallery');

    // ARCHIVE -> PARTNERS
    Route::get('admin/archive/partners/index','App\Http\Controllers\admin\PartnersController@index');
    Route::post('admin/archive/partners/sort-partners','App\Http\Controllers\admin\PartnersController@sort_partners');
    Route::get('admin/archive/partners/create','App\Http\Controllers\admin\PartnersController@create');
    Route::post('admin/archive/partners/store','App\Http\Controllers\admin\PartnersController@store');
    // Edit
    Route::get('admin/archive/partners/edit-data/{id}','App\Http\Controllers\admin\PartnersController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/partners/update-data','App\Http\Controllers\admin\PartnersController@update_data');
    Route::post('admin/archive/partners/update-image','App\Http\Controllers\admin\PartnersController@update_image');
    Route::post('admin/archive/partners/delete','App\Http\Controllers\admin\PartnersController@delete');

    // ---------------------------------------------- PRESS
//    // PRESS -> CATEGORIES
//    Route::get('admin/archive/press/categories/index','App\Http\Controllers\admin\CategoriesController@index');
//    Route::get('admin/archive/press/categories/create','App\Http\Controllers\admin\CategoriesController@create');
//    Route::post('admin/archive/press/categories/store','App\Http\Controllers\admin\CategoriesController@store');
    // Edit
    Route::get('admin/archive/press/categories/edit-data/{id}','App\Http\Controllers\admin\CategoriesController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/press/categories/update-data','App\Http\Controllers\admin\CategoriesController@update_data');
    Route::post('admin/archive/press/categories/delete','App\Http\Controllers\admin\CategoriesController@delete');
    Route::get('admin/archive/press/categories/edit-details/{id}','App\Http\Controllers\admin\CategoriesController@edit_details')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/press/categories/update-details','App\Http\Controllers\admin\CategoriesController@update_details');

    // PRESS -> TAGS
    Route::get('admin/archive/press/tags/index','App\Http\Controllers\admin\TagsController@index');
    Route::get('admin/archive/press/tags/create','App\Http\Controllers\admin\TagsController@create');
    Route::post('admin/archive/press/tags/store','App\Http\Controllers\admin\TagsController@store');
    // Edit
    Route::get('admin/archive/press/tags/edit-data/{id}','App\Http\Controllers\admin\TagsController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/press/tags/update-data','App\Http\Controllers\admin\TagsController@update_data');
    Route::post('admin/archive/press/tags/delete','App\Http\Controllers\admin\TagsController@delete');

    // PRESS -> ARTICLES
    Route::get('admin/archive/press/articles/index','App\Http\Controllers\admin\ArticlesController@index');
    Route::get('admin/archive/press/articles/create','App\Http\Controllers\admin\ArticlesController@create');
    Route::post('admin/archive/press/articles/store','App\Http\Controllers\admin\ArticlesController@store');
    // Edit
    Route::get('admin/archive/press/articles/edit-data/{id}','App\Http\Controllers\admin\ArticlesController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/press/articles/update-data','App\Http\Controllers\admin\ArticlesController@update_data');
    Route::post('admin/archive/press/articles/delete','App\Http\Controllers\admin\ArticlesController@delete');
    Route::post('admin/archive/press/articles/change-status','App\Http\Controllers\admin\ArticlesController@change_status');
    // Edit details
    Route::get('admin/archive/press/articles/edit-details/{id}','App\Http\Controllers\admin\ArticlesController@edit_details')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/press/articles/update-details','App\Http\Controllers\admin\ArticlesController@update_details');
    // Edit gallery
    Route::get('admin/archive/press/articles/edit-gallery/{id}','App\Http\Controllers\admin\ArticlesController@edit_gallery')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/archive/press/articles/update-gallery','App\Http\Controllers\admin\ArticlesController@update_gallery');

    // ---------------------------------------------- CMS
    // CMS -> MENU
    Route::get('admin/cms/menu/index','App\Http\Controllers\admin\MenuController@index');
    Route::get('admin/cms/menu/edit-data/{id}','App\Http\Controllers\admin\MenuController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/cms/menu/update_data','App\Http\Controllers\admin\MenuController@update_data');
    Route::get('admin/cms/menu/edit-details/{id}','App\Http\Controllers\admin\MenuController@edit_details')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/cms/menu/update-details','App\Http\Controllers\admin\MenuController@update_details');
    // CMS -> PAGES
    Route::get('admin/cms/pages/index','App\Http\Controllers\admin\PagesController@index');
    Route::get('admin/cms/pages/edit-data/{id}','App\Http\Controllers\admin\PagesController@edit_data')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/cms/pages/update_data','App\Http\Controllers\admin\PagesController@update_data');
    Route::get('admin/cms/pages/edit-details/{id}','App\Http\Controllers\admin\PagesController@edit_details')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/cms/pages/update-details','App\Http\Controllers\admin\PagesController@update_details');
    Route::get('admin/cms/pages/edit-sections/{id}','App\Http\Controllers\admin\PagesController@edit_sections')->where(array( 'id' => '[0-9]+'));
    // CMS -> SECTIONS
    Route::get('admin/cms/sections/index','App\Http\Controllers\admin\SectionsController@index');
    Route::get('admin/cms/sections/edit-details/{id}','App\Http\Controllers\admin\SectionsController@edit_details')->where(array( 'id' => '[0-9]+'));
    Route::post('admin/cms/sections/update-details','App\Http\Controllers\admin\SectionsController@update_details');
    Route::post('admin/cms/sections/sort-about-carousel','App\Http\Controllers\admin\SectionsController@sort_about_carousel');


    // ---------------------------------------------- SETTINGS
    // SETTINGS - COMPANY
    Route::get('admin/settings/company/index','App\Http\Controllers\admin\SettingsController@company');
    Route::post('admin/settings/company/update','App\Http\Controllers\admin\SettingsController@update_company');
    // SETTINGS - CONTACTS
    Route::get('admin/settings/contacts/index','App\Http\Controllers\admin\SettingsController@contacts');
    Route::post('admin/settings/contacts/update','App\Http\Controllers\admin\SettingsController@update_contacts');
    // SETTINGS - IMAGES
    Route::get('admin/settings/images/index','App\Http\Controllers\admin\SettingsController@images');
    Route::post('admin/settings/images/update','App\Http\Controllers\admin\SettingsController@update_images');
    // SETTINGS - API KEYS
    Route::get('admin/settings/config/api-keys/index','App\Http\Controllers\admin\SettingsController@apikeys');
    Route::post('admin/settings/config/api-keys/update','App\Http\Controllers\admin\SettingsController@update_apikeys');
    // SETTINGS - SCRIPTS
    Route::get('admin/settings/config/scripts/index','App\Http\Controllers\admin\SettingsController@scripts');
    Route::post('admin/settings/config/scripts/update','App\Http\Controllers\admin\SettingsController@update_scripts');

    // ---------------------------------------------- ACCOUNT
    Route::get('admin/account/index','App\Http\Controllers\admin\AccountController@index');
    Route::post('admin/account/update-email','App\Http\Controllers\admin\AccountController@update_email');
    Route::post('admin/account/update-pw','App\Http\Controllers\admin\AccountController@update_pw');


});//<--- End Group Role --> BACK-END [ADMIN AREA]