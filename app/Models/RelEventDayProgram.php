<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class RelEventDayProgram extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'rel_event_program';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = RelEventDayProgram::select('rel_event_program.*');
        if ($orderby == null) {
            $orderby = array('lang' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        $query = RelEventDayProgram::select('rel_event_program.id','rel_event_program.event_day_id');
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_event_lang
     */
    public static function get_list_event_lang($event_day_id,$lang)
    {
        $where = [['rel_event_program.event_day_id', '=', $event_day_id],['rel_event_program.lang', '=', $lang]];
        $query = RelEventDayProgram::select('*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_event_lang
     */
    public static function get_list_lang($lang)
    {
        $where = [['rel_event_program.lang', '=', $lang]];
        $query = RelEventDayProgram::select('*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = RelEventDayProgram::where($where)
            ->select('rel_event_program.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['rel_event_program.id', '=', $id]];
        $result = RelEventDayProgram::where($where)
            ->select('rel_event_program.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = RelEventDayProgram::count();
        } else {
            $result = RelEventDayProgram::where($where)->count();
        }
        // Return
        return $result;
    }
    /*
     | get_sorting (passing WHERE)
     */
    public static function get_sorting($event_day_id,$lang)
    {
        $sorting = 10;
        $where = [['rel_event_program.event_day_id', '=', $event_day_id],['rel_event_program.lang', '=', $lang]];
        if(self::get_count_where($where)>0){
            $orderby = array('sorting' => 'desc');
            $list = self::get_list($where,$orderby);
            $sorting = $list[0]->sorting + 10;
        }
        // Return
        return $sorting;
    }


    /*
     | check_id_exists
     */
    public static function check_id_exists($id)
    {
        $where = [['rel_event_program.id', '=', $id]];
        $result = RelEventDayProgram::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
}