<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Section extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'sections';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = Section::select('sections.*');
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['sections.status', '=', 'active']];
        $query = Section::select('sections.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }

    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id,$active)
    {
        $where = [['sections.id', '=', $id]];
        if($active){
            array_push($where,['sections.status', '=', 'active']);
        }
        $result = Section::where($where)->count();
        if($result==1){
            return true;
        }
        return false;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Section::where($where)
            ->select('sections.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['sections.id', '=', $id]];
        $result = Section::where($where)
            ->select('sections.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Section::count();
        } else {
            $result = Section::where($where)->count();
        }
        // Return
        return $result;
    }

}