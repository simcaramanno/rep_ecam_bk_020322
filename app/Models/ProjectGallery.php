<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class ProjectGallery extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'project_gallery';


    /*
     | get_sorting (passing WHERE)
     */
    public static function get_sorting($project_id)
    {
        $sorting = 10;
        $where = [['project_gallery.project_id', '=', $project_id]];
        if(self::get_count_where($where)>0){
            $orderby = array('sorting' => 'desc');
            $list = self::get_list($where,$orderby);
            $sorting = $list[0]->sorting + 10;
        }
        // Return
        return $sorting;
    }

    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = ProjectGallery::select('project_gallery.*');
        if ($orderby == null) {
            $orderby = array('project_id' => 'asc','sorting' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_bypage
     */
    public static function get_list_byproject($project_id)
    {
        $where = [['project_gallery.project_id', '=',$project_id]];
        $query = ProjectGallery::select('project_gallery.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }

    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = ProjectGallery::where($where)
            ->select('project_gallery.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['project_gallery.id', '=', $id]];
        $result = ProjectGallery::where($where)
            ->select('project_gallery.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = ProjectGallery::count();
        } else {
            $result = ProjectGallery::where($where)->count();
        }
        // Return
        return $result;
    }


    /*
     | check_id_exists
     */
    public static function check_id_exists($id)
    {
        $where = [['project_gallery.id', '=', $id]];
        $result = ProjectGallery::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
}