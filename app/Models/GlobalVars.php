<?php

namespace App\Models;
class GlobalVars
{

    /**
     * Global Variables constructor.
     */
    public function __construct()
    {
        // -------------------------------------------------------
        // BACKEND
        // -------------------------------------------------------

        // ------------>> MESSAGES
        $this->mess_generic_save = "Modifiche salvate";
        $this->mess_generic_insert = "Inserimento effettuato";
        $this->mess_image_uploaded = "Immagine inserita";

        // ------------>> ERRORS
        $this->err_generic = "Opss..qualcosa è andato storto";
        $this->err_generic_save = "Problemi durante il salvataggio";
        $this->err_generic_insert = "Problemi durante la creazione";
        $this->err_required_field = "Inserisci: ";
        $this->err_date_range_invalid = "Range date invalido";
        $this->err_image_empty = "Selezionare un immagine valida";
        $this->err_video_empty = "Selezionare un video valida";
        $this->err_image_mime = "Sono ammesse immagini di tipo .png, .jpg, .jpeg";
        $this->err_image_pdf_mimes = "Sono ammessi files di tipo .pdf";
        $this->err_video_mime = "Sono ammessi video di tipo .mp4";
        $this->err_image_size = "Sono ammesse immagini con dimensioni non superiori a: ";
        $this->err_image_width_height = "Le dimensioni devono essere: ";

        // Progetti
        $this->err_proj_no_main = "Non puoi inserire in home page un progetto non attivo ";
        // Categorie (testate) press
        $this->err_catetory_delete = "Impossibile rimuovere questa testata: ci sono degli articoli correlati";
        // TAG press
        $this->err_tag_delete = "Impossibile rimuovere questo tag: ci sono degli articoli correlati";
        // ARTICLES press
        $this->err_post_delete = "Non puoi rimuovere un immagine principale";
        // User admin
        $this->err_user_delete = "Impossibile rimuovere questo utente Super Admin";


        // ------------>> EVENT TEXTS - Default
        $this->event_txt_counter1 = "government<br>leaders";
        $this->event_txt_counter2 = "political and business<br>individuals";
        $this->event_txt_counter3 = "different<br>countries";
        $this->event_txt_home_btn_txt = "see more";


        // ------------>> IMAGE SIZES
        $this->image_mimes = array(
            "image/png","image/jpg","image/jpeg","image/svg"
        );
        $this->video_mimes = array(
            "video/mp4"
        );
        $this->pdf_mimes = array(
            "application/pdf"
        );
        $this->image_size_events = array(
            "bg_image"=>array(
                "w"=>1920,
                "h"=>1031,
                "size"=>0
            ),
            "image_box"=>array(
                "w"=>800,
                "h"=>717,
                "size"=>0
            ),
            "location_img"=>array(
                "w"=>800,
                "h"=>717,
                "size"=>0
            ),
            "speaker_image"=>array(
                "w"=>525,
                "h"=>639,
                "size"=>0
            ),
            "slider_gallery"=>array(
                "w"=>346,
                "h"=>230,
                "size"=>0
            ),
            "home_image"=>array(
                "w"=>1920,
                "h"=>1080,
                "size"=>0
            ),
            "bg_image_download_banner"=>array(
                "w"=>1920,
                "h"=>1100,
                "size"=>0
            ),
            "image1_720x886"=>array( // img upcoming event box
                "w"=>720,
                "h"=>886,
                "size"=>0
            )
        );
        $this->image_size_projects = array(
            "small_image"=>array(
                "w"=>800,
                "h"=>650,
                "size"=>0
            ),
            "big_image"=>array(
                "w"=>1920,
                "h"=>1031,
                "size"=>0
            ),
            "home_image"=>array(
                "w"=>1000,
                "h"=>771,
                "size"=>0
            ),
            "slider_gallery"=>array(
                "w"=>1400,
                "h"=>933,
                "size"=>0
            ),
        );
        $this->image_size_partners = array(
            "image"=>array(
                "w"=>232,
                "h"=>110,
                "size"=>0
            ),
        );
        $this->image_size_sections = array(
            "1_image1"=>array(
                "w"=>1920,
                "h"=>1080,
                "size"=>0
            ),
            "2_image1"=>array(
                "w"=>800,
                "h"=>717,
                "size"=>0
            ),
            "6_image1"=>array(
                "w"=>1920,
                "h"=>1100,
                "size"=>0
            ),
            "8_image1"=>array(
                "w"=>232,
                "h"=>110,
                "size"=>0
            ),
            "14_image1"=>array(
                "w"=>800,
                "h"=>594,
                "size"=>0
            ),
            "17_image_carousel"=>array(
                "w"=>360,
                "h"=>270,
                "size"=>0
            ),
            "18_image_slider"=>array(
                "w"=>800,
                "h"=>622,
                "size"=>0
            ),
            "19_image_aboutus_banner"=>array(
                "w"=>1920,
                "h"=>1100,
                "size"=>0
            ),
            "20_image_box"=>array(
                "w"=>800,
                "h"=>717,
                "size"=>0
            ),
            "24_image1"=>array(
                "w"=>232,
                "h"=>110,
                "size"=>0
            ),
        );
        $this->image_size_press = array(
            "filename_small"=>array(
                "w"=>800,
                "h"=>560,
                "size"=>0
            ),
            "filename_big"=>array(
                "w"=>780,
                "h"=>500,
                "size"=>0
            ),
            "filename_800x1010"=>array(
                "w"=>800,
                "h"=>1010,
                "size"=>0
            ),
        );
        $this->image_size_logo = array(
            "footer"=>array(
                "w"=>300,
                "h"=>150,
                "size"=>0
            ),
            "white_logo"=>array(
                "w"=>300,
                "h"=>150,
                "size"=>0
            ),
            "favicon57"=>array(
                "w"=>57,
                "h"=>57,
                "size"=>0
            ),
            "favicon72"=>array(
                "w"=>72,
                "h"=>72,
                "size"=>0
            ),
            "favicon114"=>array(
                "w"=>114,
                "h"=>114,
                "size"=>0
            ),
        );

    }

}