<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class ContactRequest extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'contact_requests';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = ContactRequest::select('contact_requests.*');
        if ($orderby == null) {
            $orderby = array('id' => 'desc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['contact_requests.status', '=', 'active']];
        $query = ContactRequest::select('contact_requests.*')->where($where);
        $orderby = array('id' => 'desc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = ContactRequest::where($where)
            ->select('contact_requests.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['contact_requests.id', '=', $id]];
        $result = ContactRequest::where($where)
            ->select('contact_requests.*')
            ->firstOrFail();
        // Return
        return $result;
    }

    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = ContactRequest::count();
        } else {
            $result = ContactRequest::where($where)->count();
        }
        // Return
        return $result;
    }

    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id,$active)
    {
        $where = [['contact_requests.id', '=', $id]];
        if($active){
            array_push($where,['contact_requests.status', '=', 'active']);
        }
        $result = ContactRequest::where($where)->count();
        if($result==1){
            return true;
        }
        return false;
    }
}