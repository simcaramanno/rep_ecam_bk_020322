<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['users.id', '=', $id]];
        $result = User::where($where)
            ->select('users.*')
            ->firstOrFail();
        // Return
        return $result;
    }


    // check_duplicati - Return TRUE/FALSE
    public static function check_duplicati_email($email,$id)
    {
        if($id==null){
            $where = [['status', '!=','deleted']];
            $result = User::where($where)->whereRaw('LOWER(`email`) LIKE ? ',[trim(strtolower($email))])->count();
        } else {
            $where = [
                ['id', '!=', $id],
                ['status', '!=','deleted']
            ];
            $result = User::where($where)
                ->whereRaw('LOWER(`email`) LIKE ? ',[trim(strtolower($email))])
                ->count();
        }

        // Return
        if($result==0){
            return true;
        } else {
            return false;
        }
    }

    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id,$active,$role)
    {
        $where = [['users.id', '=', $id]];
        if($active){
            array_push($where,['users.status', '=', 'active']);
        }
        if($role){
            array_push($where,['users.role', '=',$role]);
        }
        $result = User::where($where)->count();
        if($result==1){
            return true;
        }
        return false;
    }
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = User::select('users.*');
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
}
