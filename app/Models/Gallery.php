<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Gallery extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'gallery';


    /*
     | get_sorting (passing WHERE)
     */
    public static function get_sorting($section_id)
    {
        $sorting = 10;
        $where = [['gallery.section_id', '=', $section_id]];
        if(self::get_count_where($where)>0){
            $orderby = array('sorting' => 'desc');
            $list = self::get_list($where,$orderby);
            $sorting = $list[0]->sorting + 10;
        }
        // Return
        return $sorting;
    }


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = Gallery::select('gallery.*');
        if ($orderby == null) {
            $orderby = array('sorting' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list($section_id)
    {
        $where = [['gallery.section_id', '=',$section_id]];
        $query = Gallery::select('gallery.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_arr_list($list,$lang)
    {
        $list_arr = array();
        if(!empty($list) && sizeof($list)>0){
            foreach($list as $item){
                // Texts
                $small_title = "";
                $title = "";
                $subtitle = "";
                $btn_text = "";
                $btn_link = "";
                if(!empty($item->lang_texts)){
                    $lang_texts = json_decode($item->lang_texts,true);
                    if(isset($lang_texts[$lang])){
                        if(isset($lang_texts[$lang]["small_title"])){$small_title = $lang_texts[$lang]["small_title"];}
                        if(isset($lang_texts[$lang]["title"])){$title = $lang_texts[$lang]["title"];}
                        if(isset($lang_texts[$lang]["subtitle"])){$subtitle = $lang_texts[$lang]["subtitle"];}
                        if(isset($lang_texts[$lang]["btn_text"])){$btn_text = $lang_texts[$lang]["btn_text"];}
                        if(isset($lang_texts[$lang]["btn_link"])){$btn_link = $lang_texts[$lang]["btn_link"];}
                    }
                }

                $item_arr = array(
                    "id"=>$item->id,
                    "section_id"=>$item->section_id,
                    "image"=>$item->image,
                    "image_alt"=>$item->image_alt,
                    "sorting"=>$item->sorting,
                    "lang_texts"=>$item->lang_texts,
                    "small_title"=>$small_title,
                    "title"=>$title,
                    "subtitle"=>$subtitle,
                    "btn_text"=>$btn_text,
                    "btn_link"=>$btn_link,
                );
                // Add
                array_push($list_arr,$item_arr);
            }
        }
        // Return
        return $list_arr;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Gallery::where($where)
            ->select('gallery.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['gallery.id', '=', $id]];
        $result = Gallery::where($where)
            ->select('gallery.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Gallery::count();
        } else {
            $result = Gallery::where($where)->count();
        }
        // Return
        return $result;
    }

}