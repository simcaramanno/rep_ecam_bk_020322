<?php
namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
class Project extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'projects';
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby,$limit)
    {
        $query = Project::select('projects.*');
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        if ($limit != null) {
            $query = $query->limit($limit);
        }
        $result = $query->get();
        // Return
        return $result;
    }

    /*
     | get_prev_project
     */
    public static function get_prev_project($id,$project_date,$lang)
    {
        $where = [['projects.status', '=', 'active'],['projects.id', '!=',$id],['projects.project_date', '<=',$project_date]];
        $orderby = array('project_date' => 'desc');
        $list = self::get_list($where,$orderby, 1);
        if(!empty($list) && sizeof($list)>0){
            // Get data
            return self::get_data_id_lang($list[0]->id,$lang);
        }
        // Return
        return null;
    }

    /*
     | get_next_project
     */
    public static function get_next_project($id,$project_date,$lang)
    {
        $where = [['projects.status', '=', 'active'],['projects.id', '!=',$id],['projects.project_date', '>=',$project_date]];
        $orderby = array('project_date' => 'desc');
        $list = self::get_list($where,$orderby, 1);
        if(!empty($list) && sizeof($list)>0){
            // Get data
            return self::get_data_id_lang($list[0]->id,$lang);
        }
        // Return
        return null;
    }

    /*
     | get_active_list
     */
    public static function get_active_list($limit)
    {
        $where = [['projects.status', '=', 'active']];
        $query = Project::select('projects.*')->where($where);
        $orderby = array('project_date' => 'desc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if(!empty($limit)){
            $query = $query->limit($limit);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | check_main_project
     */
    public static function check_main_project()
    {
        $where = [['projects.status', '=', 'active'],['projects.is_main', '=',1]];
        $result = self::get_count_where($where);
        if ($result==1) {
            return true;
        }
        return false;
    }
    /*
     | get_main_project [main project EXIST!!]
     */
    public static function get_main_project($lang)
    {
        $where = [['projects.status', '=', 'active'],['projects.is_main', '=',1]];
        $project_row = self::get_data($where);
        // Get data
        $project = self::get_data_id_lang($project_row->id,$lang);
        // Return
        return $project;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang,$limit)
    {
        // Get all active projects
        $result = self::get_active_list($limit);
        $list = array();
        if (!empty($result) && sizeof($result) > 0) {
            // All post texts LANG
            $where = [['project_texts.lang', '=',$lang]];
            $projecttexts = ProjectText::get_list($where, null);
            // All gallery
            $allGallery = ProjectGallery::get_list(null,null);

            if (!empty($projecttexts) && sizeof($projecttexts) > 0) {
                foreach ($result as $project) {
                    $trovato = false;
                    $project_text = array();
                    foreach ($projecttexts as $projecttext) {
                        if ($projecttext->project_id == $project->id && $projecttext->lang == $lang) {
                            $trovato = true;
                            $project_text = $projecttext;
                            break;
                        }
                    }
                    // Add
                    if ($trovato) {
                        // Gallery
                        $gallery = array();
                        if (!empty($allGallery) && sizeof($allGallery) > 0) {
                            foreach ($allGallery as $gall) {
                                if ($project->id == $gall->project_id) {
                                    array_push($gallery,$gall);
                                }
                            }
                        }

                        // ADD ITEM
                        $_item = array(
                            "id" => $project->id,
                            "project" => $project,
                            "project_texts" => $project_text,
                            "gallery" => $gallery
                        );
                        array_push($list, $_item);
                    }
                }
            }
        }
        // Return
        return $list;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Project::where($where)
            ->select('projects.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['projects.id', '=', $id]];
        $result = Project::where($where)
            ->select('projects.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang($id, $lang)
    {
        $data = array();
        if (self::check_id_exists($id, true)) {
            $project = self::get_data_id($id);
            $where = [['project_texts.project_id', '=', $id], ['project_texts.lang', '=', $lang]];
            $projecttext = ProjectText::get_data($where);
            // Gallery
            $gallery = ProjectGallery::get_list_byproject($id);

            // all data
            $data = array(
                "project" => $project,
                "projectText" => $projecttext,
                "gallery" =>$gallery
            );
        }
        // Return
        return $data;
    }

    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang2($id, $lang,$is_active)
    {
        $data = array();
        if (self::check_id_exists($id, $is_active)) {
            $project = self::get_data_id($id);
            $where = [['project_texts.project_id', '=', $id], ['project_texts.lang', '=', $lang]];
            $projecttext = ProjectText::get_data($where);
            // Gallery
            $gallery = ProjectGallery::get_list_byproject($id);

            // all data
            $data = array(
                "project" => $project,
                "projectText" => $projecttext,
                "gallery" =>$gallery
            );
        }
        // Return
        return $data;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Project::count();
        } else {
            $result = Project::where($where)->count();
        }
        // Return
        return $result;
    }


    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id, $active)
    {
        $where = [['projects.id', '=', $id]];
        if ($active) {
            array_push($where, ['projects.status', '=', 'active']);
        }
        $result = Project::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
}