<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class RelPostTag extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'rel_post_tags';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = RelPostTag::select('rel_post_tags.*');
        if ($orderby == null) {
            $orderby = array('tag_id' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_bypage
     */
    public static function get_list_bypage($page_id)
    {
        $where = [['rel_post_tags.page_id', '=',$page_id]];
        $query = RelPostTag::select('rel_post_tags.*')->where($where);
        $orderby = array('tag_id' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_bypost
     */
    public static function get_list_bypost($post_id)
    {
        $where = [['rel_post_tags.post_id', '=',$post_id]];
        $query = RelPostTag::select('rel_post_tags.*')->where($where);
        $orderby = array('tag_id' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }

    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = RelPostTag::where($where)
            ->select('rel_post_tags.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['rel_post_tags.id', '=', $id]];
        $result = RelPostTag::where($where)
            ->select('rel_post_tags.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = RelPostTag::count();
        } else {
            $result = RelPostTag::where($where)->count();
        }
        // Return
        return $result;
    }

}