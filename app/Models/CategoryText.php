<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class CategoryText extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'category_texts';

    /*
     | check_id_exists
     */
    public static function check_id_exists($id,$category_id)
    {
        $where = [['category_texts.id', '=', $id]];
        if ($category_id) {
            array_push($where, ['category_texts.category_id', '=',$category_id]);
        }
        $result = CategoryText::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = CategoryText::select('category_texts.*');
        if ($orderby == null) {
            $orderby = array('lang' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['category_texts.status', '=', 'active']];
        $query = CategoryText::select('category_texts.*')->where($where);
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        $where = [['category_texts.status', '=', 'active']];
        $query = CategoryText::select('category_texts.id, category_texts.category_id')->where($where);
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = CategoryText::where($where)
            ->select('category_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['category_texts.id', '=', $id]];
        $result = CategoryText::where($where)
            ->select('category_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = CategoryText::count();
        } else {
            $result = CategoryText::where($where)->count();
        }
        // Return
        return $result;
    }
}