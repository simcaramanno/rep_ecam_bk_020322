<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Mail;
class AppFunctions extends Model
{

    // ----------------------------- SEND EMAIL
    public static function send_email($platform,$subject,$all_data_arr,$email_template,$email_to){
        $data = array(
            'from'=>$platform->email_from,
            'from_name'=>$platform->email_from,
            'to'=>$email_to,
            'subject'=> $subject,
            'platform'=>$platform,
            'all_data_arr'=>$all_data_arr
        );
        // Send email
        try {
            Mail::send($email_template, $data, function ($message) use ($data) {
                $message->from($data["from"], $data["from_name"]);
                $message->to($data["to"]);
                $message->subject($data["subject"]);
            });
            return "";
        }catch (\Throwable $e) {
            return $e->getMessage();
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    // CONVERT DATE FORMAT
    public static function convert_date_format($originDate,$originFormat,$resultFormat){
        // from dd-mm-yyyy to yyyy-mm-dd
        if($originFormat=='dd-mm-yyyy' && $resultFormat=='yyyy-mm-dd'){
            if(strlen($originDate)==10){
                $dd = substr($originDate,0,2);
                $mm = substr($originDate,3,2);
                $yy = substr($originDate,6,4);
                return $yy.'-'.$mm.'-'.$dd;
            }
        }
        // from 'dd-mm-yyyy H:i' to 'yyyy-mm-dd H:i:s'
        if($originFormat=='dd-mm-yyyy H:i' && $resultFormat=='yyyy-mm-dd H:i:s'){
            if(strlen($originDate)==16){
                $dd = substr($originDate,0,2);
                $mm = substr($originDate,3,2);
                $yy = substr($originDate,6,4);
                $mi = substr($originDate,11,2);
                $ss = substr($originDate,14,2);
                return $yy.'-'.$mm.'-'.$dd." ".$mi.":".$ss.":00";
            }
        }
        // Return
        return "";
    }





    // ----------------------------- return be_layout INFO BACK-END LAYOUT
    public static function get_be_layout($section,$section2)
    {
        // Page title
        $page_title = "";
        $page_subtitle = "";
        switch ($section){
            case "auth":
                $page_title = "Login";
                if($section2=='forgot'){
                    $page_title = "Recupera password";
                }
                break;
            case "users":
                $page_title = "Staff";
                $page_subtitle = "Archivio";
                if($section2=='edit'){
                    $page_subtitle = "Dettaglio";
                }
                break;
            case "contact-requests":
                $page_title = "Richieste contatto";
                $page_subtitle = "Archivio";
                if($section2=='show'){
                    $page_subtitle = "Dettaglio";
                }
                break;
            case "events":
                $page_title = "Eventi";
                $page_subtitle = "Archivio";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "projects":
                $page_title = "Progetti";
                $page_subtitle = "Archivio";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "partners":
                $page_title = "Partners";
                $page_subtitle = "Archivio";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "tags":
                $page_title = "Press";
                $page_subtitle = "Tags";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "categories":
                $page_title = "Press";
                $page_subtitle = "Testate";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "articles":
                $page_title = "Press";
                $page_subtitle = "Articoli";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "menu":
                $page_title = "CMS";
                $page_subtitle = "Menu";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "pages":
                $page_title = "CMS";
                $page_subtitle = "Pagine";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "sections":
                $page_title = "CMS";
                $page_subtitle = "Sezioni";
                if($section2=='edit'){
                    $page_subtitle = "Modifica";
                }
                if($section2=='create'){
                    $page_subtitle = "Nuovo";
                }
                break;
            case "settings":
                $page_title = "Impostazioni";
                if($section2=='company'){
                    $page_subtitle = "Dati azienda";
                }
                if($section2=='contacts'){
                    $page_subtitle = "Dati contatto";
                }
                if($section2=='images'){
                    $page_subtitle = "Loghi";
                }
                break;
            case "config":
                $page_title = "Configurazioni";
                if($section2=='apikeys'){
                    $page_subtitle = "API Keys";
                }
                if($section2=='scripts'){
                    $page_subtitle = "Scripts";
                }
                break;
            case "account":
                $page_title = "Account";
                break;
        }


        // Arr
        $be_layout = array(
            "page_title" => $page_title,
            "page_subtitle" => $page_subtitle,
        );

        // Return
        return $be_layout;
    }
    // ----------------------------- return fe_layout INFO FRONT-END LAYOUT
    public static function get_fe_layout($lang,$page_id)
    {
        // -------------------------------- >>>>>>> page
        $page = Page::get_data_id_lang($page_id,$lang);

        // -------------------------------- >>>>>>> menu items
        $menu_items = Menu::get_active_list_lang($lang);

        // -------------------------------- >>>>>>> Footer
        $footer = SectionText::get_data_lang(30,$lang);

        // -------------------------------- >>>>>>> Return
        $layout = array(
            "lang" => $lang,
            "menu_items" => $menu_items,
            "page" => $page,
            "footer" => $footer
        );
        return $layout;
    }

    public static function get_event_is_upcoming($event_date_start,$event_date_end)
    {
        if($event_date_end>=date('Y-m-d')){
            return true;
        }
        return false;
    }

    // ----------------------------- return relSection data
    public static function get_page_sections_data($lang,$page_id)
    {
        // Sections
        $sections = array();
        $relSections = RelPageSection::get_list_bypage($page_id);
        if(!empty($relSections) && sizeof($relSections)>0){
            $allSections = Section::get_list(null,null);
            $where = [['section_texts.lang', '=',$lang]];
            $sectionTexts = SectionText::get_list($where,null);
            if(!empty($sectionTexts) && sizeof($sectionTexts)>0
                && !empty($allSections) && sizeof($allSections)>0){
                $section = array();
                $sectionText = array();
                foreach($relSections as $relSection){
                    // --------- Dati sezione
                    if(!empty($allSections) && sizeof($allSections)>0){
                        foreach($allSections as $item){
                            if($item->id==$relSection->section_id){
                                $section = $item;
                                break;
                            }
                        }
                    }
                    // --------- Contenuto sezione in lingua
                    foreach($sectionTexts as $item){
                        if($item->section_id==$relSection->section_id){
                            $sectionText = $item;
                            break;
                        }
                    }
                    // --------- Upcoming event FLAG
                    $upcoming_event_flag = Event::check_upcoming_event();

                    // --------- Upcoming event FLAG
                    $item["upcoming_event_flag"] = Event::check_upcoming_event();

                    // --------- Item
                    $item = array(
                        "section"=>$section,
                        "sectionText"=>$sectionText,
                        "upcoming_event_flag"=>$upcoming_event_flag
                    );

                    // --------- Hero area / Upcoming event / Counter Upcoming event / events
                    if($relSection->section_id==1 ||$relSection->section_id==3 || $relSection->section_id==4 || $relSection->section_id==27){
                        if($upcoming_event_flag){
                            $item["upcoming_event"] = Event::get_upcoming_event($lang);
                        }
                    }
                    // --------- Hero area [sliders]
                    if($relSection->section_id==1){
                        $sliders = array();
                        if($upcoming_event_flag){
                            $itemEvent = array(
                                "area"=>"upcoming_event",
                                "image_path"=>asset('images/events/'.$item["upcoming_event"]["eventText"]["home_image"]),
                                "small_title"=>$item["upcoming_event"]["eventText"]["home_small_title"],
                                "title"=>$item["upcoming_event"]["eventText"]["home_title"],
                                "month"=>date("F", strtotime($item["upcoming_event"]["event"]["date_start"])),
                                "day"=>date("d", strtotime($item["upcoming_event"]["event"]["date_start"])),
                                "year"=>date("Y", strtotime($item["upcoming_event"]["event"]["date_start"])),
                                "location"=>$item["upcoming_event"]["eventText"]["city"].', '.$item["upcoming_event"]["eventText"]["location"],
                                "btn_text"=>$item["upcoming_event"]["eventText"]["home_btn_text"],
                                "btn_link"=>url($lang.'/events/show/'.$item["upcoming_event"]["event"]["id"].'/'.$item["upcoming_event"]["eventText"]["slug"])
                            );
                            array_push($sliders,$itemEvent);
                        }
                        // Add about us slider
                        $itemAbout = array(
                            "area"=>"about_us",
                            "image_path"=>asset('images/sections/'.$sectionText["image1"]),
                            "image_alt"=>$sectionText["image1_alt"],
                            "small_title"=>$sectionText["small_title"],
                            "title"=>$sectionText["title"],
                            "subtitle"=>$sectionText["subtitle"],
                            "btn_text"=>$sectionText["btn1_text"],
                            "btn_link"=>$sectionText["btn1_link"],//url($lang.'/about/index')
                        );
                        array_push($sliders,$itemAbout);
                        // DB Sliders
                        $_sliders_list = Gallery::get_active_list($relSection->section_id);
                        if(!empty($_sliders_list) && sizeof($_sliders_list)>0){
                            $home_sliders = Gallery::get_arr_list($_sliders_list,$lang);
                            foreach($home_sliders as $_slider){
                                // Add about us slider
                                $itemAbout = array(
                                    "area"=>"about_us",
                                    "image_path"=>asset('images/sections/'.$_slider["image"]),
                                    "image_alt"=>$_slider["image_alt"],
                                    "small_title"=>$_slider["small_title"],
                                    "title"=>$_slider["title"],
                                    "subtitle"=>$_slider["subtitle"],
                                    "btn_text"=>!empty($_slider["btn_text"]) && !empty($_slider["btn_link"]) ? $_slider["btn_text"] : "",
                                    "btn_link"=>!empty($_slider["btn_text"]) && !empty($_slider["btn_link"]) ? $_slider["btn_link"] : "",
                                );
                                array_push($sliders,$itemAbout);
                            }
                        }

                        // Sliders
                        $item["hero_area_sliders"] = $sliders;
                    }
                    // --------- Latest press
                    if($relSection->section_id==7 || $relSection->section_id==25){
                        $limit = null;
                        if($relSection->section_id==7){
                            $limit = 4;
                        }
                        $item["posts"] = Post::get_active_list_lang($lang,$limit);
                    }
                    // --------- Partners carousel / Partners gallery
                    if($relSection->section_id==8 || $relSection->section_id==24){
                        $item["partners"] = Partner::get_active_list();
                    }

                    // --------- Projects area
                    if($relSection->section_id==10){
                        //$item["projects"] = Project::get_active_list_lang($lang,3);
                        $item["is_main_project"] = Project::check_main_project();
                        $item["main_project"] = array();
                        if( $item["is_main_project"]){
                            $item["main_project"] = Project::get_main_project($lang);
                        }
                    }

                    // --------- Gallery
                    // About us: images carousel
                    if($relSection->section_id==17){
                        $item["gallery_carousel"] = Gallery::get_active_list($relSection->section_id);
                    }

                    // --------- Past events
                    if($relSection->section_id==28){
                        $item["past_events"] = Event::get_past_events($lang,null);
                    }

                    // --------- Press details
                    if($relSection->section_id==29){
                        // Recent posts
                        $item["recent_posts"] = Post::get_active_list_lang($lang,3);
                        // Tags
                        $item["tags"] = Tag::get_active_list();
                    }

                    // --------- Projects list
                    if($relSection->section_id==46){
                        // Recent posts
                        $item["projects_list"] = Project::get_active_list_lang($lang,null);
                    }

                    // --------- ADD item
                    array_push($sections,$item);
                }
            }
        }

        // -------------------------------- >>>>>>> Return
        return $sections;
    }


}