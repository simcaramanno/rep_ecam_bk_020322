<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Category extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'categories';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = Category::select('categories.*');
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['categories.status', '=', 'active']];
        $query = Category::select('categories.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        // Get all active categories
        $result = self::get_active_list();

        $list = array();
        if(!empty($result) && sizeof($result)>0){
            $categorytexts = CategoryText::get_list(null,null);
            if(!empty($categorytexts) && sizeof($categorytexts)>0){
                foreach($result as $category){
                    $trovato = false;
                    $name = "";
                    foreach($categorytexts as $cattext){
                        if($cattext->category_id==$category->id && $cattext->lang==$lang){
                            $trovato = true;
                            $name = $cattext->name;
                        }
                    }
                    // Add
                    if($trovato){
                        $_item = array(
                            "id"=>$category->id,
                            "name"=>$name,
                            "status"=>$category->status
                        );
                        array_push($list,$_item);
                    }
                }
            }
        }

        // Return
        return $list;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Category::where($where)
            ->select('categories.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['categories.id', '=', $id]];
        $result = Category::where($where)
            ->select('categories.*')
            ->firstOrFail();
        // Return
        return $result;
    }

    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang($id,$lang)
    {
        $data = array();
        if(self::check_id_exists($id,true)){
            $page = self::get_data_id($id);
            $where = [['page_texts.category_id', '=', $id],['page_texts.lang', '=', $lang]];
            $pagetext = PageText::get_data($where);
            $data = array(
                "id"=>$page->id,
                "status"=>$page->status,
                "title"=>$pagetext->title,
                "meta_title"=>$pagetext->meta_title,
                "meta_desc"=>$pagetext->meta_desc,
                "meta_keys"=>$pagetext->meta_keys,
                "html_title"=>$pagetext->html_title,
                "html_subtitle"=>$pagetext->html_subtitle,
                "head_scripts"=>$pagetext->head_scripts,
                "footer_scripts"=>$pagetext->footer_scripts
            );
        }
        // Return
        return $data;
    }

    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang2($id,$lang,$is_active)
    {
        $data = array();
        if(self::check_id_exists($id,$is_active)){
            $category = self::get_data_id($id);
            $where = [['category_texts.category_id', '=', $id],['category_texts.lang', '=', $lang]];
            $categoryext = CategoryText::get_data($where);
            $data = array(
                "id"=>$category->id,
                "status"=>$category->status,
                "name"=>$categoryext->name,
                "slug"=>$categoryext->slug
            );
        }
        // Return
        return $data;
    }

    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Category::count();
        } else {
            $result = Category::where($where)->count();
        }
        // Return
        return $result;
    }

    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id,$active)
    {
        $where = [['categories.id', '=', $id]];
        if($active){
            array_push($where,['categories.status', '=', 'active']);
        }
        $result = Category::where($where)->count();
        if($result==1){
            return true;
        }
        return false;
    }
}