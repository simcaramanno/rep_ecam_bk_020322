<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class SectionText extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'section_texts';


    /*
     | check_id_exists
     */
    public static function check_id_exists($id,$section_id)
    {
        $where = [['section_texts.id', '=', $id]];
        if ($section_id) {
            array_push($where, ['section_texts.section_id', '=',$section_id]);
        }
        $result = SectionText::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = SectionText::select('section_texts.*');
        if ($orderby == null) {
            $orderby = array('lang' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }

    /*
     | get_data_lang
     */
    public static function get_data_lang($section_id,$lang)
    {
        $where = [['section_texts.lang', '=',$lang],['section_texts.section_id', '=',$section_id]];
        $result = SectionText::select('section_texts.*')
            ->where($where)
            ->select('section_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = SectionText::where($where)
            ->select('section_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['section_texts.id', '=', $id]];
        $result = SectionText::where($where)
            ->select('section_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = SectionText::count();
        } else {
            $result = SectionText::where($where)->count();
        }
        // Return
        return $result;
    }
}