<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Language extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'languages';
    /*
     | get_count
     */
    public static function check_slug_exists($slug)
    {
        $where = [['languages.status', '=', "active"], ['languages.slug', '=', $slug]];
        $result = Language::where($where)->count();
        if ($result == 1) {
            return true;
        }
        // Return
        return false;
    }
    /*
     | get_default_lang
     */
    public static function get_default_lang()
    {
        $where = [['languages.status', '=', "active"], ['languages.default', '=', 1]];
        $result = Language::where($where)->get();
        if (!empty($result)) {
            return $result[0]->slug;
        }
        // Return
        return "en";
    }
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby, $limit)
    {
        $query = Language::select('languages.*');
        if ($orderby != null) {
            foreach ($orderby as $key => $value) {
                $query = $query->orderBy($key, $value);
            }
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list_active()
    {
        $where = [['languages.status', '=', 'active']];
        $query = Language::select('languages.*')->where($where);
        $orderby = array('slug' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Language::where($where)
            ->select('languages.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['languages.id', '=', $id]];
        $result = Language::where($where)
            ->select('languages.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Language::count();
        } else {
            $result = Language::where($where)->count();
        }
        // Return
        return $result;
    }
    /*
     | check_duplicati (passing WHERE) - Return TRUE/FALSE
     */
    public static function check_duplicati($name, $id)
    {
        if ($id == null) {
            $result = Language::whereRaw('LOWER(`name`) LIKE ? ', [trim(strtolower($name))])->count();
        } else {
            $where = [
                ['id', '!=', $id]
            ];
            $result = Language::where($where)
                ->whereRaw('LOWER(`name`) LIKE ? ', [trim(strtolower($name))])
                ->count();
        }
        // Return
        if ($result == 0) {
            return true;
        } else {
            return false;
        }
    }
}