<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Partner extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'partners';
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = Partner::select('partners.*');
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['partners.status', '=', 'active']];
        $query = Partner::select('partners.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Partner::where($where)
            ->select('partners.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['partners.id', '=', $id]];
        $result = Partner::where($where)
            ->select('partners.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Partner::count();
        } else {
            $result = Partner::where($where)->count();
        }
        // Return
        return $result;
    }
    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id, $active)
    {
        $where = [['partners.id', '=', $id]];
        if ($active) {
            array_push($where, ['partners.status', '=', 'active']);
        }
        $result = Partner::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
}