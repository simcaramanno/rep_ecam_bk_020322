<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Page extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'pages';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = Page::select('pages.*');
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['pages.status', '=', 'active']];
        $query = Page::select('pages.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        // Get all active pages
        $result = self::get_active_list();

        $list = array();
        if(!empty($result) && sizeof($result)>0){
            $menutexts = MenuText::get_list(null,null);
            if(!empty($menutexts) && sizeof($menutexts)>0){
                foreach($result as $menu){
                    $trovato = false;
                    $name = "";
                    foreach($menutexts as $menutext){
                        if($menutext->menu_id==$menu->id && $menutext->lang==$lang){
                            $trovato = true;
                            $name = $menutext->name;
                        }
                    }
                    // Add
                    if($trovato){
                        $menu_item = array(
                            "id"=>$menu->id,
                            "name"=>$name,
                            "page_id"=>$menu->page_id,
                            "path"=>$menu->path
                        );
                        array_push($list,$menu_item);
                    }
                }
            }
        }

        // Return
        return $list;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Page::where($where)
            ->select('pages.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['pages.id', '=', $id]];
        $result = Page::where($where)
            ->select('pages.*')
            ->firstOrFail();
        // Return
        return $result;
    }

    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang($id,$lang)
    {
        $data = array();
        if(self::check_id_exists($id,true)){
            $page = self::get_data_id($id);
            $where = [['page_texts.page_id', '=', $id],['page_texts.lang', '=', $lang]];
            $pagetext = PageText::get_data($where);
            $data = array(
                "id"=>$page->id,
                "status"=>$page->status,
                "title"=>$pagetext->title,
                "meta_title"=>$pagetext->meta_title,
                "meta_desc"=>$pagetext->meta_desc,
                "meta_keys"=>$pagetext->meta_keys,
                "html_title"=>$pagetext->html_title,
                "html_subtitle"=>$pagetext->html_subtitle,
                "head_scripts"=>$pagetext->head_scripts,
                "footer_scripts"=>$pagetext->footer_scripts
            );
        }
        // Return
        return $data;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang2($id,$lang,$is_active)
    {
        $data = array();
        if(self::check_id_exists($id,$is_active)){
            $page = self::get_data_id($id);
            $where = [['page_texts.page_id', '=', $id],['page_texts.lang', '=', $lang]];
            $pagetext = PageText::get_data($where);
            $data = array(
                "id"=>$page->id,
                "page_text_id"=>$pagetext->id,
                "status"=>$page->status,
                "title"=>$pagetext->title,
                "meta_title"=>$pagetext->meta_title,
                "meta_desc"=>$pagetext->meta_desc,
                "meta_keys"=>$pagetext->meta_keys,
                "html_title"=>$pagetext->html_title,
                "html_subtitle"=>$pagetext->html_subtitle,
                "head_scripts"=>$pagetext->head_scripts,
                "footer_scripts"=>$pagetext->footer_scripts
            );
        }
        // Return
        return $data;
    }

    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Page::count();
        } else {
            $result = Page::where($where)->count();
        }
        // Return
        return $result;
    }

    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id,$active)
    {
        $where = [['pages.id', '=', $id]];
        if($active){
            array_push($where,['pages.status', '=', 'active']);
        }
        $result = Page::where($where)->count();
        if($result==1){
            return true;
        }
        return false;
    }
}