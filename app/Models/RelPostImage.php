<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class RelPostImage extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'post_images';


    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id, $post_id)
    {
        $where = [['post_images.id', '=', $id]];
        if ($post_id) {
            array_push($where, ['post_images.post_id', '=',$post_id]);
        }
        $result = RelPostImage::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = RelPostImage::select('post_images.*');
        if ($orderby == null) {
            $orderby = array('post_id' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_bypage
     */
    public static function get_list_bypage($page_id)
    {
        $where = [['post_images.post_id', '=',$page_id]];
        $query = RelPostImage::select('post_images.*')->where($where);
        $orderby = array('post_id' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_by_post($post_id)
    {
        $where = [['post_images.post_id', '=',$post_id]];
        $result = RelPostImage::where($where)->count();
        // Return
        return $result;
    }

    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = RelPostImage::where($where)
            ->select('post_images.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['post_images.id', '=', $id]];
        $result = RelPostImage::where($where)
            ->select('post_images.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = RelPostImage::count();
        } else {
            $result = RelPostImage::where($where)->count();
        }
        // Return
        return $result;
    }

}