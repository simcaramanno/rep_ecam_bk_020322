<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Menu extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'menu';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = Menu::select('menu.*');
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['menu.status', '=', 'active']];
        $query = Menu::select('menu.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }

    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id,$active)
    {
        $where = [['menu.id', '=', $id]];
        if($active){
            array_push($where,['menu.status', '=', 'active']);
        }
        $result = Menu::where($where)->count();
        if($result==1){
            return true;
        }
        return false;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        // Get all active menu
        $result = self::get_active_list();

        $list = array();
        if(!empty($result) && sizeof($result)>0){
            $menutexts = MenuText::get_list(null,null);
            if(!empty($menutexts) && sizeof($menutexts)>0){
                foreach($result as $menu){
                    $trovato = false;
                    $name = "";
                    foreach($menutexts as $menutext){
                        if($menutext->menu_id==$menu->id && $menutext->lang==$lang){
                            $trovato = true;
                            $name = $menutext->name;
                        }
                    }
                    // Add
                    if($trovato){
                        $menu_item = array(
                            "id"=>$menu->id,
                            "name"=>$name,
                            "page_id"=>$menu->page_id,
                            "path"=>$menu->path
                        );
                        array_push($list,$menu_item);
                    }
                }
            }
        }

        // Return
        return $list;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Menu::where($where)
            ->select('menu.*')
            ->firstOrFail();
        // Return
        return $result;
    }

    /*
     | get_data_id_lang
     */
    public static function get_data_id_lang($id,$lang)
    {
        $where = [['menu_texts.id', '=', $id],['menu_texts.lang', '=', $lang]];
        $menutexts = MenuText::get_data($where);
        // Return
        return $menutexts;
    }

    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['menu.id', '=', $id]];
        $result = Menu::where($where)
            ->select('menu.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Menu::count();
        } else {
            $result = Menu::where($where)->count();
        }
        // Return
        return $result;
    }

}