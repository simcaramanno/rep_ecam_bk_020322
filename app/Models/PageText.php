<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class PageText extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'page_texts';


    /*
     | check_id_exists
     */
    public static function check_id_exists($id,$page_id)
    {
        $where = [['page_texts.id', '=', $id]];
        if ($page_id) {
            array_push($where, ['page_texts.page_id', '=',$page_id]);
        }
        $result = PageText::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = PageText::select('page_texts.*');
        if ($orderby == null) {
            $orderby = array('lang' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['page_texts.status', '=', 'active']];
        $query = PageText::select('page_texts.*')->where($where);
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        $where = [['page_texts.status', '=', 'active']];
        $query = PageText::select('page_texts.id, page_texts.page_id')->where($where);
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = PageText::where($where)
            ->select('page_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['page_texts.id', '=', $id]];
        $result = PageText::where($where)
            ->select('page_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = PageText::count();
        } else {
            $result = PageText::where($where)->count();
        }
        // Return
        return $result;
    }
}