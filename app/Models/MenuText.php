<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class MenuText extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'menu_texts';

    /*
     | check_id_exists
     */
    public static function check_id_exists($id,$menu_id)
    {
        $where = [['menu_texts.id', '=', $id]];
        if ($menu_id) {
            array_push($where, ['menu_texts.menu_id', '=',$menu_id]);
        }
        $result = MenuText::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }

    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = MenuText::select('menu_texts.*');
        if ($orderby == null) {
            $orderby = array('lang' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['menu_texts.status', '=', 'active']];
        $query = MenuText::select('menu_texts.*')->where($where);
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        $where = [['menu_texts.status', '=', 'active']];
        $query = MenuText::select('menu_texts.id, menu_texts.menu_id')->where($where);
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = MenuText::where($where)
            ->select('menu_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['menu_texts.id', '=', $id]];
        $result = MenuText::where($where)
            ->select('menu_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = MenuText::count();
        } else {
            $result = MenuText::where($where)->count();
        }
        // Return
        return $result;
    }
}