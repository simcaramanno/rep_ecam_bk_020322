<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Post extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'posts';
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = Post::select('posts.*');
        if ($orderby == null) {
            $orderby = array('published_at' => 'desc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list($limit)
    {
        $where = [['posts.status', '=', 'active']];
        $query = Post::select('posts.*')->where($where);
        $orderby = array('published_at' => 'desc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if(!empty($limit)){
            $query = $query->limit($limit);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang,$limit)
    {
        // Get all active posts
        $result = self::get_active_list($limit);
        $list = array();
        if (!empty($result) && sizeof($result) > 0) {
            // All post texts LANG
            $where = [['post_texts.lang', '=',$lang]];
            $posttexts = PostText::get_list($where, null);
            // All category texts LANG
            $allCategory = Category::get_active_list_lang($lang);
            // All rel tags
            $allRelTags = RelPostTag::get_list(null,null);
            // All tags
            $allTags = Tag::get_active_list();
            // All images
            $allImage = RelPostImage::get_list(null,null);

            if (!empty($posttexts) && sizeof($posttexts) > 0) {
                foreach ($result as $post) {
                    $trovato = false;
                    $post_text = array();
                    foreach ($posttexts as $posttext) {
                        if ($posttext->post_id == $post->id && $posttext->lang == $lang) {
                            $trovato = true;
                            $post_text = $posttext;
                            break;
                        }
                    }
                    // Add
                    if ($trovato) {
                        // Category
                        $category_name = "";
                        $category = array();
                        if(!empty($allCategory) && sizeof($allCategory)>0){
                            foreach($allCategory as $cat){
                                if($post->category_id==$cat["id"]){
                                    $category_name = $cat["name"];
                                    $category = $cat;
                                    break;
                                }
                            }
                        }
                        // Tags
                        $tags = array();
                        if(!empty($allRelTags) && sizeof($allRelTags)>0){
                            foreach($allRelTags as $rel){
                                if($post->id==$rel->post_id){
                                    if(!empty($allTags) && sizeof($allTags)>0){
                                        foreach($allTags as $tag){
                                            if($rel->tag_id==$tag->id){
                                                // Add tag
                                                array_push($tags,$tag);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // Gallery
                        $gallery = array();
                         if(!empty($allImage) && sizeof($allImage)>0){
                             foreach($allImage as $rel){
                                 if($post->id==$rel->post_id){
                                     // Add image
                                     array_push($gallery,$rel);
                                 }
                             }
                         }
                        // ADD ITEM
                        $_item = array(
                            "id" => $post->id,
                            "post" => $post,
                            "category_name" => $category_name,
                            "category" => $category,
                            "post_text" => $post_text,
                            "tags" => $tags,
                            "gallery" => $gallery
                        );
                        array_push($list, $_item);
                    }
                }
            }
        }
        // Return
        return $list;
    }

    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Post::where($where)
            ->select('posts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['posts.id', '=', $id]];
        $result = Post::where($where)
            ->select('posts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang($id, $lang)
    {
        // Post
        $post = Post::get_data_id($id);
        // Post text
        $where = [['post_texts.post_id', '=', $id], ['post_texts.lang', '=', $lang]];
        $post_text = PostText::get_data($where);
        // Category name
        $category_name = "";
        $category = array();
        $allCategory = Category::get_active_list_lang($lang);
        if(!empty($allCategory) && sizeof($allCategory)>0){
            foreach($allCategory as $cat){
                if($post->category_id==$cat["id"]){
                    $category_name = $cat["name"];
                    $category = $cat;
                    break;
                }
            }
        }
        // Tags
        $tags = array();
        $where = [['rel_post_tags.post_id', '=', $id]];
        $allRelTags = RelPostTag::get_list($where,null);
        $allTags = Tag::get_active_list();
        if(!empty($allRelTags) && sizeof($allRelTags)>0){
            foreach($allRelTags as $rel){
                if($post->id==$rel->post_id){
                    if(!empty($allTags) && sizeof($allTags)>0){
                        foreach($allTags as $tag){
                            if($rel->tag_id==$tag->id){
                                // Add tag
                                array_push($tags,$tag);
                            }
                        }
                    }
                }
            }
        }
        // Gallery
        $where = [['post_images.post_id', '=', $id]];
        $gallery = RelPostImage::get_list($where,null);


        // ADD ITEM
        $data = array(
            "id" => $post->id,
            "post" => $post,
            "category_name" => $category_name,
            "category" => $category,
            "post_text" => $post_text,
            "tags" => $tags,
            "gallery" => $gallery
        );
        // Return
        return $data;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang2($id, $lang,$is_active)
    {
        // Post
        $post = Post::get_data_id($id);
        // Post text
        $where = [['post_texts.post_id', '=', $id], ['post_texts.lang', '=', $lang]];
        $post_text = PostText::get_data($where);
        // Category name
        $category_name = "";
        $category = array();
        $allCategory = Category::get_active_list_lang($lang);
        if(!empty($allCategory) && sizeof($allCategory)>0){
            foreach($allCategory as $cat){
                if($post->category_id==$cat["id"]){
                    $category_name = $cat["name"];
                    $category = $cat;
                    break;
                }
            }
        }
        // Tags
        $tags = array();
        $where = [['rel_post_tags.post_id', '=', $id]];
        $allRelTags = RelPostTag::get_list($where,null);
        $allTags = Tag::get_active_list();
        if(!empty($allRelTags) && sizeof($allRelTags)>0){
            foreach($allRelTags as $rel){
                if($post->id==$rel->post_id){
                    if(!empty($allTags) && sizeof($allTags)>0){
                        foreach($allTags as $tag){
                            if($rel->tag_id==$tag->id){
                                // Add tag
                                array_push($tags,$tag);
                            }
                        }
                    }
                }
            }
        }
        // Gallery
        $where = [['post_images.post_id', '=', $id]];
        $gallery = RelPostImage::get_list($where,null);


        // ADD ITEM
        $data = array(
            "id" => $post->id,
            "post" => $post,
            "category_name" => $category_name,
            "category" => $category,
            "post_text" => $post_text,
            "tags" => $tags,
            "gallery" => $gallery
        );
        // Return
        return $data;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Post::count();
        } else {
            $result = Post::where($where)->count();
        }
        // Return
        return $result;
    }
    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id, $active)
    {
        $where = [['posts.id', '=', $id]];
        if ($active) {
            array_push($where, ['posts.status', '=', 'active']);
        }
        $result = Post::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
}