<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class PostText extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'post_texts';


    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id, $post_id)
    {
        $where = [['post_texts.id', '=', $id]];
        if ($post_id) {
            array_push($where, ['post_texts.post_id', '=',$post_id]);
        }
        $result = PostText::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = PostText::select('post_texts.*');
        if ($orderby == null) {
            $orderby = array('lang' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['post_texts.status', '=', 'active']];
        $query = PostText::select('post_texts.*')->where($where);
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        $where = [['post_texts.status', '=', 'active']];
        $query = PostText::select('post_texts.id, post_texts.post_id')->where($where);
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = PostText::where($where)
            ->select('post_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['post_texts.id', '=', $id]];
        $result = PostText::where($where)
            ->select('post_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = PostText::count();
        } else {
            $result = PostText::where($where)->count();
        }
        // Return
        return $result;
    }
}