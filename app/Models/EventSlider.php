<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class EventSlider extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'event_sliders';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = EventSlider::select('event_sliders.*');
        if ($orderby == null) {
            $orderby = array('event_id' => 'asc','sorting' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_bypage
     */
    public static function get_list_byevent($event_id)
    {
        $where = [['event_sliders.event_id', '=',$event_id]];
        $query = EventSlider::select('event_sliders.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }

    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = EventSlider::where($where)
            ->select('event_sliders.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['event_sliders.id', '=', $id]];
        $result = EventSlider::where($where)
            ->select('event_sliders.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = EventSlider::count();
        } else {
            $result = EventSlider::where($where)->count();
        }
        // Return
        return $result;
    }


    /*
     | get_sorting (passing WHERE)
     */
    public static function get_sorting($event_id)
    {
        $sorting = 10;
        $where = [['event_sliders.event_id', '=', $event_id]];
        if(self::get_count_where($where)>0){
            $orderby = array('sorting' => 'desc');
            $list = self::get_list($where,$orderby);
            $sorting = $list[0]->sorting + 10;
        }
        // Return
        return $sorting;
    }


    /*
     | check_id_exists
     */
    public static function check_id_exists($id)
    {
        $where = [['event_sliders.id', '=', $id]];
        $result = EventSlider::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }

}