<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class EventText extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'event_texts';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = EventText::select('event_texts.*');
        if ($orderby == null) {
            $orderby = array('lang' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        $query = EventText::select('event_texts.id, event_texts.event_id');
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = EventText::where($where)
            ->select('event_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['event_texts.id', '=', $id]];
        $result = EventText::where($where)
            ->select('event_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = EventText::count();
        } else {
            $result = EventText::where($where)->count();
        }
        // Return
        return $result;
    }

    /*
     | check_id_exists
     */
    public static function check_id_exists($id,$event_id)
    {
        $where = [['event_texts.id', '=', $id]];
        if ($event_id) {
            array_push($where, ['event_texts.event_id', '=',$event_id]);
        }
        $result = EventText::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }

}