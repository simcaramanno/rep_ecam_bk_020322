<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class ProjectText extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'project_texts';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = ProjectText::select('project_texts.*');
        if ($orderby == null) {
            $orderby = array('lang' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        $query = ProjectText::select('project_texts.id, project_texts.project_id');
        $orderby = array('lang' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = ProjectText::where($where)
            ->select('project_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['project_texts.id', '=', $id]];
        $result = ProjectText::where($where)
            ->select('project_texts.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = ProjectText::count();
        } else {
            $result = ProjectText::where($where)->count();
        }
        // Return
        return $result;
    }


    /*
     | check_id_exists
     */
    public static function check_id_exists($id,$project_id)
    {
        $where = [['project_texts.id', '=', $id]];
        if ($project_id) {
            array_push($where, ['project_texts.project_id', '=',$project_id]);
        }
        $result = ProjectText::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
}