<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class EventDay extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'event_days';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = EventDay::select('event_days.*');
        if ($orderby == null) {
            $orderby = array('day' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang)
    {
        $query = EventDay::select('event_days.id','event_days.event_id');
        $orderby = array('day' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_list_event($event_id)
    {
        $where = [['event_days.event_id', '=', $event_id]];
        $query = EventDay::select('event_days.id','event_days.event_id','event_days.day_date')->where($where);
        $orderby = array('day_date' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = EventDay::where($where)
            ->select('event_days.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['event_days.id', '=', $id]];
        $result = EventDay::where($where)
            ->select('event_days.*')
            ->firstOrFail();
        // Return
        return $result;
    }

    /*
     | check_id_exists
     */
    public static function check_id_exists($id,$event_id)
    {
        $where = [['event_days.id', '=', $id]];
        if ($event_id) {
            array_push($where, ['event_days.event_id', '=',$event_id]);
        }
        $result = EventDay::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = EventDay::count();
        } else {
            $result = EventDay::where($where)->count();
        }
        // Return
        return $result;
    }
}