<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Tag extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'tags';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = Tag::select('tags.*');
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list()
    {
        $where = [['tags.status', '=', 'active']];
        $query = Tag::select('tags.*')->where($where);
        $orderby = array('name' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Tag::where($where)
            ->select('tags.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['tags.id', '=', $id]];
        $result = Tag::where($where)
            ->select('tags.*')
            ->firstOrFail();
        // Return
        return $result;
    }

    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Tag::count();
        } else {
            $result = Tag::where($where)->count();
        }
        // Return
        return $result;
    }

    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id,$active)
    {
        $where = [['tags.id', '=', $id]];
        if($active){
            array_push($where,['tags.status', '=', 'active']);
        }
        $result = Tag::where($where)->count();
        if($result==1){
            return true;
        }
        return false;
    }

    /*
     | check_slug_exists (passing WHERE)
     */
    public static function check_slug_exists($slug,$active)
    {
        $where = [['tags.slug', '=', $slug]];
        if($active){
            array_push($where,['tags.status', '=', 'active']);
        }
        $result = Tag::where($where)->count();
        if($result==1){
            return true;
        }
        return false;
    }
}