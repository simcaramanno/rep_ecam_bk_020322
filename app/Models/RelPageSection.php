<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class RelPageSection extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'rel_pages_sections';


    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby)
    {
        $query = RelPageSection::select('rel_pages_sections.*');
        if ($orderby == null) {
            $orderby = array('sorting' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($where != null) {
            $query = $query->where($where);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_bypage
     */
    public static function get_list_bypage($page_id)
    {
        $where = [['rel_pages_sections.page_id', '=',$page_id]];
        $query = RelPageSection::select('rel_pages_sections.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_list_bypage
     */
    public static function get_list_bypage_join($page_id)
    {
        $where = [['rel_pages_sections.page_id', '=',$page_id]];
        $query = RelPageSection::select('rel_pages_sections.*')->where($where);
        $orderby = array('sorting' => 'asc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        $result = $query->get();
        $list = array();
        if(!empty($result) && sizeof($result)>0){
            foreach($result as $rel){
                $section = Section::get_data_id($rel->section_id);
                $item = array(
                    "rel"=>$rel,
                    "section"=>$section,
                );
                array_push($list,$item);
            }
        }
        // Return
        return $list;
    }

    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = RelPageSection::where($where)
            ->select('rel_pages_sections.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['rel_pages_sections.id', '=', $id]];
        $result = RelPageSection::where($where)
            ->select('rel_pages_sections.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = RelPageSection::count();
        } else {
            $result = RelPageSection::where($where)->count();
        }
        // Return
        return $result;
    }

}