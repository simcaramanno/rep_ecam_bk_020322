<?php
namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
class Event extends Model
{
    protected $guarded = array();
    public $timestamps = false;
    protected $table = 'events';
    /*
     | get_list (passing WHERE, ORDERBY)
     */
    public static function get_list($where, $orderby,$limit)
    {
        $query = Event::select('events.*');
        if ($where != null) {
            $query = $query->where($where);
        }
        if ($orderby == null) {
            $orderby = array('name' => 'asc');
        }
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if ($limit != null) {
            $query = $query->limit($limit);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list
     */
    public static function get_active_list($limit)
    {
        $where = [['events.status', '=', 'active']];
        $query = Event::select('events.*')->where($where);
        $orderby = array('id' => 'desc');
        foreach ($orderby as $key => $value) {
            $query = $query->orderBy($key, $value);
        }
        if(!empty($limit)){
            $query = $query->limit($limit);
        }
        $result = $query->get();
        // Return
        return $result;
    }
    /*
     | get_active_list_lang
     */
    public static function get_active_list_lang($lang,$limit)
    {
        // Get all active events
        $result = self::get_active_list($limit);
        $list = array();
        if (!empty($result) && sizeof($result) > 0) {
            // All post texts LANG
            $where = [['event_texts.lang', '=',$lang]];
            $eventtexts = EventText::get_list($where, null);
            // All rel tags
            $allRelTags = RelPostTag::get_list(null,null);
            // All tags
            $allTags = Tag::get_active_list();

            if (!empty($eventtexts) && sizeof($eventtexts) > 0) {
                foreach ($result as $event) {
                    $trovato = false;
                    $post_text = array();
                    foreach ($eventtexts as $eventtext) {
                        if ($eventtext->event_id == $event->id && $eventtext->lang == $lang) {
                            $trovato = true;
                            $post_text = $eventtext;
                            break;
                        }
                    }
                    // Add
                    if ($trovato) {
                        // Tags
                        $tags = array();
                        if(!empty($allRelTags) && sizeof($allRelTags)>0){
                            foreach($allRelTags as $rel){
                                if($event->event_id==$rel->post_id){
                                    if(!empty($allTags) && sizeof($allTags)>0){
                                        foreach($allTags as $tag){
                                            if($rel->tag_id==$tag->id){
                                                // Add tag
                                                array_push($tags,$tag);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // Gallery
                        $gallery = array();
                        // Testimonials
                        $testimonials = array();

                        // ADD ITEM
                        $_item = array(
                            "id" => $event->id,
                            "event" => $event,
                            "event_text" => $post_text,
                            "gallery" => $gallery,
                            "testimonials" => $testimonials,
                        );
                        array_push($list, $_item);
                    }
                }
            }
        }
        // Return
        return $list;
    }
    /*
     | get_data (passing WHERE)
     */
    public static function get_data($where)
    {
        $result = Event::where($where)
            ->select('events.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id($id)
    {
        $where = [['events.id', '=', $id]];
        $result = Event::where($where)
            ->select('events.*')
            ->firstOrFail();
        // Return
        return $result;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang($id, $lang)
    {
        $data = array();
        if (self::check_id_exists($id, true)) {
            $event = self::get_data_id($id);
            $where = [['event_texts.event_id', '=', $id], ['event_texts.lang', '=', $lang]];
            $eventtext = EventText::get_data($where);
            // Days
            $days_program = array();
            $days = EventDay::get_list_event($id);
            if(!empty($days) && sizeof($days)>0){
                // All content
                $allRelEventDayPrg = RelEventDayProgram::get_list_lang($lang);
                foreach($days as $day){
                    $details = array();
                    foreach ($allRelEventDayPrg as $rel){
                        if($day->id == $rel->event_day_id){
                            array_push($details,$rel);
                        }
                    }
                    $item = array(
                        "day"=>$day,
                        "details"=>$details
                    );
                    array_push($days_program,$item);
                }
            }
            // Speakers
            $speakers = EventSpeaker::get_list_ofevent($id);
            // Gallery
            $gallery = EventSlider::get_list_byevent($id);

            // all data
            $data = array(
                "event" => $event,
                "eventText" => $eventtext,
                "gallery" =>$gallery,
                "days_program" => $days_program,
                "speakers" => $speakers,
            );
        }
        // Return
        return $data;
    }
    /*
     | get_data_id (passing WHERE)
     */
    public static function get_data_id_lang2($id, $lang,$is_active)
    {
        $data = array();
        if (self::check_id_exists($id, $is_active)) {
            $event = self::get_data_id($id);
            $where = [['event_texts.event_id', '=', $id], ['event_texts.lang', '=', $lang]];
            $eventtext = EventText::get_data($where);
            // Days
            $days_program = array();
            $days = EventDay::get_list_event($id);
            if(!empty($days) && sizeof($days)>0){
                // All content
                $allRelEventDayPrg = RelEventDayProgram::get_list_lang($lang);
                foreach($days as $day){
                    $details = array();
                    foreach ($allRelEventDayPrg as $rel){
                        if($day->id == $rel->event_day_id){
                            array_push($details,$rel);
                        }
                    }
                    $item = array(
                        "day"=>$day,
                        "details"=>$details
                    );
                    array_push($days_program,$item);
                }
            }
            // Speakers
            $speakers = EventSpeaker::get_list_ofevent($id);
            // Gallery
            $gallery = EventSlider::get_list_byevent($id);

            // all data
            $data = array(
                "event" => $event,
                "eventText" => $eventtext,
                "gallery" =>$gallery,
                "days_program" => $days_program,
                "speakers" => $speakers,
            );
        }
        // Return
        return $data;
    }
    /*
     | get_count_where (passing WHERE)
     */
    public static function get_count_where($where)
    {
        if ($where == null) {
            $result = Event::count();
        } else {
            $result = Event::where($where)->count();
        }
        // Return
        return $result;
    }
    /*
     | check_upcoming_event (passing WHERE)
     */
    public static function check_upcoming_event()
    {
        $where = [['events.status', '=', 'active'],['events.date_end', '>=', date('Y-m-d')]];
        $result = self::get_count_where($where);
        if ($result >0) {
            return true;
        }
        return false;
    }
    /*
     | get_upcoming_event [upcoming event EXIST!!]
     */
    public static function get_upcoming_event($lang)
    {
        $where = [['events.status', '=', 'active'],['events.date_end', '>=',date('Y-m-d')]];
        $orderby = array('date_start' => 'desc');
        $list = self::get_list($where,$orderby, 1);
        // Get data
        $event = self::get_data_id_lang($list[0]->id,$lang);
        // Return
        return $event;
    }
    /*
     | get_past_events [upcoming event EXIST!!]
     */
    public static function get_past_events($lang,$limit)
    {
        $where = [['events.status', '=', 'active'],['events.date_end', '<',date('Y-m-d')]];
        $orderby = array('date_start' => 'desc');
        $list = self::get_list($where,$orderby,$limit);
        // Get data
        $result = array();
        if(!empty($list) && sizeof($list)>0){
            foreach($list as $item){
                $event = self::get_data_id_lang($item->id,$lang);
                array_push($result,$event);
            }
        }
        // Return
        return $result;
    }
    /*
     | check_id_exists (passing WHERE)
     */
    public static function check_id_exists($id, $active)
    {
        $where = [['events.id', '=', $id]];
        if ($active) {
            array_push($where, ['events.status', '=', 'active']);
        }
        $result = Event::where($where)->count();
        if ($result == 1) {
            return true;
        }
        return false;
    }
}