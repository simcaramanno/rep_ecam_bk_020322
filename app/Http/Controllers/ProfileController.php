<?php
namespace App\Http\Controllers;
use App\Models\AppFunctions;
use App\Models\Language;
use App\Models\Platform;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
class ProfileController  extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::get_data();
    }



    // GET - INDEX
    public function index(Request $req)
    {
        // Set lang
        $lang = Language::get_default_lang();
        App::setLocale($lang);
        $page_id = 1;

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,$page_id);

        // Page data
        $page_data = self::get_page_data($lang,$page_id);


        // Return
        return view('profile.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }

    // GET - EDIT-DATA
    public function edit_data(Request $req)
    {
        // Set lang
        $lang = Language::get_default_lang();
        App::setLocale($lang);
        $page_id = 1;

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,$page_id);

        // Page data
        $page_data = self::get_page_data($lang,$page_id);


        // Return
        return view('profile.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }

    // GET PAGE DATA
    private function get_page_data($lang,$page_id)
    {
        // Sections
        $sections = array();//AppFunctions::get_page_sections_data($lang,$page_id);

        // Return
        $data = array(
            "page"=>$sections
        );
        return $data;
    }
}