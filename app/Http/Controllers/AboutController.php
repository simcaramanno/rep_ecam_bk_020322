<?php



namespace App\Http\Controllers;

use App\Models\AppFunctions;

use App\Models\Language;

use App\Models\Platform;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Foundation\Bus\DispatchesJobs;

use Illuminate\Foundation\Validation\ValidatesRequests;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\App;

class AboutController extends \Illuminate\Routing\Controller

{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;







    /**

     * __construct

     */

    public function __construct(Platform $platform)

    {

        // Basic Data

        $this->platform = $platform::first();

    }



    // GET - INDEX

    public function index($lang="",Request $req)

    {

        // Set lang

        if(empty($lang) || !Language::check_slug_exists($lang)){

            $lang = Language::get_default_lang();

        }

        App::setLocale($lang);



        // Layout

        $layout = AppFunctions::get_fe_layout($lang,2);

        // Page data

        $page_data = self::get_page_data($lang,2);



        // Return

        return view('about.index')

            ->with('platform',$this->platform)

            ->with('layout',$layout)

            ->with('page_data',$page_data);

    }



    // GET PAGE DATA

    private function get_page_data($lang,$page_id)

    {

        // Sections

        $sections = AppFunctions::get_page_sections_data($lang,$page_id);



        // Return

        $data = array(

            "sections"=>$sections

        );

        return $data;

    }

    // GET - SHOW PREVIEW [ONLY ADMIN]

    public function show_preview($lang="",$id,$slug,Request $req)

    {

        if(!Auth::user()){

            return response()->view('errors.404', [], 404);

        }

        // Set lang

        if(empty($lang) || !Language::check_slug_exists($lang)){

            $lang = Language::get_default_lang();

        }

        App::setLocale($lang);



        // Check post exists

        if(!Post::check_id_exists($id,false)){

            return response()->view('errors.404', [], 404);

        }



        // Layout

        $layout = GeneralModel::get_fe_layout($lang,11);

        // Page data

        $page_data = self::get_page_data($lang,11);

        // Post data

        $post = Post::get_data_id_lang2($id,$lang,false);
        $category_pr = Category::get_list([['tipo_category','=','PROJECT'],['status','=','active']], null);
        $category_nw = Category::get_list([['tipo_category','=','NEWS'],['status','=','active']], null);



        // Return

        return view('mediapress.show')

            ->with('platform',$this->platform)

            ->with('layout',$layout)
            ->with('category_pr',$category_pr)
            ->with('category_nw',$category_nw)
            ->with('page_data',$page_data)
            

            ->with('post',$post);

    }





}





