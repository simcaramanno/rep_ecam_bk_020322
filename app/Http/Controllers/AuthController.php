<?php
namespace App\Http\Controllers;
use App\Models\AppFunctions;
use App\Models\EmailTemplate;
use App\Models\ErrorLog;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\LoginHistory;
use App\Models\OtherBrand;
use App\Models\Platform;
use App\Models\Provinice;
use App\Models\RelPageItem;
use App\Models\shopify\ShopifyFunc;
use App\Models\ShopifyProduct;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Mail;
class AuthController  extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * __construct
     */
    public function __construct(Platform $platform,Guard $auth)
    {
        // Basic Data
        $this->platform = $platform::get_data();
        $this->GV = new GlobalVars();
        // Guard
        $this->auth = $auth;
    }



    // GET - LOGIN
    public function login(Request $req)
    {
        // Set lang
        $lang = Language::get_default_lang();
        App::setLocale($lang);
        $page_id = 14;

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,$page_id);

        // Page data
        $page_data = self::get_page_data($lang,$page_id);


        // Return
        return view('auth.login')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }
    // POST - SIGNIN
    public function signin(Request $req)
    {
        // Set lang
        $lang = Language::get_default_lang();
        App::setLocale($lang);

        // Check posts
        $text_err = $this->check_form_posts('signin',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('auth/login')->withInput();
        }
        $text_err =  "Credenziali non valide";

        try {
            // AUTH
            $credentials = array(
                'email'=> strtolower($req->email),
                'password'=>$req->password);
            if ($this->auth->attempt($credentials, $req->has('remember'))) {
                if($this->auth->User()->status =='active'){
                    Auth::login(Auth::user(), true);
                    // LoginHistory
                    LoginHistory::store_row('web',Auth::user()->id,$req->ip());
                    if(Auth::user()->role=='admin'){
                        return redirect('admin/dashboard');
                    }
                    // Redirect
                    return redirect()->intended('/');
                }
                // NO LOGIN
                $this->auth->logout();
            }
        } catch (\Throwable $e) {
            $text_err = $e->getMessage();
        }catch (\Exception $e) {
            $text_err = $e->getMessage();
        }

        // Errors
        \Session::flash('error', $text_err);
        return redirect()->back()
            ->withInput($req->only('email', 'remember'));
    }

    // GET - REGISTER
    public function register(Request $req)
    {
        // Set lang
        $lang = Language::get_default_lang();
        App::setLocale($lang);
        $page_id = 15;

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,$page_id);

        // Page data
        $page_data = self::get_page_data($lang,$page_id);


        // Return
        return view('auth.register')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }
    // POST - SIGNUP
    public function signup(Request $req)
    {
        // Set lang
        $lang = Language::get_default_lang();
        App::setLocale($lang);

        // Check posts
        $text_err = $this->check_form_posts('signup',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('auth/login')->withInput();
        }

        try {
            $user = new User();
            $user->created_at = Carbon::now('GMT+2');
            $user->updated_at = Carbon::now('GMT+2');
            $user->email = strtolower($req->email);
            $user->ip_address = $req->ip();
            $user->role = "user";
            $user->is_admin =0;
            $user->status = "active";
            $user->first_name = $req->first_name;
            $user->last_name = $req->last_name;
            $user->nickname = $req->nickname;
            $user->birth_date = !empty($req->birth_date) ? $req->birth_date : null;
            $user->avatar = $this->GV->IMG_USER_AVATAR;// TODO
            $user->cover = $this->GV->IMG_USER_COVER;// TODO
            $user->country = "Italia";
            $user->city = !empty($req->city) ? $req->city : null;
            $user->province = !empty($req->province) ? $req->province : null;
            $user->address = !empty($req->address) ? $req->address : null;
            $user->oppo_device_id = !empty($req->oppo_device_id) ? $req->oppo_device_id : 0;
            $user->other_device_id = !empty($req->other_device_id) ? $req->other_device_id : 0;
            $user->source = 'web';
            $user->password = Hash::make($req->password);
            $user->ip_address = $req->ip();
            $user->save();

            // Create SHOPIFY USER
            $post_data = array(
                'id'=>$user->id,
                'first_name'=>$user->first_name,
                'last_name'=>$user->last_name,
                'email'=>$user->email,
                'address'=>$user->address,
                'city'=>$user->city,
                'province'=>$user->province,
                'country'=>$user->country,
            );
            $res_shopify_create = ShopifyFunc::store_customer($post_data);
            if($res_shopify_create->success){
                // Update shopify_id
                $user->shopify_id = $res_shopify_create->data["customer"]["id"];
                $user->updated_at = Carbon::now('GMT+2');
                $user->save();

                // Get Activation url
                $res_shopify_url = ShopifyFunc::get_customer_activation_url($user->shopify_id);
                if($res_shopify_url->success){
                    $activation_url = $res_shopify_url->data["url"];
                    // UPdate
                    $user->shopify_activation_url = $activation_url;
                    $user->save();
                }// endif $res_shopify_url->success
            }// success $res_shopify_create->success

            // SEND EMAIL => USER : WELCOME + SHOPIFY ACTIVATION URL
            $text_err = "";
            try {
                $html_template = EmailTemplate::get_data_slug_lang('user-registration-welcome',$lang);
                $html_content = $html_template["content"];
                $html_content = str_replace('$user_first_name$',$user->first_name,$html_content);
                $html_content = str_replace('$user_last_name$',$user->last_name,$html_content);
                $btn_login = '<a href="'.url('/').'" class="btn btn-primary">accedi</a>';
                $html_content = str_replace('$btn_login$',$btn_login,$html_content);
                $data = array(
                    'from'=>$this->platform->email_from,
                    'from_name'=>$this->platform->email_from_name,
                    'to'=>$user->email,
                    'subject'=>$html_template["title"],
                    'platform'=>$this->platform,
                    'platform_url'=> 'http',//url('/'),
                    'logo_url'=>asset('frontend/images/temp/logo.png'),
                    'html_content'=>$html_content,
                );
                Mail::send('admin.email-templates.main-layout-user', $data, function ($message) use ($data) {
                    $message->from($data["from"], $data["from_name"]);
                    $message->to($data["to"]);
                    $message->subject($data["subject"]);
                });
                if(count(Mail::failures()) > 0){}
            } catch (\Throwable $e) {
                // TODO
                $text_err = $e->getMessage();
            }catch (\Exception $e) {
                // TODO
                $text_err = $e->getMessage();
            }
            if(!empty($text_err)){
                $err_log = new ErrorLog();
                $err_log->created_at = Carbon::now('GMT+2');
                $err_log->controller = "AuthController";
                $err_log->method = "signup";
                $err_log->action = "send user email";
                $err_log->error_code = "";
                $err_log->error_txt =$text_err;
                $err_log->note ='email: '.$user->email;
                $err_log->save();
            }

            // AUTH
            $credentials = array(
                'email'=> strtolower($req->email),
                'password'=>$req->password);
            if ($this->auth->attempt($credentials, $req->has('remember'))) {
                Auth::login(Auth::user(), true);
                // LoginHistory
                LoginHistory::store_row('web',Auth::user()->id,$req->ip());
                // Redirect
                return redirect()->intended('/');
            }
        } catch (\Throwable $e) {
            $text_err = $e->getMessage();
        }catch (\Exception $e) {
            $text_err = $e->getMessage();
        }

        // Errors
        \Session::flash('error', $text_err);
        return redirect()->back()->withInput();
    }






    // -------------------------------- PRIVATE FUNCION
    // GET PAGE DATA
    private function get_page_data($lang,$page_id)
    {
        $data = array();

        // ------------------------- register
        if($page_id==15){
            // provinces
            $data["provinces"] = Provinice::get_list(null,null,null);
            // oppo devices
            $data["oppo_devices"] = ShopifyProduct::get_signin_list(null,null);
            // other_devices
            $data["other_devices"] = OtherBrand::get_active_list(null,null);
        }

        // Return
        return $data;
    }

    // Check FORM POSTS
    private function check_form_posts($method,$req)
    {
        if($method=='signin'){
            if(!isset($req->email) || empty($req->email)){
                return $this->GV->err_required_field.'email';
            }
            if(!isset($req->password) || empty($req->password)){
                return $this->GV->err_required_field.'password';
            }
        }
        if($method=='signup'){
            if(!isset($req->email) || empty($req->email)){
                return $this->GV->err_required_field.'email';
            }
            if(!User::check_duplicates_email(strtolower($req->email),null)){
                return $this->GV->err_email_not_available;
            }
            // Check exists into shopify
            $res_customer = ShopifyFunc::get_customers();
            if($res_customer->success){
                if(!empty($res_customer->data["customers"]) && sizeof($res_customer->data["customers"])>0){
                    foreach($res_customer->data["customers"] as $customer){
                        if(strtolower($customer["email"])==strtolower($req->email)){
                            return $this->GV->err_email_not_available_shopify;
                            continue;
                        }
                    }
                }
            }
            if(!isset($req->password) || empty($req->password)){
                return $this->GV->err_required_field.'password';
            }
            if(strlen($req->password)<8){
                return $this->GV->err_pw_chars;
            }

            if(!isset($req->first_name) || empty($req->first_name)){
                return $this->GV->err_required_field.'nome';
            }
            if(!isset($req->last_name) || empty($req->last_name)){
                return $this->GV->err_required_field.'cognome';
            }
            if(isset($req->birth_date) && !empty($req->birth_date)){
                if(strlen($req->birth_date)!=10){
                    return $this->GV->err_birth_date_valid;
                }
                if(substr($req->birth_date,3,1)!='-'){
                    return $this->GV->err_birth_date_valid;
                }
                if(substr($req->birth_date,2,1)!='-'){
                    return $this->GV->err_birth_date_valid;
                }
                if(substr($req->birth_date,5,1)!='-'){
                    return $this->GV->err_birth_date_valid;
                }
            }
            if(!isset($req->nickname) || empty($req->nickname)){
                return $this->GV->err_required_field.'nickname';
            }
            if(isset($req->nickname) || empty($req->nickname)){
                return $this->GV->err_required_field.'nickname';
            }
            if(!empty($req->nickname)){
                if(strlen($req->nickname)>20){
                    return $this->GV->err_nickname_chars;
                }
                if(!User::check_duplicates_nickname($req->nickname,null)){
                    return $this->GV->err_nickname_not_available;
                }
            }
            if(isset($req->province) && !empty($req->province)){
                if(strlen($req->province)!=2){
                    return $this->GV->err_province_valid;
                }
            }
            if(isset($req->province) && !empty($req->province)){
                if(strlen($req->province)!=2){
                    return $this->GV->err_province_valid;
                }
            }
            if(isset($req->oppo_device_id) && !empty($req->oppo_device_id)
                && isset($req->other_device_id) && !empty($req->other_device_id)){
                return $this->GV->err_oppo_device_others;
            }
            if(isset($req->oppo_device_id) && !empty($req->oppo_device_id)){
                if(!ShopifyProduct::check_id_exists($req->oppo_device_id,true,true)){
                    return $this->GV->err_oppo_device_invalid;
                }
            }
            if(isset($req->other_device_id) && !empty($req->other_device_id)){
                if(!OtherBrand::check_id_exists($req->other_device_id,true)){
                    return $this->GV->err_other_device_invalid;
                }
            }

        }



        // OK
        return "";
    }




}