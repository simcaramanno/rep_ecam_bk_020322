<?php
namespace App\Http\Controllers;
use App\Models\AppFunctions;
use App\Models\ContactRequest;
use App\Models\Language;
use App\Models\Platform;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
class ContactsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
    }


    // GET - INDEX
    public function index($lang="",Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,9);
        // Page data
        $page_data = self::get_page_data($lang,9);

        // Return
        return view('contacts.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }

    // GET PAGE DATA
    private function get_page_data($lang,$page_id)
    {
        // Sections
        $sections = AppFunctions::get_page_sections_data($lang,$page_id);

        // Return
        $data = array(
            "sections"=>$sections
        );
        return $data;
    }

    // POST - Send MESSAGE
    public function send_message(Request $req)
    {

        // Set LANG
        $lang = Language::get_default_lang();
        if(!empty($req->lang) && Language::check_slug_exists($lang)){
            $lang = $req->lang;
        }
        App::setLocale($lang);

        // Check posts
        $text_err = $this->check_form_posts('send_message',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect($lang.'/contacts/index')->withInput();
        }

        // TODO X SIMONE!!
        // CHECK RECAPTCHA!!!
//        $checkRecaptcha = false;
//        $checkRecaptcha_err = "Please check the the captcha form";
//        if(isset($_POST['g-recaptcha-response'])){
//            $captcha=$_POST['g-recaptcha-response'];
//            $ip = $_SERVER['REMOTE_ADDR'];
//            // post request to server
//            $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($this->platform->rechaptca_apikey) .  '&response=' . urlencode($captcha);
//            $response = file_get_contents($url);
//            $responseKeys = json_decode($response,true);
//            // should return JSON with success as true
//            if($responseKeys["success"]) {
//                $checkRecaptcha = true;
//            } else {
//                $checkRecaptcha_err ="Invalid recaptcha";
//            }
//        }
//        if(!$checkRecaptcha){
//            \Session::flash('error', $checkRecaptcha_err);
//            return redirect($lang.'/contacts/index')->withInput();
//        }

        try {
            // Insert
            $newItem = new ContactRequest();
            $newItem->created_at = Carbon::now('GMT+2');
            $newItem->lang = $lang;
            $newItem->email = strtolower($req->email);
            $newItem->name = $req->name;
            $newItem->phone =isset($req->phone) && !empty($req->phone) ? $req->phone : "";
            $newItem->message = $req->message;
            $newItem->status = 'active';
            $newItem->last_edit = Carbon::now('GMT+2');
            $newItem->save();

            // Send email
            $message = array(
                "name"=>$req->name,
                "email"=>$req->email,
                "phone"=>isset($req->phone) && !empty($req->phone) ? $req->phone : "",
                "message"=>$req->message,
            );
            // --------------------------------------- SEND EMAIL: ADMIN ---------------------------------------
            $platform_url = url($lang.'/home/index');
            $subject = $this->platform->site_name." - New contact request";
            $all_data_arr = array(
                'platform_url'=>$platform_url,
                'message'=>$message
            );
            $email_template = 'admin.email-templates.admin-new-contact';
            $emailto = $this->platform->email_to;
            $err_send_email = AppFunctions::send_email($this->platform,$subject,$all_data_arr,$email_template,$emailto);
            if(empty($err_send_email)){
                // Mess ok
                \Session::flash('message', trans('be-messages.mess_email_sent'));
                return redirect($lang.'/contacts/index');
            }
            \Session::flash('error', "Opss...something was wrang. Try again.".$err_send_email);
            //\Session::flash('error', "Opss...something was wrang. Try again.");
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', trans('be-messages.err_ops_exception', [ 'exception' =>$e->getMessage()]));
        }catch (\Exception $e) {
            \Session::flash('error', trans('be-messages.err_ops_exception', ['exception' => $e->getMessage()]));
        }
        // Return
        return redirect($lang.'/contacts/index')->withInput();
    }



    // CHECK POSTS
    private function check_form_posts($method,$req)
    {
        // -------------------------------- >>>>>>> send_message
        if ($method == 'send_message') {
            if (!isset($req->name) || empty($req->name)) {
                return trans('validation.required', ['attribute' => 'nome']);
            }
            if (!isset($req->email) || empty($req->email)) {
                return trans('validation.required', ['attribute' => 'email']);
            }
            if (!isset($req->message) || empty($req->message)) {
                return trans('validation.required', ['attribute' => 'messaggio']);
            }

        }
        // Ok
        return "";
    }

}