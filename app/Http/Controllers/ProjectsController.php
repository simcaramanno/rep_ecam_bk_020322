<?php
namespace App\Http\Controllers;
use App\Models\AppFunctions;
use App\Models\Event;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
class ProjectsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
    }

    // GET - INDEX
    public function index($lang="",Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,14);
        // Page data
        $page_data = self::get_page_data($lang,14);

        // Return
        return view('projects.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }

    // GET PAGE DATA
    private function get_page_data($lang,$page_id)
    {
        // Sections
        $sections = AppFunctions::get_page_sections_data($lang,$page_id);

        // Return
        $data = array(
            "sections"=>$sections
        );
        return $data;
    }

    // GET - SHOW
    public function show($lang="",$id,$slug,Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Check element exists
        if(!Project::check_id_exists($id,true)){
            return response()->view('errors.404', [], 404);
        }
        // Project data
        $project = Project::get_data_id_lang($id,$lang);

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,15);
        // Page data
        $page_data = self::get_page_data($lang,15);
        // Prev/Next projects
        $page_data["prev_project"] = Project::get_prev_project($id,$project["project"]["project_date"],$lang);
        $page_data["next_project"] = Project::get_next_project($id,$project["project"]["project_date"],$lang);

        // Return
        return view('projects.show')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data)
            ->with('project',$project);
    }


    // GET - SHOW
    public function show_preview($lang="",$id,$slug,Request $req)
    {
        if(!Auth::user()){
            return response()->view('errors.404', [], 404);
        }
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Check element exists
        if(!Project::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Project data
        $project = Project::get_data_id_lang2($id,$lang,false);

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,15);
        // Page data
        $page_data = self::get_page_data($lang,15);
        // Prev/Next projects
        $page_data["prev_project"] = Project::get_prev_project($id,$project["project"]["project_date"],$lang);
        $page_data["next_project"] = Project::get_next_project($id,$project["project"]["project_date"],$lang);

        // Return
        return view('projects.show')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data)
            ->with('project',$project);
    }

}