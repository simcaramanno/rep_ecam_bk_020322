<?php
namespace App\Http\Controllers;
use App\Models\AppFunctions;
use App\Models\ContactRequest;
use App\Models\Language;
use App\Models\Platform;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
class PrivacyController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
    }


    // GET - INDEX
    public function index($lang="",Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,10);
        // Page data
        $page_data = self::get_page_data($lang,10);

        // Return
        return view('privacy.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }

    // GET PAGE DATA
    private function get_page_data($lang,$page_id)
    {
        // Sections
        $sections = AppFunctions::get_page_sections_data($lang,$page_id);

        // Return
        $data = array(
            "sections"=>$sections
        );
        return $data;
    }

}