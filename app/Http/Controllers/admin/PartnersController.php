<?php
namespace App\Http\Controllers\admin;
use App\Helper;
use App\Models\AppFunctions;
use App\Models\ContactRequest;
use App\Models\Event;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Partner;
use App\Models\Platform;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
class PartnersController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('partners','index');
        // Data
        $where = [['partners.status', '!=', 'deleted']];
        $data = Partner::get_list($where,null);

        // Return
        return view('admin.partners.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }

    // GET - CREATE PROJECT
    public function create(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('partners','create');

        // Return
        return view('admin.partners.create')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('GV',$this->GV);
    }
    // POST - STORE PROJECT
    public function store(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('store',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/partners/create')->withInput();
        }



        // Insert
        try {

            // IMAGE PATHS
            $temp_path_event = 'images/partners/temp/';
            $path_event = 'images/partners/';

            // ------------------------------------- small_image
            $small_image = "";
            // CHECK && UPLOAD IMAGE
            if( !$req->hasFile('image') )	{
                \Session::flash('error',$this->GV->err_image_empty);
                return redirect('admin/archive/partners/create/'.$req->id)->withInput();
            }
            $extension       = $req->file('image')->getClientOriginalExtension();
            $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
            $type_mime_img   = $req->file('image')->getMimeType();
            // Check mime
//            if(!in_array($type_mime_img,$this->GV->image_mimes)){
//                \Session::flash('error',$this->GV->err_image_mime);
//                return redirect('admin/archive/partners/create/'.$req->id)->withInput();
//            }
            // Check sizefile
//            $sizeFile = $req->file('image')->getSize(); //bytes
//            if($this->GV->image_size_partners["image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_partners["image"]["size"]*1000000)){
//                \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_partners["image"]["size"].' MB');
//                return redirect('admin/archive/partners/create/'.$req->id)->withInput();
//            }
            // Set name
            $small_image = Str::slug($req->name,'-').'-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
            if( $req->file('image')->move($temp_path_event, $small_image) ) {
                set_time_limit(0);
                $original = $temp_path_event.$small_image;
                $width = Helper::getWidth( $original );
                $height = Helper::getHeight( $original );
                // Check width
                if($this->GV->image_size_partners["image"]["w"]>0){
                    if($width!=$this->GV->image_size_partners["image"]["w"]){
                        \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_partners["image"]["w"].'x'.$this->GV->image_size_partners["image"]["h"].'px');
                        return redirect('admin/archive/partners/create/'.$req->id)->withInput();
                    }
                }
                // Check height
                if($this->GV->image_size_partners["image"]["h"]>0){
                    if($height!=$this->GV->image_size_partners["image"]["h"]){
                        \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_partners["image"]["w"].'x'.$this->GV->image_size_partners["image"]["h"].'px');
                        return redirect('admin/archive/partners/create/'.$req->id)->withInput();
                    }
                }
            }

            $project = New Partner();
            $project->created_at = Carbon::now('GMT+2');
            $project->last_edit = Carbon::now('GMT+2');
            $project->name = $req->name;
            $project->url = $req->url;
            $project->image = $small_image;
            $project->image_hover = $small_image;
            $project->image_alt = Str::slug($req->name,'-');
            $project->status = 'inactive';
            $project->save();

            // save img def and remove temp
            \File::copy($temp_path_event.$small_image, $path_event.$small_image);
            \File::delete($temp_path_event.$small_image);

            // Return
            return redirect("admin/archive/partners/edit-data/".$project->id);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect("admin/archive/partners/create")->withInput();
    }

    // GET - EDIT DATA
    public function edit_data($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('partners','edit');
        // Check
        if(!Partner::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Partner::get_data_id($id);

        // Return
        return view('admin.partners.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('GV',$this->GV);
    }
    // POST - UPDATE DATA
    public function update_data(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_data',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $pr = Partner::get_data_id($req->id);
            $pr->last_edit = Carbon::now('GMT+2');
            $pr->name = $req->name;
            $pr->url = $req->url;
            $pr->status = $req->status;
            $pr->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/partners/edit-data/'.$req->id);
    }
    // POST - UPDATE IMAGE
    public function update_image(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_image',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
        }

        // IMAGE PATHS
        $temp_path_event = 'images/partners/temp/';
        $path_event = 'images/partners/';

        // ------------------------------------- small_image
        $small_image = "";
        // CHECK && UPLOAD IMAGE
        if( !$req->hasFile('image') )	{
            \Session::flash('error',$this->GV->err_image_empty);
            return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
        }
        $extension       = $req->file('image')->getClientOriginalExtension();
        $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
        $type_mime_img   = $req->file('image')->getMimeType();
        // Check mime
//        if(!in_array($type_mime_img,$this->GV->image_mimes)){
//            \Session::flash('error',$this->GV->err_image_mime);
//            return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
//        }
        // Check sizefile
//        $sizeFile = $req->file('image')->getSize(); //bytes
//        if($this->GV->image_size_partners["image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_partners["image"]["size"]*1000000)){
//            \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_partners["image"]["size"].' MB');
//            return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
//        }
        // Set name
        $small_image = Str::slug($req->name,'-').'-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
        if( $req->file('image')->move($temp_path_event, $small_image) ) {
            set_time_limit(0);
            $original = $temp_path_event.$small_image;
            $width = Helper::getWidth( $original );
            $height = Helper::getHeight( $original );
//            // Check width
//            if($this->GV->image_size_partners["image"]["w"]>0){
//                if($width!=$this->GV->image_size_partners["image"]["w"]){
//                    \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_partners["image"]["w"].'x'.$this->GV->image_size_partners["image"]["h"].'px');
//                    return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
//                }
//            }
//            // Check height
//            if($this->GV->image_size_partners["image"]["h"]>0){
//                if($height!=$this->GV->image_size_partners["image"]["h"]){
//                    \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_partners["image"]["w"].'x'.$this->GV->image_size_partners["image"]["h"].'px');
//                    return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
//                }
//            }
        }

        // Update
        try {
            $pr = Partner::get_data_id($req->id);
            $pr->last_edit = Carbon::now('GMT+2');
            $pr->image = $small_image;
            $pr->save();

            // save img def and remove temp
            \File::copy($temp_path_event.$small_image, $path_event.$small_image);
            \File::delete($temp_path_event.$small_image);

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/partners/edit-data/'.$req->id);
    }
    // POST - SORT PARTNERS
    public function sort_partners(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('sort_partners',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/partners/index')->withInput();
        }

        // Update
        try {
            $pr = Partner::get_active_list();
            if(!empty($pr) && sizeof($pr)>0){
                foreach($pr as $sl){
                    if($req->get('sorting-'.$sl->id) && !empty($req->get('sorting-'.$sl->id))){
                        $sl->sorting = $req->get('sorting-'.$sl->id);
                        $sl->save();
                    }
                }
            }

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/partners/index')->withInput();
    }

    // POST - DELETE
    public function delete(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('delete',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $pr = Partner::get_data_id($req->id);
            $pr->delete();

            // Mess ko
            return redirect("admin/archive/partners/index");
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/partners/edit-data/'.$req->id)->withInput();
    }


    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='update_image' || $method=='delete'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!Partner::check_id_exists($req->id,false)){
                return $this->GV->err_generic_save.' (progetto non trovato)';
            }
        }
        if($method=='update_data'){
            if(!isset($req->status) || empty($req->status) || ($req->status!=='active' && $req->status!=='inactive')){
                return $this->GV->err_generic_save.' (status non trovato)';
            }
        }


        if($method=='store' || $method=='update_data'){
            if(!isset($req->name) || empty($req->name)){
                return $this->GV->err_required_field.'nome';
            }
            if(!isset($req->url) || empty($req->url)){
                return $this->GV->err_required_field.'link';
            }
        }

        if($method=='delete'){
            /*if(!isset($req->status) || ($req->status!=='active' && $req->status!=='inactive') ){
                return $this->GV->err_generic_save;
            }
            $pro = Partner::get_data_id($req->id);
            if(($pro->status=='active' && $req->status=='active')
                || ($pro->status=='inactive' && $req->status=='inactive') ){
                return $this->GV->err_generic_save;
            }*/
        }


        // OK
        return "";
    }

}