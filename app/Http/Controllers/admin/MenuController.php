<?php
namespace App\Http\Controllers\admin;
use App\Models\AppFunctions;
use App\Models\CategoryText;
use App\Models\ContactRequest;
use App\Models\Event;
use App\Models\GlobalVars;
use App\Models\Menu;
use App\Models\MenuText;
use App\Models\Platform;
use App\Models\Post;
use App\Models\Project;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
class MenuController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('menu','index');
        // Data
        $where = [['menu.status', '!=', 'deleted']];
        $orderby = array('menu.name' => 'asc');
        $data = Menu::get_list($where,$orderby);

        // Return
        return view('admin.cms.menu.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }


    // GET - EDIT DATA
    public function edit_data($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('menu','edit');
        // Check
        if(!Menu::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Menu::get_data_id($id);
        $data_texts = Menu::get_data_id_lang($id,$this->G_active_lang);

        // Return
        return view('admin.cms.menu.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE DATA
    public function update_data(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_data',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/cms/menu/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $tag = Menu::get_data_id($req->id);
            $tag->name = $req->name;
            if($req->id!=1 && isset($req->status)){
                $tag->status = $req->status;
            }
            $tag->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/cms/menu/edit-data/'.$req->id);
    }

    // GET - EDIT DETAILS
    public function edit_details($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('menu','edit');
        // Check
        if(!Menu::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Menu::get_data_id($id);
        $data_texts = Menu::get_data_id_lang($id,$this->G_active_lang);

        // Return
        return view('admin.cms.menu.edit-details')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE DETAILS
    public function update_details(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_details',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/cms/menu/edit-details/'.$req->id)->withInput();
        }

        // Update
        try {
            $tagT = MenuText::get_data_id($req->text_id);
            $tagT->name = $req->name;
            $tagT->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/cms/menu/edit-details/'.$req->id);
    }


    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='update_details'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!Menu::check_id_exists($req->id,false)){
                return $this->GV->err_generic_save.' (menu non trovato)';
            }
            if(!isset($req->name) || empty($req->name)){
                return $this->GV->err_required_field.'nome';
            }
        }
        if($method=='update_data' && $req->id!=1){
            if(!isset($req->status) || ($req->status!=='active' && $req->status!=='inactive') ){
                return $this->GV->err_required_field.'status';
            }
        }

        if($method=='update_details'){
            if(!isset($req->lang) || empty($req->lang)){
                return $this->GV->err_generic_save.' (lingua non trovata)';
            }
            if(!isset($req->lang) || empty($req->lang)
                || !isset($req->text_id) || empty($req->text_id)){
                return $this->GV->err_generic_save.' (id testo non trovato)';
            }
            if(!MenuText::check_id_exists($req->text_id,$req->id)){
                return $this->GV->err_generic_save.' (testo progetto non trovato)';
            }
            if(!isset($req->section) || empty($req->section)){
                return $this->GV->err_generic_save.' (sezione non trovata)';
            }
        }

        // OK
        return "";
    }


}