<?php
namespace App\Http\Controllers\admin;
use App\Helper;
use App\Models\AppFunctions;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Post;
use App\Models\PostText;
use App\Models\RelPostImage;
use App\Models\RelPostTag;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
class ArticlesController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('articles','index');
        // Data
        $where = [['posts.status', '!=', 'deleted']];
        $orderby = array('posts.id' => 'desc');
        $data = Post::get_list($where,$orderby);

        // Return
        return view('admin.press.articles.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }


    // GET - CREATE ARTICLE
    public function create(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('articles','create');

        // Return
        return view('admin.press.articles.create')
            ->with('platform',$this->platform)
            ->with('layout',$layout);
    }
    // POST - STORE ARTICLE
    public function store(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('store',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/articles/create')->withInput();
        }

        // Insert
        try {
            $pos = New Post();
            $pos->created_at = Carbon::now('GMT+2');
            $pos->last_edit = Carbon::now('GMT+2');
            $pos->user_id = Auth::user()->id;
            $pos->name = $req->name;
            $pos->newspaper = $req->newspaper;
            $pos->external_link = $req->external_link;
            $pos->is_arabic = $req->is_arabic;
            $pos->category_id = 1;
            $pos->slug = Str::slug($pos->name,'-');
            $pos->status = 'inactive';
            $pos->published_at = Carbon::now('GMT+2');
            $pos->save();

            // Insert POST-TEXTS
            $langs = Language::get_list(null,null,null);
            if(!empty($langs) && sizeof($langs)>0){
                foreach($langs as $lang){
                    $proT = new PostText();
                    $proT->created_at = Carbon::now('GMT+2');
                    $proT->last_edit = Carbon::now('GMT+2');
                    $proT->post_id = $pos->id;
                    $proT->lang = $lang->slug;
                    $proT->title = $pos->name;
                    $proT->slug = Str::slug($pos->name,'-');
                    $proT->save();
                }
            }

            // Return
            return redirect("admin/archive/press/articles/edit-data/".$pos->id);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect("admin/archive/press/articles/create")->withInput();
    }

    // GET - EDIT DATA
    public function edit_data($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('articles','edit');
        // Check
        if(!Post::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Post::get_data_id($id);
        $data_texts = Post::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_post_is_activable($data->id);

        // Return
        return view('admin.press.articles.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('lang',$this->G_active_lang)
            ->with('GV',$this->GV)
            ->with('activable',$activable);
    }
    // POST - UPDATE DATA
    public function update_data(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_data',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/articles/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $pos = Post::get_data_id($req->id);
            $pos->last_edit = Carbon::now('GMT+2');
            $pos->name = $req->name;
            $pos->newspaper = $req->newspaper;
            $pos->external_link = $req->external_link;
            $pos->is_arabic = $req->is_arabic;
            if($pos->status=='active' && isset($req->published_at) && !empty($req->published_at)){
                $date = AppFunctions::convert_date_format($req->published_at,'dd-mm-yyyy','yyyy-mm-dd');
                $pos->published_at = $date.' 00:00:00';
            }
            $pos->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/articles/edit-data/'.$req->id);
    }

    // GET - EDIT DETAILS
    public function edit_details($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('articles','edit');
        // Check
        if(!Post::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Post::get_data_id($id);
        $data_texts = Post::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_post_is_activable($data->id);
        // Tags
        $all_tags = array();
        $data_tags = array();
        $tags = Tag::get_active_list();
        $rel_tags = RelPostTag::get_list_bypost($id);
        if(!empty($tags) && sizeof($tags)>0){
            foreach($tags as $tag){
                // All tags
                $where = [['rel_post_tags.post_id', '=', $id],['rel_post_tags.tag_id', '=',$tag->id]];
                if(RelPostTag::get_count_where($where)==0){
                    array_push($all_tags,$tag);
                }
                // Rel tags
                if(!empty($rel_tags) && sizeof($rel_tags)>0){
                    foreach($rel_tags as $rel){
                        if($rel->tag_id==$tag->id){
                            $item = array(
                                "id"=>$rel->id,
                                "tag_id"=>$rel->tag_id,
                                "tag_name"=>$tag->name,
                            );
                            array_push($data_tags,$item);
                        }
                    }
                }
            }
        }



        // Return
        return view('admin.press.articles.edit-details')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('data_tags',$data_tags)
            ->with('all_tags',$all_tags)
            ->with('lang',$this->G_active_lang)
            ->with('GV',$this->GV)
            ->with('activable',$activable);
    }
    // POST - UPDATE DETAILS
    public function update_details(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_details',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/articles/edit-details/'.$req->id)->withInput();
        }

        // Data
        $pos = Post::get_data_id($req->id);
        $pos_text = PostText::get_data_id($req->text_id);

        // Update
        try {
            // descriptions
            if($req->section=='descriptions'){
                $pos->last_edit = Carbon::now('GMT+2');
                $pos->save();
                $pos_text->last_edit = Carbon::now('GMT+2');
                $pos_text->title = $req->title;
                $pos_text->slug = Str::slug($req->title,'-');
                $pos_text->content = $req->post_content;
                $pos_text->save();

                // Clear tags
                if(isset($req->btnRemoveHtml) && $req->btnRemoveHtml==1){
                    //echo '<br>Originale:<br>';
                    //echo $pos_text->content;

                    $content_review = str_replace("</p>","|||",$pos_text->content);
                    //echo '<br><br>content_review:<br>'.$content_review;
                    //$cleanText = strip_tags($content_review, ['p','b','i','br']);
                    $cleanText = strip_tags($content_review, '<br>');
                    //echo '<br><br>cleanText:<br>'.$cleanText;
                    $new_content =  str_replace("|||","<br>",$cleanText);
                    //echo '<br><br>new_content:<br>'.$new_content;
                    $pos_text->content = $new_content;
                    $pos_text->save();
                    //exit;
                }
            }
            // meta
            if($req->section=='meta'){
                $pos->last_edit = Carbon::now('GMT+2');
                $pos->save();
                $pos_text->last_edit = Carbon::now('GMT+2');
                $pos_text->meta_title = $req->meta_title;
                $pos_text->meta_desc = $req->meta_desc;
                $pos_text->meta_keys = $req->meta_keys;
                $pos_text->save();
            }
            // add_tag
            if($req->section=='add_tag'){
                $pos->last_edit = Carbon::now('GMT+2');
                $pos->save();
                // New tag
                $tag = new RelPostTag();
                $tag->post_id = $req->id;
                $tag->tag_id = $req->tag_id;
                $tag->save();
            }
            // delete_tag
            if($req->section=='delete_tag'){
                $pos->last_edit = Carbon::now('GMT+2');
                $pos->save();
                // Delete tag
                $where = [['rel_post_tags.id', '=', $req->item_id],['rel_post_tags.post_id', '=', $req->id]];
                if(RelPostTag::get_count_where($where)==1){
                    $tag = RelPostTag::get_data($where);
                    $tag->delete();
                }
            }


            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/articles/edit-details/'.$req->id);
    }

    // GET - EDIT GALLERY
    public function edit_gallery($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('articles','edit');
        // Check
        if(!Post::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Post::get_data_id($id);
        $data_texts = Post::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_post_is_activable($data->id);

        // Return
        return view('admin.press.articles.edit-gallery')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('lang',$this->G_active_lang)
            ->with('GV',$this->GV)
            ->with('activable',$activable);
    }
    // POST - UPDATE IMAGES
    public function update_gallery(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_gallery',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
        }

        // Data
        $pos = Post::get_data_id($req->id);
        $pos_text = PostText::get_data_id($req->text_id);


        // Update
        try {

            // IMAGE PATHS
            $temp_path_event = 'images/press/temp/';
            $path_event = 'images/press/';

            // ------------------------------------- store_new_item
            $filename_small = "";
            $filename_big = "";
            $filename_800x1010 = "";
            if($req->section=='store_new_item'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty.' (Immagine miniatura)');
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image2') )	{
                    \Session::flash('error',$this->GV->err_image_empty.' (Immagine dettaglio)');
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image3') )	{
                    \Session::flash('error',$this->GV->err_image_empty.' (Immagine lista)');
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }

                // ------------------------------------- $filename_small
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime.' (Immagine miniatura)');
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_press["filename_small"]["size"]>0 && ($sizeFile)>($this->GV->image_size_press["filename_small"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_press["filename_small"]["size"].' (Immagine miniatura)');
//                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                }
                // Set name
                $filename_small = $pos->id.'-small-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $filename_small) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$filename_small;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_press["filename_small"]["w"]>0){
//                        if($width!=$this->GV->image_size_press["filename_small"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_press["filename_small"]["w"].'x'.$this->GV->image_size_press["filename_small"]["h"].'px'.' (Immagine miniatura)');
//                            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_press["filename_small"]["h"]>0){
//                        if($height!=$this->GV->image_size_press["filename_small"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_press["filename_small"]["w"].'x'.$this->GV->image_size_press["filename_small"]["h"].'px'.' (Immagine miniatura)');
//                            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                        }
//                    }
                }


                // ------------------------------------- $filename_big
                $extension       = $req->file('image2')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image2')->getClientOriginalName());
                $type_mime_img   = $req->file('image2')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime.' (Immagine dettaglio)');
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }
//                // Check sizefile
//                $sizeFile = $req->file('image2')->getSize(); //bytes
//                if($this->GV->image_size_press["filename_big"]["size"]>0 && ($sizeFile)>($this->GV->image_size_press["filename_big"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_press["filename_big"]["size"].' (Immagine dettaglio)');
//                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                }
                // Set name
                $filename_big = $pos->id.'-big-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image2')->move($temp_path_event, $filename_big) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$filename_big;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_press["filename_big"]["w"]>0){
//                        if($width!=$this->GV->image_size_press["filename_big"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_press["filename_big"]["w"].'x'.$this->GV->image_size_press["filename_big"]["h"].'px'.' (Immagine dettaglio)');
//                            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_press["filename_big"]["h"]>0){
//                        if($height!=$this->GV->image_size_press["filename_big"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_press["filename_big"]["w"].'x'.$this->GV->image_size_press["filename_big"]["h"].'px'.' (Immagine dettaglio)');
//                            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                        }
//                    }
                }



                // ------------------------------------- $filename_800x1010
                $extension       = $req->file('image3')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image3')->getClientOriginalName());
                $type_mime_img   = $req->file('image3')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime.' (Immagine lista)');
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }
//                // Check sizefile
//                $sizeFile = $req->file('image3')->getSize(); //bytes
//                if($this->GV->image_size_press["filename_800x1010"]["size"]>0 && ($sizeFile)>($this->GV->image_size_press["filename_800x1010"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_press["filename_800x1010"]["size"].' (Immagine lista)');
//                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                }
                // Set name
                $filename_800x1010 = $pos->id.'-big2-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image3')->move($temp_path_event, $filename_800x1010) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$filename_800x1010;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_press["filename_800x1010"]["w"]>0){
//                        if($width!=$this->GV->image_size_press["filename_800x1010"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_press["filename_800x1010"]["w"].'x'.$this->GV->image_size_press["filename_800x1010"]["h"].'px'.' (Immagine lista)');
//                            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_press["filename_800x1010"]["h"]>0){
//                        if($height!=$this->GV->image_size_press["filename_800x1010"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_press["filename_800x1010"]["w"].'x'.$this->GV->image_size_press["filename_800x1010"]["h"].'px'.' (Immagine lista)');
//                            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                        }
//                    }
                }


            }

            // ------------------------------------- edit_item
            $new_image = "";
            if($req->section=='edit_item'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }

                $size_w = 0;
                $size_h = 0;
                $size_dim = 0;
                if($req->image_type=='filename_small'){
                    $size_dim = $this->GV->image_size_press["filename_small"]["size"];
                    $size_w = $this->GV->image_size_press["filename_small"]["w"];
                    $size_h = $this->GV->image_size_press["filename_small"]["h"];
                }
                if($req->image_type=='filename_big'){
                    $size_dim = $this->GV->image_size_press["filename_big"]["size"];
                    $size_w = $this->GV->image_size_press["filename_big"]["w"];
                    $size_h = $this->GV->image_size_press["filename_big"]["h"];
                }
                if($req->image_type=='filename_800x1010'){
                    $size_dim = $this->GV->image_size_press["filename_800x1010"]["size"];
                    $size_w = $this->GV->image_size_press["filename_800x1010"]["w"];
                    $size_h = $this->GV->image_size_press["filename_800x1010"]["h"];
                }

                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($size_dim>0 && ($sizeFile)>($size_dim*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $size_dim.' MB');
//                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                }
                // Set name
                $new_image = $pos->id.'-'.$req->image_type.'-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $new_image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$new_image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($size_w>0){
//                        if($width!=$size_w){
//                            \Session::flash('error',$this->GV->err_image_width_height. $size_w.'x'.$size_h.'px');
//                            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                        }
//                    }
//                    // Check height
//                    if($size_h>0){
//                        if($height!=$size_h){
//                            \Session::flash('error',$this->GV->err_image_width_height. $size_w.'x'.$size_h.'px');
//                            return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
//                        }
//                    }
                } else {
                    \Session::flash('error',$this->GV->err_image_empty.' (err2)');
                    return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
                }
            }

            // store_new_item
            if($req->section=='store_new_item'){
                // save img def and remove temp
                \File::copy($temp_path_event.$filename_small, $path_event.$filename_small);
                \File::delete($temp_path_event.$filename_small);

                // save img def and remove temp
                \File::copy($temp_path_event.$filename_big, $path_event.$filename_big);
                \File::delete($temp_path_event.$filename_big);

                // save img def and remove temp
                \File::copy($temp_path_event.$filename_800x1010, $path_event.$filename_800x1010);
                \File::delete($temp_path_event.$filename_800x1010);

                // Is main
                $is_main = 0;
                if(RelPostImage::get_count_by_post($req->id)==0){
                    $is_main = 1;
                }

                // New
                $gall = new RelPostImage();
                $gall->created_at = Carbon::now('GMT+2');
                $gall->post_id = $pos->id;
                $gall->is_main = $is_main;
                $gall->filename_small = $filename_small;
                $gall->filename_big = $filename_big;
                $gall->filename_800x1010 = $filename_800x1010;
                $gall->save();

                // Post
                $pos->last_edit = Carbon::now('GMT+2');
                $pos->save();

            }
            // delete_item
            if($req->section=='delete_item'){
                $pos_gall = RelPostImage::get_data_id($req->item_id);
                $pos_gall->delete();

                // Post
                $pos->last_edit = Carbon::now('GMT+2');
                $pos->save();
            }
            // edit_item
            if($req->section=='edit_item'){
                // save img def and remove temp
                \File::copy($temp_path_event.$new_image, $path_event.$new_image);
                \File::delete($temp_path_event.$new_image);

                $pos_gall = RelPostImage::get_data_id($req->item_id);
                if($req->image_type=='filename_small'){
                    $pos_gall->filename_small = $new_image;
                    $pos_gall->save();
                }
                if($req->image_type=='filename_big'){
                    $pos_gall->filename_big = $new_image;
                    $pos_gall->save();
                }
                if($req->image_type=='filename_800x1010'){
                    $pos_gall->filename_800x1010 = $new_image;
                    $pos_gall->save();
                }

                // Post
                $pos->last_edit = Carbon::now('GMT+2');
                $pos->save();
            }
            // main_item
            if($req->section=='main_item'){
                // Others not main
                $where = [['post_images.post_id', '=', $req->id]];
                $others = RelPostImage::get_list($where,null);
                if(!empty($others) && sizeof($others)>0){
                    foreach($others as $ite){
                        $ite->is_main =0;
                        $ite->save();
                    }
                }

                // Is main
                $pos_gall = RelPostImage::get_data_id($req->item_id);
                $pos_gall->is_main = 1;
                $pos_gall->save();

                // Post
                $pos->last_edit = Carbon::now('GMT+2');
                $pos->save();
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/articles/edit-gallery/'.$req->id);
    }
    // POST - CHANGE STATUS
    public function change_status(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('change_status',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/articles/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $pos = Post::get_data_id($req->id);
            $pos->last_edit = Carbon::now('GMT+2');
            $pos->published_at = Carbon::now('GMT+2');
            $pos->status = $req->status;
            if($pos->status=='active'){
                $date = AppFunctions::convert_date_format($req->published_at,'dd-mm-yyyy','yyyy-mm-dd');
                $pos->published_at = $date.' 00:00:00';
            }
            $pos->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect("admin/archive/press/articles/edit-data/".$req->id);
    }
    // POST - DELETE
    public function delete(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('delete',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/articles/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $pos = Post::get_data_id($req->id);
            $pos->last_edit = Carbon::now('GMT+2');
            $pos->status = "deleted";
            $pos->save();

            // Mess ko
            return redirect("admin/archive/press/articles/index");
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect("admin/archive/press/articles/edit-data/".$req->id);
    }


    // Check if inactive post is activable
    private function check_post_is_activable($post_id)
    {
        $is_activable = true;
        $info = array();

        $pos = Post::get_data_id($post_id);

        // Texts
        $langs = Language::get_list(null,null,null);
        if(!empty($langs) && sizeof($langs)>0) {
            foreach ($langs as $lang) {
                $data_texts = Post::get_data_id_lang2($post_id,$lang->slug,false);
                $info_txt_lang = '[Lingua: '.$lang->slug.'] - ';

                if(empty($data_texts["post_text"]["title"])){
                    array_push($info,$info_txt_lang.'Titolo');
                }
                if(empty($data_texts["post_text"]["content"])){
                    array_push($info,$info_txt_lang.'Testo articolo');
                }
                if(empty($data_texts["post_text"]["meta_title"])){
                    array_push($info,$info_txt_lang.'Meta title');
                }
                if(empty($data_texts["post_text"]["meta_desc"])){
                    array_push($info,$info_txt_lang.'Meta description');
                }
                if(empty($data_texts["post_text"]["meta_keys"])){
                    array_push($info,$info_txt_lang.'Meta key words');
                }
            }
        }
        // Gallery
        if(RelPostImage::get_count_by_post($post_id)==0){
            array_push($info,'Immagine principale');
        }

        if(sizeof($info)>0){
            $is_activable = false;
        }


        $res = array(
            "is_activable"=>$is_activable,
            "info"=>$info
        );

        return $res;
    }

    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='delete' || $method=='change_status'
            || $method=='update_details' || $method=='update_gallery'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!Post::check_id_exists($req->id,false)){
                return $this->GV->err_generic_save.' (articolo non trovato)';
            }
        }

        if($method=='update_details' || $method=='update_gallery' ){
            if(!isset($req->lang) || empty($req->lang)){
                return $this->GV->err_generic_save.' (lingua non trovata)';
            }
            if(!isset($req->lang) || empty($req->lang)
                || !isset($req->text_id) || empty($req->text_id)){
                return $this->GV->err_generic_save.' (id testo non trovato)';
            }
            if(!PostText::check_id_exists($req->text_id,$req->id)){
                return $this->GV->err_generic_save.' (testo articolo non trovato)';
            }
            if(!isset($req->section) || empty($req->section)){
                return $this->GV->err_generic_save.' (sezione non trovata)';
            }
        }

        if($method=='update_gallery'){
            if($req->section=='delete_item' || $req->section=='edit_item' || $req->section=='main_item'){
                if(!isset($req->item_id) || empty($req->item_id)){
                    return $this->GV->err_required_field.'elemento non trovato';
                }
                if(!RelPostImage::check_id_exists($req->item_id,$req->id)){
                    return $this->GV->err_generic_save.' (elemento non trovato)';
                }
                if($req->section=='delete_item'){
                    $pos_gall = RelPostImage::get_data_id($req->item_id);
                    if($pos_gall->is_main==1){
                        return $this->GV->err_post_delete;
                    }
                }
                if($req->section!='main_item' && $req->section!='delete_item'){
                    if(!isset($req->image_type) || ($req->image_type!='filename_small' && $req->image_type!='filename_big' && $req->image_type!='filename_800x1010') ){
                        return $this->GV->err_generic_save.' (immagine non trovata)';
                    }
                }
                if($req->section=='main_item'){
                    $pos_gall = RelPostImage::get_data_id($req->item_id);
                    if(empty($pos_gall->filename_small) || empty($pos_gall->filename_big) || empty($pos_gall->filename_800x1010) ){
                        return $this->GV->err_generic_save.' (tutte le immagini devono essere inserite per impostare come elemento principale)';
                    }
                }
            }
        }



        if($method=='store' || $method=='update_data'){
            if(!isset($req->name) || empty($req->name)){
                return $this->GV->err_required_field.'nome';
            }
            if(!isset($req->newspaper) || empty($req->newspaper)){
                return $this->GV->err_required_field.'testata';
            }
            if(!isset($req->is_arabic)){
                return $this->GV->err_required_field.'articolo arabo';
            }
            if($method=='update_data'){
                $post = Post::get_data_id($req->id);
                if($post->status=='active'){
                    if(!isset($req->published_at) || empty($req->published_at)){
                        return $this->GV->err_required_field.'data pubblicazione';
                    }
                    if(strlen($req->published_at)!=10){
                        return $this->GV->err_required_field.'data pubblicazione valida';
                    }
                }
            }
        }

        if($method=='change_status'){
            if($req->status=='active'){
                $act = self::check_post_is_activable($req->id);
                if(!$act["is_activable"]){
                    return 'Articolo non pubblicabile: mancano le informazioni';
                }
                if(!isset($req->published_at) || empty($req->published_at)){
                    return $this->GV->err_required_field.'data pubblicazione';
                }
                if(strlen($req->published_at)!=10){
                    return $this->GV->err_required_field.'data pubblicazione valida';
                }
            }
        }

        if($method=='update_details'){
            if($req->section=='descriptions'){
                if(!isset($req->title) || empty($req->title)){
                    return $this->GV->err_required_field.'titolo';
                }
            }
            if($req->section=='meta'){
                if(!isset($req->meta_title) || empty($req->meta_title)){
                    return $this->GV->err_required_field.'meta title';
                }
                if(!isset($req->meta_desc) || empty($req->meta_desc)){
                    return $this->GV->err_required_field.'meta description';
                }
                if(!isset($req->meta_keys) || empty($req->meta_keys)){
                    return $this->GV->err_required_field.'meta keywords';
                }
            }
        }

        if($method=='delete'){
//            if(!isset($req->status) || ($req->status!=='active' && $req->status!=='inactive') ){
//                return $this->GV->err_generic_save;
//            }
//            $pro = Project::get_data_id($req->id);
//            if(($pro->status=='active' && $req->status=='active')
//                || ($pro->status=='inactive' && $req->status=='inactive') ){
//                return $this->GV->err_generic_save;
//            }
        }


        // OK
        return "";
    }

}