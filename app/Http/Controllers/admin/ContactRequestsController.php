<?php
namespace App\Http\Controllers\admin;
use App\Models\AppFunctions;
use App\Models\ContactRequest;
use App\Models\Platform;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
class ContactRequestsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('contact-requests','index');
        // Data
        $date_to = date('Y-m-d');
        $date_from = date('Y-m-d', strtotime($date_to. ' - 30 days'));
        if(!empty($req->get('date_from'))){
            $date_from = $req->get('date_from');
        }
        if(!empty($req->get('date_to'))){
            $date_to = $req->get('date_to');
        }
        $where = [['contact_requests.status', '=', 'active'],
            ['contact_requests.created_at', '<=',$date_to.' 23:23:00'],
            ['contact_requests.created_at', '>=',$date_from.' 00:00:01']];
        $data = ContactRequest::get_list($where,null);


        // Return
        return view('admin.contact-requests.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('date_from',$date_from)
            ->with('date_to',$date_to);
    }


    // GET - SHOW
    public function show($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('contact-requests','show');
        // Check
        if(!ContactRequest::check_id_exists($id,true)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = ContactRequest::get_data_id($id);

        // Return
        return view('admin.contact-requests.show')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }



}