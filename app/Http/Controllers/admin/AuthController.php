<?php

namespace App\Http\Controllers\admin;
use App\Models\AppFunctions;
use App\Models\Platform;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class AuthController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * __construct
     */
    public function __construct(Platform $platform,Guard $auth)
    {
        // Basic Data
        $this->platform = $platform::first();
        // Guard
        $this->auth = $auth;
    }

    // GET - Index
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('auth','login');

        // LOGOUT
        if(Auth::user()){
            Auth::logout();
            //Artisan::call('cache:clear');
            //\Illuminate\Support\Facades\Session::flush();
        }

        // Return
        return view('admin.auth.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout);
    }
    // POST - Signin
    public function signin(Request $req)
    {
        // VALIDATE
        $this->validate($req, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $text_err =  trans('auth.failed');
        $credentials = array(
            'email'=> strtolower($req->email),
            'password'=>$req->password);
        if ($this->auth->attempt($credentials, $req->has('remember'))) {
            if($this->auth->User()->status ==\Config::get('constants.status_active')
                && $this->auth->User()->role=='admin'){
                Auth::login(Auth::user(), true);
                // LOGIN
                return redirect()->intended('admin/contact-requests/index');
            }
            // NO LOGIN
            $this->auth->logout();
        }

        // Errors
        \Session::flash('error', $text_err);
        return redirect()
            ->back()
            ->withInput($req->only('email', 'remember'));
    }


    // GET - Forgot
    public function forgot(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('auth','forgot');

        // LOGOUT
        if(Auth::user()){
            Auth::logout();
            Artisan::call('cache:clear');
            \Illuminate\Support\Facades\Session::flush();
        }

        // Return
        return view('admin.auth.forgot')
            ->with('platform',$this->platform)
            ->with('layout',$layout);
    }

    /*
    |-----------------------------------
    | LOGOUT
    |--------- -------------------------
    */
    // POST - Logout
    public function logout(){
        if(Auth::user()){Auth::logout();  }
        //Artisan::call('cache:clear');
        //\Illuminate\Support\Facades\Session::flush();
        return redirect()->intended('home/index');
    }


    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        // ----------------------------------- recover
        if($method=='recover'){
            if(!isset($req->email) || empty($req->email)){
                return trans('validation.required', [ 'attribute' => 'email']);
            }
//            // Check user
//            $where = [['users.role', '=', 'admin'],
//                ['users.status', '=',\Config::get('constants.status_active')],
//                ['users.email', '=',strtolower($req->email)]];
//            if(User::get_count_where($where)!=1){
//                return trans('auth.recover_email_not_found');
//            }
        }

        // ----------------------------------- save_pw
        if($method=='save_pw'){
            if(!isset($req->tk) || empty($req->tk) || !isset($req->ft) || empty($req->ft)){
                return trans('auth.recover_impossibile');
            }
            // Check user
            $where = [['users.role', '=', 'admin'],
                ['users.status', '=', \Config::get('constants.status_active')],
                ['users.token', '=', $req->tk],
                ['users.forgot_token', '=', $req->ft]];
//            if(User::get_count_where($where)!=1){
//                return trans('auth.recover_impossibile');
//            }
            if(!isset($req->password) || empty($req->password)){
                return trans('validation.required', [ 'attribute' => 'nuova password']);
            }
            if(!isset($req->password2) || empty($req->password2)){
                return trans('validation.required', [ 'attribute' => 'conferma password']);
            }
            if( strlen($req->password)<8 ) {
                return trans('validation.min', [ 'attribute' => 'password'],[ 'min' => '8']);
            }
            if($req->password!=$req->password2 ) {
                return trans('auth.password_diverse');
            }
        }


        return "";
    }

}