<?php
namespace App\Http\Controllers\admin;
use App\Models\AppFunctions;
use App\Models\Category;
use App\Models\ContactRequest;
use App\Models\Event;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Post;
use App\Models\Project;
use App\Models\RelPostTag;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
class TagsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;
    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('tags','index');
        // Data
        $where = [['tags.status', '!=', 'deleted']];
        $orderby = array('tags.name' => 'asc');
        $data = Tag::get_list($where,$orderby);

        // Return
        return view('admin.press.tags.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }


    // GET - CREATE
    public function create(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('tags','create');

        // Return
        return view('admin.press.tags.create')
            ->with('platform',$this->platform)
            ->with('layout',$layout);
    }
    // POST - STORE
    public function store(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('store',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/tags/create')->withInput();
        }

        // Insert
        try {
            $cate = New Tag();
            $cate->created_at = Carbon::now('GMT+2');
            $cate->last_edit = Carbon::now('GMT+2');
            $cate->name = $req->name;
            $cate->slug = Str::slug($req->name,'-');
            $cate->status = 'active';
            $cate->save();
            // Return
            return redirect("admin/archive/press/tags/edit-data/".$cate->id);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/tags/create')->withInput();
    }

    // GET - EDIT DATA
    public function edit_data($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('tags','edit');
        // Check
        if(!Tag::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Tag::get_data_id($id);

        // Return
        return view('admin.press.tags.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }
    // POST - UPDATE DATA
    public function update_data(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_data',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/tags/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $tag = Tag::get_data_id($req->id);
            $tag->last_edit = Carbon::now('GMT+2');
            $tag->name = $req->name;
            $tag->slug = Str::slug($req->name,'-');
            $tag->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/tags/edit-data/'.$req->id);
    }
    // POST - DELETE
    public function delete(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('delete',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/tags/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $tag = Tag::get_data_id($req->id);
            $tag->delete();
            return redirect('admin/archive/press/tags/index');

        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/tags/edit-data/'.$req->id);
    }



    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='delete'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!Tag::check_id_exists($req->id,false)){
                return $this->GV->err_generic_save.' (tag non trovato)';
            }
        }

        if($method=='store' || $method=='update_data'){
            if(!isset($req->name) || empty($req->name)){
                return $this->GV->err_required_field.'nome';
            }
        }

        if($method=='delete'){
            $where = [['rel_post_tags.tag_id', '=', $req->id]];
            if(RelPostTag::get_count_where($where)>0){
                return $this->GV->err_tag_delete;
            }
        }


        // OK
        return "";
    }

}