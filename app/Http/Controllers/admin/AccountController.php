<?php
namespace App\Http\Controllers\admin;
use App\Helper;
use App\Models\AppFunctions;
use App\Models\Category;
use App\Models\ContactRequest;
use App\Models\Event;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Post;
use App\Models\Project;
use App\Models\RelPostTag;
use App\Models\Tag;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class AccountController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;
    /**
     * __construct
     */
    public function __construct(Platform $platform,Guard $auth)
    {
        $this->auth = $auth;
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('account','');
        // Data
        $data = User::get_data_id(Auth::user()->id);

        // Return
        return view('admin.account.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }
    // GET - UPDATE EMAIL
    public function update_email(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_email',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/account/index')->withInput();
        }

        // Update
        try {
            $data = User::get_data_id(Auth::user()->id);
            $data->email = strtolower($req->email);
            $data->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/account/index')->withInput();
    }
    // GET - UPDATE PW
    public function update_pw(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_pw',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/account/index')->withInput();
        }

        // Update
        try {
            $data = User::get_data_id(Auth::user()->id);
            $data->password = Hash::make($req->password2);
            $data->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/account/index')->withInput();
    }


    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if(!isset($req->id) || empty($req->id)){
            return $this->GV->err_generic_save.' (id non trovato)';
        }
        if(Auth::user()->id!=$req->id){
            return $this->GV->err_generic_save;
        }
        if($method=='update_email'){
            if(!isset($req->email) || empty($req->email)){
                return $this->GV->err_required_field.'email';
            }
            // dup email
            if(!User::check_duplicati_email(strtolower($req->email),Auth::user()->id)){
                return 'email non disponibile';
            }
        }
        if($method=='update_pw'){
            if (!isset($req->password1) || empty($req->password1)) {
                return $this->GV->err_required_field.'password attuale';
            }
            $credentials = array(
                'email'=> Auth::user()->email,
                'password'=>$req->password1);
            if (!$this->auth->attempt($credentials, $req->has('remember'))) {
                return 'password attuale invalida';
            }
            if (!isset($req->password2) || empty($req->password2)) {
                return $this->GV->err_required_field.'nuova password';
            }
            if (!isset($req->password3) || empty($req->password3)) {
                return $this->GV->err_required_field.'conferma nuova password';
            }
            if( strlen($req->password2)<8 ) {
                return 'la password deve avere almeno 8 caratteri';
            }
            if($req->password2!=$req->password3 ) {
                return 'le password non sono uguali';
            }
        }


        // OK
        return "";
    }

}