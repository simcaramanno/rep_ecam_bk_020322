<?php
namespace App\Http\Controllers\admin;
use App\Models\AppFunctions;
use App\Models\Category;
use App\Models\CategoryText;
use App\Models\ContactRequest;
use App\Models\Event;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Post;
use App\Models\Project;
use App\Models\Tag;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
class UsersController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;
    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('users','index');
        // Data
        $where = [['users.role', '=', 'admin']];
        $orderby = array('users.name' => 'asc','users.surname' => 'asc');
        $data = User::get_list($where,$orderby);

        // Return
        return view('admin.users.staff.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }

    // GET - CREATE
    public function create(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('users','create');

        // Return
        return view('admin.users.staff.create')
            ->with('platform',$this->platform)
            ->with('layout',$layout);
    }
    // POST - STORE
    public function store(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('store',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/staff/users/create')->withInput();
        }

        try {
            // Insert
            $cate = New User();
            $cate->created_at = Carbon::now('GMT+2');
            $cate->updated_at = Carbon::now('GMT+2');
            $cate->name = $req->name;
            $cate->surname = $req->surname;
            $cate->email = strtolower($req->email);
            $cate->role = 'admin';
            $cate->status = 'active';
            $cate->save();

            // Return
            return redirect("admin/staff/users/edit-data/".$cate->id);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/staff/users/create')->withInput();
    }

    // GET - EDIT DATA
    public function edit_data($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('users','edit');
        // Check
        if(!User::check_id_exists($id,false,'admin')){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = User::get_data_id($id);

        // Return
        return view('admin.users.staff.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }
    // POST - UPDATE DATA
    public function update_data(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_data',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/staff/users/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $user = User::get_data_id($req->id);
            $user->updated_at = Carbon::now('GMT+2');
            $user->name = $req->name;
            $user->surname = $req->surname;
            $user->email = strtolower($req->email);
            $user->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/staff/users/edit-data/'.$req->id)->withInput();
    }
    // POST - SEND CREDENTIALS
    public function send_credentials(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('send_credentials',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/staff/users/edit-data/'.$req->id)->withInput();
        }

        try {
            // Create password
            $password = Str::random(8);
            if(isset($req->password) && !empty($req->password)){
                $password = $req->password;
            }

            // Update
            $us = User::get_data_id($req->id);
            $us->updated_at = Carbon::now('GMT+2');
            $us->password = Hash::make($password);
            $us->save();

            // Send email
            $user_data = array(
                "name"=>$us->name,
                "surname"=>$us->surname,
                "email"=>$us->email,
                "password"=>$password
            );
            // --------------------------------------- SEND EMAIL: USER ---------------------------------------
            $platform_url = url('/');
            $login_url = url('admin-area/login');
            $subject = $this->platform->site_name." - Admin Panel";
            $all_data_arr = array(
                'platform_url'=>$platform_url,
                'login_url'=>$login_url,
                'site_name'=>$this->platform->site_name,
                'user'=>$user_data
            );
            $email_template = 'admin.email-templates.admin-user-credentials';
            $emailto = strtolower($us->email);
            $err_send_email = AppFunctions::send_email($this->platform,$subject,$all_data_arr,$email_template,$emailto);
            if(empty($err_send_email)){
                // Mess ok
                \Session::flash('message', "Credenziali di accesso inviate");
                return redirect('admin/staff/users/edit-data/'.$req->id)->withInput();
            }

            // Return
            \Session::flash('error',"Impossibile inviare le credenziali". ' ('.$err_send_email.')');
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/staff/users/edit-data/'.$req->id)->withInput();
    }
    // POST - DELETE
    public function delete(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('delete',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/staff/users/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $user = User::get_data_id($req->id);
            $user->delete();
            return redirect('admin/staff/users/index');

        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/staff/users/edit-data/'.$req->id)->withInput();
    }


    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='delete' || $method=='send_credentials'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!User::check_id_exists($req->id,false,'admin')){
                return $this->GV->err_generic_save.' (utente non trovato)';
            }
        }

        if($method=='store' || $method=='update_data'){
            $id=null;
            if($method=='update_data'){
                $id=$req->id;
            }
            if(!User::check_duplicati_email(strtolower($req->email),$id)){
                return 'Email non disponibile';
            }
            if(!isset($req->name) || empty($req->name)){
                return $this->GV->err_required_field.'nome';
            }
            if(!isset($req->surname) || empty($req->surname)){
                return $this->GV->err_required_field.'cognome';
            }
            if(!isset($req->email) || empty($req->email)){
                return $this->GV->err_required_field.'email';
            }
        }

        if($method=='delete'){
            if($req->id==1){
                return $this->GV->err_user_delete;
            }
        }


        // OK
        return "";
    }

}