<?php
namespace App\Http\Controllers\admin;
use App\Helper;
use App\Models\AppFunctions;
use App\Models\Category;
use App\Models\ContactRequest;
use App\Models\Event;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Post;
use App\Models\Project;
use App\Models\RelPostTag;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
class SettingsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;
    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - COMPANY
    public function company(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('settings','company');
        // Data
        $data = Platform::first();

        // Return
        return view('admin.settings.company')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }
    // GET - UPDATE COMPANY
    public function update_company(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_company',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/settings/company/index')->withInput();
        }

        // Update
        try {
            $data = Platform::first();
            $data->company_name = $req->company_name;
            $data->company_country = $req->company_country;
            $data->company_city = $req->company_city;
            $data->company_address = $req->company_address;
            $data->company_zip = $req->company_zip;
            $data->company_vatcode = $req->company_vatcode;
            $data->company_fc = $req->company_fc;
            $data->phone = $req->phone;
            $data->email_to = $req->email_to;
            $data->email_from = $req->email_from;
            $data->email_from_name = $req->email_from_name;
            $data->email_sign = $req->email_sign;
            $data->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/settings/company/index')->withInput();
    }

    // GET - CONTACTS
    public function contacts(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('settings','contacts');
        // Data
        $data = Platform::first();

        // Return
        return view('admin.settings.contacts')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }
    // GET - UPDATE CONTACTS
    public function update_contacts(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_contacts',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/settings/contacts/index')->withInput();
        }

        // Update
        try {
            $data = Platform::first();
            $data->url_facebook = $req->url_facebook;
            $data->url_instagram = $req->url_instagram;
            $data->url_twitter = $req->url_twitter;
            $data->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/settings/contacts/index')->withInput();
    }

    // GET - IMAGES
    public function images(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('settings','images');
        // Data
        $data = Platform::first();

        // Return
        return view('admin.settings.images')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('GV',$this->GV);
    }
    // GET - UPDATE IMAGES
    public function update_images(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_contacts',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/settings/images/index')->withInput();
        }

        // IMAGE PATHS
        $temp_path_event = 'images/logo/temp/';
        $path_event = 'images/logo/';

        $image = "";
        // CHECK && UPLOAD IMAGE
        if( !$req->hasFile('image') )	{
            \Session::flash('error',$this->GV->err_image_empty);
            return redirect('admin/settings/images/index')->withInput();
        }
        $extension       = $req->file('image')->getClientOriginalExtension();
        $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
        $type_mime_img   = $req->file('image')->getMimeType();
        // Check mime
//        if(!in_array($type_mime_img,$this->GV->image_mimes)){
//            \Session::flash('error',$this->GV->err_image_mime);
//            return redirect('admin/settings/images/index')->withInput();
//        }

        // Set name
        $image = $req->image_type.'-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
        if( $req->file('image')->move($temp_path_event, $image) ) {
            set_time_limit(0);
            $original = $temp_path_event.$image;
        }

        // Update
        try {
            $data = Platform::first();

            // save img def and remove temp
            \File::copy($temp_path_event.$image, $path_event.$image);
            \File::delete($temp_path_event.$image);

            if($req->image_type=='main_logo'){
                $data->main_logo = $image;
                $data->save();
            }
            if($req->image_type=='logo_light'){
                $data->logo_light = $image;
                $data->save();
            }
            if($req->image_type=='logo_light_2x'){
                $data->logo_light_2x = $image;
                $data->save();
            }
            if($req->image_type=='logo_light_3x'){
                $data->logo_light_3x = $image;
                $data->save();
            }
            if($req->image_type=='logo_dark'){
                $data->logo_dark = $image;
                $data->save();
            }
            if($req->image_type=='logo_dark_2x'){
                $data->logo_dark_2x = $image;
                $data->save();
            }
            if($req->image_type=='logo_dark_3x'){
                $data->logo_dark_3x = $image;
                $data->save();
            }
            if($req->image_type=='favicon57'){
                $data->favicon57 = $image;
                $data->save();
            }
            if($req->image_type=='favicon72'){
                $data->favicon72 = $image;
                $data->save();
            }
            if($req->image_type=='favicon114'){
                $data->favicon114 = $image;
                $data->save();
            }


            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/settings/images/index')->withInput();
    }

    // GET - API KEYS
    public function apikeys(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('config','apikeys');
        // Data
        $data = Platform::first();

        // Return
        return view('admin.settings.api-keys')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }
    // GET - UPDATE API KEYS
    public function update_apikeys(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_apikeys',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/settings/config/api-keys/index')->withInput();
        }

        // Update
        try {
            $data = Platform::first();
            $data->google_apikey = $req->google_apikey;
            $data->rechaptca_apikey = $req->rechaptca_apikey;
            $data->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/settings/config/api-keys/index')->withInput();
    }

    // GET - META
    public function scripts(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('config','scripts');
        // Data
        $data = Platform::first();

        // Return
        return view('admin.settings.scripts')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }
    // GET - UPDATE META
    public function update_scripts(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_scripts',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/settings/config/scripts/index')->withInput();
        }

        // Update
        try {
            $data = Platform::first();
            $data->header_scripts = $req->header_scripts;
            $data->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/settings/config/scripts/index')->withInput();
    }


    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if(!isset($req->id) || empty($req->id) || $req->id!=1){
            return $this->GV->err_generic_save.' (id non trovato)';
        }

        if($method=='update_images'){
            if(!isset($req->image_type)){
                return $this->GV->err_generic_save.' (tipo immagine vuoto)';
            }
        }


        // OK
        return "";
    }

}