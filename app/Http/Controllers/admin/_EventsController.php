<?php
namespace App\Http\Controllers\admin;
use App\Helper;
use App\Models\AppFunctions;
use App\Models\ContactRequest;
use App\Models\Event;
use App\Models\EventDay;
use App\Models\EventSlider;
use App\Models\EventSpeaker;
use App\Models\EventText;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Platform;
use App\Models\RelEventDayProgram;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
class EventsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('events','index');
        // Data
        $where = [['events.status', '!=', 'deleted']];
        $data = Event::get_list($where,null,null);

        // Return
        return view('admin.events.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }

    // GET - CREATE EVENT
    public function create(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('events','create');

        // Return
        return view('admin.events.create')
            ->with('platform',$this->platform)
            ->with('layout',$layout);
    }
    // POST - STORE EVENT
    public function store(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('store',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/create')->withInput();
        }

        // Insert
        try {
            $event = New Event();
            $event->created_at = Carbon::now('GMT+2');
            $event->last_edit = Carbon::now('GMT+2');
            $event->name = $req->name;
            $event->date_start = AppFunctions::convert_date_format($req->date_start,'dd-mm-yyyy','yyyy-mm-dd');
            $event->date_end = AppFunctions::convert_date_format($req->date_end,'dd-mm-yyyy','yyyy-mm-dd');
            $event->location_address = $req->location_address;
            $event->location_marker = 'Italy,' . Str::slug($req->location_address,'-');
            $event->status = 'inactive';
            $event->save();

            // Insert EVENT-DAYS
            $start = new \DateTime($event->date_start);
            $end = new \DateTime($event->date_end);
            $i=0;
            while($start <= $end){
                $day_date = $start->format('Y-m-d');
                // Insert
                $eventDay = new EventDay();
                $eventDay->created_at = Carbon::now('GMT+2');
                $eventDay->last_edit = Carbon::now('GMT+2');
                $eventDay->event_id = $event->id;
                $eventDay->day_date = $day_date;
                $eventDay->save();
                // Add day
                $i++;
                $start->modify('+1 day');
            }

            // Insert EVENT-TEXTS
            $langs = Language::get_list(null,null,null);
            if(!empty($langs) && sizeof($langs)>0){
                foreach($langs as $lang){
                    $eventT = new EventText();
                    $eventT->created_at = Carbon::now('GMT+2');
                    $eventT->last_edit = Carbon::now('GMT+2');
                    $eventT->event_id = $event->id;
                    $eventT->lang = $lang->slug;
                    $eventT->slug = Str::slug($event->name,'-');
                    $eventT->counter1_txt = $this->GV->event_txt_counter1;
                    $eventT->counter2_txt = $this->GV->event_txt_counter2;
                    $eventT->counter3_txt = $this->GV->event_txt_counter3;
                    $eventT->home_btn_text = $this->GV->event_txt_home_btn_txt;
                    $eventT->save();
                }
            }

            // Return
            return redirect("admin/archive/events/edit-data/".$event->id);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect("admin/archive/events/create")->withInput();
    }

    // GET - EDIT DATA
    public function edit_data($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('events','edit');
        // Check
        if(!Event::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Event::get_data_id($id);
        $data_texts = Event::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_event_is_activable($data->id);

        // Return
        return view('admin.events.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE DATA
    public function update_data(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_data',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $event = Event::get_data_id($req->id);

            // Check EVENT-DAYS
            $edit_days = false;
            $old_date_start = $event->date_start;
            $old_date_end = $event->date_end;
            $new_date_start = AppFunctions::convert_date_format($req->date_start,'dd-mm-yyyy','yyyy-mm-dd');
            $new_date_end = AppFunctions::convert_date_format($req->date_end,'dd-mm-yyyy','yyyy-mm-dd');
            if($old_date_start!=$new_date_start || $old_date_end!=$new_date_end){
                $edit_days = true;
            }

            // Update event
            $event->last_edit = Carbon::now('GMT+2');
            $event->name = $req->name;
            $event->date_start = AppFunctions::convert_date_format($req->date_start,'dd-mm-yyyy','yyyy-mm-dd');
            $event->date_end = AppFunctions::convert_date_format($req->date_end,'dd-mm-yyyy','yyyy-mm-dd');
            $event->location_address = $req->location_address;
            $event->location_marker = 'Italy,' . Str::slug($req->location_address,'-');
            $event->save();

            // Update days
            if($edit_days){
                // New days
                $new_days = array();
                $start = new \DateTime($new_date_start);
                $end = new \DateTime($new_date_end);
                $i=0;
                while($start <= $end){
                    $day_date = $start->format('Y-m-d');
                    array_push($new_days,$day_date);
                    // Add day
                    $i++;
                    $start->modify('+1 day');
                }
                // Old days
                $old_days = EventDay::get_list_event($event->id);
                if(!empty($old_days) && sizeof($old_days)>0){
                    foreach($old_days as $old_day){
                        $to_delete = true;
                        foreach($new_days as $new_day){
                            if($old_day->day_date==$new_day){
                                $to_delete = false;
                            }
                        }
                        // Delete
                        if($to_delete){
                            // All program records
                            $where = [['rel_event_program.event_day_id', '=', $old_day->id]];
                            $program_items = RelEventDayProgram::get_list($where,null);
                            if(!empty($program_items) && sizeof($program_items)>0){
                                foreach($program_items as $program_item){
                                    // Delete day program row
                                    $program_item->delete();
                                }
                            }
                            // Delete day
                            $old_day->delete();
                        }
                    }
                }
                // New days
                foreach($new_days as $new_day){
                    // Check exists
                    $where = [['event_days.event_id', '=', $event->id],['event_days.day_date', '=',$new_day]];
                    if(EventDay::get_count_where($where)==0){
                        // Insert
                        $eventDay = new EventDay();
                        $eventDay->created_at = Carbon::now('GMT+2');
                        $eventDay->last_edit = Carbon::now('GMT+2');
                        $eventDay->event_id = $event->id;
                        $eventDay->day_date = $new_day;
                        $eventDay->save();
                    }
                }
            }// endif edit_days

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/events/edit-data/'.$req->id);
    }
    // POST - CHANGE STATUS
    public function change_status(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('change_status',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $event = Event::get_data_id($req->id);
            $event->last_edit = Carbon::now('GMT+2');
            $event->status = $req->status;
            $event->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect("admin/archive/events/edit-data/".$req->id);
    }
    // POST - DELETE
    public function delete(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('delete',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $event = Event::get_data_id($req->id);
            $event->last_edit = Carbon::now('GMT+2');
            $event->status = "deleted";
            $event->save();

            // Mess ko
            return redirect("admin/archive/events/index");
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect("admin/archive/events/edit-data/".$req->id);
    }

    // GET - EDIT DETAILS
    public function edit_details($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('events','edit');
        // Check
        if(!Event::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Event::get_data_id($id);
        $data_texts = Event::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_event_is_activable($data->id);

        // Return
        return view('admin.events.edit-details')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('GV',$this->GV)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE DETAILS
    public function update_details(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_details',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
        }

        // Update
        try {

            // Data
            $event = Event::get_data_id($req->id);
            $event_text = EventText::get_data_id($req->text_id);

            // IMAGE PATHS
            $temp_path_event = 'images/events/temp/';
            $path_event = 'images/events/';
            $path_video = 'images/video/';
            $temp_path_video = 'images/video/temp/';

            // ------------------------------------- upload_bg_image
            $bg_image = "";
            if($req->section=='upload_bg_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["bg_image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["bg_image"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["bg_image"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                }
                // Set name
                $bg_image = $event->id.'-header-bg-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $bg_image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$bg_image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["bg_image"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["bg_image"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["bg_image"]["w"].'x'.$this->GV->image_size_events["bg_image"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["bg_image"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["bg_image"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["bg_image"]["w"].'x'.$this->GV->image_size_events["bg_image"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- upload_video_url
            $video_url = "";
            if($req->section=='upload_video_url'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_video_empty);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->video_mimes)){
                    \Session::flash('error',$this->GV->err_video_mime);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $video_url = 'event-'.$event->id.'-header-video-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_video, $video_url) ) {
                    set_time_limit(0);
                }
            }

            // ------------------------------------- upload_image_box
            $image_box = "";
            if($req->section=='upload_image_box'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["image_box"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["image_box"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["image_box"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                }
                // Set name
                $image_box = $event->id.'-image-box-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $image_box) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$image_box;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["image_box"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["image_box"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["image_box"]["w"].'x'.$this->GV->image_size_events["image_box"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["image_box"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["image_box"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["image_box"]["w"].'x'.$this->GV->image_size_events["image_box"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- upload_image_location_box
            $image_location_box = "";
            if($req->section=='upload_image_location_box'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["image_box"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["image_box"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["image_box"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                }
                // Set name
                $image_location_box = $event->id.'-location-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $image_location_box) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$image_location_box;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["image_box"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["image_box"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["image_box"]["w"].'x'.$this->GV->image_size_events["image_box"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["image_box"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["image_box"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["image_box"]["w"].'x'.$this->GV->image_size_events["image_box"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- upload_image_location_box
            $home_image = "";
            if($req->section=='upload_home_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["home_image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["home_image"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["home_image"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                }
                // Set name
                $home_image = $event->id.'-home-image-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $home_image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$home_image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["home_image"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["home_image"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["home_image"]["w"].'x'.$this->GV->image_size_events["home_image"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["home_image"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["home_image"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["home_image"]["w"].'x'.$this->GV->image_size_events["home_image"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- upcoming_event_image
            $image1_720x886 = "";
            if($req->section=='upcoming_event_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
                }
//                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["image1_720x886"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["image1_720x886"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["image1_720x886"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                }
                // Set name
                $image1_720x886 = $event->id.'-upcoming-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $image1_720x886) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$image1_720x886;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["image1_720x886"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["image1_720x886"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["image1_720x886"]["w"].'x'.$this->GV->image_size_events["image1_720x886"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["image1_720x886"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["image1_720x886"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["image1_720x886"]["w"].'x'.$this->GV->image_size_events["image1_720x886"]["h"].'px');
//                            return redirect('admin/archive/events/edit-details/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // descriptions
            if($req->section=='descriptions'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->title1 = $req->title1;
                $event_text->slug = Str::slug($req->title1,'-');
                $event_text->subtitle = $req->subtitle;
                $event_text->full_desc = $req->full_desc;
                $event_text->save();
            }
            // upload_bg_image
            if($req->section=='upload_bg_image'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->bg_image = $bg_image;
                $event_text->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$bg_image, $path_event.$bg_image);
                \File::delete($temp_path_event.$bg_image);
            }
            // upload_video_url
            if($req->section=='upload_video_url'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->video_url = $video_url;
                $event_text->save();

                // save img def and remove temp
                \File::copy($temp_path_video.$video_url, $path_video.$video_url);
                \File::delete($temp_path_video.$video_url);
            }
            // update_video_url_youtube
            if($req->section=='update_video_url_youtube'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->video_url_youtube = isset($req->video_url_youtube) && !empty($req->video_url_youtube) ? $req->video_url_youtube : null;
                $event_text->save();
            }
            // image_box
            if($req->section=='image_box'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->box_title = $req->box_title;
                $event_text->description = $req->description;
                $event_text->city = $req->city;
                $event_text->location = $req->location;
                $event_text->save();
            }
            // upload_image_box
            if($req->section=='upload_image_box'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->image_box = $image_box;
                $event_text->image_box_alt = $req->image_box_alt;
                $event_text->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$image_box, $path_event.$image_box);
                \File::delete($temp_path_event.$image_box);
            }
            // location_box
            if($req->section=='location_box'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->location_box = $req->location_box;
                $event_text->location_desc = $req->location_desc;
                $event_text->save();
            }
            // upload_image_location_box
            if($req->section=='upload_image_location_box'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->location_img = $image_location_box;
                $event->location_img_alt = $req->location_img_alt;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$image_location_box, $path_event.$image_location_box);
                \File::delete($temp_path_event.$image_location_box);
            }
            // home_data
            if($req->section=='home_data'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->home_small_title = $req->home_small_title;
                $event_text->home_title = $req->home_title;
                $event_text->home_btn_text = $req->home_btn_text;
                $event_text->save();
            }
            // upload_home_image
            if($req->section=='upload_home_image'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->home_image = $home_image;
                $event_text->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$home_image, $path_event.$home_image);
                \File::delete($temp_path_event.$home_image);
            }
            // upcoming_event_box
            if($req->section=='upcoming_event_box'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->title = $req->title;
                $event_text->short_description = $req->short_description;
                $event_text->blockquote1 = $req->blockquote1;
                $event_text->blockquote2 = $req->blockquote2;
                $event_text->save();
            }
            // upcoming_event_image
            if($req->section=='upcoming_event_image'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->image1_720x886 = $image1_720x886;
                $event_text->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$image1_720x886, $path_event.$image1_720x886);
                \File::delete($temp_path_event.$image1_720x886);
            }
            // meta
            if($req->section=='meta'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->meta_title = $req->meta_title;
                $event_text->meta_desc = $req->meta_desc;
                $event_text->meta_keys = $req->meta_keys;
                $event_text->save();
            }


            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/events/edit-details/'.$req->id);
    }

    // GET - EDIT PROGRAM
    public function edit_program($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('events','edit');
        // Check
        if(!Event::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Event::get_data_id($id);
        $data_texts = Event::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_event_is_activable($data->id);

        // Return
        return view('admin.events.edit-program')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('GV',$this->GV)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE PROGRAM
    public function update_program(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_program',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-program/'.$req->id)->withInput();
        }

        // Data
        $event = Event::get_data_id($req->id);
        $event_text = EventText::get_data_id($req->text_id);


        // Update
        try {

            // IMAGE PATHS
            $temp_path_event = 'images/events/programs/temp/';
            $path_event = 'images/events/programs/';
            $temp_path_event2 = 'images/events/temp/';
            $path_event2 = 'images/events/';

            // ------------------------------------- upload_bg_image
            $program_pdf = "";
            if($req->section=='upload_program'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-program/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->pdf_mimes)){
                    \Session::flash('error',$this->GV->err_image_pdf_mimes);
                    return redirect('admin/archive/events/edit-program/'.$req->id)->withInput();
                }
                // Set name
                $program_pdf = $event->id.'-program-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $program_pdf) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- upload_bg_image_banner
            $bg_image_download = "";
            if($req->section=='upload_bg_image_banner'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-program/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-program/'.$req->id)->withInput();
                }
                // Set name
                $bg_image_download = $event->id.'-bg-download-program-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event2, $bg_image_download) ) {
                    set_time_limit(0);
                    $original = $temp_path_event2.$bg_image_download;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                    // Check width
                    if($this->GV->image_size_events["bg_image_download_banner"]["w"]>0){
                        if($width!=$this->GV->image_size_events["bg_image_download_banner"]["w"]){
                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["bg_image_download_banner"]["w"].'x'.$this->GV->image_size_events["bg_image_download_banner"]["h"].'px');
                            return redirect('admin/archive/events/edit-program/'.$req->id)->withInput();
                        }
                    }
                    // Check height
                    if($this->GV->image_size_events["bg_image_download_banner"]["h"]>0){
                        if($height!=$this->GV->image_size_events["bg_image_download_banner"]["h"]){
                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["bg_image_download_banner"]["w"].'x'.$this->GV->image_size_events["bg_image_download_banner"]["h"].'px');
                            return redirect('admin/archive/events/edit-program/'.$req->id)->withInput();
                        }
                    }
                }
            }


            // description
            if($req->section=='description'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->program_desc = $req->program_desc;
                $event_text->save();
            }
            // upload_program
            if($req->section=='upload_program'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->program_pdf = $program_pdf;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$program_pdf, $path_event.$program_pdf);
                \File::delete($temp_path_event.$program_pdf);
            }
            // delete_pdf
            if($req->section=='delete_pdf'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->program_pdf = null;
                $event->save();
            }
            // store_new_item
            if($req->section=='store_new_item'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // last sort
                $sorting = RelEventDayProgram::get_sorting($req->event_day_id,$this->G_active_lang);
                // New item x lang
                $langs = Language::get_list(null,null,null);
                if(!empty($langs) && sizeof($langs)>0) {
                    foreach ($langs as $lang) {
                        $prog = new RelEventDayProgram();
                        $prog->created_at = Carbon::now('GMT+2');
                        $prog->last_edit = Carbon::now('GMT+2');
                        $prog->lang = $lang->slug;
                        $prog->event_day_id = $req->event_day_id;
                        $prog->content1 = $req->content1;
                        $prog->time_from = $req->time_from;
                        $prog->time_to = $req->time_to;
                        $prog->sorting = $sorting;
                        $prog->save();
                    }
                }
            }
            // edit_item_id
            if($req->section=='edit_item'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // Delete
                $prog = RelEventDayProgram::get_data_id($req->item_id);
                $prog->content1 = $req->content1;
                $prog->time_from = $req->time_from;
                $prog->time_to = $req->time_to;
                $prog->save();
            }
            // delete_item_id
            if($req->section=='delete_item'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // Update
                $prog = RelEventDayProgram::get_data_id($req->item_id);
                $prog->delete();
            }
            // upload_bg_image_banner
            if($req->section=='upload_bg_image_banner'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->bg_image_download = $bg_image_download;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event2.$bg_image_download, $path_event2.$bg_image_download);
                \File::delete($temp_path_event2.$bg_image_download);
            }


            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/events/edit-program/'.$req->id);
    }

    // GET - EDIT SPEAKERS
    public function edit_speakers($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('events','edit');
        // Check
        if(!Event::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Event::get_data_id($id);
        $data_texts = Event::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_event_is_activable($data->id);

        // Return
        return view('admin.events.edit-speakers')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('GV',$this->GV)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE SPEAKERS
    public function update_speakers(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_speakers',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
        }

        // Data
        $event = Event::get_data_id($req->id);
        $event_text = EventText::get_data_id($req->text_id);


        // Update
        try {

            // IMAGE PATHS
            $temp_path_event = 'images/events/temp/';
            $path_event = 'images/events/';

            // ------------------------------------- store_new_item
            $image = "";
            if($req->section=='store_new_item'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["speaker_image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["speaker_image"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["speaker_image"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
//                }
                // Set name
                $image = $event->id.'-speaker-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["speaker_image"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["speaker_image"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["speaker_image"]["w"].'x'.$this->GV->image_size_events["speaker_image"]["h"].'px');
//                            return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["speaker_image"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["speaker_image"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["speaker_image"]["w"].'x'.$this->GV->image_size_events["speaker_image"]["h"].'px');
//                            return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
//                        }
//                    }
                }
            }
            // ------------------------------------- edit_image
            $new_image = "";
            if($req->section=='edit_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
                }
//                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["speaker_image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["speaker_image"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["speaker_image"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
//                }
                // Set name
                $new_image = $event->id.'-speaker-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $new_image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$new_image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["speaker_image"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["speaker_image"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["speaker_image"]["w"].'x'.$this->GV->image_size_events["speaker_image"]["h"].'px');
//                            return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["speaker_image"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["speaker_image"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["speaker_image"]["w"].'x'.$this->GV->image_size_events["speaker_image"]["h"].'px');
//                            return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // store_new_item
            if($req->section=='store_new_item'){
                // last sort
                $sorting = EventSpeaker::get_sorting($event->id);
                // Add
                $sp = new EventSpeaker();
                $sp->created_at = Carbon::now('GMT+2');
                $sp->last_edit = Carbon::now('GMT+2');
                $sp->event_id = $event->id;
                $sp->first_name = $req->first_name;
                $sp->last_name = $req->last_name;
                $sp->country = !empty($req->country) ? $req->country : "";
                $sp->role = $req->role;
                $sp->image = $image;
                $sp->sorting = $sorting;
                $sp->status = 'active';
                $sp->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$image, $path_event.$image);
                \File::delete($temp_path_event.$image);
            }
            // delete_item_id
            if($req->section=='delete_item'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // Delete
                $speaker = EventSpeaker::get_data_id($req->item_id);
                $speaker->delete();
            }
            // edit_item
            if($req->section=='edit_item'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // Update
                $speaker = EventSpeaker::get_data_id($req->item_id);
                $speaker->last_edit = Carbon::now('GMT+2');
                $speaker->first_name = $req->first_name;
                $speaker->last_name = $req->last_name;
                $speaker->role = $req->role;
                $speaker->country = $req->country;
                $speaker->save();
            }
            // edit_image
            if($req->section=='edit_image'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // Update
                $speaker = EventSpeaker::get_data_id($req->item_id);
                $speaker->last_edit = Carbon::now('GMT+2');
                $speaker->image = $new_image;
                $speaker->save();


                // save img def and remove temp
                \File::copy($temp_path_event.$new_image, $path_event.$new_image);
                \File::delete($temp_path_event.$new_image);
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/events/edit-speakers/'.$req->id);
    }
    // POST - SORT SPEAKERS
    public function sort_speakers(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('sort_speakers',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-speakers/'.$req->id)->withInput();
        }

        // Data
        $event = Event::get_data_id($req->id);

        // Update
        try {
            $event->last_edit = Carbon::now('GMT+2');
            $event->save();

            $sliders = EventSpeaker::get_list_ofevent($req->id);
            if(!empty($sliders) && sizeof($sliders)>0){
                foreach($sliders as $sl){
                    if($req->get('sorting-'.$sl->id) && !empty($req->get('sorting-'.$sl->id))){
                        $sl->sorting = $req->get('sorting-'.$sl->id);
                        $sl->save();
                    }
                }
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/events/edit-speakers/'.$req->id);
    }

    // GET - EDIT GALLERY
    public function edit_gallery($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('events','edit');
        // Check
        if(!Event::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Event::get_data_id($id);
        $data_texts = Event::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_event_is_activable($data->id);

        // Return
        return view('admin.events.edit-gallery')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('GV',$this->GV)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE GALLERY
    public function update_gallery(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_gallery',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
        }

        // Data
        $event = Event::get_data_id($req->id);
        $event_text = EventText::get_data_id($req->text_id);


        // Update
        try {

            // IMAGE PATHS
            $temp_path_event = 'images/events/temp/';
            $path_event = 'images/events/';


            // ------------------------------------- edit_image
            $new_image = "";
            if($req->section=='edit_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["slider_gallery"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["slider_gallery"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["slider_gallery"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
//                }
                // Set name
                $new_image = $event->id.'-gallery-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $new_image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$new_image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["slider_gallery"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["slider_gallery"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["slider_gallery"]["w"].'x'.$this->GV->image_size_events["slider_gallery"]["h"].'px');
//                            return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["slider_gallery"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["slider_gallery"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["slider_gallery"]["w"].'x'.$this->GV->image_size_events["slider_gallery"]["h"].'px');
//                            return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- store_new_item
            $filename = "";
            if($req->section=='store_new_item'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_events["slider_gallery"]["size"]>0 && ($sizeFile)>($this->GV->image_size_events["slider_gallery"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_events["slider_gallery"]["size"].' MB');
//                    return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
//                }
                // Set name
                $filename = $event->id.'-gallery-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $filename) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$filename;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_events["slider_gallery"]["w"]>0){
//                        if($width!=$this->GV->image_size_events["slider_gallery"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["slider_gallery"]["w"].'x'.$this->GV->image_size_events["slider_gallery"]["h"].'px');
//                            return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_events["slider_gallery"]["h"]>0){
//                        if($height!=$this->GV->image_size_events["slider_gallery"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_events["slider_gallery"]["w"].'x'.$this->GV->image_size_events["slider_gallery"]["h"].'px');
//                            return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // store_new_item
            if($req->section=='store_new_item'){
                // last sort
                $sorting = EventSlider::get_sorting($event->id);
                // Add
                $sp = new EventSlider();
                $sp->created_at = Carbon::now('GMT+2');
                $sp->event_id = $event->id;
                $sp->filename = $filename;
                $sp->alt_text = empty($req->alt_text) ? '.' : $req->alt_text;
                $sp->sorting = $sorting;
                $sp->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$filename, $path_event.$filename);
                \File::delete($temp_path_event.$filename);
            }
            // delete_item_id
            if($req->section=='delete_item'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // Delete
                $gall = EventSlider::get_data_id($req->item_id);
                $gall->delete();
            }
            // edit_item
            if($req->section=='edit_item'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // Delete
                $gall = EventSlider::get_data_id($req->item_id);
                $gall->alt_text = empty($req->alt_text) ? '.' : $req->alt_text;
                $gall->save();
            }
            // edit_image
            if($req->section=='edit_image'){
                $event->last_edit = Carbon::now('GMT+2');
                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->save();
                // Update
                $speaker = EventSlider::get_data_id($req->item_id);
                $speaker->filename = $new_image;
                $speaker->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$new_image, $path_event.$new_image);
                \File::delete($temp_path_event.$new_image);
            }



            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/events/edit-gallery/'.$req->id);
    }
    // POST - SORT GALLERY
    public function sort_gallery(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('sort_gallery',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-gallery/'.$req->id)->withInput();
        }

        // Data
        $event = Event::get_data_id($req->id);

        // Update
        try {
            $event->last_edit = Carbon::now('GMT+2');
            $event->save();

            $sliders = EventSlider::get_list_byevent($req->id);
            if(!empty($sliders) && sizeof($sliders)>0){
                foreach($sliders as $sl){
                    if($req->get('sorting-'.$sl->id) && !empty($req->get('sorting-'.$sl->id))){
                        $sl->sorting = $req->get('sorting-'.$sl->id);
                        $sl->save();
                    }
                }
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/events/edit-gallery/'.$req->id);
    }



    // GET - EDIT REPORTS
    public function edit_reports($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('events','edit');
        // Check
        if(!Event::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Event::get_data_id($id);
        $data_texts = Event::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_event_is_activable($data->id);

        // Return
        return view('admin.events.edit-reports')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('GV',$this->GV)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE REPORTS
    public function update_reports(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_reports',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
        }

        // Data
        $event = Event::get_data_id($req->id);
        $event_text = EventText::get_data_id($req->text_id);

        // Update
        try {

            // IMAGE PATHS
            $temp_path_event = 'images/events/reports/temp/';
            $path_event = 'images/events/reports/';

            // ------------------------------------- report1
            $report1_link = "";
            if($req->section=='report1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report1_link = $event->id.'-report1-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report1_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report2
            $report2_link = "";
            if($req->section=='report2'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report2_link = $event->id.'-report2-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report2_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report3
            $report3_link = "";
            if($req->section=='report3'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report3_link = $event->id.'-report3-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report3_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report4
            $report4_link = "";
            if($req->section=='report4'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report4_link = $event->id.'-report4-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report4_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report5
            $report5_link = "";
            if($req->section=='report5'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report5_link = $event->id.'-report5-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report5_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report6
            $report6_link = "";
            if($req->section=='report6'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report6_link = $event->id.'-report6-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report6_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report7
            $report7_link = "";
            if($req->section=='report7'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report7_link = $event->id.'-report7-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report7_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report8
            $report8_link = "";
            if($req->section=='report8'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report8_link = $event->id.'-report8-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report8_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report9
            $report9_link = "";
            if($req->section=='report9'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report9_link = $event->id.'-report9-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report9_link) ) {
                    set_time_limit(0);
                }
            }
            // ------------------------------------- report10
            $report10_link = "";
            if($req->section=='report10'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/events/edit-reports/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Set name
                $report10_link = $event->id.'-report10-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $report10_link) ) {
                    set_time_limit(0);
                }
            }

            // counters
            if($req->section=='counters'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->save();

                $event_text->last_edit = Carbon::now('GMT+2');
                $event_text->counter1_txt = !empty($req->counter1_txt) ? $req->counter1_txt : null;
                $event_text->counter1_num = !empty($req->counter1_num) ? $req->counter1_num : null;
                $event_text->counter2_txt = !empty($req->counter2_txt) ? $req->counter2_txt : null;
                $event_text->counter2_num = !empty($req->counter2_num) ? $req->counter2_num : null;
                $event_text->counter3_txt = !empty($req->counter3_txt) ? $req->counter3_txt : null;
                $event_text->counter3_num = !empty($req->counter3_num) ? $req->counter3_num : null;
                $event_text->save();
            }
            // descriptions
            if($req->section=='descriptions'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->title_report = !empty($req->title_report) ? $req->title_report : null;
                $event->subtitle_report = !empty($req->subtitle_report) ? $req->subtitle_report : null;
                $event->content_report = !empty($req->content_report) ? $req->content_report : null;
                $event->save();
            }
            // report1
            if($req->section=='report1'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report1_link = $report1_link;
                $event->report1_name = $req->report1_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report1_link, $path_event.$report1_link);
                \File::delete($temp_path_event.$report1_link);
            }
            // report2
            if($req->section=='report2'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report2_link = $report2_link;
                $event->report2_name = $req->report2_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report2_link, $path_event.$report2_link);
                \File::delete($temp_path_event.$report2_link);
            }
            // report3
            if($req->section=='report3'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report3_link = $report3_link;
                $event->report3_name = $req->report3_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report3_link, $path_event.$report3_link);
                \File::delete($temp_path_event.$report3_link);
            }
            // report4
            if($req->section=='report4'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report4_link = $report4_link;
                $event->report4_name = $req->report4_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report4_link, $path_event.$report4_link);
                \File::delete($temp_path_event.$report4_link);
            }
            // report5
            if($req->section=='report5'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report5_link = $report5_link;
                $event->report5_name = $req->report5_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report5_link, $path_event.$report5_link);
                \File::delete($temp_path_event.$report5_link);
            }
            // report6
            if($req->section=='report6'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report6_link = $report6_link;
                $event->report6_name = $req->report6_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report6_link, $path_event.$report6_link);
                \File::delete($temp_path_event.$report6_link);
            }
            // report7
            if($req->section=='report7'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report7_link = $report7_link;
                $event->report7_name = $req->report7_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report7_link, $path_event.$report7_link);
                \File::delete($temp_path_event.$report7_link);
            }
            // report8
            if($req->section=='report8'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report8_link = $report8_link;
                $event->report8_name = $req->report8_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report8_link, $path_event.$report8_link);
                \File::delete($temp_path_event.$report8_link);
            }
            // report9
            if($req->section=='report9'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report9_link = $report9_link;
                $event->report9_name = $req->report9_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report9_link, $path_event.$report9_link);
                \File::delete($temp_path_event.$report9_link);
            }
            // report10
            if($req->section=='report10'){
                $event->last_edit = Carbon::now('GMT+2');
                $event->report10_link = $report10_link;
                $event->report10_name = $req->report10_name;
                $event->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$report10_link, $path_event.$report10_link);
                \File::delete($temp_path_event.$report10_link);
            }
            // delete_item
            if($req->section=='delete_item'){
                $event->last_edit = Carbon::now('GMT+2');
                if($req->item_id=='1'){
                    $event->report1_link = null;
                    $event->report1_name = null;
                }
                if($req->item_id=='2'){
                    $event->report2_link = null;
                    $event->report2_name = null;
                }
                if($req->item_id=='3'){
                    $event->report3_link = null;
                    $event->report3_name = null;
                }
                if($req->item_id=='4'){
                    $event->report4_link = null;
                    $event->report4_name = null;
                }
                if($req->item_id=='5'){
                    $event->report5_link = null;
                    $event->report5_name = null;
                }
                if($req->item_id=='6'){
                    $event->report6_link = null;
                    $event->report6_name = null;
                }
                if($req->item_id=='7'){
                    $event->report7_link = null;
                    $event->report7_name = null;
                }
                if($req->item_id=='8'){
                    $event->report8_link = null;
                    $event->report8_name = null;
                }
                if($req->item_id=='9'){
                    $event->report9_link = null;
                    $event->report9_name = null;
                }
                if($req->item_id=='10'){
                    $event->report10_link = null;
                    $event->report10_name = null;
                }
                $event->save();
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/events/edit-reports/'.$req->id);
    }



    // Check if inactive event is activable
    private function check_event_is_activable($event_id)
    {
        $is_activable = true;
        $info = array();

        $event = Event::get_data_id($event_id);
//        if(empty($event->bg_image)){
//            array_push($info,'Immagine background [pagina dettaglio evento]');
//        }
        if($event->date_end>date('Y-m-d')){
            if(empty($event->location_img)){
                array_push($info,'Immagine location');
            }
        }
        if(empty($event->program_pdf)){
            array_push($info,'Programma evento');
        }
        if(empty($event->bg_image_download)){
            array_push($info,'Immagine background download programma evento');
        }

        // Texts
        $langs = Language::get_list(null,null,null);
        if(!empty($langs) && sizeof($langs)>0) {
            foreach ($langs as $lang) {
                $data_texts = Event::get_data_id_lang2($event_id,$lang->slug,false);
                $info_txt_lang = '[Lingua: '.$lang->slug.'] - ';

                // Pagina dettaglio evento --> HEADER
                if(empty($data_texts["eventText"]["title1"])){
                    array_push($info,$info_txt_lang.'Titolo header [pagina dettaglio evento]');
                }
//                if(empty($data_texts["eventText"]["subtitle"])){
//                    array_push($info,$info_txt_lang.'Sottotitolo header [pagina dettaglio evento]');
//                }

                // Pagina dettaglio evento --> FULL DESCRIPTION
                if(empty($data_texts["eventText"]["full_desc"])){
                    array_push($info,$info_txt_lang.'Descrizione completa');
                }

                // Pagina dettaglio evento --> BOX CON IMMAGINE
                // Box con immagine su pagina dettaglio evento
                if(empty($data_texts["eventText"]["box_title"])){
                    array_push($info,$info_txt_lang.'Titolo box con immagine [pagina dettaglio evento]');
                }
                if(empty($data_texts["eventText"]["description"])){
                    array_push($info,$info_txt_lang.'Descrizione box con immagine [pagina dettaglio evento]');
                }
                if(empty($data_texts["eventText"]["image_box"])){
                    array_push($info,$info_txt_lang.'Immagine box [pagina dettaglio evento]');
                }

//                if(empty($data_texts["eventText"]["small_title"])){
//                    array_push($info,$info_txt_lang.'Titoletto');
//                }

                if($event->date_start>date('Y-m-d')){
                    if(empty($data_texts["eventText"]["title"])){
                        array_push($info,$info_txt_lang.'Titolo [presente in Upcoming event box in home page e in Events]');
                    }
                    if(empty($data_texts["eventText"]["short_description"])){
                        array_push($info,$info_txt_lang.'Descrizione breve [presente in Upcoming event box in home page e in Events]');
                    }
                    if(empty($data_texts["eventText"]["image1_720x886"])){
                        array_push($info,$info_txt_lang.'Immagine presente in Upcoming event box in home page e in Events');
                    }
                    if(empty($data_texts["eventText"]["location_desc"])){
                        array_push($info,$info_txt_lang.'Location: descrizione');
                    }
                    if(empty($data_texts["eventText"]["home_image"])){
                        array_push($info,$info_txt_lang.'Immagine Home');
                    }
                    if(empty($data_texts["eventText"]["home_small_title"])){
                        array_push($info,$info_txt_lang.'Testo piccolo home');
                    }
                    if(empty($data_texts["eventText"]["home_title"])){
                        array_push($info,$info_txt_lang.'Testo titolo home');
                    }
                }
//                if(empty($data_texts["eventText"]["blockquote1"])){
//                    array_push($info,$info_txt_lang.'Testo blockquote 1');
//                }
//                if(empty($data_texts["eventText"]["blockquote2"])){
//                    array_push($info,$info_txt_lang.'Testo blockquote [location]');
//                }
                if(empty($data_texts["eventText"]["city"])){
                    array_push($info,$info_txt_lang.'Location: città');
                }
                if(empty($data_texts["eventText"]["location"])){
                    array_push($info,$info_txt_lang.'Location: nome');
                }
                if(empty($data_texts["eventText"]["location"])){
                    array_push($info,$info_txt_lang.'Nome location nel box');
                }
                if(empty($data_texts["eventText"]["bg_image"])){
                    array_push($info,$info_txt_lang.'Immagine background [Header pagina dettaglio evento]');
                }
                if(empty($data_texts["eventText"]["meta_title"])){
                    array_push($info,$info_txt_lang.'Meta title');
                }

                if($event->date_end>date('Y-m-d')){
                    if(empty($data_texts["eventText"]["program_desc"])){
                        array_push($info,$info_txt_lang.'Descrizione programma');
                    }
                }
            }
        }

        if(sizeof($info)>0){
            $is_activable = false;
        }


        $res = array(
            "is_activable"=>$is_activable,
            "info"=>$info
        );

        return $res;
    }





    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='delete' || $method=='change_status'
            || $method=='update_details' || $method=='update_program'
            || $method=='update_speakers'
            || $method=='update_gallery'
            || $method=='sort_gallery'
            || $method=='sort_speakers'
            || $method=='update_reports'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!Event::check_id_exists($req->id,false)){
                return $this->GV->err_generic_save.' (evento non trovato)';
            }
        }

        if($method=='update_details' || $method=='update_program' || $method=='update_speakers'
            || $method=='update_gallery'
            || $method=='update_reports' ){
            if(!isset($req->lang) || empty($req->lang)){
                return $this->GV->err_generic_save.' (lingua non trovata)';
            }
            if(!isset($req->lang) || empty($req->lang)
                || !isset($req->text_id) || empty($req->text_id)){
                return $this->GV->err_generic_save.' (id testo non trovato)';
            }
            if(!EventText::check_id_exists($req->text_id,$req->id)){
                return $this->GV->err_generic_save.' (testo evento non trovato)';
            }
            if(!isset($req->section) || empty($req->section)){
                return $this->GV->err_generic_save.' (sezione non trovata)';
            }
        }

        if($method=='update_program'){
            if($req->section=='store_new_item'){
                if(!isset($req->event_day_id) || empty($req->event_day_id)){
                    return $this->GV->err_required_field.'giorno';
                }
                if(!EventDay::check_id_exists($req->event_day_id,$req->id)){
                    return $this->GV->err_generic_save.' (giorno non trovato)';
                }
                if(!isset($req->time_from) || empty($req->time_from)){
                    return $this->GV->err_required_field.'orario da';
                }
                if(!isset($req->time_to) || empty($req->time_to)){
                    return $this->GV->err_required_field.'orario a';
                }
                if(!isset($req->content1) || empty($req->content1)){
                    return $this->GV->err_required_field.'testo';
                }
            }
            if($req->section=='delete_item' || $req->section=='edit_item'){
                if(!isset($req->item_id) || empty($req->item_id)){
                    return $this->GV->err_required_field.'elemento non trovato';
                }
                if(!RelEventDayProgram::check_id_exists($req->item_id)){
                    return $this->GV->err_generic_save.' (elemento non trovato)';
                }
                if($req->section=='edit_item'){
                    if(!isset($req->time_from) || empty($req->time_from)){
                        return $this->GV->err_required_field.'orario da';
                    }
                    if(!isset($req->time_to) || empty($req->time_to)){
                        return $this->GV->err_required_field.'orario a';
                    }
                    if(!isset($req->content1) || empty($req->content1)){
                        return $this->GV->err_required_field.'testo';
                    }
                }
            }
        }

        if($method=='update_speakers'){
            if($req->section=='store_new_item'){
                if(!isset($req->first_name) || empty($req->first_name)){
                    return $this->GV->err_required_field.'nome';
                }
                if(!isset($req->last_name) || empty($req->last_name)){
                    return $this->GV->err_required_field.'cognome';
                }
                if(!isset($req->role) || empty($req->role)){
                    return $this->GV->err_required_field.'ruolo';
                }
            }
            if($req->section=='delete_item' || $req->section=='edit_item' || $req->section=='edit_image'){
                if(!isset($req->item_id) || empty($req->item_id)){
                    return $this->GV->err_required_field.'elemento non trovato';
                }
                if(!EventSpeaker::check_id_exists($req->item_id)){
                    return $this->GV->err_generic_save.' (elemento non trovato)';
                }
            }
        }

        if($method=='update_gallery'){
            if($req->section=='store_new_item'){
//                if(!isset($req->alt_text) || empty($req->alt_text)){
//                    return $this->GV->err_required_field.'alt text';
//                }
            }
            if($req->section=='delete_item' || $req->section=='edit_item' || $req->section=='edit_image'){
                if(!isset($req->item_id) || empty($req->item_id)){
                    return $this->GV->err_required_field.'elemento non trovato';
                }
                if(!EventSlider::check_id_exists($req->item_id)){
                    return $this->GV->err_generic_save.' (elemento non trovato)';
                }
            }
        }

        if($method=='update_reports'){
            if($req->section=='delete_item'){
                if(!isset($req->item_id) || empty($req->item_id)
                    || ($req->item_id!='1' && $req->item_id!='2' && $req->item_id!='3' && $req->item_id!='4'
                        && $req->item_id!='5'  && $req->item_id!='6'  && $req->item_id!='7'
                        && $req->item_id!='8'  && $req->item_id!='9'  && $req->item_id!='10') ){
                    return $this->GV->err_required_field.'elemento non trovato';
                }
                if(!EventSlider::check_id_exists($req->item_id)){
                    return $this->GV->err_generic_save.' (elemento non trovato)';
                }
            }
        }


        if($method=='store' || $method=='update_data'){
            if(!isset($req->name) || empty($req->name)){
                return $this->GV->err_required_field.'nome';
            }
            if(!isset($req->date_start) || empty($req->date_start)){
                return $this->GV->err_required_field.'data inizio';
            }
            if(!isset($req->date_end) || empty($req->date_end)){
                return $this->GV->err_required_field.'data fine';
            }
            $date_start = AppFunctions::convert_date_format($req->date_start,'dd-mm-yyyy','yyyy-mm-dd');
            $date_end = AppFunctions::convert_date_format($req->date_end,'dd-mm-yyyy','yyyy-mm-dd');
            if($date_start>$date_end){
                return $this->GV->err_date_range_invalid;
            }

        }

        if($method=='delete'){
//            if(!isset($req->status) || ($req->status!=='active' && $req->status!=='inactive') ){
//                return $this->GV->err_generic_save;
//            }
//            $event = Event::get_data_id($req->id);
//            if(($event->status=='active' && $req->status=='active')
//                || ($event->status=='inactive' && $req->status=='inactive') ){
//                return $this->GV->err_generic_save;
//            }
        }


        // OK
        return "";
    }


}