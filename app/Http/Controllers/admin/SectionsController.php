<?php
namespace App\Http\Controllers\admin;
use App\Helper;
use App\Models\AppFunctions;
use App\Models\Gallery;
use App\Models\GlobalVars;
use App\Models\PageText;
use App\Models\Platform;
use App\Models\Section;
use App\Models\SectionText;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
class SectionsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;
    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('sections','index');
        // Data
        $where = [['sections.status', '!=', 'deleted']];
        $orderby = array('sections.name' => 'asc');
        $data = Section::get_list($where,$orderby);

        // Return
        return view('admin.cms.sections.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }


    // GET - EDIT DATA
    public function edit_details($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('sections','edit');
        // Check
        if(!Section::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Section::get_data_id($id);
        $data_texts = SectionText::get_data_lang($id,$this->G_active_lang);
        $data_gallery = Gallery::get_active_list($id);
        $home_sliders = array();
        if($id==1){
            $home_sliders = Gallery::get_arr_list($data_gallery,$this->G_active_lang);
        }

        // Return
        return view('admin.cms.sections.edit-details')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('data_gallery',$data_gallery)
            ->with('home_sliders',$home_sliders)
            ->with('lang',$this->G_active_lang)
            ->with('GV',$this->GV);
    }
    // POST - UPDATE DETAILS
    public function update_details(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_details',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
        }

        // Data
        $section = Section::get_data_id($req->id);
        $sectionText = SectionText::get_data_id($req->text_id);

        // Update
        try {

            // IMAGE PATHS
            $temp_path_section = 'images/sections/temp/';
            $path_section = 'images/sections/';
            $temp_path_partners = 'images/partners/temp/';
            $path_partners = 'images/partners/';

            // ------------------------------------- 1 - Home - hero area - image1
            $image1 = "";
            if($req->id==1 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_sections["1_image1"]["size"]>0 && ($sizeFile)>($this->GV->image_size_sections["1_image1"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_sections["1_image1"]["size"].' MB');
//                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
//                }
                // Set name
                $image1 = $section->id.'-hero-area-image1-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_sections["1_image1"]["w"]>0){
//                        if($width!=$this->GV->image_size_sections["1_image1"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_sections["1_image1"]["w"].'x'.$this->GV->image_size_sections["1_image1"]["h"].'px');
//                            return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_sections["1_image1"]["h"]>0){
//                        if($height!=$this->GV->image_size_sections["1_image1"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_sections["1_image1"]["w"].'x'.$this->GV->image_size_sections["1_image1"]["h"].'px');
//                            return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- 1 - Home - about-us-box - image1
            // image1
            $sec2_image1 = "";
            if($req->id==2 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_sections["2_image1"]["size"]>0 && ($sizeFile)>($this->GV->image_size_sections["2_image1"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_sections["2_image1"]["size"].' MB');
//                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
//                }
                // Set name
                $sec2_image1 = $section->id.'-about-us-box-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $sec2_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$sec2_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_sections["2_image1"]["w"]>0){
//                        if($width!=$this->GV->image_size_sections["2_image1"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_sections["2_image1"]["w"].'x'.$this->GV->image_size_sections["2_image1"]["h"].'px');
//                            return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_sections["2_image1"]["h"]>0){
//                        if($height!=$this->GV->image_size_sections["2_image1"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_sections["2_image1"]["w"].'x'.$this->GV->image_size_sections["2_image1"]["h"].'px');
//                            return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- 1 - Home - slider image ADD/CHANGE
            $slider_image = "";
            if($req->id==1 && ($req->section=='add_slider_img' || $req->section=='edit_image_slider')){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $slider_image = $section->id.'-slider-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $slider_image) ) {

                }
            }

            // ------------------------------------- 6 - Home - Banner togheter we can overcome - image1
            // image1
            $sec6_image1 = "";
            if($req->id==6 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_sections["6_image1"]["size"]>0 && ($sizeFile)>($this->GV->image_size_sections["6_image1"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_sections["6_image1"]["size"].' MB');
//                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
//                }
                // Set name
                $sec6_image1 = $section->id.'-home-banner-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $sec6_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$sec6_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // ------------------------------------- 8 - Home - Partners carousel - image1
            // image1
            $sec8_image1 = "";
            if($req->id==8 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $sec8_image1 = $section->id.'-partner-carousel-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_partners, $sec8_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_partners.$sec8_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // ------------------------------------- 14 - About us - Image with blockquote - image1
            // image1
            $sec14_image1 = "";
            if($req->id==14 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $sec14_image1 = $section->id.'-about-us-box-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $sec14_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$sec14_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // ------------------------------------- 17 - About us - Images carousel
            // image1
            $sec17_image1 = "";
            if($req->id==17 && ($req->section=='add_image' || $req->section=='edit_image')){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $sec17_image1 = $section->id.'-about-us-carousel-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $sec17_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$sec17_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // ------------------------------------- 18 - About us - Slider images & box
            // image1
            $sec18_image1 = "";
            if($req->id==18 && $req->section=='add_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $sec18_image1 = $section->id.'-about-us-carousel-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $sec18_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$sec18_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // ------------------------------------- 19 - About us - Banner
            // image1
            $sec19_image1 = "";
            if($req->id==19 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $sec19_image1 = $section->id.'-about-us-banner-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $sec19_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$sec19_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // ------------------------------------- 20 - About us - Image + Text Part 1
            // image1
            $sec20_image1 = "";
            if($req->id==20 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $sec20_image1 = $section->id.'-about-us-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $sec20_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$sec20_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // ------------------------------------- 21 - About us - Image + Text Part 2
            // image1
            $sec21_image1 = "";
            if($req->id==21 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $sec21_image1 = $section->id.'-about-us-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_section, $sec21_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_section.$sec21_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // ------------------------------------- 24 - Partners - gallery
            // image1
            $sec24_image1 = "";
            if($req->id==24 && $req->section=='image1'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                }
                // Set name
                $sec24_image1 = $section->id.'-partner-gallery-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_partners, $sec24_image1) ) {
                    set_time_limit(0);
                    $original = $temp_path_partners.$sec24_image1;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                }
            }

            // --------------- Home - hero area
            if($req->id==1){
                if($req->section=='texts'){
                    $sectionText->small_title = $req->small_title;
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->btn1_text = $req->btn1_text;
                    $sectionText->btn1_link = $req->btn1_link;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_section.$image1, $path_section.$image1);
                    \File::delete($temp_path_section.$image1);

                    $sectionText->image1 = $image1;
                    $sectionText->save();
                }
                if($req->section=='add_slider_img'){
                    // save img def and remove temp
                    \File::copy($temp_path_section.$slider_image, $path_section.$slider_image);
                    \File::delete($temp_path_section.$slider_image);

                    // $lang_texts
                    $lang_texts_arr = array("en"=>
                        array(
                            "small_title"=>isset($req->slider_small_title) && !empty($req->slider_small_title) ? $req->slider_small_title: "",
                            "title"=>isset($req->slider_title) && !empty($req->slider_title) ? $req->slider_title: "",
                            "subtitle"=>isset($req->slider_subtitle) && !empty($req->slider_subtitle) ? $req->slider_subtitle: "",
                            "btn_text"=>isset($req->slider_btn_text) && !empty($req->slider_btn_text) ? $req->slider_btn_text: "",
                            "btn_link"=>isset($req->slider_btn_link) && !empty($req->slider_btn_link) ? $req->slider_btn_link: "",
                        )
                    );
                    $lang_texts = json_encode($lang_texts_arr,true);

                    // Add image slider
                    $new_gallery = new Gallery();
                    $new_gallery->created_at = Carbon::now();
                    $new_gallery->section_id = $req->id;
                    $new_gallery->image = $slider_image;
                    $new_gallery->image_alt = isset($req->slider_image_alt) && !empty($req->slider_image_alt) ? $req->slider_image_alt: ".";
                    $new_gallery->sorting = Gallery::get_sorting($req->id);
                    $new_gallery->lang_texts = $lang_texts;
                    $new_gallery->save();
                }
                // Delete/edit slider item
                if($req->section=='delete_slider_img' || $req->section=='edit_slider' || $req->section=='edit_image_slider'){
                    $where = [['gallery.section_id', '=', $req->id],['gallery.id', '=', $req->item_id]];
                    if(Gallery::get_count_where($where)!=1){
                        \Session::flash('error',"Immagine non trovata");
                        return redirect('admin/cms/sections/edit-details/'.$req->id)->withInput();
                    }
                    // Get item
                    $item_toupd = Gallery::get_data($where);
                    // DELETE IMAGE
                    if($req->section=='delete_slider_img'){
                        $old_image = $item_toupd->image;
                        $item_toupd->delete();
                        // Remove old image
                        if(!empty($old_image)){
                            $image_path = $path_section.$old_image;
                            if(\File::exists($image_path)) {
                                \File::delete($image_path);
                            }
                        }
                    }
                    // EDIT TEXTS
                    if($req->section=='edit_slider'){
                        // $lang_texts
                        $lang_texts_arr = array("en"=>
                            array(
                                "small_title"=>isset($req->slider_small_title) && !empty($req->slider_small_title) ? $req->slider_small_title: "",
                                "title"=>isset($req->slider_title) && !empty($req->slider_title) ? $req->slider_title: "",
                                "subtitle"=>isset($req->slider_subtitle) && !empty($req->slider_subtitle) ? $req->slider_subtitle: "",
                                "btn_text"=>isset($req->slider_btn_text) && !empty($req->slider_btn_text) ? $req->slider_btn_text: "",
                                "btn_link"=>isset($req->slider_btn_link) && !empty($req->slider_btn_link) ? $req->slider_btn_link: "",
                            )
                        );
                        $lang_texts = json_encode($lang_texts_arr,true);
                        // Update
                        $item_toupd->image_alt = isset($req->slider_image_alt) && !empty($req->slider_image_alt) ? $req->slider_image_alt: ".";
                        $item_toupd->lang_texts = $lang_texts;
                        $item_toupd->save();
                    }
                    // EDIT IMAGE
                    if($req->section=='edit_image_slider'){
                        $old_image = $item_toupd->image;
                        // save img def and remove temp
                        \File::copy($temp_path_section.$slider_image, $path_section.$slider_image);
                        \File::delete($temp_path_section.$slider_image);
                        // Update
                        $item_toupd->image = $slider_image;
                        $item_toupd->save();
                        // Remove old image
                        if(!empty($old_image)){
                            $image_path = $path_section.$old_image;
                            if(\File::exists($image_path)) {
                                \File::delete($image_path);
                            }
                        }
                    }
                }
                // Sorting sliders
                if($req->section=='update_slider_sort'){
                    $where = [['gallery.section_id', '=',$req->id]];
                    $list = Gallery::get_list($where,null);
                    if(!empty($list) && sizeof($list)>0){
                        foreach($list as $sl){
                            if($req->get('sorting-'.$sl->id) && !empty($req->get('sorting-'.$sl->id))){
                                $sl->sorting = $req->get('sorting-'.$sl->id);
                                $sl->save();
                            }
                        }
                    }
                }
            }
            // --------------- Home - about-us-box
            if($req->id==2){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->blockquote1 = $req->blockquote1;
                    $sectionText->blockquote2 = $req->blockquote2;
                    $sectionText->btn1_text = $req->btn1_text;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec2_image1, $path_section.$sec2_image1);
                    \File::delete($temp_path_section.$sec2_image1);

                    $sectionText->image1 = $sec2_image1;
                    $sectionText->save();
                }
            }
            // --------------- Home - Home - Counter upcoming event
            if($req->id==4){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->btn1_text = $req->btn1_text;
                    $sectionText->save();
                }
            }
            // --------------- Promoting universal healthcare
            if($req->id==5){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->item1_title = $req->item1_title;
                    $sectionText->item1_subtitle = $req->item1_subtitle;
                    $sectionText->item2_title = $req->item2_title;
                    $sectionText->item2_subtitle = $req->item2_subtitle;
                    $sectionText->item3_title = $req->item3_title;
                    $sectionText->item3_subtitle = $req->item3_subtitle;
                    $sectionText->save();
                }
            }
            // --------------- Home - Banner togheter we can overcome
            if($req->id==6){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec6_image1, $path_section.$sec6_image1);
                    \File::delete($temp_path_section.$sec6_image1);

                    $sectionText->image1 = $sec6_image1;
                    $sectionText->save();
                }
            }
            // --------------- Home - Last press
            if($req->id==7){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->save();
                }
            }
            // --------------- Home - Partners carousel
            if($req->id==8){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->item1_title = $req->item1_title;
                    $sectionText->image1_link = $req->image1_link;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_partners.$sec8_image1, $path_partners.$sec8_image1);
                    \File::delete($temp_path_partners.$sec8_image1);

                    $sectionText->image1 = $sec8_image1;
                    $sectionText->save();
                }
            }
            // --------------- Home - latest projects
            if($req->id==10){
                if($req->section=='texts'){
                    $sectionText->blockquote1 = $req->blockquote1;
                    $sectionText->small_title = $req->small_title;
                    $sectionText->btn1_text = $req->btn1_text;
                    $sectionText->save();
                }
            }
            // --------------- About us - Header
            if($req->id==13){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->save();
                }
            }
            // --------------- About us - Image with blockquote
            if($req->id==14){
                if($req->section=='texts'){
                    $sectionText->blockquote1 = $req->blockquote1;
                    $sectionText->blockquote2 = $req->blockquote2;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec14_image1, $path_section.$sec14_image1);
                    \File::delete($temp_path_section.$sec14_image1);

                    $sectionText->image1 = $sec14_image1;
                    $sectionText->save();
                }
            }
            // --------------- About us - Description Part 1
            if($req->id==15){
                if($req->section=='texts'){
                    $sectionText->content1 = empty($req->content1) ? null : $req->content1;
                    $sectionText->save();
                }
            }
            // --------------- About us - Description Part 2
            if($req->id==16){
                if($req->section=='texts'){
                    $sectionText->content2 = empty($req->content2) ? null : $req->content2;
                    $sectionText->save();
                }
            }
            // --------------- About us - Images carousel
            if($req->id==17){
                if($req->section=='add_image'){

                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec17_image1, $path_section.$sec17_image1);
                    \File::delete($temp_path_section.$sec17_image1);

                    // New gallery
                    $new_gallery = new Gallery();
                    $new_gallery->created_at = Carbon::now();
                    $new_gallery->section_id = $req->id;
                    $new_gallery->image = $sec17_image1;
                    $new_gallery->image_alt = ".";
                    $new_gallery->sorting = Gallery::get_sorting($req->id);
                    $new_gallery->save();
                }
                if($req->section=='delete_image'){
                    $item_gallery = Gallery::get_data_id($req->item_id);
                    $item_gallery->delete();
                }
                if($req->section=='edit_image'){

                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec17_image1, $path_section.$sec17_image1);
                    \File::delete($temp_path_section.$sec17_image1);

                    // New gallery
                    $item_gallery = Gallery::get_data_id($req->item_id);
                    $item_gallery->image = $sec17_image1;
                    $item_gallery->save();
                }
            }
            // --------------- About us - Slider images & box
            if($req->id==18){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->content1 = $req->content1;
                    $sectionText->save();
                }
                if($req->section=='text'){
                    if($req->num==1){
                        $sectionText->image1_alt = $req->item_title;
                        $sectionText->item1_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==2){
                        $sectionText->image2_alt = $req->item_title;
                        $sectionText->item2_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==3){
                        $sectionText->image3_alt = $req->item_title;
                        $sectionText->item3_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==4){
                        $sectionText->image4_alt = $req->item_title;
                        $sectionText->item4_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==5){
                        $sectionText->image5_alt = $req->item_title;
                        $sectionText->item5_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==6){
                        $sectionText->image6_alt = $req->item_title;
                        $sectionText->item6_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==7){
                        $sectionText->image7_alt = $req->item_title;
                        $sectionText->item7_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==8){
                        $sectionText->image8_alt = $req->item_title;
                        $sectionText->item8_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==9){
                        $sectionText->image9_alt = $req->item_title;
                        $sectionText->item9_title = $req->item_title;
                        $sectionText->save();
                    }
                    if($req->num==10){
                        $sectionText->image10_alt = $req->item_title;
                        $sectionText->item10_title = $req->item_title;
                        $sectionText->save();
                    }
                }
                if($req->section=='add_image'){

                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec18_image1, $path_section.$sec18_image1);
                    \File::delete($temp_path_section.$sec18_image1);
                    if($req->num==1){
                        $sectionText->image1 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==2){
                        $sectionText->image2 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==3){
                        $sectionText->image3 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==4){
                        $sectionText->image4 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==5){
                        $sectionText->image5 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==6){
                        $sectionText->image6 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==7){
                        $sectionText->image7 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==8){
                        $sectionText->image8 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==9){
                        $sectionText->image9 = $sec18_image1;
                        $sectionText->save();
                    }
                    if($req->num==10){
                        $sectionText->image10 = $sec18_image1;
                        $sectionText->save();
                    }
                }
                if($req->section=='delete_image'){
                    if($req->num==1){
                        $sectionText->image1 = null;
                        $sectionText->image1_alt = null;
                        $sectionText->item1_title = null;
                        $sectionText->save();
                    }
                    if($req->num==2){
                        $sectionText->image2 = null;
                        $sectionText->image2_alt = null;
                        $sectionText->item2_title = null;
                        $sectionText->save();
                    }
                    if($req->num==3){
                        $sectionText->image3 = null;
                        $sectionText->image3_alt = null;
                        $sectionText->item3_title = null;
                        $sectionText->save();
                    }
                    if($req->num==4){
                        $sectionText->image4 = null;
                        $sectionText->image4_alt = null;
                        $sectionText->item4_title = null;
                        $sectionText->save();
                    }
                    if($req->num==5){
                        $sectionText->image5 = null;
                        $sectionText->image5_alt = null;
                        $sectionText->item5_title = null;
                        $sectionText->save();
                    }
                    if($req->num==6){
                        $sectionText->image6 = null;
                        $sectionText->image6_alt = null;
                        $sectionText->item6_title = null;
                        $sectionText->save();
                    }
                    if($req->num==7){
                        $sectionText->image7 = null;
                        $sectionText->image7_alt = null;
                        $sectionText->item7_title = null;
                        $sectionText->save();
                    }
                    if($req->num==9){
                        $sectionText->image9 = null;
                        $sectionText->image9_alt = null;
                        $sectionText->item9_title = null;
                        $sectionText->save();
                    }
                    if($req->num==9){
                        $sectionText->image9 = null;
                        $sectionText->image9_alt = null;
                        $sectionText->item9_title = null;
                        $sectionText->save();
                    }
                    if($req->num==10){
                        $sectionText->image10 = null;
                        $sectionText->image10_alt = null;
                        $sectionText->item10_title = null;
                        $sectionText->save();
                    }
                }
            }
            // --------------- About us - Banner
            if($req->id==19){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->content1 = $req->content1;
                    $sectionText->btn1_text = empty($req->btn1_text) ? null : $req->btn1_text;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec19_image1, $path_section.$sec19_image1);
                    \File::delete($temp_path_section.$sec19_image1);

                    $sectionText->image1 = $sec19_image1;
                    $sectionText->save();
                }
            }
            // --------------- About us - Image + Text Part 1
            if($req->id==20){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->content1 = $req->content1;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec20_image1, $path_section.$sec20_image1);
                    \File::delete($temp_path_section.$sec20_image1);

                    $sectionText->image1 = $sec20_image1;
                    $sectionText->save();
                }
            }
            // --------------- About us - Image + Text Part 2
            if($req->id==21){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->content1 = $req->content1;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_section.$sec21_image1, $path_section.$sec21_image1);
                    \File::delete($temp_path_section.$sec21_image1);

                    $sectionText->image1 = $sec21_image1;
                    $sectionText->save();
                }
            }
            // --------------- About us - Image + Text Part 2
            if($req->id==22){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->field1 = $req->field1;
                    $sectionText->field2 = $req->field2;
                    $sectionText->field3 = $req->field3;
                    $sectionText->field4 = $req->field4;
                    $sectionText->content1 = $req->content1;
                    $sectionText->btn1_text = $req->btn1_text;
                    $sectionText->save();
                }
            }
            // --------------- Contact info
            if($req->id==23){
                if($req->section=='texts'){
                    $sectionText->item1_title = $req->item1_title;
                    $sectionText->item1_subtitle = $req->item1_subtitle;
                    $sectionText->item2_title = $req->item2_title;
                    $sectionText->item2_subtitle = $req->item2_subtitle;
                    $sectionText->item3_title = $req->item3_title;
                    $sectionText->item3_subtitle = $req->item3_subtitle;
                    $sectionText->save();
                }
            }
            // --------------- Partners - gallery
            if($req->id==24){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->item1_title = $req->item1_title;
                    $sectionText->save();
                }
                if($req->section=='image1'){
                    // save img def and remove temp
                    \File::copy($temp_path_partners.$sec24_image1, $path_partners.$sec24_image1);
                    \File::delete($temp_path_partners.$sec24_image1);

                    $sectionText->image1 = $sec24_image1;
                    $sectionText->save();
                }
            }
            // --------------- Posts list
            if($req->id==25){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->save();
                }
            }
            // --------------- Events list - Header
            if($req->id==26){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->save();
                }
            }
            // --------------- Events - Upcoming event
            if($req->id==27){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->btn1_text = empty($req->btn1_text) ? null : $req->btn1_text;
                    $sectionText->save();
                }
            }
            // --------------- Events - Past events
            if($req->id==28){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->btn1_text = empty($req->btn1_text) ? null : $req->btn1_text;
                    $sectionText->save();
                }
            }
            // --------------- Press details
            if($req->id==29){
                if($req->section=='texts'){
                    $sectionText->btn1_text = empty($req->btn1_text) ? null : $req->btn1_text;
                    $sectionText->btn2_text = empty($req->btn2_text) ? null : $req->btn2_text;
                    $sectionText->item1_title = empty($req->item1_title) ? null : $req->item1_title;
                    $sectionText->field1 = empty($req->field1) ? null : $req->field1;
                    $sectionText->item2_title = empty($req->item2_title) ? null : $req->item2_title;
                    $sectionText->item3_title = empty($req->item3_title) ? null : $req->item3_title;
                    $sectionText->save();
                }
            }
            // --------------- Footer
            if($req->id==30){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->item1_title = empty($req->item1_title) ? null : $req->item1_title;
                    $sectionText->item2_title = empty($req->item2_title) ? null : $req->item2_title;
                    $sectionText->item3_title = empty($req->item3_title) ? null : $req->item3_title;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - Header - Upcoming
            if($req->id==31){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - Header - Past
            if($req->id==32){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - Program details
            if($req->id==35){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - Download program
            if($req->id==36){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->btn1_text = empty($req->btn1_text) ? null : $req->btn1_text;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - Location & map
            if($req->id==37){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->save();
                }
            }
            // --------------- Contact us box
            if($req->id==38){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->btn1_text = empty($req->btn1_text) ? null : $req->btn1_text;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - Speakers
            if($req->id==39){
                if($req->section=='texts'){
                    $sectionText->title = empty($req->title) ? null : $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - counters
            if($req->id==40){
                if($req->section=='texts'){
                    $sectionText->item1_title = $req->item1_title;
                    $sectionText->item2_title = $req->item2_title;
                    $sectionText->item3_title = $req->item3_title;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - gallery
            if($req->id==41){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->save();
                }
            }
            // --------------- Event detail - Reports
            if($req->id==42){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->content1 = $req->content1;
                    $sectionText->save();
                }
            }
            // --------------- Privacy - content
            if($req->id==43){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->content1 = $req->content1;
                    $sectionText->save();
                }
            }
            // --------------- Cookies - content
            if($req->id==44){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = empty($req->subtitle) ? null : $req->subtitle;
                    $sectionText->content1 = $req->content1;
                    $sectionText->save();
                }
            }
            // --------------- Projects list - header
            if($req->id==45){
                if($req->section=='texts'){
                    $sectionText->title = $req->title;
                    $sectionText->subtitle = $req->subtitle;
                    $sectionText->save();
                }
            }
            // --------------- Project details - description
            if($req->id==48){
                if($req->section=='texts'){
                    $sectionText->item1_title = $req->item1_title;
                    $sectionText->item2_title = $req->item2_title;
                    $sectionText->item3_title = $req->item3_title;
                    $sectionText->title = $req->title;
                    $sectionText->save();
                }
            }
            // --------------- Project details - prev/next projects
            if($req->id==52){
                if($req->section=='texts'){
                    $sectionText->item1_title = $req->item1_title;
                    $sectionText->item2_title = $req->item2_title;
                    $sectionText->save();
                }
            }


            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/cms/sections/edit-details/'.$req->id);
    }
    // POST - SORT ABOUT CAROUSEL
    public function sort_about_carousel(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('sort_about_carousel',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/cms/sections/edit-details/'.$req->id);
        }

        // Data
        $section = Section::get_data_id($req->id);

        // Update
        try {
            $where = [['gallery.section_id', '=',$req->id]];
            $list = Gallery::get_list($where,null);
            if(!empty($list) && sizeof($list)>0){
                foreach($list as $sl){
                    if($req->get('sorting-'.$sl->id) && !empty($req->get('sorting-'.$sl->id))){
                        $sl->sorting = $req->get('sorting-'.$sl->id);
                        $sl->save();
                    }
                }
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/cms/sections/edit-details/'.$req->id);
    }





    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='update_details' || $method=='sort_about_carousel'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!Section::check_id_exists($req->id,false)){
                return $this->GV->err_generic_save.' (sezione non trovato)';
            }
        }
        if($method=='sort_about_carousel' && $req->id!=17){
            return $this->GV->err_generic_save.' (sezione non corretta)';
        }

        if($method=='update_details'){
            if(!isset($req->lang) || empty($req->lang)){
                return $this->GV->err_generic_save.' (lingua non trovata)';
            }
            if(!isset($req->lang) || empty($req->lang)
                || !isset($req->text_id) || empty($req->text_id)){
                return $this->GV->err_generic_save.' (id testo non trovato)';
            }
            if(!SectionText::check_id_exists($req->text_id,$req->id)){
                return $this->GV->err_generic_save.' (testo sezione non trovato)';
            }
            if(!isset($req->section) || empty($req->section)){
                return $this->GV->err_generic_save.' (sez non trovata)';
            }

            // --------------- Home - hero area
            if($req->id==1){
                if($req->section=='texts'){
                    if(!isset($req->small_title) || empty($req->small_title)){
                        return $this->GV->err_required_field.' Titoletto';
                    }
                    if(!isset($req->title) || empty($req->title)){
                        return $this->GV->err_required_field.' Titolo';
                    }
                    if(!isset($req->subtitle) || empty($req->subtitle)){
                        return $this->GV->err_required_field.' Sottotitolo';
                    }
                    if(!isset($req->btn1_text) || empty($req->btn1_text)){
                        return $this->GV->err_required_field.' Testo pulsante vs About us';
                    }
                }
            }

            // --------------- About us - Images carousel
            if($req->id==17){
                if($req->section=='delete_image'){
                    if(!isset($req->item_id) || empty($req->item_id)){
                        return $this->GV->err_required_field.' Immagine non trovata';
                    }
                    $where = [['gallery.id', '=', $req->item_id]];
                    if(Gallery::get_count_where($where)!=1){
                        return $this->GV->err_required_field.' Immagine non trovata';
                    }
                }
            }

        }

        // OK
        return "";
    }


}