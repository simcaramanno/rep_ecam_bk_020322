<?php
namespace App\Http\Controllers\admin;
use App\Helper;
use App\Models\AppFunctions;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Project;
use App\Models\ProjectGallery;
use App\Models\ProjectText;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
class ProjectsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;

    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('projects','index');
        // Data
        $where = [['projects.status', '!=', 'deleted']];
        $data = Project::get_list($where,null,null);

        // Return
        return view('admin.projects.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }


    // GET - CREATE PROJECT
    public function create(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('projects','create');

        // Return
        return view('admin.projects.create')
            ->with('platform',$this->platform)
            ->with('layout',$layout);
    }
    // POST - STORE PROJECT
    public function store(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('store',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/projects/create')->withInput();
        }

        // Insert
        try {
            $project = New Project();
            $project->created_at = Carbon::now('GMT+2');
            $project->last_edit = Carbon::now('GMT+2');
            $project->name = $req->name;
            $project->project_date = AppFunctions::convert_date_format($req->project_date,'dd-mm-yyyy','yyyy-mm-dd');
            $project->status = 'inactive';
            $project->save();

            // Insert PROJECT-TEXTS
            $langs = Language::get_list(null,null,null);
            if(!empty($langs) && sizeof($langs)>0){
                foreach($langs as $lang){
                    $proT = new ProjectText();
                    $proT->project_id = $project->id;
                    $proT->lang = $lang->slug;
                    $proT->slug = Str::slug($project->name,'-');
                    $proT->save();
                }
            }

            // Return
            return redirect("admin/archive/projects/edit-data/".$project->id);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect("admin/archive/projects/create")->withInput();
    }

    // GET - EDIT DATA
    public function edit_data($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('projects','edit');
        // Check
        if(!Project::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Project::get_data_id($id);
        $data_texts = Project::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_project_is_activable($data->id);

        // Return
        return view('admin.projects.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE DATA
    public function update_data(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_data',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/projects/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            // set is_main=0 other projects
            if($req->is_main ==1){
                $all_projects = Project::get_list(null,null,null);
                if(!empty($all_projects) && sizeof($all_projects)>0){
                    foreach($all_projects as $pro){
                        $pro->is_main = 0;
                        $pro->save();
                    }
                }
            }
            // Updte project
            $project = Project::get_data_id($req->id);
            $project->last_edit = Carbon::now('GMT+2');
            $project->name = $req->name;
            $project->project_date = AppFunctions::convert_date_format($req->project_date,'dd-mm-yyyy','yyyy-mm-dd');
            $project->is_main = $req->is_main;
            $project->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/projects/edit-data/'.$req->id);
    }
    // POST - CHANGE STATUS
    public function change_status(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('change_status',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/projects/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $roject = Project::get_data_id($req->id);
            $roject->last_edit = Carbon::now('GMT+2');
            $roject->status = $req->status;
            $roject->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/projects/edit-data/'.$req->id)->withInput();
    }
    // POST - DELETE
    public function delete(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('delete',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/projects/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $project = Project::get_data_id($req->id);
            $project->last_edit = Carbon::now('GMT+2');
            $project->status = "deleted";
            $project->save();

            // Mess ko
            return redirect("admin/archive/projects/index");
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/projects/edit-data/'.$req->id)->withInput();
    }


    // GET - EDIT DETAILS
    public function edit_details($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('projects','edit');
        // Check
        if(!Project::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Project::get_data_id($id);
        $data_texts = Project::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_project_is_activable($data->id);

        // Return
        return view('admin.projects.edit-details')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE DETAILS
    public function update_details(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_details',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/projects/edit-details/'.$req->id)->withInput();
        }

        // Data
        $project = Project::get_data_id($req->id);
        $project_text = ProjectText::get_data_id($req->text_id);

        // Update
        try {
            // descriptions
            if($req->section=='descriptions'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->save();
                $project_text->header_title = $req->header_title;
                $project_text->slug = Str::slug($req->header_title,'-');
                $project_text->title_into_list = $req->title_into_list;
                $project_text->project_type = $req->project_type;
                $project_text->location = $req->location;
                $project_text->description = $req->description;
                $project_text->short_description = $req->short_description;
                $project_text->save();
            }
            // meta
            if($req->section=='meta'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->save();
                $project_text->meta_title = $req->meta_title;
                $project_text->meta_desc = $req->meta_desc;
                $project_text->meta_keys = $req->meta_keys;
                $project_text->save();
            }


            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/projects/edit-details/'.$req->id);
    }

    // GET - EDIT IMAGES
    public function edit_images($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('projects','edit');
        // Check
        if(!Project::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Project::get_data_id($id);
        $data_texts = Project::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_project_is_activable($data->id);

        // Return
        return view('admin.projects.edit-images')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('GV',$this->GV)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE IMAGES
    public function update_images(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_images',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
        }

        // Data
        $project = Project::get_data_id($req->id);
        $project_text = ProjectText::get_data_id($req->text_id);



        // Update
        try {


            // IMAGE PATHS
            $temp_path_event = 'images/projects/temp/';
            $path_event = 'images/projects/';

            // ------------------------------------- small_image
            $small_image = "";
            if($req->section=='small_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_projects["small_image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_projects["small_image"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_projects["small_image"]["size"].' MB');
//                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                }
                // Set name
                $small_image = $project->id.'-small-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $small_image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$small_image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_projects["small_image"]["w"]>0){
//                        if($width!=$this->GV->image_size_projects["small_image"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_projects["small_image"]["w"].'x'.$this->GV->image_size_projects["small_image"]["h"].'px');
//                            return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_projects["small_image"]["h"]>0){
//                        if($height!=$this->GV->image_size_projects["small_image"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_projects["small_image"]["w"].'x'.$this->GV->image_size_projects["small_image"]["h"].'px');
//                            return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- big_image
            $big_image = "";
            if($req->section=='big_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_projects["big_image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_projects["big_image"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_projects["big_image"]["size"].' MB');
//                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                }
                // Set name
                $big_image = $project->id.'-big-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $big_image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$big_image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
//                    // Check width
//                    if($this->GV->image_size_projects["big_image"]["w"]>0){
//                        if($width!=$this->GV->image_size_projects["big_image"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_projects["big_image"]["w"].'x'.$this->GV->image_size_projects["big_image"]["h"].'px');
//                            return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_projects["big_image"]["h"]>0){
//                        if($height!=$this->GV->image_size_projects["big_image"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_projects["big_image"]["w"].'x'.$this->GV->image_size_projects["big_image"]["h"].'px');
//                            return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // ------------------------------------- home_image
            $home_image = "";
            if($req->section=='home_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_projects["home_image"]["size"]>0 && ($sizeFile)>($this->GV->image_size_projects["home_image"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_projects["home_image"]["size"].' MB');
//                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                }
                // Set name
                $home_image = $project->id.'-home-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $home_image) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$home_image;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                    // Check width
//                    if($this->GV->image_size_projects["home_image"]["w"]>0){
//                        if($width!=$this->GV->image_size_projects["home_image"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_projects["home_image"]["w"].'x'.$this->GV->image_size_projects["home_image"]["h"].'px');
//                            return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_projects["home_image"]["h"]>0){
//                        if($height!=$this->GV->image_size_projects["home_image"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_projects["home_image"]["w"].'x'.$this->GV->image_size_projects["home_image"]["h"].'px');
//                            return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
//                        }
//                    }
                }
            }


            // ------------------------------------- video_url
            $video_url = "";
            if($req->section=='video_url'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_video_empty);
                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->video_mimes)){
                    \Session::flash('error',$this->GV->err_video_mime);
                    return redirect('admin/archive/projects/edit-images/'.$req->id)->withInput();
                }
                // Set name
                $video_url = 'project-'.$project->id.'-header-video-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $video_url) ) {
                    set_time_limit(0);
                }
            }

            // small_image
            if($req->section=='small_image'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->small_image = $small_image;
                $project->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$small_image, $path_event.$small_image);
                \File::delete($temp_path_event.$small_image);
            }
            // big_image
            if($req->section=='big_image'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->big_image = $big_image;
                $project->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$big_image, $path_event.$big_image);
                \File::delete($temp_path_event.$big_image);
            }
            // home_image
            if($req->section=='home_image'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->home_image = $home_image;
                $project->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$home_image, $path_event.$home_image);
                \File::delete($temp_path_event.$home_image);
            }
            // video_url
            if($req->section=='video_url'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->video_url = $video_url;
                $project->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$video_url, $path_event.$video_url);
                \File::delete($temp_path_event.$video_url);
            }
            // update_video_url_youtube
            if($req->section=='update_video_url_youtube'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->video_url_youtube = isset($req->video_url_youtube) && !empty($req->video_url_youtube) ? $req->video_url_youtube : null;
                $project->save();
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/projects/edit-images/'.$req->id);
    }

    // GET - EDIT GALLERY
    public function edit_gallery($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('projects','edit');
        // Check
        if(!Project::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Project::get_data_id($id);
        $data_texts = Project::get_data_id_lang2($id,$this->G_active_lang,false);
        $activable = self::check_project_is_activable($data->id);

        // Return
        return view('admin.projects.edit-gallery')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('activable',$activable)
            ->with('GV',$this->GV)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE GALLERY
    public function update_gallery(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_gallery',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/projects/edit-gallery/'.$req->id)->withInput();
        }

        // Data
        $project = Project::get_data_id($req->id);
        $project_text = ProjectText::get_data_id($req->text_id);


        // Update
        try {

            // IMAGE PATHS
            $temp_path_event = 'images/projects/temp/';
            $path_event = 'images/projects/';

            // ------------------------------------- store_new_item
            $filename = "";
            if($req->section=='store_new_item' || $req->section=='edit_image'){
                // CHECK && UPLOAD IMAGE
                if( !$req->hasFile('image') )	{
                    \Session::flash('error',$this->GV->err_image_empty);
                    return redirect('admin/archive/projects/edit-gallery/'.$req->id)->withInput();
                }
                $extension       = $req->file('image')->getClientOriginalExtension();
                $originalName    = Helper::fileNameOriginal($req->file('image')->getClientOriginalName());
                $type_mime_img   = $req->file('image')->getMimeType();
                // Check mime
                if(!in_array($type_mime_img,$this->GV->image_mimes)){
                    \Session::flash('error',$this->GV->err_image_mime);
                    return redirect('admin/archive/projects/edit-gallery/'.$req->id)->withInput();
                }
                // Check sizefile
//                $sizeFile = $req->file('image')->getSize(); //bytes
//                if($this->GV->image_size_projects["slider_gallery"]["size"]>0 && ($sizeFile)>($this->GV->image_size_projects["slider_gallery"]["size"]*1000000)){
//                    \Session::flash('error',$this->GV->err_image_size. $this->GV->image_size_projects["slider_gallery"]["size"].' MB');
//                    return redirect('admin/archive/projects/edit-gallery/'.$req->id)->withInput();
//                }
                // Set name
                $filename = $project->id.'-gallery-'.time().\Illuminate\Support\Str::random(10).'.'.$extension;
                if( $req->file('image')->move($temp_path_event, $filename) ) {
                    set_time_limit(0);
                    $original = $temp_path_event.$filename;
                    $width = Helper::getWidth( $original );
                    $height = Helper::getHeight( $original );
                    // Check width
//                    if($this->GV->image_size_projects["slider_gallery"]["w"]>0){
//                        if($width!=$this->GV->image_size_projects["slider_gallery"]["w"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_projects["slider_gallery"]["w"].'x'.$this->GV->image_size_projects["slider_gallery"]["h"].'px');
//                            return redirect('admin/archive/projects/edit-gallery/'.$req->id)->withInput();
//                        }
//                    }
//                    // Check height
//                    if($this->GV->image_size_projects["slider_gallery"]["h"]>0){
//                        if($height!=$this->GV->image_size_projects["slider_gallery"]["h"]){
//                            \Session::flash('error',$this->GV->err_image_width_height. $this->GV->image_size_projects["slider_gallery"]["w"].'x'.$this->GV->image_size_projects["slider_gallery"]["h"].'px');
//                            return redirect('admin/archive/projects/edit-gallery/'.$req->id)->withInput();
//                        }
//                    }
                }
            }

            // store_new_item
            if($req->section=='store_new_item'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->save();
                // last sort
                $sorting = ProjectGallery::get_sorting($project->id);
                // Add
                $sp = new ProjectGallery();
                $sp->created_at = Carbon::now('GMT+2');
                $sp->project_id = $project->id;
                $sp->filename = $filename;
                $sp->alt_text = empty($req->alt_text) ? '.' : $req->alt_text;
                $sp->sorting = $sorting;
                $sp->save();

                // save img def and remove temp
                \File::copy($temp_path_event.$filename, $path_event.$filename);
                \File::delete($temp_path_event.$filename);
            }
            // delete_item_id
            if($req->section=='delete_item'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->save();
                // Delete
                $gall = ProjectGallery::get_data_id($req->item_id);
                $gall->delete();
            }
            // edit_item
            if($req->section=='edit_item'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->save();
                // Update
                $gall = ProjectGallery::get_data_id($req->item_id);
                $gall->alt_text = empty($req->alt_text) ? '.' : $req->alt_text;
                $gall->save();
            }
            // edit_image
            if($req->section=='edit_image'){
                $project->last_edit = Carbon::now('GMT+2');
                $project->save();
                // Update
                $gall = ProjectGallery::get_data_id($req->item_id);
                $gall->filename = $filename;
                $gall->save();


                // save img def and remove temp
                \File::copy($temp_path_event.$filename, $path_event.$filename);
                \File::delete($temp_path_event.$filename);
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/projects/edit-gallery/'.$req->id);
    }
    // POST - SORT GALLERY
    public function sort_gallery(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('sort_gallery',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/projects/edit-gallery/'.$req->id)->withInput();
        }

        // Data
        $project = Project::get_data_id($req->id);

        // Update
        try {
            $gallery = ProjectGallery::get_list_byproject($req->id);
            if(!empty($gallery) && sizeof($gallery)>0){
                foreach($gallery as $sl){
                    if($req->get('sorting-'.$sl->id) && !empty($req->get('sorting-'.$sl->id))){
                        $sl->sorting = $req->get('sorting-'.$sl->id);
                        $sl->save();
                    }
                }
            }

            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/projects/edit-gallery/'.$req->id)->withInput();
    }




    // Check if inactive project is activable
    private function check_project_is_activable($project_id)
    {
        $is_activable = true;
        $info = array();

        $project = Project::get_data_id($project_id);
        if(empty($project->small_image)){
            array_push($info,'Immagine small');
        }
        if(empty($project->big_image)){
            array_push($info,'Immagine big');
        }
        if(empty($project->home_image)){
            array_push($info,'Immagine home page');
        }

        // Texts
        $langs = Language::get_list(null,null,null);
        if(!empty($langs) && sizeof($langs)>0) {
            foreach ($langs as $lang) {
                $data_texts = Project::get_data_id_lang2($project_id,$lang->slug,false);
                $info_txt_lang = '[Lingua: '.$lang->slug.'] - ';

                if(empty($data_texts["projectText"]["header_title"])){
                    array_push($info,$info_txt_lang.'Titolo header [pagina dettaglio progetto]');
                }
                if(empty($data_texts["projectText"]["title_into_list"])){
                    array_push($info,$info_txt_lang.'Titoletto [pagina lista progetti]');
                }
                if(empty($data_texts["projectText"]["project_type"])){
                    array_push($info,$info_txt_lang.'Tipo progetto');
                }
                if(empty($data_texts["projectText"]["location"])){
                    array_push($info,$info_txt_lang.'Location');
                }
                if(empty($data_texts["projectText"]["description"])){
                    array_push($info,$info_txt_lang.'Descrizione');
                }
                if(empty($data_texts["projectText"]["short_description"])){
                    array_push($info,$info_txt_lang.'Descrizione breve');
                }

            }
        }

        if(sizeof($info)>0){
            $is_activable = false;
        }


        $res = array(
            "is_activable"=>$is_activable,
            "info"=>$info
        );

        return $res;
    }

    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='delete' || $method=='change_status'
            || $method=='update_details' || $method=='update_images' || $method=='sort_gallery'
            || $method=='update_gallery'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!Project::check_id_exists($req->id,false)){
                return $this->GV->err_generic_save.' (progetto non trovato)';
            }
        }

        if($method=='update_details' || $method=='update_gallery' ){
            if(!isset($req->lang) || empty($req->lang)){
                return $this->GV->err_generic_save.' (lingua non trovata)';
            }
            if(!isset($req->lang) || empty($req->lang)
                || !isset($req->text_id) || empty($req->text_id)){
                return $this->GV->err_generic_save.' (id testo non trovato)';
            }
            if(!ProjectText::check_id_exists($req->text_id,$req->id)){
                return $this->GV->err_generic_save.' (testo progetto non trovato)';
            }
            if(!isset($req->section) || empty($req->section)){
                return $this->GV->err_generic_save.' (sezione non trovata)';
            }
        }

        if($method=='update_gallery'){
            if($req->section=='store_new_item'){
//                if(!isset($req->alt_text) || empty($req->alt_text)){
//                    return $this->GV->err_required_field.'alt text';
//                }
            }
            if($req->section=='delete_item' || $req->section=='edit_item' || $req->section=='edit_image'){
                if(!isset($req->item_id) || empty($req->item_id)){
                    return $this->GV->err_required_field.'elemento non trovato';
                }
                if(!ProjectGallery::check_id_exists($req->item_id)){
                    return $this->GV->err_generic_save.' (elemento non trovato)';
                }
            }
        }



        if($method=='store' || $method=='update_data'){
            if(!isset($req->name) || empty($req->name)){
                return $this->GV->err_required_field.'nome';
            }
            if(!isset($req->project_date) || empty($req->project_date)){
                return $this->GV->err_required_field.'data';
            }
            if( $method=='update_data'){
                if(!isset($req->is_main)){
                    return $this->GV->err_required_field.'se progetto in home page';
                }
                if($req->is_main==1 || $req->is_main=='1'){
                    $res_att = self::check_project_is_activable($req->id);
                    if(!$res_att["is_activable"]){
                        return $this->GV->err_proj_no_main;
                    }
                }
            }
        }

        if($method=='change_status'){
            if($req->status=='active'){
                $act = self::check_project_is_activable($req->id);
                if(!$act["is_activable"]){
                    return 'Progetto non pubblicabile: mancano le informazioni';
                }
            }
        }
        if($method=='delete'){
//            if(!isset($req->status) || ($req->status!=='active' && $req->status!=='inactive') ){
//                return $this->GV->err_generic_save;
//            }
//            $pro = Project::get_data_id($req->id);
//            if(($pro->status=='active' && $req->status=='active')
//                || ($pro->status=='inactive' && $req->status=='inactive') ){
//                return $this->GV->err_generic_save;
//            }
        }


        // OK
        return "";
    }

}