<?php
namespace App\Http\Controllers\admin;
use App\Models\AppFunctions;
use App\Models\Category;
use App\Models\CategoryText;
use App\Models\ContactRequest;
use App\Models\Event;
use App\Models\GlobalVars;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Post;
use App\Models\Project;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
class CategoriesController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // Base
    private $GV;
    private $G_active_lang;
    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
        $this->GV = new GlobalVars();
        $this->G_active_lang = "en";
    }

    // GET - INDEX
    public function index(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('categories','index');
        // Data
        $where = [['categories.status', '!=', 'deleted']];
        $orderby = array('categories.name' => 'asc');
        $data = Category::get_list($where,$orderby);

        // Return
        return view('admin.press.categories.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data);
    }

    // GET - CREATE
    public function create(Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('categories','create');

        // Return
        return view('admin.press.categories.create')
            ->with('platform',$this->platform)
            ->with('layout',$layout);
    }
    // POST - STORE
    public function store(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('store',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/categories/create')->withInput();
        }

        // Insert
        try {
            $cate = New Category();
            $cate->created_at = Carbon::now('GMT+2');
            $cate->last_edit = Carbon::now('GMT+2');
            $cate->name = $req->name;
            $cate->slug = Str::slug($req->name,'-');
            $cate->status = 'active';
            $cate->save();

            // Insert PROJECT-TEXTS
            $langs = Language::get_list(null,null,null);
            if(!empty($langs) && sizeof($langs)>0){
                foreach($langs as $lang){
                    $proT = new CategoryText();
                    $proT->created_at = Carbon::now('GMT+2');
                    $proT->last_edit = Carbon::now('GMT+2');
                    $proT->category_id = $cate->id;
                    $proT->lang = $lang->slug;
                    $proT->name = $cate->name;
                    $proT->save();
                }
            }

            // Return
            return redirect("admin/archive/press/categories/edit-data/".$cate->id);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/categories/create')->withInput();
    }

    // GET - EDIT DATA
    public function edit_data($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('categories','edit');
        // Check
        if(!Category::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Category::get_data_id($id);
        $data_texts = Category::get_data_id_lang2($id,$this->G_active_lang,false);

        // Return
        return view('admin.press.categories.edit-data')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE DATA
    public function update_data(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_data',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/categories/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $category = Category::get_data_id($req->id);
            $category->last_edit = Carbon::now('GMT+2');
            $category->name = $req->name;
            $category->save();

            // Mess ko
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/categories/edit-data/'.$req->id);
    }
    // POST - DELETE
    public function delete(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('delete',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/categories/edit-data/'.$req->id)->withInput();
        }

        // Update
        try {
            $category = Category::get_data_id($req->id);
            $category->delete();
            return redirect('admin/archive/press/categories/index');

        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/categories/edit-data/'.$req->id);
    }

    // GET - EDIT DETAILS
    public function edit_details($id,Request $req)
    {
        // Layout
        $layout = AppFunctions::get_be_layout('categories','edit');
        // Check
        if(!Category::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Page data
        $data = Category::get_data_id($id);
        $data_texts = Category::get_data_id_lang2($id,$this->G_active_lang,false);

        // Return
        return view('admin.press.categories.edit-details')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('data',$data)
            ->with('data_texts',$data_texts)
            ->with('lang',$this->G_active_lang);
    }
    // POST - UPDATE DETAILS
    public function update_details(Request $req)
    {
        // Check posts
        $text_err = $this->check_form_posts('update_details',$req);
        if(!empty($text_err)){
            \Session::flash('error', $text_err);
            return redirect('admin/archive/press/categories/edit-details/'.$req->id)->withInput();
        }

        // Data
        $category = Category::get_data_id($req->id);
        $category_texts = CategoryText::get_data_id($req->text_id);

        // Update
        try {
            // descriptions
            if($req->section=='descriptions'){
                $category->last_edit = Carbon::now('GMT+2');
                $category->save();
                $category_texts->name = $req->name;
                $category_texts->save();
            }
            // Mess OK
            \Session::flash('message', $this->GV->mess_generic_save);
        }catch (\Throwable $e) {
            // Mess ko
            \Session::flash('error', $this->GV->err_generic. ' ('.$e->getMessage().')');
        }
        // Return
        return redirect('admin/archive/press/categories/edit-details/'.$req->id);
    }



    // PRIVATE FUNCION
    private function check_form_posts($method,$req)
    {
        if($method=='update_data' || $method=='delete' || $method=='update_details'){
            if(!isset($req->id) || empty($req->id)){
                return $this->GV->err_generic_save.' (id non trovato)';
            }
            if(!Category::check_id_exists($req->id,false)){
                return $this->GV->err_generic_save.' (testata non trovato)';
            }
        }

        if($method=='update_details'){
            if(!isset($req->lang) || empty($req->lang)){
                return $this->GV->err_generic_save.' (lingua non trovata)';
            }
            if(!isset($req->lang) || empty($req->lang)
                || !isset($req->text_id) || empty($req->text_id)){
                return $this->GV->err_generic_save.' (id testo non trovato)';
            }
            if(!CategoryText::check_id_exists($req->text_id,$req->id)){
                return $this->GV->err_generic_save.' (testo progetto non trovato)';
            }
            if(!isset($req->section) || empty($req->section)){
                return $this->GV->err_generic_save.' (sezione non trovata)';
            }
        }

        if($method=='store' || $method=='update_data' || $method=='update_details'){
            if(!isset($req->name) || empty($req->name)){
                return $this->GV->err_required_field.'nome';
            }
        }

        if($method=='delete'){
            $where = [['posts.category_id', '=', $req->id],['posts.status', '!=',"deleted"]];
            if(Post::get_count_where($where)>0){
                return $this->GV->err_catetory_delete;
            }
        }


        // OK
        return "";
    }

}