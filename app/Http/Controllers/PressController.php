<?php

namespace App\Http\Controllers;
use App\Models\AppFunctions;
use App\Models\Language;
use App\Models\Platform;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
class PressController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
    }

    // GET - INDEX
    public function index($lang="",Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,6);
        // Page data
        $page_data = self::get_page_data($lang,6);

        // Return
        return view('press.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }


    // GET - SEARCH
    public function search($lang="",Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,13);
        // Page data
        $page_data = self::get_page_data($lang,13);

        // Search values
        $tag = "";
        if(isset($req->tag) && !empty($req->tag) && Tag::check_slug_exists($req->tag,true)){
            $tag = $req->tag;
        }
        $searchTxt = "";
        if(isset($req->s) && !empty($req->s)){
            $searchTxt = $req->s;
        }
        // Search Posts
        $list = Post::get_active_list_lang($lang,null);
        $posts = array();
        if(!empty($list) && sizeof($list)>0){
            foreach($list as $post){
                if(empty($tag) && empty($searchTxt)){
                    array_push($posts,$post);
                } else {
                    // Cerca
                    $toAdd = false;
                    if(!empty($tag)){
                        if(!empty($post["tags"]) && sizeof($post["tags"])>0){
                            foreach($post["tags"] as $_itemTag){
                                if($tag==$_itemTag["slug"]){
                                    $toAdd = true;
                                }
                            }
                        }
                    }
                    if(!empty($searchTxt)){
                        $pos = strpos(strtoupper($post["post_text"]["title"]), $searchTxt);
                        if ($pos === false) {

                        } else {
                            $toAdd = true;
                        }
                    }

                    if($toAdd){
                        array_push($posts,$post);
                    }
                }
            }
        }


        // Return
        return view('press.search')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data)
            ->with('posts',$posts);
    }

    // GET - SHOW
    public function show($lang="",$id,$slug,Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Check post exists
        if(!Post::check_id_exists($id,true)){
            return response()->view('errors.404', [], 404);
        }

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,7);
        // Page data
        $page_data = self::get_page_data($lang,7);
        // Post data
        $post = Post::get_data_id_lang($id,$lang);

        // Return
        return view('press.show')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data)
            ->with('post',$post);
    }

    // GET - SHOW PREVIEW [ONLY ADMIN]
    public function show_preview($lang="",$id,$slug,Request $req)
    {
        if(!Auth::user()){
            return response()->view('errors.404', [], 404);
        }
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Check post exists
        if(!Post::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,7);
        // Page data
        $page_data = self::get_page_data($lang,7);
        // Post data
        $post = Post::get_data_id_lang2($id,$lang,false);

        // Return
        return view('press.show')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data)
            ->with('post',$post);
    }



    // GET PAGE DATA
    private function get_page_data($lang,$page_id)
    {
        // Sections
        $sections = AppFunctions::get_page_sections_data($lang,$page_id);

        // Return
        $data = array(
            "sections"=>$sections
        );
        return $data;
    }

}