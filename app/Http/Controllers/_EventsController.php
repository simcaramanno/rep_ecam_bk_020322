<?php

namespace App\Http\Controllers;
use App\Models\AppFunctions;
use App\Models\Event;
use App\Models\Language;
use App\Models\Platform;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
class EventsController extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;



    /**
     * __construct
     */
    public function __construct(Platform $platform)
    {
        // Basic Data
        $this->platform = $platform::first();
    }

    // GET - INDEX
    public function index($lang="",Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,3);
        // Page data
        $page_data = self::get_page_data($lang,3);

        // Return
        return view('events.index')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data);
    }

    // GET PAGE DATA
    private function get_page_data($lang,$page_id)
    {
        // Sections
        $sections = AppFunctions::get_page_sections_data($lang,$page_id);

        // Return
        $data = array(
            "sections"=>$sections
        );
        return $data;
    }

    // GET - SHOW
    public function show($lang="",$id,$slug,Request $req)
    {
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Check element exists
        if(!Event::check_id_exists($id,true)){
            return response()->view('errors.404', [], 404);
        }
        // Event data
        $event = Event::get_data_id_lang($id,$lang);

        // Upcoming / Past
        $upcoming = false;
        $page_id = 4;// Past-event
//        if($event["event"]["date_end"]>=date('Y-m-d')){
//            // UPCOMING
//            $upcoming = true;
//            $page_id = 5;// Upcoming-event
//        }
        // NEWW
        if(AppFunctions::get_event_is_upcoming($event["event"]["date_start"],$event["event"]["date_end"])){
            // UPCOMING
            $upcoming = true;
            $page_id = 5;// Upcoming-event
        }

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,$page_id);
        // Page data
        $page_data = self::get_page_data($lang,$page_id);

        // Return
        return view('events.show')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data)
            ->with('event',$event)
            ->with('is_upcoming',$upcoming);
    }



    // GET - SHOW
    public function show_preview($lang="",$id,$slug,Request $req)
    {
        if(!Auth::user()){
            return response()->view('errors.404', [], 404);
        }
        // Set lang
        if(empty($lang) || !Language::check_slug_exists($lang)){
            $lang = Language::get_default_lang();
        }
        App::setLocale($lang);

        // Check element exists
        if(!Event::check_id_exists($id,false)){
            return response()->view('errors.404', [], 404);
        }
        // Event data
        $event = Event::get_data_id_lang2($id,$lang,false);

        // Upcoming / Past
        $upcoming = false;
        $page_id = 4;// Past-event
        if($event["event"]["date_end"]>date('Y-m-d')){
            // UPCOMING
            $upcoming = true;
            $page_id = 5;// Upcoming-event
        }

        // Layout
        $layout = AppFunctions::get_fe_layout($lang,$page_id);
        // Page data
        $page_data = self::get_page_data($lang,$page_id);

        // Return
        return view('events.show')
            ->with('platform',$this->platform)
            ->with('layout',$layout)
            ->with('page_data',$page_data)
            ->with('event',$event)
            ->with('is_upcoming',$upcoming);
    }

}