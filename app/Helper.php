<?php

namespace App;
use Image;
class Helper
{

    public static function formatNumber( $number ) {
        if( $number >= 1000 &&  $number < 1000000 ) {

            return number_format( $number/1000, 1 ). "k";
        } else if( $number >= 1000000 ) {
            return number_format( $number/1000000, 1 ). "M";
        } else {
            return $number;
        }
    }//<<<<--- End Function

    public static function getHeight( $image ) {
        $size   = getimagesize( $image );
        $height = $size[1];
        return $height;
    }

    public static function getWidth( $image ) {
        $size  = getimagesize( $image);
        $width = $size[0];
        return $width;
    }

    /*
     * fileNameOriginal - Ritorna NOME ORIGINALE FILE
     * */
    public static function fileNameOriginal($string){
        return pathinfo($string, PATHINFO_FILENAME);
    }
}