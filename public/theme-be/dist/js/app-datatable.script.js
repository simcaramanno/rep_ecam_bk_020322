(function ($) {
    "use strict";
    var editor;

    // USERS
    $('#tab-users').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-users").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });


    // REQUESTS
    $('#tab-requests').DataTable({
        // dom: 'Bfrtip',
        //     buttons: [
        //         'copy', 'csv', 'excel', 'pdf', 'print'
        //     ],
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-requests").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'show/'+val;  }
    });

    // tab-events
    $('#tab-events').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-events").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });

    // tab-speakers
    $('#tab-speakers').DataTable({
        responsive: true,
        pageLength:50,
        sortable:true,
        order: [[ 0, "asc" ]],
    });

    // tab-event-gallery
    $('#tab-event-gallery').DataTable({
        responsive: true,
        pageLength:50,
        sortable:true,
        order: [[ 0, "asc" ]],
    });

    // tab-home-sliders
    $('#tab-home-sliders').DataTable({
        responsive: true,
        pageLength:50,
        sortable:true,
        order: [[ 0, "asc" ]],
    });

    // tab-projects
    $('#tab-projects').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-projects").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });

    // tab-project-gallery
    $('#tab-project-gallery').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });

    // tab-partners
    $('#tab-partners').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-partners").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });


    // tab-press-tags
    $('#tab-press-tags').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-press-tags").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });

    // tab-press-categories
    $('#tab-press-categories').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-press-categories").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });

    // tab-press-articles
    $('#tab-press-articles').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "desc" ]],
    });
    // Row click
    $("#tab-press-articles").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });


    // tab-press-gallery
    $('#tab-press-gallery').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "desc" ]],
    });

    // tab-cms-menu
    $('#tab-cms-menu').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-cms-menu").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });


    // tab-cms-pages
    $('#tab-cms-pages').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-cms-pages").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-data/'+val;  }
    });

    // tab-pages-sections
    $('#tab-pages-sections').DataTable({
        responsive: true,
        pageLength:30,
        sortable:true,
        order: [[ 0, "asc" ]],
    });

    // tab-cms-sections
    $('#tab-cms-sections').DataTable({
        responsive: true,
        pageLength:20,
        sortable:true,
        order: [[ 0, "asc" ]],
    });
    // Row click
    $("#tab-cms-sections").on("click", "tr", function() {
        var val= $(this).attr('id');
        if( val>0){ window.location.href = 'edit-details/'+val;  }
    });


    // tab-section-gallery
    $('#tab-section-gallery').DataTable({
        responsive: true,
        pageLength:30,
        sortable:true,
        order: [[ 0, "asc" ]],
    });



})(jQuery);